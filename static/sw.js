const CACHE_VERSION = 1;

const BASE_CACHE_FILES = [
    '/css/lpa.css',
    '/css/sakura.css',
    '/css/sakura-dark.css',
    '/css/baguetteBox.min.css',
    '/css/datatable.min.css',
    '/js/search.js',
    '/js/baguetteBox.min.js',
    '/js/datatables.min.js',
    '/js/moment.min.js',
    '/js/table_apps.js',
    '/js/table_games.js',
    '/search_index.en.js',
    '/elasticlunr.min.js',
    '/manifest.json',
    '/browserconfig.xml',
    '/favicon-96x96.png',
    '/logo-lpa.png',
];

// Assume this is filled with the actual array of URL strings directly
const OFFLINE_CACHE_FILES = "INSERT STUFF HERE"

const NOT_FOUND_CACHE_FILES = [
    '/404.html',
];

const OFFLINE_PAGE = '/index.html';
const NOT_FOUND_PAGE = '/404.html';

const CACHE_VERSIONS = {
    assets: 'assets-v' + CACHE_VERSION,
    content: 'content-v' + CACHE_VERSION,
    offline: 'offline-v' + CACHE_VERSION,
    notFound: '404-v' + CACHE_VERSION,
};

const MAX_TTL = {
    '/': 3600,
    html: 86400,
    json: 86400,
    js: 86400,
    css: 86400,
};

const CACHE_BLACKLIST = [
    (str) => {
        return !str.startsWith('http://localhost');
    },
];

const SUPPORTED_METHODS = [
    'GET',
];

function isBlacklisted(url) {
    return CACHE_BLACKLIST.some(rule => typeof rule === 'function' && !rule(url));
}

function getFileExtension(url) {
    let extension = url.split('.').reverse()[0].split('?')[0];
    return (extension.endsWith('/')) ? '/' : extension;
}

function getTTL(url) {
    let extension = getFileExtension(url);
    return MAX_TTL[extension] || null;
}

function installServiceWorker() {
    return caches.open(CACHE_VERSIONS.assets)
        .then((cache) => cache.addAll(BASE_CACHE_FILES))
        .then(() => caches.open(CACHE_VERSIONS.offline)
            .then((cache) => cache.addAll(OFFLINE_CACHE_FILES.concat(NOT_FOUND_CACHE_FILES))))
        .then(() => console.log('Service worker installed'))
        .catch((error) => console.error('Failed to install service worker:', error));
}

function cleanupLegacyCache() {
    return caches.keys().then((keys) => {
        let promises = keys.filter(key => !Object.values(CACHE_VERSIONS).includes(key))
            .map(key => caches.delete(key));
        return Promise.all(promises);
    });
}

self.addEventListener('install', event => {
    event.waitUntil(installServiceWorker());
});

self.addEventListener('activate', event => {
    event.waitUntil(cleanupLegacyCache());
});

self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then((response) => {
                return response || fetch(event.request)
                    .then((response) => {
                        let cacheName = CACHE_VERSIONS.content;
                        let shouldCache = SUPPORTED_METHODS.includes(event.request.method) &&
                                          !isBlacklisted(event.request.url);

                        if (shouldCache && response && response.status === 200) {
                            caches.open(cacheName).then((cache) => {
                                cache.put(event.request, response.clone());
                            });
                        }
                        return response;
                    })
                    .catch(() => {
                        if (event.request.mode === 'navigate') {
                            return caches.match(OFFLINE_PAGE);
                        }
                    });
            })
    );
});

