#!/bin/sh

# Step 1: Generate the transformed URLs and convert them to a JSON array
urls=$(xmlstarlet sel -N x="http://www.sitemaps.org/schemas/sitemap/0.9" -t -m "//x:url/x:loc" -v . -n public/sitemap.xml \
  | sed 's|https://linuxphoneapps.org||; s|/$|/index.html|' \
  | jq -R . | jq -s . | jq -c .)

# Step 2: Replace the placeholder in sw.js with the new JSON, properly formatted for JS
sed -i "s|\"INSERT STUFF HERE\"|$urls|" public/sw.js

