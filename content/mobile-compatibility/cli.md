+++
title = "Mobile Compatibility: CLI"
description = "Non GUI Apps meant to be run in a terminal emulator"
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++
CLI app - meant to be run in a terminal emulator, e.g., on <a href="https://sxmo.org">Sxmo</a>. Check the App's Notice for recommended settings.
