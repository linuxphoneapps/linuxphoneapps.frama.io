+++
title = "Mobile Compatibility"
description = "Further information on Mobile Compatibility."
draft = false
sort_by = "title"

[extra.mobilecompat_pages]
"5" = "mobile-compatibility/5.md"
"4" = "mobile-compatibility/4.md"
"3" = "mobile-compatibility/3.md"
"2" = "mobile-compatibility/2.md"
"1" = "mobile-compatibility/1.md"
"cli" = "mobile-compatibility/cli.md"
"needs-testing" = "mobile-compatibility/needs-testing.md"
+++
This page lists the mobile compatibility ratings (which are only just that, and not a more generic assessment): a number between 5 (best) and 0 (worst) to rate the mobile compatibility of apps.

* 5 = perfect - nothing left to be done, fits the screen and works fine with touch input
* 4 = almost perfect, works fine with tweaks for scaling, like `scale-to-fit´ on Phosh
* 3 = some parts of the app are not usable with touch input on small screens
* 2 = many parts of the app are not usable with touch input on small screens
* 1 = app is unusable with touch input on small screens – why is this app listed here?
* CLI = non-GUI app meant to be run in a terminal emulator. The app's notice section should have some recommmendations regarding font size etc. if necessary.
