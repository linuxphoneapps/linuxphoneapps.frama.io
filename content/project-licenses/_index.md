+++
title = "Project Licenses"
description = "Further information on used Software Licenses"
draft = false

# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.projectlicenses_pages]
"mit" = "project-licenses/mit.md"
+++

An overview of the licenses used.
