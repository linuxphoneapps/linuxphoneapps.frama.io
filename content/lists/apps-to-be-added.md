+++
title = "Apps to be added"
description = "Apps that need to be evaluated and added."
date = 2023-01-08
draft = false

[extra]
lead = 'Apps that need to be evaluated.'
images = []
+++

## Purpose

This list contains apps that have not been added as a proper listing. This list can be useful if

- the app list does not contain what you need,
- you want to help adding apps (see [Contributing](@/docs/contributing/_index.md) for how to do this),
- or you want to see if someone has already started developing an app in order to join an existing project.


## Apps with tagged releases (sorted 2024-02-14)

### 2024
* xournalpp - Repo: <https://github.com/xournalpp/xournalpp>, Tag: nightly, Last Commit: 2024-02-13
* authpass, Flutter,  Repo: <https://github.com/authpass/authpass>, Tag: flathub-v1.9.11, Last Commit: 2024-02-03
* webapp-manager, GTK3/xapps, unoptimized desktop app - Repo: <https://github.com/linuxmint/webapp-manager>, Tag: master.lmde6, Last Commit: 2024-01-31
* intiface-central, Flutter, Repo: <https://github.com/intiface/intiface-central>, Tag: v2.5.5, Last Commit: 2024-01-28
* spotube, Flutter, Repo: <https://github.com/KRTirtho/spotube/>, Tag: v3.4.1, Last Commit: 2024-01-27
### 2022
* keyring, GTK3,  Repo: <https://git.sr.ht/~martijnbraam/keyring>, Tag: 0.2.0, Last Commit: 2023-08-26
* brun (MauiKit calculator), Repo: <https://invent.kde.org/maui/brun>, Tag: v0.0.5, Last Commit: 2023-08-20
### 2021 (cutoff for released apps -  this can be considered abandonned)
* agenda, GTK3, Granite  - Repo: <https://github.com/dahenson/agenda>, Tag: 1.1.2, Last Commit: 2021-08-14
* om-camera, QtWidget, PinePhone only - Repo: <https://github.com/OpenMandrivaSoftware/om-camera>, Tag: 0.0.4, Last Commit: 2021-04-19

## Apps without git tags

### 2024
* simple-scan - Repo: <https://gitlab.gnome.org/GNOME/simple-scan>, Last Commit: 2024-02-14
* Kooha - Repo: <https://github.com/SeaDve/Kooha>, Last Commit: 2024-02-14
* booth - Repo: <https://invent.kde.org/maui/booth>, Last Commit: 2024-02-14
* icon-library - Repo: <https://gitlab.gnome.org/World/design/icon-library>, Last Commit: 2024-02-12
* Kaiteki - Repo: <https://github.com/Kaiteki-Fedi/Kaiteki>, Last Commit: 2024-02-12
* neosurf - Repo: <https://github.com/CobaltBSD/neosurf>, Last Commit: 2024-02-08
* nextcloud_password_client - Repo: <https://gitlab.com/j0chn/nextcloud_password_client>, Last Commit: 2024-02-06
* pods - Repo: <https://github.com/marhkb/pods>, Last Commit: 2024-02-05
* citations - Repo: <https://gitlab.gnome.org/World/citations>, Last Commit: 2024-02-05
* Noteworthy - Repo: <https://github.com/SeaDve/Noteworthy>, Last Commit: 2024-02-04
* argos - Repo: <https://github.com/orontee/argos>, Last Commit: 2024-02-02
* webfont-kit-generator - Repo: <https://github.com/rafaelmardojai/webfont-kit-generator>, Last Commit: 2024-01-31
* Personal-Voice-Assistent - Repo: <https://github.com/Cyborgscode/Personal-Voice-Assistent>, Last Commit: 2024-01-30
* kazv - Repo: <https://lily-is.land/kazv/kazv>, Last Commit: 2024-01-28
* sharit - Repo: <https://gitlab.com/LyesSaadi/sharit>, Last Commit: 2024-01-27
* Vremenar - Repo: <https://github.com/ntadej/Vremenar>, Last Commit: 2024-01-26
* Replay - Repo: <https://github.com/ReplayDev/Replay>, Last Commit: 2024-01-25
* argos-translate - Repo: <https://github.com/argosopentech/argos-translate>, Last Commit: 2024-01-24
* itd - Repo: <https://gitea.elara.ws/Elara6331/itd>, Last Commit: 2024-01-20
* abaddon - Repo: <https://github.com/uowuo/abaddon>, Last Commit: 2024-01-18
* share-preview - Repo: <https://github.com/rafaelmardojai/share-preview>, Last Commit: 2024-01-17
* settings - Repo: <https://github.com/lirios/settings>, Last Commit: 2024-01-15
* lorem - Repo: <https://gitlab.gnome.org/World/design/lorem>, Last Commit: 2024-01-08
* desktop-files-creator - Repo: <https://github.com/alexkdeveloper/desktop-files-creator>, Last Commit: 2024-01-06
### 2023
* PineCast - Repo: <https://github.com/Jeremiah-Roise/PineCast>, Last Commit: 2023-12-19
* powerplant - Repo: <https://invent.kde.org/utilities/powerplant>, Last Commit: 2023-12-03
* liftoff - Repo: <https://github.com/liftoff-app/liftoff>, Last Commit: 2023-11-27
* qomp - Repo: <https://github.com/qomp/qomp>, Last Commit: 2023-11-02
* appimagepool - Repo: <https://github.com/prateekmedia/appimagepool>, Last Commit: 2023-10-23
* summarizer - Repo: <https://github.com/Nalsai/summarizer>, Last Commit: 2023-10-11
* astroid - Repo: <https://github.com/astroidmail/astroid/>, Last Commit: 2023-09-17
* Retiled - Repo: <https://github.com/DrewNaylor/Retiled>, Last Commit: 2023-09-13
* barcelona-trees - Repo: <https://github.com/pedrolcl/barcelona-trees>, Last Commit: 2023-08-05
* quickmail - Repo: <https://invent.kde.org/carlschwan/quickmail>, Last Commit: 2023-06-09
* toxer-desktop - Repo: <https://gitlab.com/Toxer/toxer-desktop>, Last Commit: 2023-06-03
* Katana - Repo: <https://github.com/gavr123456789/Katana>, Last Commit: 2023-04-21
* talk-desktop - Repo: <https://github.com/CarlSchwan/talk-desktop>, Last Commit: 2023-03-16
* bitsteward - Repo: <https://github.com/Bitsteward/bitsteward>, Last Commit: 2023-03-12
* sncf - Repo: <https://git.sr.ht/~yukikoo/sncf/>, Last Commit: 2023-03-08
* keyboard - Repo: <https://github.com/grelltrier/keyboard>, Last Commit: 2023-02-09
* ghostshot - Repo: <https://gitlab.gnome.org/GabMus/ghostshot>, Last Commit: 2023-01-22
* dot-matrix - Repo: <https://github.com/lainsce/dot-matrix/>, Last Commit: 2023-01-15

-------------------

CUT OFF LINE - things below are likely abandoned and not worth adding, unless they work and fill a niche

-------------------

### 2022
* chert - Repo: <https://gitlab.com/high-creek-software/chert>, Last Commit: 2022-11-24
* wifiscanner - Repo: <https://github.com/Cyborgscode/wifiscanner>, Last Commit: 2022-11-11
* screenshot - Repo: <https://github.com/lirios/screenshot>, Last Commit: 2022-10-22
* networkmanager - Repo: <https://github.com/lirios/networkmanager>, Last Commit: 2022-10-22
* screencast - Repo: <https://github.com/lirios/screencast>, Last Commit: 2022-10-22
* keylight-control - Repo: <https://github.com/mschneider82/keylight-control>, Last Commit: 2022-10-04
* covidpass - Repo: <https://github.com/pentamassiv/covidpass>, Last Commit: 2022-09-27
* tunes, GTK3, mobile friendly MPD client, Repo: <https://git.sr.ht/~jakob/tunes>, Last Commit: 2022-08-07
* Qt-Audiobook-Player - Repo: <https://github.com/rushic24/Qt-Audiobook-Player/>, Last Commit: 2022-05-17
* mdwriter - Repo: <https://gitlab.com/gabmus/mdwriter>, Last Commit: 2022-04-12
* visurf, netsurf based, keyboard focussed browser using Cairo, see also neosurf - Repo: <https://git.sr.ht/~sircmpwn/visurf/>, Last Commit: 2022-03-14
* sharmavid, GTK4 invidious client - Repo: <https://gitlab.gnome.org/ranfdev/sharmavid>, Last Commit: 2022-02-11
* AugSteam, HELP NEEDED! If you use Steam's mobile authenticator app, please help out! - Repo: <https://github.com/Augmeneco/AugSteam>, Last Commit: 2022-02-10
* music - Repo: <https://github.com/lirios/music>, Last Commit: 2022-02-09
* pala - Repo: <https://gitlab.com/gabmus/pala>, Last Commit: 2022-01-26

### 2021
* AugVK-Messenger - Repo: <https://github.com/Augmeneco/AugVK-Messenger>, Last Commit: 2021-11-27
* GrepToolQML - Repo: <https://github.com/presire/GrepToolQML>, Last Commit: 2021-11-22
* pKPR - Repo: <https://github.com/juliendecharentenay/pKPR>, Last Commit: 2021-11-19
* umtp-responder-gui - Repo: <https://github.com/JollyDevelopment/umtp-responder-gui>, Last Commit: 2021-11-14
* camcam - Repo: <https://github.com/JNissi/camcam>, Last Commit: 2021-10-16
* trackball - Repo: <https://github.com/NthElse/trackball>, Last Commit: 2021-10-05
* mirdorph - Repo: <https://gitlab.gnome.org/ranchester/mirdorph>, Last Commit: 2021-09-05
* hydro_bot - Repo: <https://github.com/ripxorip/hydro_bot>, Last Commit: 2021-08-09
* rokugtk - Repo: <https://github.com/cliftonts/rokugtk>, Last Commit: 2021-07-28
* PineConnect - Repo: <https://github.com/timaios/PineConnect>, Last Commit: 2021-07-27
* mactrack - Repo: <https://gitlab.com/Aresesi/mactrack>, Last Commit: 2021-06-05
* Tracks - Repo: <https://codeberg.org/som/Tracks>, Last Commit: 2021-05-28
* solio - Repo: <https://invent.kde.org/devinlin/solio>, Last Commit: 2021-05-20
* LibreInvoice - Repo: <https://codeberg.org/matiaslavik/LibreInvoice>, Last Commit: 2021-04-21
* Flashcards - Repo: <https://github.com/SeaDve/Flashcards>, Last Commit: 2021-04-14
* hn - Repo: <https://github.com/DavidVentura/hn>, Last Commit: 2021-04-12
* mailmodel - Repo: <https://invent.kde.org/vandenoever/mailmodel>, Last Commit: 2021-04-04
* Interpret - Repo: <https://codeberg.org/xaviers/Interpret>, Last Commit: 2021-03-23
* fastcal - Repo: <https://github.com/JNissi/fastcal>, Last Commit: 2021-03-21
* clip2deepl - Repo: <https://codeberg.org/yasht/clip2deepl>, Last Commit: 2021-03-02
* telephant - Repo: <https://github.com/muesli/telephant>, Last Commit: 2021-01-30
* pui - Repo: <https://github.com/nbdy/pui>, Last Commit: 2021-01-30

### 2020
* camera-rs - Repo: <https://gitlab.gnome.org/bilelmoussaoui/camera-rs>, Last Commit: 2020-12-25
* paintable-rs - Repo: <https://gitlab.gnome.org/bilelmoussaoui/paintable-rs>, Last Commit: 2020-12-23
* lurkymclurkface - Repo: <https://gitlab.com/armen138/lurkymclurkface>, Last Commit: 2020-12-16
* pinephone-modemfw - Repo: <https://git.sr.ht/~martijnbraam/pinephone-modemfw>, Last Commit: 2020-11-12
* orion - Repo: <https://github.com/drac69/orion>, Last Commit: 2020-11-10
* papper - Repo: <https://gitlab.com/GunnarGrop/papper>, Last Commit: 2020-07-01
* simple-rss-reader-using-kde-kirigami - Repo: <https://gitlab.com/SiriBeta/simple-rss-reader-using-kde-kirigami>, Last Commit: 2020-06-13
* bitguarden - Repo: <https://github.com/DenisPalchuk/bitguarden>, Last Commit: 2020-04-29
* hangouts-gtk - Repo: <https://github.com/do-sch/hangouts-gtk>, Last Commit: 2020-04-19
* moodle - Repo: <https://gitlab.gnugen.ch/afontain/moodle>, Last Commit: 2020-03-21
* l5_shoppinglist - Repo: <https://source.puri.sm/fphemeral/l5_shoppinglist>, Last Commit: 2020-03-07
* Gifup - Repo: <https://github.com/BharatKalluri/Gifup>, Last Commit: 2020-03-03
* librem5_utils - Repo: <https://source.puri.sm/fphemeral/librem5_utils>, Last Commit: 2020-01-26

### 2019
* kodimote - Repo: <https://github.com/scandinave/kodimote>, Last Commit: 2019-12-26
* coffee - Repo: <https://github.com/nick92/coffee>, Last Commit: 2019-11-14
* wallet - Repo: <https://gitlab.gnome.org/bilelmoussaoui/wallet>, Last Commit: 2019-09-28
* venom - Repo: <https://github.com/naxuroqa/venom>, Last Commit: 2019-02-18

### 2018
* client - Repo: <https://github.com/TheToxProject/client>, Last Commit: 2018-08-13

### 2017
* medOS-kirigami - Repo: <https://github.com/milohr/medOS-kirigami>, Last Commit: 2016-10-22

### 2016
* Cooking-Calc - Repo: <https://codeberg.org/WammKD/Cooking-Calc>, Last Commit: 2016-05-04

## Archived
* lyrebird - unoptimized desktop app, Repo: <https://github.com/constcharptr/lyrebird>, Last Commit: 2023-12-13
* camera - Repo: <https://gitlab.gnome.org/jwestman/camera>, Last Commit: 2022-12-19



## OLD ORDER - augment lines above, then delete this

### Needs help testing
* keyring <https://git.sr.ht/~martijnbraam/keyring> (needs Himitsu user https://himitsustore.org/ / packaging of himitsu/hare)
* rokugtk <https://github.com/cliftonts/rokugtk> (a Roku remote app, requires a Roku device to test)
* AugVK-Messenger <https://github.com/Augmeneco/AugVK-Messenger> (requires a VKontakte account)
* AugSteam <https://github.com/Augmeneco/AugSteam>
* Kazv <https://lily-is.land/kazv/kazv> (can't even successfully build libkazv)
* Libre Invoice <https://codeberg.org/matiaslavik/LibreInvoice> Looks like LimeReport might need to be rebuild, but since I know nothing about Qt Android apps, that might not be all.
* Open Mandriva Camera app <https://github.com/OpenMandrivaSoftware/om-camera> (can't get this to work on other distributions)
* Bitsteward <https://github.com/Bitsteward/bitsteward> (Bitwarden client, needs bitwarden user)


### Needs testing

#### Kirigami
* PowerPlant <https://invent.kde.org/mbruchert/powerplant>
* Nextcloud Talk <https://github.com/CarlSchwan/talk-desktop>  (Kirigami-fication of a SailfishOS app) <https://codeberg.org/blizzz/harbour-nextcloud-talk/pulls/37>, see also: <https://carlschwan.eu/2021/12/18/more-kde-apps/>
* Clip2DeepL <https://codeberg.org/yasht/clip2deepl> (works when started on terminal on Phosh, needs testing on Plasma Mobile, last commit 2021-03-02
* Pelikan <https://invent.kde.org/carlschwan/quickmail> (now called "Pelikan", see https://carlschwan.eu/2021/12/18/more-kde-apps/)
* mailmodel <https://invent.kde.org/vandenoever/mailmodel> (last commit 2021-04-04
* Solio <https://invent.kde.org/devinlin/solio> Soundcloud client (last commit 2021-05-21
* Booth <https://invent.kde.org/maui/booth> (convergent camera app)

#### QtWidget
* opensnitch <https://github.com/evilsocket/opensnitch/discussions/415>
* Qt-Audiobook-Player <https://github.com/rushic24/Qt-Audiobook-Player/>

#### QtQuick
* Grep Tool QML <https://github.com/presire/GrepToolQML>
* Barcelona Trees <https://github.com/pedrolcl/barcelona-trees>
* Toxer Desktop <https://gitlab.com/Toxer/toxer-desktop>
* Orion <https://github.com/drac69/orion> twitch client, 4 (last commit 2022-11-10
* Telephant <https://github.com/muesli/telephant> mastodon client
* Qomp <https://github.com/qomp/qomp> music player
* Vrenemar <https://github.com/ntadej/Vremenar> weather app
* Project Trackball <https://github.com/NthElse/trackball> (Todo.txt desktop apps, scales fine on 20210930, but adding tasks does not seem to work yet, last commit 20211005)


#### GTK4
* Desktop Files Creator <https://github.com/alexkdeveloper/desktop-files-creator>
* Lorem	<https://gitlab.gnome.org/World/design/lorem> (at best a 4, and.. do we need Lorem Ipsum on a Phone?)
* Dot Matrix (<https://github.com/lainsce/dot-matrix/>, needs scale-to-fit io.github.lainsce.DotMatrix on)
* Kooha <https://github.com/SeaDve/Kooha>
* Share Preview <https://github.com/rafaelmardojai/share-preview>
* Webfont Kit Generator <https://github.com/rafaelmardojai/webfont-kit-generator>
* Icon Library <https://gitlab.gnome.org/World/design/icon-library>
* Mirdorph (Discord client) <https://gitlab.gnome.org/ranchester/mirdorph> - last commit 2021-09-05
* Tracks <https://codeberg.org/som/Tracks>
* Summarizer <https://github.com/Nalsai/summarizer>
* Ghostshot <https://gitlab.gnome.org/GabMus/ghostshot> (last commit 20230122)
* MD Writer <https://gitlab.com/gabmus/mdwriter>
* Noteworthy <https://github.com/SeaDve/Noteworthy> (builds now, not yet tested on mobile)
* Pods <https://github.com/marhkb/pods> (Podman client)
* Citations <https://gitlab.gnome.org/World/citations>
* SNCF <https://git.sr.ht/~yukikoo/sncf/>
* Replay <https://github.com/ReplayDev/Replay> (YouTube client)
* Sharit <https://gitlab.com/LyesSaadi/sharit> (YouTube client)
* Katana <https://github.com/gavr123456789/Katana> (GTK Tree Structure Browser)
* Chert <https://gitlab.com/high-creek-software/chert> (Note taking app)


#### GTK3
* Abbadon (Discord client) <https://github.com/uowuo/abaddon>
* PineCast <https://github.com/Jeremiah-Roise/PineCast>
* xournalpp <https://github.com/xournalpp/xournalpp>
* keylight-control <https://github.com/mschneider82/keylight-control> builds, but quits on restart
* Coffee <https://github.com/nick92/coffee> (Settings don't work well (janky), rest is okay) - last commit 5 years ago
* umtp-responder-gui <https://github.com/JollyDevelopment/umtp-responder-gui>
* Astroid <https://github.com/astroidmail/astroid/>
* Argos <https://github.com/orontee/argos> (likely ironically not mobile frienly, last commit 5 years ago)
* Camera <https://gitlab.gnome.org/jwestman/camera>
* GNOME Terminal (fine except for settings, but .., add after GTK4 rewrite)
* Camcam <https://github.com/JNissi/camcam> WIP Camera app written in Rust (last commit 2021-10-16
* Fastcal <https://github.com/JNissi/fastcal> WIP Calendar app written in Rust (last commit 2021-03-21
* hn <https://github.com/DavidVentura/hn> Python hacker news client (last commit 2021-04-12
* Hydro Bot <https://github.com/ripxorip/hydro_bot> ("Pinephone app that keeps you hydrated", too much WIP to add on 2022-04-12
* wifiscanner <https://github.com/Cyborgscode/wifiscanner> (last commit 2020-12-31
* GTK Hangouts Client <https://github.com/do-sch/hangouts-gtk>
* pKPR <https://github.com/juliendecharentenay/pKPR>

##### GTK3 + Granite
* Agenda <https://github.com/dahenson/agenda>
* Bitguarden <https://github.com/DenisPalchuk/bitguarden> (last commit 2020-04-29
* Gifup <https://github.com/BharatKalluri/Gifup> (last commit 2020-03-03

#### Flutter
* Liftoff <https://github.com/liftoff-app/liftoff> (lemmy client)
* AppImage Pool (<https://github.com/prateekmedia/appimagepool>)
* Nextcloud Password Client <https://gitlab.com/j0chn/nextcloud_password_client>
* Spotube <https://github.com/KRTirtho/spotube/>
* AuthPass (Keepass client) <https://github.com/authpass/authpass> (Did not work in linmob's attempt to build it (white window after startup), supposedly there's an ARM64 snap build, but it returns an architecture warning.)
* Kaiteki (mastodon client) <https://github.com/Kaiteki-Fedi/Kaiteki>
* Intiface® Central <https://github.com/intiface/intiface-central>
* More here: <https://github.com/leanflutter/awesome-flutter-desktop#open-source-apps>

#### Fyne
* itd-gui <https://gitea.arsenm.dev/Arsen6331/itd#itgui>

#### Ubuntu Components
* Cooking calc <https://codeberg.org/WammKD/Cooking-Calc>

### Downstream forks
#### GTK3
* Evince (Purism) - do not add, superseeded by <https://gitlab.gnome.org/GNOME/Incubator/papers>


### Needs initial testing
* Personal Voice Assistant <https://github.com/Cyborgscode/Personal-Voice-Assistent> (Currently German only, requires Java. See also <https://marius.bloggt-in-braunschweig.de/2021/07/15/hallo-computer-bist-du-da/>)
* Pine Connect <https://github.com/timaios/PineConnect> (early state of development, work on the app has not started yet (but the daemon is being worked on)) - last commit 2021-07-27

## From other applists
* <https://cubocore.org/> apps, see also <https://wiki.postmarketos.org/wiki/CoreApps>


### old Linux Phone apps:
* Navit
* FoxtrotGPS
* Maep




### unoptimized desktop apps that have been run on the Librem 5 or PinePhone successfully (only add if they are at least somewhat usable and have an advantage compared to more mobile-ready apps):
* Gnome Photos
* Gthumb
* Shotwell // apps from here on where spotted on a screenshot
* Claws Mail
* D-Feet
* Gpredict
* KiCad
* Shadowsocks-Qt5
* Verbiste <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7>
* Simple scan <https://gitlab.gnome.org/GNOME/simple-scan> (newer versions are only fine after "scale-to-fit simple-scan on")
* Marble (<https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/27>)
* Orage (<https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/28>)
* Solar System (<https://flathub.org/apps/details/org.sugarlabs.SolarSystem; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/37>)
* AusweisApp 2 (<https://flathub.org/apps/details/de.bund.ausweisapp.ausweisapp2>; <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/51>)
* Nixwriter <https://flathub.org/apps/details/com.gitlab.adnan338.Nixwriter>; <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/69>
* SongRec <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/91>
* Bleachbit <https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/101>
* Argos Translate <https://github.com/argosopentech/argos-translate>

## Needs help testing (low priority, inactive - check periodically for activity)
### GTK3
* Moody (Moodle client, GTK3/libhandy) <https://gitlab.gnugen.ch/afontain/moodle> (last commit 2020-03-20
* Venom <https://github.com/naxuroqa/venom> (A modern Tox client for the Linux desktop, not mobile friendly, last commit 2020-04-23

### Projects that seem to have been abandoned or are archived
* Wallet <https://gitlab.gnome.org/bilelmoussaoui/wallet>
* Kodimote <https://github.com/scandinave/kodimote> (WIP, last commit 2019-12-23
* l5_shoppinglist <https://source.puri.sm/fphemeral/l5_shoppinglist>,<https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter>
* librem5_utils <https://source.puri.sm/fphemeral/librem5_utils>,<https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter> <https://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218/>
* Flashcards <https://github.com/SeaDve/Flashcards> (only UI, non-functional, archived)
* PinePhone modem firmware updater UI: <https://git.sr.ht/~martijnbraam/pinephone-modemfw> (solved with GNOME Firmware, still listed for historic purposes)
* camera-rs <https://gitlab.gnome.org/bilelmoussaoui/camera-rs> (last commit 2020-12-25
* qrcode-generator <https://gitlab.gnome.org/bilelmoussaoui/paintable-rs> (last commit 2020-12-23
* Interpret <https://codeberg.org/xaviers/Interpret> (GTK3 deepl based translation app, archived)
* Tox Client (Electron) <https://github.com/TheToxProject/client>
* Simple RSS Reader using KDE Kirigami <https://gitlab.com/SiriBeta/simple-rss-reader-using-kde-kirigami> (last commit 2020-06-13
* Papper <https://gitlab.com/GunnarGrop/papper> (Kirigami reddit client, last commit 2020-07-01
* LurkyMcLurkFace <https://gitlab.com/armen138/lurkymclurkface> (Kirigami reddit client, last commit 2020-12-16
* Pala <https://gitlab.com/gabmus/pala>
* covidpass <https://github.com/pentamassiv/covidpass>

### Projects that seem to be gone
* clan <https://web.archive.org/web/20201201114016/https://github.com/mauikit/clan> (Seems to have been a launcher)

### Not an app
* Retiled (Windows Phone style launcher) <https://github.com/DrewNaylor/Retiled> (are launchers in scope?)
* PUI <https://github.com/nbdy/pui> (launcher?)
* <https://github.com/grelltrier/keyboard> Fingerboard

### No GUI
* Mactrack <https://gitlab.com/Aresesi/mactrack>


## Remains of other-apps.csv
Check, then sort or remove the following:

### This is a loose and non-formal collection of apps which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the other list
* MedOS-kirigami <https://github.com/milohr/medOS-kirigami>
* Liri Screencast <https://github.com/lirios/screencast>
* Liri Screenshot <https://github.com/lirios/screenshot>
* Liri music <https://github.com/lirios/music>
* Liri Settings <https://github.com/lirios/settings>
* Liri NetworkManager <https://github.com/lirios/networkmanager>
* OwnCloud Sync <https://open-store.io/app/owncloud-sync>
* Music <https://wiki.gnome.org/Apps/Music>, <https://tchncs.de/_matrix/media/v1/download/talk.puri.sm/wDbVJsNtmaLUljbuxVVzRRhf>
* GadgetBridge <https://github.com/Freeyourgadget/Gadgetbridge> (afaik Android only, so: why?)
* Linphone (not mobile compatible, Ubuntu Touch version does not exist for 64bit yet)
* <https://doc.qt.io/qt-5/qtquick-codesamples.html>

### In planning stage:
* Compass <https://phabricator.kde.org/T8905>
* Konversation 2.0

### Sources for apps to be included:
* <https://binary-factory.kde.org/view/Android/>,28 apps (02.03.2020)
* <https://www.youtube.com/channel/UCIVaIdjtr6aNPdTm-JNbFAg/videos>
* <https://www.youtube.com/watch?v=K4OfNFis--g>
* <https://puri.sm/posts/what-is-mobile-pureos/>
* <https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development>
