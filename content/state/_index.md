+++
title = "Project state" 
description = "Further information on the state of the app"
draft = false

# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.state_pages]
"wip" = "state/wip.md"
+++

An overview of the states an app can have.
