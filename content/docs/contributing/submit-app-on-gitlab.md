+++
title = "Submit apps via GitLab"
description = "Adding or editing existing listings"
date = 2022-04-07T19:30:00+02:00
updated = 2025-02-10
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

### Introduction

To edit or add an app, you will have to edit a markdown file with extensive TOML frontmatter, which then is going to be rendered by [Zola](https://getzola.org).

All content regarding apps or games lives in the [LinuxPhoneApps repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io) in content/apps or content/games/, depending on what you want to edit.

To do so, you need an account with [Framagit](https://framagit.org/users/sign_up).

### Edit an app listing

Just click on the "edit this page on framagit" link at the bottom of the app listing page, and then click "Edit - Edit single file" on framagit GitLab.

Now make your changes, Check [Listings explained](@/docs/contributing/listings-explained.md) for reference.

In the Commit message, enter a reason for the changes.

Select the "Start a new merge request with these changes" checkbox if it is shown.

Finally select "Commit changes". A merge request will be automatically started.

### Adding an app

Use [LPA Helper](https://linuxphoneapps.org/lpa_helper.html) to create a listing.

If you want help out, but have not found an app to add, help working through [our To Do list](https://linuxphoneapps.org/lists/apps-to-be-added/)!

### Creating a merge request

Navigate to the repository at [https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io) and upload your new file into the content/apps/ folder using the + dropdown button.

Please make sure to put a good commit message, in the form of "/content/apps/your.new.file.md: New listing: App Name".

Select the "Start a new merge request with these changes" checkbox if it is shown.

Finally select "Commit changes". That's it, a merge request will be automatically started. If the merge request is accepted your new file, or edit, will be added to the repository.
