+++
title = "Docs"
description = "The documentation of LinuxPhoneApps.org."
sort_by = "weight"
weight = 1
generate_feeds = true
template = "docs/section.html"
in_search_index = false
+++
