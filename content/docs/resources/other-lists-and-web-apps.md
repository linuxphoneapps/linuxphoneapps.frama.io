+++
title = "Other Lists and Web Apps"
description = "Friendly coexistence, not competition."
date = 2022-04-06T22:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

## Other lists

Aside from the origin of this list, [MGLapps](https://mglapps.frama.io), other projects have collected their own app or games lists, that are usually distribution specific.

### Flathub
* [Mobile Apps | Flathub](https://flathub.org/apps/collection/mobile/1)

### postmarketOS
* [postmarketOS Wiki: Potential apps](https://wiki.postmarketos.org/wiki/Potential_apps)
* [postmarketOS Wiki: Applications by category](https://wiki.postmarketos.org/wiki/Applications_by_category)
* [postmarketOS Wiki: Applications by function](https://wiki.postmarketos.org/wiki/Applications_by_function)

### PureOS
* [PureOS: Mobile-optimized apps](https://tracker.pureos.net/w/pureos/mobile_optimized_apps/)
* [PureOS: Mobile-optimized apps from 3rd party repos](https://tracker.pureos.net/w/pureos/3rd-party_mobile_optimized_apps/)
* [Purism: List of Apps in Development](https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development)
* [Purism Forums: List of Apps that fit and function well](https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well/11361)
* [Purism: Librem5: Apps gIssues](https://source.puri.sm/Librem5/Apps_Issues)

### Flathub
* [Flathub on Mobile](https://github.com/tchx84/flathub-mobile), a list of mobile Flathub apps.

### Mobian
* [archive.org/Mobian Wiki: Apps](https://web.archive.org/web/20230921185251/https://wiki.mobian.org/doku.php?id=apps)
* [archive.org/Mobian Wiki: Wishlist](https://web.archive.org/web/20230921194031/https://wiki.mobian.org/doku.php?id=wishlist)

### Openmoko
* [Openmoko Wiki: Applications](http://wiki.openmoko.org/wiki/Applications).

### App Stores/Listings:

* [Open Store: Ubuntu Touch Apps](https://open-store.io/)
* [Open Repos.net: Open Source Apps for Maemo, Harmattan, Sailfish OS, Nemo Mobile and PureOS](https://openrepos.net/)
* [SailfishOS:Chum, a Sailfish OS software repository by the community](https://sailfishos-chum.github.io/)

## Web Apps

Besides all varieties of Linux apps, web apps are a thing. The memento of the old Mobian Wiki has some [tips on how to make use of websites](https://web.archive.org/web/20230622042011/https://wiki.mobian-project.org/doku.php?id=webapps) on Linux Phones.

### Matrix Web Apps
* [Hydrogen](https://hydrogen.element.io/#/login)
* [Cinny.in](https://cinny.in/)

### Mastodon
* [Pinafore](https://pinafore.social/) (no longer developed)

_If you use another web app or know another collection of apps, [please get in touch](@/docs/help/faq.md#join-the-discussion-and-stay-informed)!_


