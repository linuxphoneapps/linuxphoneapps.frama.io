+++
title = "Games"
description = "A list of games that work on Linux Phones like the PinePhone or Librem 5."
sort_by = "title"
generate_feeds = true
in_search_index = true
redirect_to = "freedesktop-categories/game/"
+++

