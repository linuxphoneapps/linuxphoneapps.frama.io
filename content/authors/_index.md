+++
title = "Authors"
description = "The authors of blog articles."
draft = false

# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.author_pages]
"1peter10" = "authors/1peter10.md"
+++

The authors of blog articles.
