+++
title = "Requires Internet"
description = "Whether and how often an app requires a connection to the internet."
draft = false
sort_by = "title"

[extra.requiresinternet_pages]
+++

Use these pages to see apps and games that do or don't require an internet connection to work.

Note: This is a new feature of LinuxPhoneApps.org and currently the metadata is added on a purely manual basis - thus only a few apps show up here.

