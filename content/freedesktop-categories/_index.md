+++
title = "All FreeDesktop.org Categories"
description = "Categories as specified upstream according to the FreeDesktop.org spec."
draft = false
sort_by = "title"
generate_feeds = true
# If add a new author page in this section, please add a new item,
# and the format is as follows:
#
# "author-name-in-url" = "the-full-path-of-the-author-page"
#
# Note: We use quoted keys here.
[extra.freedesktopcategories_pages]
"game" = "freedesktop-categories/game.md"
+++

Apps and Games listed by their FreeDesktop.org categories, according to the official specification of Desktop Menu categories:

- [Registered Categories | Desktop Menu Specification](https://specifications.freedesktop.org/menu-spec/latest/category-registry.html#main-category-registry)
- [Additional Categories | Desktop Menu Specification](https://specifications.freedesktop.org/menu-spec/latest/additional-category-registry.html)
