+++
title = "Kivy"
description = "The Open Source Python App Development Framework."
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++

Kivy has been built to be easy to use, cross-platform and fast. It is released under the MIT License, is 100% free to use, and is professionally developed, backed and maintained.

- [Kivy: Cross-platform Python Framework for GUI apps Development](https://kivy.org/)
