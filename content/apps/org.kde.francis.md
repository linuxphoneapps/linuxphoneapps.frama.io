+++
title = "Francis"
description = "Track your time"
aliases = []
date = 2023-09-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/francis"
homepage = "http://apps.kde.org/francis"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Francis"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/francis/main.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.francis"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.francis"
flatpak_link = "https://flathub.org/apps/org.kde.francis.flatpakref"
flatpak_recipe = "https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "francis",]
appstream_xml_url = "https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2022-06-18"

+++

### Description

Francis uses the well-known pomodoro technique to help you get more productive.

[Source](https://invent.kde.org/utilities/francis/-/raw/master/org.kde.francis.metainfo.xml)