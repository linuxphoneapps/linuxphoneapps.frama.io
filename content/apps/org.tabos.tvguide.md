+++
title = "TV Guide"
description = "European televion guide for GNOME"
aliases = []
date = 2019-02-01
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Tabos Team",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/tabos/tvguide"
homepage = "https://www.tabos.org"
bugtracker = "https://gitlab.com/tabos/tvguide/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in"
screenshots = [ "https://www.tabos.org/project/tvguide/tvguide.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/tabos/tvguide/-/raw/master/data/icons/hicolor/scalable/apps/org.tabos.tvguide.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.tabos.tvguide"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/tabos/tvguide/-/raw/master/org.tabos.tvguide.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
latest_repo_commit = "2022-07-29"
repo_created_date = "2018-10-03"

+++


### Description

TV Guide for GNOME offers an overview and a detailed schedule page for European televion programms.

This software uses open data provided by xmltv.se.

[Source](https://gitlab.com/tabos/tvguide/-/raw/master/data/org.tabos.tvguide.appdata.xml.in)