+++
title = "BurgerParty"
description = "A time management game for Android where you play a fast-food owner who must put together the burgers ordered by her customers before time runs out."
aliases = ["games/com.agateau.burgerparty/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = ["agateau",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ActionGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/agateau/burgerparty"
homepage = "https://agateau.com/projects/burgerparty/"
bugtracker = "https://github.com/agateau/burgerparty/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/agateau/burgerparty"
screenshots = [ "https://agateau.com/projects/burgerparty/",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.agateau.burgerparty"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/com.agateau.burgerparty/"
latest_repo_commit = "2024-07-02"
repo_created_date = "2018-12-21"

+++

### Description

Burger Party is a fun and cute burger game for Android.

You just opened a burger restaurant, let's see if you can put together burgers fast enough to satisfy your ever growing number of customers! As you progress through the levels, unlock new items and visit new places!

[Source](https://agateau.com/projects/burgerparty/)
