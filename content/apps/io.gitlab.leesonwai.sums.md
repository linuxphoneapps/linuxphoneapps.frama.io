+++
title = "Sums"
description = "Calculate with postfix notation"
aliases = []
date = 2021-03-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Joshua Lee",]
categories = [ "calculator",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/leesonwai/sums"
homepage = "https://gitlab.com/leesonwai/sums"
bugtracker = "https://gitlab.com/leesonwai/sums/-/issues/new"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in"
screenshots = [ "https://gitlab.com/leesonwai/sums/-/raw/0.13/data/screenshots/sums-busy.png", "https://gitlab.com/leesonwai/sums/-/raw/0.13/data/screenshots/sums-new.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/leesonwai/sums/-/raw/main/data/icons/io.gitlab.leesonwai.Sums.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.gitlab.leesonwai.Sums"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.leesonwai.Sums"
flatpak_link = "https://flathub.org/apps/io.gitlab.leesonwai.Sums.flatpakref"
flatpak_recipe = "https://gitlab.com/leesonwai/sums/-/raw/main/io.gitlab.leesonwai.Sums.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "sums",]
appstream_xml_url = "https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-28"
repo_created_date = "2020-06-29"

+++

### Description

Sums is a postfix calculator designed for quick calculations and focused
on simplicity. Features include:

* A selection of core mathematical operations and constants
* Two-way history navigation
* Copying your results with one click

[Source](https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in)

### Notice

A few pixels too wide.