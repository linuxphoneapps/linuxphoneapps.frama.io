+++
title = "Lollypop"
description = "Play and organize your music collection"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Cédric Bellegarde",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/lollypop"
homepage = "https://wiki.gnome.org/Apps/Lollypop"
bugtracker = "https://gitlab.gnome.org/World/lollypop/-/issues"
donations = "https://www.paypal.me/lollypopgnome"
translations = "https://hosted.weblate.org/projects/gnumdk/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/org.gnome.Lollypop.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/gnumdk/lollypop-help/raw/master/screenshots/lollypop.jpg", "https://gitlab.gnome.org/gnumdk/lollypop-help/raw/master/screenshots/lollypop2.jpg", "https://gitlab.gnome.org/gnumdk/lollypop-help/raw/master/screenshots/lollypop3.jpg",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/icons/hicolor/scalable/apps/org.gnome.Lollypop.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Lollypop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Lollypop"
flatpak_link = "https://flathub.org/apps/org.gnome.Lollypop.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/lollypop/-/raw/master/org.gnome.Lollypop.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lollypop",]
appstream_xml_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/org.gnome.Lollypop.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-20"
repo_created_date = "2017-12-07"

+++

### Description

Lollypop is a lightweight modern music player designed to work excellently on the GNOME desktop environment. It also features a party mode which will auto-select party-related playlists; allows access the player from your couch thanks to it fullscreen mode; fetches lyrics, artwork and biography online; and provides native support for ReplayGain.

[Source](https://gitlab.gnome.org/World/lollypop/-/raw/master/data/org.gnome.Lollypop.appdata.xml.in)