+++
title = "Pixel Wheels"
description = "Pixel Wheels is a retro top-down race game"
aliases = [ "games/com.agateau.tinywheels.android/", "com.agateau.pixelwheels"]
date = 2019-02-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Aurélien Gâteau",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "ArcadeGame", "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/agateau/pixelwheels"
homepage = "https://agateau.com/projects/pixelwheels"
bugtracker = "https://github.com/agateau/pixelwheels/issues"
donations = "https://agateau.com/support"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.agateau.PixelWheels"
screenshots = [ "https://agateau.com/projects/pixelwheels/0.17.0/go.png", "https://agateau.com/projects/pixelwheels/0.17.0/rescued.png", "https://agateau.com/projects/pixelwheels/0.17.0/turbo.png", "https://agateau.com/projects/pixelwheels/0.18.0/a-rocket-for-rocket.png", "https://agateau.com/projects/pixelwheels/0.26.0/gun.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.agateau.PixelWheels"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.agateau.PixelWheels"
flatpak_link = "https://flathub.org/apps/com.agateau.PixelWheels.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pixel-wheels",]
appstream_xml_url = "https://raw.githubusercontent.com/agateau/pixelwheels/master/tools/packaging/linux/share/metainfo/com.agateau.PixelWheels.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/com.agateau.tinywheels.android/"
latest_repo_commit = "2024-12-16"
repo_created_date = "2015-05-19"

+++

### Description

Pixel Wheels is a retro top-down race game for Linux, macOS, Windows and Android.

It features multiple tracks, vehicles. Bonus and weapons can be picked up to help you get to the finish line first!

You can play Pixel Wheels alone or with friends.

[Source](https://raw.githubusercontent.com/agateau/pixelwheels/master/tools/packaging/linux/share/metainfo/com.agateau.PixelWheels.metainfo.xml)

### Notice

Patched to run by Moxvallix: https://github.com/moxvallix/LinMobGames
