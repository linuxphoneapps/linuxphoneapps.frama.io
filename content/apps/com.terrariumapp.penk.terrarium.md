+++
title = "Terrarium"
description = ""
aliases = []
date = 2020-03-04
updated = 2024-12-30

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "Penk Chen", ]
categories = [ "UI prototyping",]
mobile_compatibility = [ "needs testing",]
status = [ "released", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/penk/terrarium-app"
homepage = ""
bugtracker = "https://github.com/penk/terrarium-app/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
non_svg_icon_url = "https://github.com/penk/terrarium-app/blob/master/terrarium-app.png?raw=true"
all_features_touch = false
intended_for_mobile = false
app_id = "com.terrariumapp.penk.Terrarium"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.terrariumapp.penk.terrarium/"
latest_repo_commit = "2018-09-17"
repo_created_date = "2014-06-07"

+++

### Description

Terrarium is a cross-platform QML Playground: the view renders lively as you type in the editor, makes prototyping and experimenting with QtQuick a lot more fun!
