+++
title = "GhostCloud"
description = " A modern cross-platform client for Nextcloud, ownCloud & WebDav."
aliases = []
date = 2024-09-24
updated = 2024-10-25

[taxonomies]
project_licenses = [ "GPL-2.0-only AND LGPL-2.1-only",]
metadata_licenses = []
app_author = [ "Alfred E. Neumayer",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = [ "qwebdavlib",]
services = [ "Nextcloud", "Owncloud", "WebDAV",]
packaged_in = [ "alpine_edge", "aur", "snapcraft",]
freedesktop_categories = [ "Qt", "Network", "FileTransfer", "FileTools",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/fredldotme/harbour-owncloud"
homepage = "https://github.com/fredldotme/harbour-owncloud/"
bugtracker = "https://github.com/fredldotme/harbour-owncloud/issues"
donations = "https://paypal.me/beidl"
translations = ""
more_information = [ "https://open-store.io/app/me.fredl.ghostcloud", "https://openrepos.net/content/karry/ghostcloud-aarch64",]
summary_source_url = "https://github.com/fredldotme/harbour-owncloud"
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/fredldotme/harbour-owncloud/refs/heads/master/src/app/icons/harbour-owncloud.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "me.fredl.ghostcloud"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/ghostcloud"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/fredldotme/harbour-owncloud/refs/heads/master/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ghostcloud",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_repology"
feed_entry_id = "https://linuxphoneapps.org/apps/me.fredl.ghostcloud/"
latest_repo_commit = "2024-07-01"
repo_created_date = "2014-11-14"

+++

### Description

Allows you to easily access your ownCloud, NextCloud and WebDav instances with a native Free Software application.

For automatic camera photo backup and upload from within other apps (i.e. Gallery) take a look at the "Nextcloud/ownCloud additionals" over at OpenRepos.

Current qmake build configurations:

    SailfishOS: CONFIG+=sailfish_build
    GNU/Linux desktop: CONFIG+=noadditionals CONFIG+=quickcontrols
    Ubuntu Touch: CONFIG+=noadditionals CONFIG+=quickcontrols CONFIG+=click
    Android: CONFIG+=noadditionals CONFIG+=quickcontrols
    macOS/iOS: CONFIG+=noadditionals CONFIG+=quickcontrols

[Source](https://github.com/fredldotme/harbour-owncloud/)