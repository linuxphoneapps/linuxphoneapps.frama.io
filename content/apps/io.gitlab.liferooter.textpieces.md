+++
title = "Text Pieces"
description = "Developer's scratchpad"
aliases = [ "apps/com.github.liferooter.textpieces/", "apps/com.github.liferooter.textpieces.desktop/",]
date = 2022-09-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gleb Smirnov",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "TextTools", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.com/liferooter/textpieces"
homepage = "https://gitlab.com/liferooter/textpieces"
bugtracker = "https://gitlab.com/liferooter/textpieces/-/issues"
donations = ""
translations = "https://gitlab.com/liferooter/textpieces/-/tree/main/po"
more_information = [ "https://apps.gnome.org/Textpieces/",]
summary_source_url = "https://gitlab.com/liferooter/textpieces/-/raw/main/data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in?ref_type=heads"
screenshots = [ "https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-1.png", "https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-2.png", "https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-3.png", "https://gitlab.com/liferooter/textpieces/-/raw/31d47dcb999918e20ab6005cfc0554f0918f4961/data/screenshots/screenshot-4.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/1.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/2.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/3.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/4.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/5.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/6.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/7.png", "https://img.linuxphoneapps.org/com.github.liferooter.textpieces/8.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.liferooter.TextPieces"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.liferooter.TextPieces"
flatpak_link = "https://flathub.org/apps/io.gitlab.liferooter.TextPieces.flatpakref"
flatpak_recipe = "https://gitlab.com/liferooter/textpieces/-/raw/main/build-aux/io.gitlab.liferooter.TextPieces.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "textpieces",]
appstream_xml_url = "https://gitlab.com/liferooter/textpieces/-/raw/main/data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/com.github.liferooter.textpieces/"
latest_repo_commit = "2024-11-21"
repo_created_date = "2023-06-25"

+++


### Description

Powerful scratchpad with ability to perform a lot of text transformations, such as:

* Calculate hashes
* Encode text
* Decode text
* Remove trailing spaces and lines
* Count lines, symbols and words
* Format JSON and XML
* Escape and unescape strings
* Convert JSON to YAML and vice versa
* Filter lines
* And so on.

The application is extendable with your own script-based actions.

[Source](https://gitlab.com/liferooter/textpieces/-/raw/main/data/io.gitlab.liferooter.TextPieces.metainfo.xml.in.in)