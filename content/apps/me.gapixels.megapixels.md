+++
title = "Megapixels"
description = "A gnome camera application for phones"
aliases = [ "apps/org.postmarketos.megapixels/",]
date = 2020-09-12
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "postmarketOS Developers",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/postmarketOS/megapixels"
homepage = "https://sr.ht/~martijnbraam/megapixels"
bugtracker = "https://gitlab.com/postmarketOS/megapixels/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/postmarketOS/megapixels/-/raw/master/data/org.postmarketos.Megapixels.metainfo.xml"
screenshots = [ "http://brixitcdn.net/metainfo/megapixels.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "me.gapixels.Megapixels"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "megapixels",]
appstream_xml_url = "https://gitlab.com/megapixels-org/Megapixels/-/raw/master/data/me.gapixels.Megapixels.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
feed_entry_id = "https://linuxphoneapps.org/apps/org.postmarketos.megapixels/"
latest_repo_commit = "2024-02-07"
repo_created_date = "2021-11-08"

+++

### Description

Megapixels is a camera application designed for phones and tablets. It
 implements the v4l2 and media\-request apis so set up camera pipelines on
 ARM hardware and uses the raw data modes of the sensors to get the best
 quality pictures.

[Source](https://gitlab.com/megapixels-org/Megapixels/-/raw/master/data/me.gapixels.Megapixels.metainfo.xml)

### Notice

GTK3 branch is still available. Megapixels also scans QR codes.