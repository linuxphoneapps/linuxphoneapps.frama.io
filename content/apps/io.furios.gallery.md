+++
title = "FuriOS Gallery"
description = "View your Photos and Videos"
aliases = []
date = 2025-02-10
updated = 2025-02-17

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "FuriOS Gallery Team",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "FuriOS",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "Viewer",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/FuriLabs/furios-gallery"
homepage = "https://github.com/FuriLabs/furios-gallery"
bugtracker = "https://github.com/FuriLabs/furios-gallery/issues"
donations = ""
translations = ""
more_information = [ "https://furilabs.com/gallery-app/", "https://www.youtube.com/watch?v=aLt51NLZ4dM",]
summary_source_url = ""
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/FuriLabs/furios-gallery/refs/heads/trixie/data/io.FuriOS.Gallery.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.FuriOS.Gallery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/FuriLabs/furios-gallery/refs/heads/trixie/io.FuriOS.Gallery.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.furios.gallery/"
latest_repo_commit = "2025-02-16"
repo_created_date = "2025-01-24"

+++

### Description

Media gallery application for FuriOS

[Source](https://raw.githubusercontent.com/FuriLabs/furios-gallery/refs/heads/trixie/io.FuriOS.Gallery.metainfo.xml)

### Notice

If you want to try it (and don't use FuriOS), the easiest way currently is installing this [flatpak](https://github.com/luigi311/furios-gallery/actions/runs/13096124319).
