+++
title = "Alpaka"
description = "An AI chat client"
aliases = []
date = 2024-06-23
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community", ]
categories = [ "llm chat",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami",]
backends = [ "Ollama",]
services = []
packaged_in = [ "aur", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/alpaka"
homepage = "https://apps.kde.org/alpaka/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=Alpaka"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/alpaka/-/raw/master/org.kde.alpaka.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.alpaka"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "alpaka",]
appstream_xml_url = "https://invent.kde.org/utilities/alpaka/-/raw/master/org.kde.alpaka.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-12-02"

+++

### Description

Alpaka is a client for the Ollama AI service, which lets you run LLM models such as Llama 2.

[Source](https://invent.kde.org/utilities/alpaka/-/raw/master/org.kde.alpaka.metainfo.xml)
