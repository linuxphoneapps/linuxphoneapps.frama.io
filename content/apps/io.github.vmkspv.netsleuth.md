+++
title = "Netsleuth"
description = "Calculate IP subnets"
aliases = []
date = 2024-10-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Vladimir Kosolapov",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/vmkspv/netsleuth"
homepage = "https://github.com/vmkspv/netsleuth"
bugtracker = "https://github.com/vmkspv/netsleuth/issues"
donations = ""
translations = "https://github.com/vmkspv/netsleuth/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.vmkspv.netsleuth"
screenshots = [ "https://raw.githubusercontent.com/vmkspv/netsleuth/main/data/screenshots/main-1.png", "https://raw.githubusercontent.com/vmkspv/netsleuth/main/data/screenshots/main-2.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/vmkspv/netsleuth/refs/heads/main/data/icons/hicolor/scalable/apps/io.github.vmkspv.netsleuth.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.vmkspv.netsleuth"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.vmkspv.netsleuth"
flatpak_link = "https://flathub.org/apps/io.github.vmkspv.netsleuth.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/vmkspv/netsleuth/refs/heads/main/io.github.vmkspv.netsleuth.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/vmkspv/netsleuth/refs/heads/main/data/io.github.vmkspv.netsleuth.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.vmkspv.netsleuth/"
latest_repo_commit = "2024-12-23"
repo_created_date = "2024-09-25"

+++

### Description

A simple utility for the calculation and analysis of IP subnet values, designed to simplify network configuration tasks.

[Source](https://raw.githubusercontent.com/vmkspv/netsleuth/refs/heads/main/data/io.github.vmkspv.netsleuth.metainfo.xml.in)