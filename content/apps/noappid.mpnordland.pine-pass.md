+++
title = "Pine Pass"
description = "GUI for password-store.org written with Python and GTK"
aliases = []
date = 2021-06-04
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "mpnordland",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3",]
backends = [ "pass", "password-store.org",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://github.com/mpnordland/pine_pass"
homepage = ""
bugtracker = "https://github.com/mpnordland/pine_pass/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/mpnordland/pine_pass"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2022-03-14"
repo_created_date = "2021-04-20"

+++

### Description

GUI for password-store.org written with Python and GTK.
Originally written for the PinePhone but should be compatible
with most Linux distros.

#### Current Features

* Can search through passwords in repository and copy the password to the clipboard
* Can display/edit passwords
* Can generate and insert new passwords
* Has a preference screen that can handle simple repository and gpg key id management
* Shows you the first SSH key it found or offers to generate one (only generates RSA keys right now, uses ssh-keygen)