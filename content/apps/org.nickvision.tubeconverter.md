+++
title = "Parabolic"
description = "Download web video and audio"
aliases = []
date = 2024-02-07
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nickvision",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp",]
services = [ "YouTube",]
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "AudioVideo", "Network",]
programming_languages = [ "Csharp",]
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/NickvisionApps/parabolic"
homepage = "https://github.com/NickvisionApps/Parabolic"
bugtracker = "https://github.com/NickvisionApps/Parabolic/issues"
donations = "https://github.com/sponsors/nlogozzo"
translations = "https://github.com/NickvisionApps/Parabolic/blob/master/CONTRIBUTING.md#providing-translations"
more_information = []
summary_source_url = "https://flathub.org/apps/org.nickvision.tubeconverter"
screenshots = [ "https://dl.flathub.org/media/org/nickvision/tubeconverter/845d969afadbf3b1ddb45e421d349591/screenshots/image-1_orig.png", "https://dl.flathub.org/media/org/nickvision/tubeconverter/845d969afadbf3b1ddb45e421d349591/screenshots/image-2_orig.png", "https://dl.flathub.org/media/org/nickvision/tubeconverter/845d969afadbf3b1ddb45e421d349591/screenshots/image-3_orig.png", "https://dl.flathub.org/media/org/nickvision/tubeconverter/845d969afadbf3b1ddb45e421d349591/screenshots/image-4_orig.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.nickvision.tubeconverter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.nickvision.tubeconverter"
flatpak_link = "https://flathub.org/apps/org.nickvision.tubeconverter.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/NickvisionApps/Parabolic/main/flatpak/org.nickvision.tubeconverter.json"
snapcraft = "https://snapcraft.io/tube-converter"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/NickvisionApps/Parabolic/main/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "parabolic",]
appstream_xml_url = "https://raw.githubusercontent.com/NickvisionApps/Parabolic/dd6f8838bdc907f72ad6c9c562e127d2767795df/NickvisionTubeConverter.Shared/org.nickvision.tubeconverter.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2021-12-18"

+++

### Description

— A basic yt-dlp frontend

— Supports downloading videos in multiple formats (mp4, webm, mp3, opus, flac, and wav)

— Run multiple downloads at a time

— Supports downloading metadata and video subtitles

[Source](https://raw.githubusercontent.com/NickvisionApps/Parabolic/dd6f8838bdc907f72ad6c9c562e127d2767795df/NickvisionTubeConverter.Shared/org.nickvision.tubeconverter.metainfo.xml.in)