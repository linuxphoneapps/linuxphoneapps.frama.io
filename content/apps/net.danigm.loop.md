+++
title = "Loop"
description = "A simple audio loop machine for GNOME"
aliases = [ "apps/net.danigm.loop.desktop/",]
date = 2021-12-08
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Daniel Garcia Moreno",]
categories = [ "musical tool",]
mobile_compatibility = [ "2",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Music",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/danigm/loop"
homepage = "https://gitlab.gnome.org/danigm/loop"
bugtracker = "https://gitlab.gnome.org/danigm/loop/-/issues"
donations = ""
translations = ""
more_information = [ "https://danigm.net/loop.html",]
summary_source_url = "https://gitlab.gnome.org/danigm/loop/-/raw/main/data/net.danigm.loop.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/danigm/loop/raw/main/screenshots/loop01.png", "https://gitlab.gnome.org/danigm/loop/raw/main/screenshots/loop02.png", "https://gitlab.gnome.org/danigm/loop/raw/main/screenshots/loop03.png", "https://gitlab.gnome.org/danigm/loop/raw/main/screenshots/loop04.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "net.danigm.loop"
scale_to_fit = "net.danigm.loop"
flathub = "https://flathub.org/apps/net.danigm.loop"
flatpak_link = "https://flathub.org/apps/net.danigm.loop.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-loop",]
appstream_xml_url = "https://gitlab.gnome.org/danigm/loop/-/raw/main/data/net.danigm.loop.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/net.danigm.loop/"
latest_repo_commit = "2024-06-06"
repo_created_date = "2021-11-30"

+++


### Description

Simple audio loop machine application to create music

[Source](https://gitlab.gnome.org/danigm/loop/-/raw/main/data/net.danigm.loop.appdata.xml.in)

### Notice

With release 1.0 this app does not fit the screen anymore, still works okay-ish with scale-to-fit in landscape.
Last commit 2022-03-24.