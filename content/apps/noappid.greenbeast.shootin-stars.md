+++
title = "Shootin Stars"
description = "Round based endless runner style space shooter"
aliases = ["games/noappid.greenbeast.shootin-stars/"]
date = 2021-12-02
updated = 2025-01-07

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = ["greenbeast",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Godot",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = [ "Python",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/greenbeast/shootin-stars"
homepage = ""
bugtracker = "https://gitlab.com/greenbeast/shootin-stars/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/shootin-stars"
screenshots = [ "https://gitlab.com/greenbeast/shootin-stars",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "Hank"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.greenbeast.shootin-stars/"
latest_repo_commit = "2024-08-05"
repo_created_date = "2021-12-02"

+++

### Notice

Built for the PinePhone and runs just fine
