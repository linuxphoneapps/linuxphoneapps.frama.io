+++
title = "Keypunch"
description = "Practice your typing skills"
aliases = []
date = 2024-11-10
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Brage Fuglseth",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Education",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/bragefuglseth/keypunch"
homepage = "https://github.com/bragefuglseth/keypunch"
bugtracker = "https://github.com/bragefuglseth/keypunch/issues"
donations = ""
translations = "https://github.com/bragefuglseth/keypunch/blob/main/CONTRIBUTING.md#translating"
more_information = []
summary_source_url = "https://flathub.org/apps/dev.bragefuglseth.Keypunch"
screenshots = [ "https://raw.githubusercontent.com/bragefuglseth/keypunch/main/data/screenshots/1-session.png", "https://raw.githubusercontent.com/bragefuglseth/keypunch/main/data/screenshots/2-ready.png", "https://raw.githubusercontent.com/bragefuglseth/keypunch/main/data/screenshots/3-results.png", "https://raw.githubusercontent.com/bragefuglseth/keypunch/main/data/screenshots/4-languages.png", "https://raw.githubusercontent.com/bragefuglseth/keypunch/main/data/screenshots/5-custom-text.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/bragefuglseth/keypunch/refs/heads/main/data/icons/dev.bragefuglseth.Keypunch.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "dev.bragefuglseth.Keypunch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.bragefuglseth.Keypunch"
flatpak_link = "https://flathub.org/apps/dev.bragefuglseth.Keypunch.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/bragefuglseth/keypunch/refs/heads/main/build-aux/dev.bragefuglseth.Keypunch.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "keypunch",]
appstream_xml_url = "https://raw.githubusercontent.com/bragefuglseth/keypunch/refs/heads/main/data/dev.bragefuglseth.Keypunch.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/dev.bragefuglseth.keypunch/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-04-03"

+++

### Description

There is no doubt that typing on a keyboard is an essential skill in the
digital age. Typing fast and accurately gives you more opportunities,
more time for what matters to you, and a sense of self-accomplishment.

Keypunch lets you practice typing. Thanks to fast-paced
sessions with instant feedback afterwards and a plethora of available languages,
you might even have a little fun doing so. With determination, proper technique
and some time, you will observe a noticeable increase in both the speed
and the accuracy of which your thoughts and ideas are put into words on the screen.

If you are already a racer at typing, Keypunch still has something for you.
Try practicing with numbers and punctuation, or choose your own text to type
out as fast as you can.

Get ready to accelerate your typing!

[Source](https://raw.githubusercontent.com/bragefuglseth/keypunch/refs/heads/main/data/dev.bragefuglseth.Keypunch.metainfo.xml.in.in)