+++
title = "Idok Remote"
description = "A simple Kodi remote that supports multiple Kodi instances."
aliases = []
date = 2021-03-10
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Kodi",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "RemoteAccess",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/The3DmaN/idokremote"
homepage = "https://the3dman.gitlab.io/"
bugtracker = "https://gitlab.com/The3DmaN/idokremote/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/idokremote"
screenshots = [ "https://gitlab.com/The3DmaN/idokremote",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/The3DmaN/idokremote/-/raw/master/images/Idok.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "idokremote",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "check_via_git_api"
latest_repo_commit = "2023-01-16"
repo_created_date = "2021-03-06"

+++

### Notice

Last commit February 27th, 2022.