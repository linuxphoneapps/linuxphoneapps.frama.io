+++
title = "Pamac"
description = "Graphical Package Manager for Manjaro Linux with Alpm, AUR, Appstream, Flatpak and Snap support"
aliases = []
date = 2021-04-18
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "applications",]
categories = [ "app store",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "PackageManager",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/manjaro/pamac"
homepage = ""
bugtracker = "https://github.com/manjaro/pamac/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/manjaro/pamac"
screenshots = [ "https://fosstodon.org/@linmob/106086019748787319", "https://twitter.com/ManjaroLinux/status/1383498747464753153",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/manjaro/pamac/refs/heads/master/data/icons/hicolor/48x48/apps/system-software-install.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.manjaro.pamac.manager"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pamac",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-10-26"
repo_created_date = "2014-09-20"

+++

### Notice

Previous to pamac 11, pamac used GTK3 with libadwaita.
