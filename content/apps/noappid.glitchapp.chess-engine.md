+++
title = "Chess engine"
description = "A fully compliant, playable chess engine written in Lua and rendered with the Love2D game framework forked from https://github.com/Azzla/chess-engine."
aliases = ["games/noappid.glitchapp.chess-engine/"]
date = 2024-10-05
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "glitchapp",]
categories = [ "game", "board game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = [ "LÖVE2D",]
services = []
packaged_in = []
freedesktop_categories = [ "Game", "BoardGame",]
programming_languages = [ "Lua",]
build_systems = [ "make",]
requires_internet = [ "requires offline-only",]
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://codeberg.org/glitchapp/chess-engine"
homepage = "https://codeberg.org/glitchapp/chess-engine"
bugtracker = "https://codeberg.org/glitchapp/chess-engine/issues"
donations = "https://glitchapp.codeberg.page/img/ZCashQr.webp"
translations = ""
more_information = []
summary_source_url = "https://github.com/Azzla/chess-engine"
screenshots = [ "https://codeberg.org/glitchapp/chess-engine/media/branch/main/screenshots/sc1.webp",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "glitchapp"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-11-11"
repo_created_date = "2024-09-29"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.glitchapp.chess-engine/"

+++

### Description

A fully compliant, playable chess engine written in Lua and rendered with the Love2D game framework forked from https://github.com/Azzla/chess-engine.

This fork is aimed at mobiles and adds some tweaks such as zoom out the table and tweak the screen resolution to make it fit on portrait mobile screens.

### Notice

An .apk for postmarketOS (and a .rpm for Sailfish OS) can be found under [releases](https://codeberg.org/glitchapp/chess-engine/releases).
