+++
title = "EV"
description = "Electric Vehicle information"
aliases = []
date = 2024-08-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Guido Günther",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Hyundai-Kia-Connect",]
services = [ "hyundai_kia_connect",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.gnome.org/guidog/phosh-ev"
homepage = "https://gitlab.gnome.org/guidog/phosh-ev"
bugtracker = "https://gitlab.gnome.org/guidog/phosh-ev/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/guidog/phosh-ev/-/raw/main/data/mobi.phosh.Ev.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/guidog/phosh-ev/-/raw/main/screenshots/charging.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/guidog/phosh-ev/-/raw/main/data/icons/mobi.phosh.Ev-symbolic.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "mobi.phosh.Ev"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/guidog/phosh-ev/-/raw/main/data/mobi.phosh.Ev.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/guidog/phosh-ev/-/raw/main/data/mobi.phosh.Ev.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/mobi.phosh.ev/"
latest_repo_commit = "2024-12-07"
repo_created_date = "2023-10-03"

+++

### Description

A simple GTK4 based app to display EV information. This
currently supports vehicles supported by the hyundai\_kia\_connect API.

[Source](https://gitlab.gnome.org/guidog/phosh-ev/-/raw/main/data/mobi.phosh.Ev.metainfo.xml.in)