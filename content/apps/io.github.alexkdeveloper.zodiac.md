+++
title = "Zodiac"
description = "A simple program for plotting horoscopes"
aliases = []
date = 2024-02-10
updated = 2024-10-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex Kryuchkov",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Science",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/alexkdeveloper/zodiac"
homepage = "http://github.com/alexkdeveloper/zodiac"
bugtracker = "http://github.com/alexkdeveloper/zodiac/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.alexkdeveloper.zodiac"
screenshots = [ "https://dl.flathub.org/media/io/github/alexkdeveloper.zodiac/915a93f515ffa737e5ac485648ecaf61/screenshots/image-1_orig.png", "https://dl.flathub.org/media/io/github/alexkdeveloper.zodiac/915a93f515ffa737e5ac485648ecaf61/screenshots/image-2_orig.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/icons/hicolor/scalable/apps/io.github.alexkdeveloper.zodiac.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.alexkdeveloper.zodiac"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.alexkdeveloper.zodiac"
flatpak_link = "https://flathub.org/apps/io.github.alexkdeveloper.zodiac.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/io.github.alexkdeveloper.zodiac.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/io.github.alexkdeveloper.zodiac.appdata.xml.in"
reported_by = "1peter10"
updated_by = "script"
latest_repo_commit = "2024-03-23"
repo_created_date = "2023-08-27"

+++

### Description

The application generates horoscopes in svg and txt format. The user only needs to fill in the required fields. The application can make horoscopes for both one and two people.

[Source](https://raw.githubusercontent.com/alexkdeveloper/zodiac/main/data/io.github.alexkdeveloper.zodiac.appdata.xml.in)

### Notice

Just a few pixels too wide.