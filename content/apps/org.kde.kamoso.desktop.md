+++
title = "Kamoso"
description = "Use your webcam to take pictures and make videos"
aliases = [ "apps/org.kde.kamoso/",]
date = 2020-02-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "AudioVideo",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/multimedia/kamoso"
homepage = "https://userbase.kde.org/Kamoso"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kamoso"
donations = ""
translations = ""
more_information = [ "https://userbase.kde.org/Kamoso",]
summary_source_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kamoso/kamoso.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/icons/sc-apps-kamoso.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kamoso.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kamoso"
flatpak_link = "https://flathub.org/apps/org.kde.kamoso.flatpakref"
flatpak_recipe = "https://invent.kde.org/multimedia/kamoso/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = "https://invent.kde.org/multimedia/kamoso/-/raw/master/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kamoso",]
appstream_xml_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.kamoso/"
latest_repo_commit = "2025-01-09"
repo_created_date = "2019-08-27"

+++

### Description

Kamoso is a simple and friendly program to use your camera. Use it to take pictures and make videos to share.

[Source](https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml)