+++
title = "BitRitter"
description = "Interact with Bitwarden/Vaultwarden vaults"
aliases = [ "apps/noappid.chfkch.bitritter/",]
date = 2024-09-26
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "ChfKch",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "rbw",]
services = [ "Vaultwarden", "Bitwarden",]
packaged_in = [ "alpine_edge",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/Chfkch/bitritter"
homepage = "https://codeberg.org/Chfkch/bitritter"
bugtracker = "https://codeberg.org/Chfkch/bitritter/issues"
donations = ""
translations = ""
more_information = [ "https://ruhr.social/tags/BitRitter",]
summary_source_url = "https://codeberg.org/Chfkch/bitritter/raw/branch/main/dev.bitritter.BitRitter.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "dev.bitritter.BitRitter"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "bitritter",]
appstream_xml_url = "https://codeberg.org/Chfkch/bitritter/raw/branch/main/dev.bitritter.BitRitter.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.chfkch.bitritter/"
latest_repo_commit = "2024-12-03"
repo_created_date = "2024-03-21"

+++

### Description

This app is aimed at Linux mobile devices and thus it is designed to support small screens and touch input. The intention is to implement a basic suit of features from the official Bitwarden clients and then reach feature parity in the future.

[Source](https://codeberg.org/Chfkch/bitritter/raw/branch/main/dev.bitritter.BitRitter.metainfo.xml)