+++
title = "NymphCast Player"
description = "NymphCast Player provides NymphCast client functionality format in a graphical (Qt-based) format"
aliases = []
date = 2024-10-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "BSD-3-Clause",]
metadata_licenses = []
app_author = [ "Maya Posch",]
categories = [ "audio player", "video player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = [ "nymphcast",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur",]
freedesktop_categories = [ "AudioVideo", "Audio", "Video", "Player", "Network",]
programming_languages = [ "C", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/MayaPosch/NymphCast/tree/master/player"
homepage = "http://nyanko.ws/nymphcast.php"
bugtracker = "https://github.com/MayaPosch/NymphCast/issues"
donations = "https://ko-fi.com/mayaposch."
translations = ""
more_information = [ "https://github.com/MayaPosch/NymphCast/blob/master/doc/building_nymphcast_player.md",]
summary_source_url = "https://github.com/MayaPosch/NymphCast?tab=readme-ov-file#nymphcast-player-client"
screenshots = []
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = "https://raw.githubusercontent.com/MayaPosch/NymphCast/refs/heads/master/player/playstore/nymphcast_logo_no_text_512x512.png"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "nymphcast",]
appstream_xml_url = ""
reported_by = "krzno"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.mayaposch.nymphcast/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2019-02-07"

+++

### Description

The NymphCast Player provides NymphCast client functionality in a graphical (Qt-based) format. It is also a demonstration platform for the NymphCast SDK (see details on the SDK later in this document). It is designed to run on any OS that is supported by the Qt framework.

[Source](https://github.com/MayaPosch/NymphCast?tab=readme-ov-file#nymphcast-player-client)




### Notice

Nymphcast can be used to stream content locally to other devices, e.g., a Raspberry Pi connected to speakers or a TV.
This app is part of Alpine's nymphcast-client package. To start the GUI, run "NymphCastPlayer" on  your favorite CLI.
