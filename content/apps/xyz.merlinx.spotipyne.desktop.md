+++
title = "Spotipyne"
description = "Gtk Spotify client written in python made to be compatible with mobile formfactors like a pinephone."
aliases = [ "apps/xyz.merlinx.spotipyne/",]
date = 2020-11-07
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "dann-merlin",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Spotify",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/dann-merlin/spotipyne"
homepage = ""
bugtracker = "https://gitlab.com/dann-merlin/spotipyne/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/dann-merlin/spotipyne"
screenshots = [ "https://gitlab.com/dann-merlin/spotipyne",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "xyz.merlinx.Spotipyne.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/dann-merlin/spotipyne/-/raw/master/xyz.merlinx.Spotipyne.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/dann-merlin/spotipyne/-/raw/master/data/xyz.merlinx.Spotipyne.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/xyz.merlinx.spotipyne/"
latest_repo_commit = "2022-12-10"
repo_created_date = "2020-10-11"

+++

### Description

A gtk spotify client capable of handling mobile formfactors.

[Source](https://gitlab.com/dann-merlin/spotipyne/-/raw/master/data/xyz.merlinx.Spotipyne.appdata.xml.in)