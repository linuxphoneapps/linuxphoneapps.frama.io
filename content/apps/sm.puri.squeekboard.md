+++
title = "Squeekboard"
description = "The final Librem5 keyboard"
aliases = []
date = 2020-09-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.gnome.org/World/Phosh/squeekboard"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Phosh/squeekboard/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/Librem5/squeekboard"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "sm.puri.Squeekboard"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "squeekboard",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-21"
repo_created_date = "2021-08-04"

+++

### Description

Squeekboard is a virtual keyboard supporting Wayland, built primarily for the Librem 5 phone. It squeaks because some Rust got inside. [Source](https://gitlab.gnome.org/World/Phosh/squeekboard)

### Notice

Default Phosh OSK, replaced virtboard.