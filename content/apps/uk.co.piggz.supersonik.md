+++
title = "Supersonik"
description = "Subsonic music client"
aliases = []
date = 2024-12-22

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "piggz",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Subsonic",]
packaged_in = [ "alpine_edge",]
freedesktop_categories = [ "KDE", "Qt", "Network", "Audio",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/piggz/supersonik"
homepage = ""
bugtracker = "https://github.com/piggz/supersonik/issues"
donations = ""
translations = ""
more_information = [ "https://fosstodon.org/@piggz/113142756388688488", "https://fosstodon.org/@piggz/113696808059089666", "https://fosstodon.org/@piggz/113686232862509512",]
summary_source_url = "https://raw.githubusercontent.com/piggz/supersonik/refs/heads/main/rpm/harbour-supersonik.spec"
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/piggz/supersonik/refs/heads/main/harbour-supersonik.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "uk.co.piggz.supersonik"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "supersonik",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = ""
feed_entry_id = "https://linuxphoneapps.org/apps/uk.co.piggz.supersonik/"
latest_repo_commit = "2024-12-22"
repo_created_date = "2024-09-14"

+++

### Description

Subsonic music client for KDE desktops and mobile devices

[Source](https://raw.githubusercontent.com/piggz/supersonik/refs/heads/main/rpm/harbour-supersonik.spec)




### Notice

Also supports Navidrome and other Subsonic-compatible services.
