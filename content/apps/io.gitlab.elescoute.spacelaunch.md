+++
title = "Space Launch"
description = "Rocket launches tracker"
aliases = [ "apps/org.emilien.spacelaunch/",]
date = 2021-11-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emilien Lescoute",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "spacelaunchnow.me",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Education", "Science",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/elescoute/spacelaunch"
homepage = "https://gitlab.com/elescoute/spacelaunch"
bugtracker = "https://gitlab.com/elescoute/spacelaunch/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/io.gitlab.elescoute.spacelaunch.metainfo.xml.in"
screenshots = [ "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot01.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot02.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot03.png", "https://gitlab.com/elescoute/spacelaunch/raw/main/data/appdata/screenshot04.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/icons/hicolor/scalable/apps/io.gitlab.elescoute.spacelaunch.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.elescoute.spacelaunch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.elescoute.spacelaunch"
flatpak_link = "https://flathub.org/apps/io.gitlab.elescoute.spacelaunch.flatpakref"
flatpak_recipe = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/build-aux/flatpak/io.gitlab.elescoute.spacelaunch.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/io.gitlab.elescoute.spacelaunch.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/org.emilien.spacelaunch/"
latest_repo_commit = "2024-07-24"
repo_created_date = "2024-05-23"

+++


### Description

With Space Launch you can keep track of upcoming launches.

Data on the launches are provided by spacelaunchnow.me.

Space Launch is currently in alpha release. Some crashes due to problems of communication with spacelaunchnow.me server can remain. More features will be added later.

Compatible with GNOME Mobile (PinePhone, Librem 5).

[Source](https://gitlab.com/elescoute/spacelaunch/-/raw/main/data/io.gitlab.elescoute.spacelaunch.metainfo.xml.in)

### Notice

This app has a great custom animation while loading data.