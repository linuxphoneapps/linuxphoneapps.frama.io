+++
title = "Switcheroo"
description = "Convert and manipulate images"
aliases = []
date = 2023-04-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Khaleel Al-Adhami",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "ImageMagick",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Graphics", "ImageProcessing", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.com/adhami3310/Switcheroo"
homepage = "https://apps.gnome.org/Converter/"
bugtracker = "https://gitlab.com/adhami3310/Switcheroo/-/issues"
donations = "https://opencollective.com/switcheroo"
translations = "https://gitlab.com/adhami3310/Switcheroo/-/tree/main/po"
more_information = [ "https://apps.gnome.org/Converter/",]
summary_source_url = "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/io.gitlab.adhami3310.Switcheroo.metainfo.xml.in"
screenshots = [ "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/resources/screenshots/0.png", "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/resources/screenshots/1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/1.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/2.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/3.png", "https://img.linuxphoneapps.org/io.gitlab.adhami3310.converter/4.png",]
all_features_touch = true
intended_for_mobile = false
app_id = "io.gitlab.adhami3310.Converter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.adhami3310.Converter"
flatpak_link = "https://flathub.org/apps/io.gitlab.adhami3310.Converter.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "switcheroo",]
appstream_xml_url = "https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/io.gitlab.adhami3310.Converter.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-04"
repo_created_date = "2022-11-23"

+++

### Description

Convert between different image filetypes and resize them easily.

[Source](https://gitlab.com/adhami3310/Switcheroo/-/raw/main/data/io.gitlab.adhami3310.Switcheroo.metainfo.xml.in.in)

### Notice

Previously named Converter (until release 2.0).