+++
title = "ABV Calculator"
description = "ABV Calculator is a refractometer ABV calculator intended for beer or wine"
aliases = []
date = 2021-01-24
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/The3DmaN/abv-calc"
homepage = ""
bugtracker = "https://gitlab.com/The3DmaN/abv-calc/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/abv-calc"
screenshots = []
screenshots_img = []
svg_icon_url = "https://gitlab.com/The3DmaN/abv-calc/-/raw/master/abvcalc/icons/abv.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "abv-calc",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "check_via_git_api"
latest_repo_commit = "2021-08-09"
repo_created_date = "2020-11-23"

+++

### Notice

Last commit in August 2021.