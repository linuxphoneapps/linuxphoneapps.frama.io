+++
title = "Media Server Connect (MSC)"
description = "Media Server Connect is a simple web app for connecting to the web interface of an Emby server."
aliases = []
date = 2021-01-24
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "archived", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Emby",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "RemoteAccess",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/The3DmaN/media-server-connect"
homepage = ""
bugtracker = "https://gitlab.com/The3DmaN/media-server-connect/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/media-server-connect"
screenshots = [ "https://gitlab.com/The3DmaN/media-server-connect",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/The3DmaN/media-server-connect/-/raw/master/images/media-server-connect.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "media-server-connect",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "check_via_git_api"
latest_repo_commit = "2021-10-13"
repo_created_date = "2020-12-06"

+++