+++
title = "FluffyChat (QtQuick)"
description = "FluffyChat is a chat client for Matrix, originally created for Ubuntu Touch"
aliases = []
date = 2019-09-30
updated = 2024-12-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "krillefear",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Matrix",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "QML", "JavaScript", "Cpp",]
build_systems = [ "cmake", "clickable",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/KrilleFear/fluffychat"
homepage = ""
bugtracker = "https://gitlab.com/KrilleFear/fluffychat/-/issues/"
donations = ""
translations = ""
more_information = [ "https://ubports.com/de_DE/blog/ubports-blog-1/post/fluffychat-188",]
summary_source_url = ""
screenshots = [ "https://invidious.kavin.rocks/watch?v=ZKMIdr6bqg4", "https://open-store.io/app/fluffychat.christianpauly",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/KrilleFear/fluffychat/-/raw/master/assets/logo.svg"
all_features_touch = false
intended_for_mobile = true
app_id = "fluffychat.christianpauly"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = "https://gitlab.com/KrilleFear/fluffychat/-/raw/master/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "maxnet"
latest_repo_commit = "2024-10-20"
repo_created_date = "2019-04-18"

+++

### Notice

This project is no longer in active development, replaced by [a Flutter based app](@/apps/im.fluffychat.fluffychat.md) also called Fluffychat. Merge Requests are still welcome, as QML-based Fluffychat is still in use on Ubuntu Touch.
