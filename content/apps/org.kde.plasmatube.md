+++
title = "PlasmaTube"
description = "Watch YouTube videos"
aliases = []
date = 2019-04-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = [ "libmpv", "youtube-dl",]
services = [ "invidious API", "YouTube",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed",]
freedesktop_categories = [ "AudioVideo", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/multimedia/plasmatube"
homepage = "https://apps.kde.org/plasmatube/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=PlasmaTube"
donations = "https://kde.org/community/donations/?app=org.kde.plasmatube"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://kde.org/announcements/megarelease/6/#plasmatube",]
summary_source_url = "https://invent.kde.org/multimedia/plasmatube/-/raw/master/org.kde.plasmatube.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/plasmatube/plasmatube.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/multimedia/plasmatube/-/raw/master/org.kde.plasmatube.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.plasmatube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.plasmatube"
flatpak_link = "https://flathub.org/apps/org.kde.plasmatube.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasmatube",]
appstream_xml_url = "https://invent.kde.org/multimedia/plasmatube/-/raw/master/org.kde.plasmatube.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2019-03-16"

+++

### Description

PlasmaTube allows you to watch YouTube videos on your phone or desktop using a elegant user interface integrated with the rest of Plasma.

[Source](https://invent.kde.org/multimedia/plasmatube/-/raw/master/org.kde.plasmatube.appdata.xml)