+++
title = "gtkcord3"
description = "A lightweight Discord client which uses GTK3 for the user interface."
aliases = []
date = 2020-08-26
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "diamondburned",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Discord",]
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Go",]
build_systems = [ "go",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/diamondburned/gtkcord3"
homepage = ""
bugtracker = "https://github.com/diamondburned/gtkcord3/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/diamondburned/gtkcord3"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.diamondburned.gtkcord3"
scale_to_fit = "com.github.diamondburned.gtkcord3"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2022-04-05"
repo_created_date = "2020-02-07"

+++

### Notice

Chat screens don’t scale properly without scale-to-fit, for abandonned preparations for flathub publication see issue #80.