+++
title = "Font Downloader"
description = "Install fonts from online sources"
aliases = []
date = 2020-10-26
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gustavo Peredo",]
categories = [ "font downloader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Google Fonts",]
packaged_in = [ "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/GustavoPeredo/font-downloader"
homepage = "https://github.com/GustavoPeredo/font-downloader"
bugtracker = "https://github.com/GustavoPeredo/Font-Downloader/issues"
donations = ""
translations = "https://poeditor.com/join/project?hash=hfnXv8Iw4o"
more_information = [ "https://apps.gnome.org/app/org.gustavoperedo.FontDownloader/",]
summary_source_url = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/compact.png", "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/dark_compact.png", "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/dark_entire.png", "https://raw.githubusercontent.com/GustavoPeredo/font-downloader/master/data/screenshots/entire.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/refs/heads/master/data/icons/hicolor/scalable/apps/org.gustavoperedo.FontDownloader.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gustavoperedo.FontDownloader"
scale_to_fit = "fontdownloader"
flathub = "https://flathub.org/apps/org.gustavoperedo.FontDownloader"
flatpak_link = "https://flathub.org/apps/org.gustavoperedo.FontDownloader.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/refs/heads/master/org.gustavoperedo.FontDownloader.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "font-downloader",]
appstream_xml_url = "https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-06-02"
repo_created_date = "2020-08-30"

+++


### Description

Have you ever wanted to change the font in your terminal, but didn't want to go through the entire process of searching, downloading and installing a font? This simple to use and adaptive GTK application allows you to search and install fonts directly from Google Fonts' website!

[Source](https://raw.githubusercontent.com/GustavoPeredo/Font-Downloader/master/data/org.gustavoperedo.FontDownloader.appdata.xml.in)