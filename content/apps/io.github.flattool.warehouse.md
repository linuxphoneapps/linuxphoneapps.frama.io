+++
title = "Warehouse"
description = "Manage all things Flatpak"
aliases = []
date = 2024-02-10
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Heliguy",]
categories = [ "settings", "flatpak management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "flatpak",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "System", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/flattool/warehouse"
homepage = "https://github.com/flattool/warehouse"
bugtracker = "https://github.com/flattool/warehouse/issues"
donations = "https://ko-fi.com/heliguy"
translations = "https://weblate.fyralabs.com/projects/flattool/warehouse/"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.flattool.Warehouse"
screenshots = [ "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/data_page_wide.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/install_page_skinny.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/install_page_wide.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/packages_page_wide.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/propteries_page_skinny.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/remotes_page_wide.png", "https://raw.githubusercontent.com/flattool/warehouse/main/app_page_screeshots/snapshots_page_wide.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/flattool/warehouse/main/data/icons/hicolor/scalable/apps/io.github.flattool.Warehouse.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.flattool.Warehouse"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.flattool.Warehouse"
flatpak_link = "https://flathub.org/apps/io.github.flattool.Warehouse.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flattool/warehouse/main/io.github.flattool.Warehouse.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "warehouse",]
appstream_xml_url = "https://raw.githubusercontent.com/flattool/warehouse/main/data/io.github.flattool.Warehouse.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-08-31"

+++

### Description

Warehouse provides a simple UI to control complex Flatpak options, all without resorting to the command line.

Features:

* Manage installed Flatpaks and view properties of any package
* Change versions of a Flatpak to rollback any unwanted updates
* Pin runtimes and mask Flatpaks
* Filter packages and sort data, to help find anything easily
* See current app user data, and cleanup any unused data left behind
* Add popular Flatpak remotes with a few clicks or add custom remotes instead
* Take snapshots of your apps' user data, saving your data
* Install new packages from any remote, or from your system
* Responsive UI to fit large and small screen sizes

[Source](https://raw.githubusercontent.com/flattool/warehouse/main/data/io.github.flattool.Warehouse.metainfo.xml.in)