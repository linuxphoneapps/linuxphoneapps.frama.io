+++
title = "Dukto"
description = "A simple multi-platform file transfer application especially designed for LAN users and which supports sending text, files or folders. It supports Windows, OS X, Linux, iOS, Android and more."
aliases = []
date = 2019-03-04
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "ttys3",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "System", "FileTools", "P2P",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/ttys3/dukto"
homepage = "https://www.msec.it/blog/dukto/"
bugtracker = "https://github.com/ttys3/dukto/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://web.archive.org/web/20180822053309/httpss://play.google.com/store/apps/details?id=it.msec.dukto"
screenshots = [ "http://web.archive.org/web/20180822053309/https://play.google.com/store/apps/details?id=it.msec.dukto",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dukto",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_github_api"
latest_repo_commit = "2020-06-02"
repo_created_date = "2019-11-19"

+++

### Notice

No longer maintained, used to have n900 variant at http://talk.maemo.org/showpost.php?p=900004&postcount=44. To be moved to archive.