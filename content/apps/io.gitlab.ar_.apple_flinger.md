+++
title = "Apple Flinger"
description = "Funny single- and multiplayer game for Android - Use a slingshot to shoot with apples"
aliases = [ "games/com.gitlab.ardash.appleflinger/",]
date = 2019-02-01
updated = 2025-02-21

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andreas Redmer",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "ArcadeGame", "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/ar-/apple-flinger"
homepage = ""
bugtracker = "https://gitlab.com/ar-/apple-flinger/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/ar-/apple-flinger/-/raw/master/desktop/io.gitlab.ar_.apple_flinger.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.gitlab.ar_.apple_flinger"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/ar-/apple-flinger/-/raw/master/desktop/io.gitlab.ar_.apple_flinger.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/games/com.gitlab.ardash.appleflinger/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2017-09-12"

+++


### Description

Features:
- high resolution quality graphics
- realistic physics
- lag-free and smooth gameplay
- fine detailed animations and particle systems
- brand new and innovative game concept
- single/multiplayer
- 100% open source (GPL3)
You use a slingshot to shoot with apples. Be first to destroy the whole enemy base, but be aware, the other side shoots back. This game has a balanced mix of puzzle, strategy, patience and action.
This is for one or two players! You can play this against anyone who sits next to you - or against yourself. You can also play against a computer controlled opponent. More features, more fun and more levels will come soon.

[Source](https://gitlab.com/ar-/apple-flinger/-/raw/master/desktop/io.gitlab.ar_.apple_flinger.metainfo.xml)
