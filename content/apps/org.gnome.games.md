+++
title = "Games"
description = "Simple game launcher for GNOME"
aliases = []
date = 2019-02-16
updated = 2024-10-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "game launcher",]
mobile_compatibility = [ "5",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "devuan_4_0", "flathub", "manjaro_stable", "manjaro_unstable", "openmandriva_5_0", "pureos_byzantium",]
freedesktop_categories = [ "GTK", "GNOME", "Game", "Emulator",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/Archive/gnome-games"
homepage = "https://wiki.gnome.org/Apps/Games"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-games/issues"
donations = "http://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://gitlab.gnome.org/Archive/gnome-games"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-gamepad-mapping.png", "https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-games-collection.png", "https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-platforms.png", "https://gitlab.gnome.org/GNOME/gnome-games/raw/master/data/appdata/3-36-retro-snapshots.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Games"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Games"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-games",]
appstream_xml_url = "https://gitlab.gnome.org/Archive/gnome-games/-/raw/master/data/org.gnome.Games.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_repology"
latest_repo_commit = "2021-06-23"
repo_created_date = "2017-11-24"

+++

### Description

Games is a GNOME 3 application to browse your video games library and to
 easily pick and play a game from it. It aims to do for games what Music
 already does for your music library.


You want to install Games if you just want a very simple and comfortable
 way to play your games and you don’t need advanced features such as
 speedrunning tools or video game development tools.


Features:


* List your installed games, your Steam games, your game ROMs…
* Search in your games collection
* Play your games
* Resume your game to where you left it

[Source](https://gitlab.gnome.org/Archive/gnome-games/-/raw/master/data/org.gnome.Games.appdata.xml.in.in)

### Notice

Repo is archived, project seems to continue at https://gitlab.gnome.org/World/highscore.