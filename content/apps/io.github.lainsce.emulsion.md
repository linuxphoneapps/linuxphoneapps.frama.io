+++
title = "Emulsion"
description = "Stock up on colors"
aliases = []
date = 2021-05-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lainsce/emulsion"
homepage = "https://github.com/lainsce/emulsion/"
bugtracker = "https://github.com/lainsce/emulsion/issues"
donations = "https://www.ko-fi.com/lainsce/"
translations = "https://github.com/lainsce/emulsion/blob/master/po/README.md"
more_information = []
summary_source_url = "https://github.com/lainsce/emulsion/"
screenshots = [ "https://raw.githubusercontent.com/lainsce/emulsion/master/data/shot.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/lainsce/emulsion/refs/heads/main/data/icons/io.github.lainsce.Emulsion.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lainsce.Emulsion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Emulsion"
flatpak_link = "https://flathub.org/apps/io.github.lainsce.Emulsion.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/lainsce/emulsion/refs/heads/main/io.github.lainsce.Emulsion.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "emulsion",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/emulsion/main/data/io.github.lainsce.Emulsion.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-04-22"
repo_created_date = "2021-04-29"

+++


### Description

Store your palettes in an easy way, and edit them if needed.

[Source](https://raw.githubusercontent.com/lainsce/emulsion/main/data/io.github.lainsce.Emulsion.metainfo.xml.in)