+++
title = "Timer"
description = "Simple countdown timer"
aliases = []
date = 2025-01-20
updated = 2025-01-20

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "vikdevelop",]
categories = [ "timer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vikdevelop/timer"
homepage = "https://github.com/vikdevelop/timer"
bugtracker = "https://github.com/vikdevelop/timer/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/vikdevelop/"
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.vikdevelop.timer"
screenshots = [ "https://raw.githubusercontent.com/vikdevelop/timer/main/img/timer_keyboard-shortcuts.png", "https://raw.githubusercontent.com/vikdevelop/timer/main/img/timer_main-window.png", "https://raw.githubusercontent.com/vikdevelop/timer/main/img/timer_more-settings.png", "https://raw.githubusercontent.com/vikdevelop/timer/main/img/timer_notification-settings.png", "https://raw.githubusercontent.com/vikdevelop/timer/main/img/timer_timing-page.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = "https://github.com/vikdevelop/timer/blob/main/flatpak/appicon/com.github.vikdevelop.timer.png?raw=true"
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.vikdevelop.timer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.vikdevelop.timer"
flatpak_link = "https://flathub.org/apps/com.github.vikdevelop.timer.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/vikdevelop/timer/refs/heads/main/com.github.vikdevelop.timer.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/vikdevelop/timer/refs/heads/main/flatpak/com.github.vikdevelop.timer.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.github.vikdevelop.timer/"
latest_repo_commit = "2025-01-02"
repo_created_date = "2021-06-13"

+++

### Description

Timer is an open-source and simple application. It enables countdown timing in hours/minutes/seconds and:

* After finished timing possible shut down/reboot/suspend system, play beep and send notification
* It is possible allow/disable playing beep
* Set custom text for notification and alarm clock dialog
* Use keyboard shortcuts for start, pause or stop timer and reset timer values

[Source](https://raw.githubusercontent.com/vikdevelop/timer/refs/heads/main/flatpak/com.github.vikdevelop.timer.metainfo.xml)