+++
title = "Barcode Scanner"
description = "Scan and create QR-Codes"
aliases = []
date = 2019-09-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "qr code",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami",]
backends = [ "gstreamer", "zxing-cpp",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "fedora_40", "fedora_41", "fedora_rawhide",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/qrca"
homepage = "https://apps.kde.org/en/qrca"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=qrca"
donations = "https://www.kde.org/community/donations/?app=qrca&source=appdata"
translations = ""
more_information = [ "https://phabricator.kde.org/T8906",]
summary_source_url = "https://invent.kde.org/utilities/qrca/-/raw/master/org.kde.qrca.appdata.xml"
screenshots = []
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/qrca/-/raw/master/org.kde.qrca.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.qrca"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/qrca/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "qrca",]
appstream_xml_url = "https://invent.kde.org/utilities/qrca/-/raw/master/org.kde.qrca.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2019-06-10"

+++

### Description

Scan QR-Codes with your camera on phones and laptops, and create your own for easily sharing data between devices.

[Source](https://invent.kde.org/utilities/qrca/-/raw/master/org.kde.qrca.appdata.xml)