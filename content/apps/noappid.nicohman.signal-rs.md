+++
title = "signal-rs"
description = "A Rust-based signal app with a QML/Kirigami frontend."
aliases = []
date = 2021-11-18
updated = 2024-10-28

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "signal-rs",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "Kirigami",]
backends = [ "presage",]
services = [ "Signal",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust", "QML",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~nicohman/signal-rs"
latest_repo_commit = "2021-12-12"
homepage = "https://sr.ht/~nicohman/signal-rs/"
bugtracker = "https://todo.sr.ht/~nicohman/signal-rs"
donations = ""
translations = ""
more_information = [ "https://teddit.net/r/PINE64official/comments/qthw58/looking_for_some_basic_feedback_on_native_signal/",]
summary_source_url = "https://sr.ht/~nicohman/signal-rs/"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
repo_created_date = "2020-09-10"

+++

### Description

A Rust-based signal app with a QML/Kirigami frontend. Uses presage as a backend. Better name pending. Many features also still pending. This is definitely a beta version, but I'd love feedback/feature requests. [Source](https://sr.ht/~nicohman/signal-rs/)

### Notice

WIP app which at some point worked - but Signal likely changed something, so it does not work anymore. Last commit in December 2021.
