+++
title = "Picker"
description = "Randomly pick something to do"
aliases = []
date = 2024-08-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "MezoAhmedII",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Amusement", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mezoahmedii/picker.git"
homepage = "https://github.com/mezoahmedii/picker"
bugtracker = "https://github.com/mezoahmedii/picker/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/mezoahmedii/picker/main/data/io.github.mezoahmedii.Picker.metainfo.xml.in"
screenshots = [ "https://github.com/mezoahmedii/picker/blob/v1.3.0/screenshots/x86_64/adding.png?raw=true", "https://github.com/mezoahmedii/picker/blob/v1.3.0/screenshots/x86_64/choosing.png?raw=true",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/mezoahmedii/picker/main/data/icons/hicolor/scalable/apps/io.github.mezoahmedii.Picker.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.mezoahmedii.Picker"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.mezoahmedii.Picker"
flatpak_link = "https://flathub.org/apps/io.github.mezoahmedii.Picker.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/mezoahmedii/picker/main/io.github.mezoahmedii.Picker.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mezoahmedii/picker/main/data/io.github.mezoahmedii.Picker.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.mezoahmedii.picker/"
latest_repo_commit = "2025-01-01"
repo_created_date = "2024-07-23"

+++

### Description

Picker is a simple app that can pick something for you from a list of things.
For example, if you can't decide what to have for dinner, put your favorite meals in and let
the Picker choose one for you.

[Source](https://raw.githubusercontent.com/mezoahmedii/picker/main/data/io.github.mezoahmedii.Picker.metainfo.xml.in)