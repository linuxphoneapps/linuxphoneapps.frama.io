+++
title = "Exercise Timer"
description = "Train and rest with high intensity"
aliases = []
date = 2023-12-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "safe worlds",]
categories = [ "fitness", "health",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust", "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mfep/exercise-timer"
homepage = "https://github.com/mfep/exercise-timer"
bugtracker = "https://github.com/mfep/exercise-timer/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/exercise-timer/exercise-timer/"
more_information = []
summary_source_url = "https://flathub.org/apps/xyz.safeworlds.hiit"
screenshots = [ "https://media.githubusercontent.com/media/mfep/exercise-timer/main/data/screenshots/dark_exercise_list.png", "https://media.githubusercontent.com/media/mfep/exercise-timer/main/data/screenshots/dark_timer.png", "https://media.githubusercontent.com/media/mfep/exercise-timer/main/data/screenshots/light_new_training.png", "https://media.githubusercontent.com/media/mfep/exercise-timer/main/data/screenshots/light_timer.png",]
screenshots_img = []
svg_icon_url = "https://github.com/mfep/exercise-timer/raw/main/data/icons/xyz.safeworlds.hiit.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "xyz.safeworlds.hiit"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.safeworlds.hiit"
flatpak_link = "https://flathub.org/apps/xyz.safeworlds.hiit.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/mfep/exercise-timer/main/build-aux/xyz.safeworlds.hiit.Devel.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mfep/exercise-timer/main/data/xyz.safeworlds.hiit.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2023-09-26"

+++

### Description

Exercise timer is a simple utility to conduct interval training.

Following a short preparation period, a prescribed number of exercise sets are played. In between each exercise, there is a resting period.

The app does not make an assumption about the kind of the exercise.

Features:

* Save and recall presets containing the number of sets and the duration of the exercise, rest and preparation periods.
* A beeping sound is played at- and prior to each transition.
* The volume of the sound can be adjusted.
* Light and dark mode follows the system's setting.

[Source](https://raw.githubusercontent.com/mfep/exercise-timer/main/data/xyz.safeworlds.hiit.metainfo.xml.in.in)