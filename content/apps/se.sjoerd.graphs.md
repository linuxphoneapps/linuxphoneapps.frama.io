+++
title = "Graphs"
description = "Plot and manipulate data"
aliases = []
date = 2024-05-05
updated = 2025-02-18

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sjoerd Stendahl et al.",]
categories = [ "education",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Education", "Science",]
programming_languages = [ "Python", "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/Graphs"
homepage = "https://apps.gnome.org/Graphs/"
bugtracker = "https://gitlab.gnome.org/World/Graphs/issues"
donations = ""
translations = "https://l10n.gnome.org/module/Graphs/"
more_information = [ "https://apps.gnome.org/Graphs/",]
summary_source_url = "https://flathub.org/apps/se.sjoerd.Graphs"
screenshots = [ "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/screenshots/add_equation.png", "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/screenshots/curve_fitting.png", "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/screenshots/previews.png", "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/screenshots/sin_cos.png", "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/screenshots/sin_cos_dark.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/icons/hicolor/scalable/apps/se.sjoerd.Graphs.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "se.sjoerd.Graphs"
scale_to_fit = ""
flathub = "https://flathub.org/apps/se.sjoerd.Graphs"
flatpak_link = "https://flathub.org/apps/se.sjoerd.Graphs.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/Graphs/-/raw/main/se.sjoerd.Graphs.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-graphs",]
appstream_xml_url = "https://gitlab.gnome.org/World/Graphs/-/raw/main/data/se.sjoerd.Graphs.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-01-18"

+++

### Description

Graphs is a simple, yet powerful tool that allows you to plot and manipulate your data with ease. New data can be imported from a wide variety of filetypes, or generated by equation.
All data can be manipulated using a variety of operations such as the ability to select, cut, translate, multiply, center and smoothen data, as well as apply any custom transformations on the data.

Moreover, Graphs supports curve fitting on imported data and offers extensive customization options for the style of the plots. You can fine-tune and personalize stylesheets to your liking.

Graphs is an excellent fit for both plotting and data manipulation. The plots created with Graphs can be saved in a variety of formats suitable for sharing and presenting to a wide audience, such as in a scientific publication or presentations.
Additionally, the option to save plots as vector images allows for easy editing in software like Inkscape for further refinement. Graphs is written with the GNOME environment in mind, but should be suitable for any other desktop environment as well.

For any feedback, questions or general issues, please file an issue on the GitLab page.

[Source](https://gitlab.gnome.org/World/Graphs/-/raw/main/data/se.sjoerd.Graphs.appdata.xml.in)

### Notice

The main window and the dialog to add new formulas are a tad too wide with release 1.8.4, otherwise the app is great on mobile.
