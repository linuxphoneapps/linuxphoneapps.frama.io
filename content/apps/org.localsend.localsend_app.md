+++
title = "LocalSend"
description = "Share files to nearby devices"
aliases = []
date = 2024-02-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Tien Do Nam",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Flutter",]
backends = []
services = [ "LocalSend",]
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Dart",]
build_systems = [ "cmake", "flutter",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/localsend/localsend"
homepage = "https://localsend.org/"
bugtracker = "https://github.com/localsend/localsend/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.localsend.localsend_app"
screenshots = [ "https://raw.githubusercontent.com/flathub/org.localsend.localsend_app/refs/heads/master/Media/Receive.png", "https://raw.githubusercontent.com/flathub/org.localsend.localsend_app/refs/heads/master/Media/send.png", "https://raw.githubusercontent.com/flathub/org.localsend.localsend_app/refs/heads/master/Media/settings.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.localsend.localsend_app"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.localsend.localsend_app"
flatpak_link = "https://flathub.org/apps/org.localsend.localsend_app.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "localsend",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.localsend.localsend_app/master/org.localsend.localsend_app.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2022-12-16"

+++

### Description

This app allows you to send files and messages over the local LAN network.

In contrast to most alternatives, no external servers are needed.
Everything happens locally in the wifi network.

[Source](https://raw.githubusercontent.com/flathub/org.localsend.localsend_app/master/org.localsend.localsend_app.metainfo.xml)