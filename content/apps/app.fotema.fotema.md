+++
title = "Fotema"
description = "Admire your photos"
aliases = []
date = 2024-05-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "David Bliss",]
categories = [ "photo management", "image viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GNOME", "GTK", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/blissd/fotema"
homepage = "https://github.com/blissd/fotema"
bugtracker = "https://github.com/blissd/fotema/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/fotema/app"
more_information = []
summary_source_url = "https://flathub.org/apps/app.fotema.Fotema"
screenshots = [ "https://github.com/blissd/fotema/blob/v1.10.0/data/resources/screenshots/all-photos.png?raw=true", "https://github.com/blissd/fotema/blob/v1.10.0/data/resources/screenshots/folders-view.png?raw=true", "https://github.com/blissd/fotema/blob/v1.10.0/data/resources/screenshots/people-album.png?raw=true", "https://github.com/blissd/fotema/blob/v1.10.0/data/resources/screenshots/person-select.png?raw=true", "https://github.com/blissd/fotema/blob/v1.10.0/data/resources/screenshots/places-view.png?raw=true", "https://github.com/blissd/fotema/blob/v1.19.0/data/resources/screenshots/person-thumbnails.png?raw=true",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/blissd/fotema/main/data/icons/app.fotema.Fotema.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "app.fotema.Fotema"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.fotema.Fotema"
flatpak_link = "https://flathub.org/apps/app.fotema.Fotema.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/blissd/fotema/main/build-aux/app.fotema.Fotema.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/blissd/fotema/main/data/app.fotema.Fotema.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-03-14"

+++

### Description

A photo gallery for everyone who wants their photos to live locally on their devices.

Why enjoy your photo library with Fotema?

* View iOS Live Photos and Android motion photos (Samsung devices only).
* View photos plotted on a map.
* Automatically detects faces.
* View your library by year or month.
* Play videos.
* Many supported image formats. Fotema supports the same image formats as Loupe (the GNOME image viewer).

[Source](https://raw.githubusercontent.com/blissd/fotema/main/data/app.fotema.Fotema.metainfo.xml.in.in)

### Notice

Mobile friendly since release 1.5.0.