+++
title = "Wifi2QR"
description = "Display QR codes to easily connect to any Wifi for which a NetworkManager connection profile exists."
aliases = []
date = 2021-12-18
updated = 2024-10-28

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "wisperwind",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK4",]
backends = [ "networkmanager", "qrencode",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "manual status maintenance", "no icon",]

[extra]
repository = "https://git.sr.ht/~wisperwind/wifi2qr"
homepage = ""
bugtracker = "https://todo.sr.ht/~wisperwind/wifi2qr"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~wisperwind/wifi2qr"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = "com.example.apps.wifi2qr"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
latest_repo_commit = "2021-02-13"
repo_created_date = "2020-10-22"

+++

### Notice
WIP Wifi QR-Code generator (works fine despite not being designed for mobile - no desktop file).
