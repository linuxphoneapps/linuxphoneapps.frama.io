+++
title = "Osmin"
description = "Navigator"
aliases = [ "apps/noappid.janbar.osmin/",]
date = 2022-01-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jean-Luc Barriere",]
categories = [ "maps and navigation",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = [ "libosmscout",]
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur",]
freedesktop_categories = [ "Qt", "Utility", "Maps",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/janbar/osmin"
homepage = "https://github.com/janbar/osmin/wiki"
bugtracker = "https://github.com/janbar/osmin/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/janbar/osmin/master/screenshots/informations.png", "https://raw.githubusercontent.com/janbar/osmin/master/screenshots/routing.png", "https://raw.githubusercontent.com/janbar/osmin/master/screenshots/tracking.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.janbar.osmin"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "osmin",]
appstream_xml_url = "https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in"
reported_by = "Karry"
updated_by = "check_via_repology"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.janbar.osmin/"
latest_repo_commit = "2024-11-07"
repo_created_date = "2020-02-09"

+++

### Description

Navigator based on OSM maps

[Source](https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in)
