+++
title = "Stockpile"
description = "Keep count of restockable items"
aliases = []
date = 2023-11-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sunniva Løvstad",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://codeberg.org/turtle/stockpile"
homepage = "https://codeberg.org/turtle/stockpile"
bugtracker = "https://codeberg.org/turtle/stockpile/issues"
donations = ""
translations = "https://translate.codeberg.org/engage/jellybean/"
more_information = []
summary_source_url = "https://codeberg.org/turtle/stockpile/raw/branch/main/data/garden.turtle.Jellybean.metainfo.xml.in"
screenshots = [ "https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/edit-view.png", "https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/item-view.png", "https://codeberg.org/turtle/stockpile/raw/branch/main/screenshots/main-view.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = true
app_id = "garden.turtle.Jellybean"
scale_to_fit = ""
flathub = "https://flathub.org/apps/garden.turtle.Jellybean"
flatpak_link = "https://flathub.org/apps/garden.turtle.Jellybean.flatpakref"
flatpak_recipe = "https://codeberg.org/turtle/jellybean/raw/branch/main/garden.turtle.Jellybean.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "jellybean",]
appstream_xml_url = "https://codeberg.org/turtle/stockpile/raw/branch/main/data/garden.turtle.Jellybean.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-03"
repo_created_date = "2023-10-25"

+++

### Description

Never forget to refill again! Stockpile lets you manage your
inventory of medication, food, beauty products, or anything else.
The app will warn you whenever you are short on something,
so you can obtain more and not have to go empty.

[Source](https://codeberg.org/turtle/stockpile/raw/branch/main/data/garden.turtle.Jellybean.metainfo.xml.in)