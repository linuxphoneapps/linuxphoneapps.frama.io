+++
title = "Spacebar"
description = "SMS/MMS messaging client"
aliases = []
date = 2019-09-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "SMS", "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = [ "ofono", "ModemManager",]
services = [ "SMS", "MMS",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/plasma-mobile/spacebar"
homepage = "https://apps.kde.org/"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=spacebar"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/project/profile/66/", "https://phabricator.kde.org/T6936", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#spacebar",]
summary_source_url = "https://invent.kde.org/plasma-mobile/spacebar/-/raw/master/org.kde.spacebar.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.spacebar"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "spacebar",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/spacebar/-/raw/master/org.kde.spacebar.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2019-11-26"

+++

### Description

Spacebar is a SMS/MMS messaging client. It allows you to send text messages, pictures and other files over a cellular network.

[Source](https://invent.kde.org/plasma-mobile/spacebar/-/raw/master/org.kde.spacebar.appdata.xml)