+++
title = "PowerPlant"
description = "Keep your plants alive"
aliases = []
date = 2024-07-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://invent.kde.org/utilities/powerplant"
homepage = "https://apps.kde.org/powerplant"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=powerplant"
donations = "https://kde.org/community/donations/?app=powerplant"
translations = ""
more_information = [ "https://discuss.kde.org/t/little-plant-care-app/790",]
summary_source_url = "https://invent.kde.org/utilities/powerplant/-/raw/master/org.kde.powerplant.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/powerplant/powerplant-detail.png", "https://cdn.kde.org/screenshots/powerplant/powerplant-task.png", "https://cdn.kde.org/screenshots/powerplant/powerplant.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/powerplant/-/raw/master/org.kde.powerplant.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.powerplant"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/powerplant/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/powerplant/-/raw/master/org.kde.powerplant.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-04-10"

+++

### Description

PowerPlant lets you track when your plants have to be watered and how their health develops over time.

[Source](https://invent.kde.org/utilities/powerplant/-/raw/master/org.kde.powerplant.metainfo.xml)
