+++
title = "Spectral"
description = "IM client for the Matrix protocol"
aliases = []
date = 2020-03-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Black Hat",]
categories = [ "chat", "matrix"]
mobile_compatibility = [ "3",]
status = [ "released", "inactive",]
frameworks = [ "QtQuick",]
backends = [ "libQuotient",]
services = [ "Matrix",]
packaged_in = [ "aur", "debian_11", "debian_12", "devuan_4_0", "flathub", "pureos_byzantium",]
freedesktop_categories = [ "Network", "Chat",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = [ "requires always"]
tags = []

[extra]
repository = "https://gitlab.com/spectral-im/spectral"
homepage = "https://gitlab.com/spectral-im/spectral/"
bugtracker = "https://gitlab.com/spectral-im/spectral/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml"
screenshots = [ "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/1.png", "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/2.png", "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/3.png", "https://gitlab.com/spectral-im/spectral/raw/master/screenshots/4.png",]
screenshots_img = []
non_svg_icon_url = "https://gitlab.com/spectral-im/spectral/-/raw/master/icons/hicolor/128-apps-org.eu.encom.spectral.png"
all_features_touch = false
intended_for_mobile = false
app_id = "org.eu.encom.spectral"
scale_to_fit = "org.eu.encom.spectral"
flathub = "https://flathub.org/apps/org.eu.encom.spectral"
flatpak_link = "https://flathub.org/apps/org.eu.encom.spectral.flatpakref"
flatpak_recipe = "https://gitlab.com/spectral-im/spectral/-/raw/master/flatpak/org.eu.encom.spectral.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "spectral-matrix",]
appstream_xml_url = "https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
latest_repo_commit = "2022-08-28"
repo_created_date = "2018-02-23"

+++


### Description

Spectral is a glossy, cross-platform client for Matrix, the decentralized communication protocol for instant messaging.

[Source](https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml)

### Notice

Last commit in January 2021.
