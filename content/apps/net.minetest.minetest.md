+++
title = "Luanti"
description = "Block-based multiplayer game platform"
aliases = ["games/net.minetest.minetest/"]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "Apache-2.0", "CC-BY-SA-3.0", "LGPL-2.1-or-later", "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Luanti Team",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = []
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Game", "Simulation",]
programming_languages = []
build_systems = []
requires_internet = [ "supports offline-only",]
tags = []

[extra]
repository = "https://github.com/minetest/minetest"
homepage = "https://www.minetest.net"
bugtracker = "https://www.minetest.net/get-involved/#reporting-issues"
donations = "https://www.minetest.net/get-involved/#donate"
translations = "https://dev.minetest.net/Translation"
more_information = [ "https://ixit.cz/blog/2023-12-21-Minetest-Linux-touch-pt1",]
summary_source_url = "https://flathub.org/apps/net.minetest.Minetest"
screenshots = [ "https://www.minetest.net/media/gallery/1.jpg", "https://www.minetest.net/media/gallery/3.jpg", "https://www.minetest.net/media/gallery/5.jpg",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/minetest/minetest/master/misc/minetest.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "net.minetest.minetest"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.minetest.Minetest"
flatpak_link = "https://flathub.org/apps/net.minetest.Minetest.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "luanti",]
appstream_xml_url = "https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_repology"
latest_repo_commit = "2025-01-13"
repo_created_date = "2011-08-07"
feed_entry_id = "https://linuxphoneapps.org/games/net.minetest.minetest/"

+++

### Description

Luanti is a block-based sandbox game platform.

Players can create and destroy various types of blocks in a
three-dimensional open world. This allows forming structures in
every possible creation, on multiplayer servers or in singleplayer.

Luanti is designed to be simple, stable, and portable.
It is lightweight enough to run on fairly old hardware.

Luanti has many features, including:

* Ability to walk around, dig, and build in a near-infinite voxel world
* Crafting of items from raw materials
* A simple modding API that supports many additions and modifications to the game
* Multiplayer support via servers hosted by users
* Beautiful lightning-fast map generator

[Source](https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.metainfo.xml)
