+++
title = "Calligraphy"
description = "Turn text into ASCII banners"
aliases = []
date = 2024-11-10
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos \"GeopJr\" Paterakis",]
categories = [ "ASCII art generator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "TextTools", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/GeopJr/Calligraphy"
homepage = "https://calligraphy.geopjr.dev/"
bugtracker = "https://gitlab.gnome.org/GeopJr/Calligraphy/-/issues"
donations = "https://geopjr.dev/donate"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/dev.geopjr.Calligraphy"
screenshots = [ "https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/data/screenshots/detailed-view.png", "https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/data/screenshots/entry.png", "https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/data/screenshots/search.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/data/icons/hicolor/scalable/apps/dev.geopjr.Calligraphy.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "dev.geopjr.Calligraphy"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.geopjr.Calligraphy"
flatpak_link = "https://flathub.org/apps/dev.geopjr.Calligraphy.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/build-aux/dev.geopjr.Calligraphy.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "calligraphy",]
appstream_xml_url = "https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/data/dev.geopjr.Calligraphy.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/dev.geopjr.calligraphy/"
latest_repo_commit = "2024-12-12"
repo_created_date = "2024-04-30"

+++

### Description

Calligraphy turns short texts into large, impressive banners made up of ASCII Characters, ready to be copied or exported to images.
Spice up your online conversations by adding some extra oompf to your messages!

[Source](https://gitlab.gnome.org/GeopJr/Calligraphy/-/raw/main/data/dev.geopjr.Calligraphy.metainfo.xml.in)