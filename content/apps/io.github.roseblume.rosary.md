+++
title = "Rosary"
description = "Study Christianity"
aliases = []
date = 2024-10-11
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "James A. Rose",]
categories = [ "bible",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Tauri",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Education",]
programming_languages = [ "HTML",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/RoseBlume/Rosary"
homepage = "https://github.com/RoseBlume/Rosary"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.roseblume.rosary"
screenshots = [ "https://raw.githubusercontent.com/RoseBlume/Rosary/side/main.png", "https://raw.githubusercontent.com/RoseBlume/Rosary/side/menu.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/flathub/io.github.roseblume.rosary/refs/heads/master/Flatpak.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.roseblume.rosary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.roseblume.rosary"
flatpak_link = "https://flathub.org/apps/io.github.roseblume.rosary.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/io.github.roseblume.rosary/refs/heads/master/io.github.roseblume.rosary.yaml"
snapcraft = "https://snapcraft.io/jrosarybibleapp"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "rosary-tauri",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/io.github.roseblume.rosary/refs/heads/master/io.github.roseblume.rosary.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.roseblume.rosary/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2024-04-15"

+++

### Description

Read the Bible and other Christian texts. This projects goal is to allow each person to study the bible and come to their own conclusion about their faith.

[Source](https://raw.githubusercontent.com/flathub/io.github.roseblume.rosary/refs/heads/master/io.github.roseblume.rosary.metainfo.xml)