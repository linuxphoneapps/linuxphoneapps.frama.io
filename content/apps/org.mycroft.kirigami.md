+++
title = "Mycroft"
description = "Mycroft Kirigami Application for Plasma Mobile"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "aiix",]
categories = [ "voice assistant",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/AIIX/mycroft-kirigami-application"
homepage = ""
bugtracker = "https://github.com/AIIX/mycroft-kirigami-application/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/AIIX/mycroft-kirigami-application"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.mycroft.kirigami"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-mycroft", "mycroft-gui",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_repology"
latest_repo_commit = "2018-09-26"
repo_created_date = "2017-10-30"

+++

### Notice

Last commit September 2018.