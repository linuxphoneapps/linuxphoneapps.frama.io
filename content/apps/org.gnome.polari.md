+++
title = "Polari"
description = "Talk to people on IRC"
aliases = []
date = 2022-03-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Florian Müllner",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "IRC",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "IRCClient", "Network",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/polari/"
homepage = "https://apps.gnome.org/Polari"
bugtracker = "https://gitlab.gnome.org/GNOME/polari/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/polari/"
more_information = [ "https://apps.gnome.org/Polari/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/polari/-/raw/main/data/appdata/org.gnome.Polari.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/polari/raw/HEAD/data/metainfo/polari-connection-properties.png", "https://gitlab.gnome.org/GNOME/polari/raw/HEAD/data/metainfo/polari-connection.png", "https://gitlab.gnome.org/GNOME/polari/raw/HEAD/data/metainfo/polari-join-dialog.png", "https://gitlab.gnome.org/GNOME/polari/raw/HEAD/data/metainfo/polari-main.png", "https://gitlab.gnome.org/GNOME/polari/raw/HEAD/data/metainfo/polari-user-list.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Polari"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Polari"
flatpak_link = "https://flathub.org/apps/org.gnome.Polari.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/polari/-/raw/main/flatpak/org.gnome.Polari.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "polari",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/polari/-/raw/main/data/metainfo/org.gnome.Polari.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-02"
repo_created_date = "2017-09-18"

+++

### Description

A simple Internet Relay Chat (IRC) client that is designed to integrate
seamlessly with GNOME; it features a simple and beautiful interface which
allows you to focus on your conversations.

You can use Polari to publicly chat with people in a channel, and to
have private one-to-one conversations. Notifications make sure that
you never miss an important message.

[Source](https://gitlab.gnome.org/GNOME/polari/-/raw/main/data/metainfo/org.gnome.Polari.metainfo.xml.in)

### Notice

Works on mobile since release 42. In release 45 it works pretty well - the channel overview does not fit the screen yet.