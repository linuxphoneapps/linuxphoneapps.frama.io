+++
title = "CRUMPET"
description = "App to control our new breed of animatroinic tails!"
aliases = []
date = 2019-03-19
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "mastertailer",]
categories = [ "smart home",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/OpenTails/CRUMPET-Android"
homepage = ""
bugtracker = "https://github.com/OpenTails/CRUMPET-Android/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/OpenTails/CRUMPET-Android"
screenshots = [ "https://play.google.com/store/apps/details?id=org.thetailcompany.digitail",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/OpenTails/CRUMPET-Android/refs/heads/master/digitail.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.thetailcompany.digitail"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-11-13"
repo_created_date = "2018-10-27"

+++