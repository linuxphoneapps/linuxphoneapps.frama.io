+++
title = "Moonlander"
description = "Just another 'fancy' Gemini client."
aliases = []
date = 2021-05-13
updated = 2024-12-01

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "admicos",]
categories = [ "gemini browser",]
mobile_compatibility = [ "4",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~admicos/moonlander"
latest_repo_commit = "2021-05-23"
homepage = ""
bugtracker = "https://todo.sr.ht/~admicos/moonlander"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://sr.ht/~admicos/moonlander/"
screenshots = [ "https://sr.ht/~admicos/moonlander/",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~admicos/moonlander/blob/main/misc/moonlander.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = "moonlander"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "moonlander",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_repology"
repo_created_date = "2021-03-09"

+++

### Description

Just another "fancy" Gemini client.

Status: Alpha, Maintenance mode

Moonlander is currently in maintenance mode. It works and I still use it as a daily driver, but I probably won't add any new features, bug fixes, or anything else for some time. I will most likely visit it later on, but just not today.

[Source](https://git.sr.ht/~admicos/moonlander)