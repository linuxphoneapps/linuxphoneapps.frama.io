+++
title = "Max Control"
description = "Control software for Max! devices"
aliases = [ "apps/org.tabos.maxcontrol.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "smart home",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/tabos/maxcontrol"
homepage = "https://tabos.org/projects/maxcontrol/"
bugtracker = "https://gitlab.com/tabos/maxcontrol/-/issues"
donations = ""
translations = "https://gitlab.com/tabos/maxcontrol/-/tree/master/po"
more_information = []
summary_source_url = "https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in"
screenshots = [ "https://tabos.org/projects/maxcontrol/maxcontrol_hu67063bbc633d1ebedefc66ed58c40b3a_18153_315x0_resize_box_3.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.tabos.maxcontrol"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.maxcontrol"
flatpak_link = "https://flathub.org/apps/org.tabos.maxcontrol.flatpakref"
flatpak_recipe = "https://gitlab.com/tabos/maxcontrol/-/raw/master/org.tabos.maxcontrol.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "max-control",]
appstream_xml_url = "https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/org.tabos.maxcontrol/"
latest_repo_commit = "2024-06-16"
repo_created_date = "2019-01-04"

+++


### Description

Max Control is a user interface to control and administrate Max! devices, e.g. Max!Cube, Heating device, ...

Add/Remove device, set temperature levels and set modus for rooms and complete house!

[Source](https://gitlab.com/tabos/maxcontrol/-/raw/master/data/org.tabos.maxcontrol.appdata.xml.in)

### Notice

Proper testing requires a Max!Cube device, help needed.