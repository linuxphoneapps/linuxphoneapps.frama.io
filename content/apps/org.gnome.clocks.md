+++
title = "Clocks"
description = "Keep track of time"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Clock", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-clocks"
homepage = "https://apps.gnome.org/Clocks/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-clocks/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-clocks/"
more_information = [ "https://apps.gnome.org/app/org.gnome.clocks/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-clocks/-/raw/master/data/org.gnome.clocks.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/alarm.png", "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/stopwatch.png", "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/timer.png", "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/world.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.clocks/1.png", "https://img.linuxphoneapps.org/org.gnome.clocks/2.png", "https://img.linuxphoneapps.org/org.gnome.clocks/3.png", "https://img.linuxphoneapps.org/org.gnome.clocks/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.clocks"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.clocks"
flatpak_link = "https://flathub.org/apps/org.gnome.clocks.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-clocks",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-clocks/-/raw/master/data/org.gnome.clocks.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2018-05-23"

+++

### Description

A simple and elegant clock application.
It includes world clocks, alarms, a stopwatch, and timers.

* Show the time in different cities around the world
* Set alarms to wake you up
* Measure elapsed time with an accurate stopwatch
* Set timers to properly cook your food

[Source](https://gitlab.gnome.org/GNOME/gnome-clocks/-/raw/master/data/org.gnome.clocks.metainfo.xml.in.in)

### Notice

Was GTK3/libhandy before 42. Note: If your device uses suspend to prolong battery life, timers, stopwatch and alarms will stop working whenever the device is suspended.