+++
title = "qalendar"
description = "Qalendar is Qt5 implementation of the Maemo Calendar"
aliases = []
date = 2020-10-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "maemo-leste",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Office", "Calendar",]
programming_languages = [ "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://github.com/maemo-leste/qalendar"
homepage = "https://wiki.maemo.org/Qalendar"
bugtracker = "https://github.com/maemo-leste/qalendar/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://leste.maemo.org/Calendar"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-03"
repo_created_date = "2020-09-18"

+++

### Notice

Maemo Leste has its own app store and apps are not very portable, thus candidate for removal