+++
title = "H-Craft"
description = "H-Craft Championship is a fun to play scifi-racer."
aliases = ["games/noappid.mzeilfelder.hc1/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "z-lib and others", "graphics and fonts are not free licensed!",]
metadata_licenses = []
app_author = ["mzeilfelder",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "SDL",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ActionGame"]
programming_languages = [ "C", "Cpp",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/mzeilfelder/hc1"
homepage = "http://www.irrgheist.com/games.htm"
bugtracker = "https://github.com/mzeilfelder/hc1/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "http://www.irrgheist.com/games.htm"
screenshots = [ "http://www.irrgheist.com/hcraftscreenshots.htm", "https://play.google.com/store/apps/details?id=com.irrgheist.hcraft_championship",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.mzeilfelder.hc1/"
latest_repo_commit = "2023-12-05"
repo_created_date = "2020-06-16"

+++

### Description


H-Craft Championship is a fun to play scifi-racer.
It features 28+ racetracks, a fresh design and unique driving physics.
It comes with a challenging Championship, an arcade and 2 timeattack modes.
Also on "Rivals" up to 4 players can have an exciting tournament on a single PC.

[Source](http://www.irrgheist.com/games.htm)
