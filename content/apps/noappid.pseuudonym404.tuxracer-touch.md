+++
title = "TuxRacer-Touch"
description = "Ubuntu touch tux racer port"
aliases = ["games/noappid.pseuudonym404.tuxracer-touch/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = ["pseuudonym404",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/pseuudonym404/tuxracer-touch"
homepage = ""
bugtracker = "https://github.com/pseuudonym404/tuxracer-touch/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/pseuudonym404/tuxracer-touch"
screenshots = [ "https://uappexplorer.com/app/tuxracer.lb",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.pseuudonym404.tuxracer-touch/"
latest_repo_commit = "2016-11-18"
repo_created_date = "2015-08-06"

+++
