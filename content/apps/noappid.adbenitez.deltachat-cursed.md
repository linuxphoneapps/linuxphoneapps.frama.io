+++
title = "Cursed Delta"
description = "A lightweight Delta Chat client for the command line"
aliases = [ "delta",]
date = 2024-10-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "adbenitez",]
categories = [ "chat",]
mobile_compatibility = [ "cli",]
status = [ "released",]
frameworks = [ "ncurses", "urwid",]
backends = [ "libdeltachat",]
services = [ "deltachat",]
packaged_in = [ "aur", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Chat", "InstantMessaging",]
programming_languages = [ "Python",]
build_systems = [ "pyproject",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/adbenitez/deltachat-cursed"
homepage = "https://github.com/adbenitez/deltachat-cursed"
bugtracker = "https://github.com/adbenitez/deltachat-cursed/issues/"
donations = ""
translations = ""
more_information = [ "https://github.com/adbenitez/deltachat-cursed/blob/main/docs/user-guide.md",]
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/adbenitez/deltachat-cursed/refs/heads/main/screenshots/e1.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "deltachat-cursed",]
appstream_xml_url = ""
reported_by = "magdesign"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.adbenitez.deltachat-cursed/"
latest_repo_commit = "2024-12-15"
repo_created_date = "2020-04-16"

+++

### Description

Cursed Delta is a ncurses Delta Chat client developed in Python with the urwid library.

### Notice

Works on mobile screen better when launching with a fontsize:8.

To link as second device import a database export from your android with `delta import /link/to/database.db`
So far only usable if you have also installed a deltachat client on another system to add contacts!
Installable with `pip install -U deltachat-cursed`.
