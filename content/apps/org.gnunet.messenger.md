+++
title = "GNUnet Messenger"
description = "Private and secure communication"
aliases = []
date = 2021-12-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNUnet e.V.",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "libgnunetchat",]
services = [ "GNUnet Messenger",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Chat", "InstantMessaging", "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "manual status maintenance",]

[extra]
repository = "https://git.gnunet.org/messenger-gtk.git"
homepage = "https://www.gnunet.org"
bugtracker = "https://bugs.gnunet.org/my_view_page.php"
donations = "https://www.gnunet.org/en/ev.html"
translations = ""
more_information = [ "https://git.gnunet.org/messenger-gtk.git/tree/README.md", "https://www.reddit.com/r/pinephone/comments/rk5zyb/gnunet_messenger_for_mobile_linux/", "https://thejackimonster.blogspot.com/2022/02/gnunet-messenger-api-february.html", "https://thejackimonster.blogspot.com/2022/03/gnunet-messenger-api-march.html", "https://thejackimonster.blogspot.com/2022/09/gnunet-messenger-api-september.html", "https://thejackimonster.blogspot.com/2022/12/development-in-2022.html",]
summary_source_url = "https://git.gnunet.org/messenger-gtk.git/plain/resources/org.gnunet.Messenger.appdata.xml"
screenshots = [ "https://gitlab.com/gnunet-messenger/messenger-gtk/-/raw/master/screenshots/messenger.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.gnunet.Messenger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnunet.Messenger"
flatpak_link = "https://flathub.org/apps/org.gnunet.Messenger.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = "https://git.gnunet.org/messenger-gtk.git/plain/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "messenger-gtk",]
appstream_xml_url = "https://git.gnunet.org/messenger-gtk.git/plain/resources/org.gnunet.Messenger.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-10-24"
repo_created_date = "2021-06-06"

+++


### Description

Messenger-GTK is a convergent GTK messaging application using the GNUnet Messenger service. The goal is to provide private and secure communication between any group of devices.

The application provides the following features:

* Creating direct chats and group chats
* Managing your contacts and groups
* Invite contacts to a group
* Sending text messages
* Sending voice recordings
* Sharing files privately
* Deleting messages with any custom delay
* Renaming contacts
* Exchanging contact details physically
* Verifying contact identities
* Switching between different accounts
* Chatting live via video or voice streaming
* Sharing your screen with a selected group

Chats will generally created as opt-in. So you can decide who may contact you directly and who does not, accepting to a direct chat. Leaving a chat is also always possible.

[Source](https://git.gnunet.org/messenger-gtk.git/plain/resources/org.gnunet.Messenger.appdata.xml)