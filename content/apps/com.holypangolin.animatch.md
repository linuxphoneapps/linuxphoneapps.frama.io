+++
title = "Animatch"
description = "A cute match-3 game"
aliases = ["games/com.holypangolin.animatch/"]
date = 2024-04-12
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Holy Pangolin",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = [ "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/HolyPangolin/animatch"
homepage = "https://holypangolin.com"
bugtracker = "https://gitlab.com/HolyPangolin/animatch/-/issues"
donations = "https://ko-fi.com/holypangolin"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/HolyPangolin/animatch/-/raw/master/data/com.holypangolin.Animatch.appdata.xml"
screenshots = [ "https://dl.flathub.org/media/com/holypangolin/Animatch/3bb72d5e86b08ba804faf995d57d286d/screenshots/image-1_orig.png", "https://dl.flathub.org/media/com/holypangolin/Animatch/3bb72d5e86b08ba804faf995d57d286d/screenshots/image-2_orig.png", "https://dl.flathub.org/media/com/holypangolin/Animatch/3bb72d5e86b08ba804faf995d57d286d/screenshots/image-3_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "com.holypangolin.Animatch"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.holypangolin.Animatch"
flatpak_link = "https://flathub.org/apps/com.holypangolin.Animatch.flatpakref"
flatpak_recipe = "https://gitlab.com/HolyPangolin/animatch/-/raw/master/flatpak/com.holypangolin.Animatch.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "animatch",]
appstream_xml_url = "https://gitlab.com/HolyPangolin/animatch/-/raw/master/data/com.holypangolin.Animatch.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-09-26"
repo_created_date = "2018-10-27"

+++

### Description

Animatch is a match-three game with cute animals.

[Source](https://gitlab.com/HolyPangolin/animatch/-/raw/master/data/com.holypangolin.Animatch.appdata.xml)
