+++
title = "Contrast"
description = "Check contrast between two colors"
aliases = []
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/design/contrast"
homepage = "https://gitlab.gnome.org/World/design/contrast"
bugtracker = "https://gitlab.gnome.org/World/design/contrast/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/contrast/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/design/contrast/-/raw/master/data/org.gnome.design.Contrast.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/design/contrast/raw/master/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.design.Contrast"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.design.Contrast"
flatpak_link = "https://flathub.org/apps/org.gnome.design.Contrast.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "contrast",]
appstream_xml_url = "https://gitlab.gnome.org/World/design/contrast/-/raw/master/data/org.gnome.design.Contrast.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-15"
repo_created_date = "2019-07-24"

+++


### Description

Contrast checks whether the contrast between two colors meet the WCAG requirements.

[Source](https://gitlab.gnome.org/World/design/contrast/-/raw/master/data/org.gnome.design.Contrast.metainfo.xml.in.in)