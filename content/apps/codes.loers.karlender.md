+++
title = "Events"
description = "Manage your schedule"
aliases = []
date = 2022-03-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Florian Loers",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "minicaldav",]
services = [ "CalDAV",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Calendar", "GTK", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "Unsupported custom Appstream Metadata format",]

[extra]
repository = "https://gitlab.com/floers/calendar-stuff/karlender"
homepage = "https://gitlab.com/floers/calendar-stuff/karlender"
bugtracker = "https://gitlab.com/floers/calendar-stuff/karlender/-/issues"
donations = ""
translations = ""
more_information = [ "https://gitlab.com/floers/calendar-stuff/karlender/-/blob/main/ROADMAP.md",]
summary_source_url = "https://flathub.org/apps/codes.loers.Karlender"
screenshots = [ "https://dl.flathub.org/media/c/co/codes.loers.karlender/5a5bb6bcd2a86e794948adf3c0fae1f1/screenshots/image-1_orig.png", "https://dl.flathub.org/media/c/co/codes.loers.karlender/5a5bb6bcd2a86e794948adf3c0fae1f1/screenshots/image-2_orig.png", "https://dl.flathub.org/media/c/co/codes.loers.karlender/5a5bb6bcd2a86e794948adf3c0fae1f1/screenshots/image-3_orig.png", "https://dl.flathub.org/media/c/co/codes.loers.karlender/5a5bb6bcd2a86e794948adf3c0fae1f1/screenshots/image-4_orig.png", "https://dl.flathub.org/media/c/co/codes.loers.karlender/5a5bb6bcd2a86e794948adf3c0fae1f1/screenshots/image-5_orig.png", "https://dl.flathub.org/media/c/co/codes.loers.karlender/5a5bb6bcd2a86e794948adf3c0fae1f1/screenshots/image-6_orig.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/floers/calendar-stuff/karlender/-/raw/main/assets/icon.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "codes.loers.Karlender"
scale_to_fit = ""
flathub = "https://flathub.org/apps/codes.loers.Karlender"
flatpak_link = "https://flathub.org/apps/codes.loers.Karlender.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "karlender",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-11-11"
repo_created_date = "2022-01-10"

+++

### Description

Events is an adaptive calendar app for GNOME and Phosh.

With Events you can

*   CalDAV syncing
*   Local calendars
*   Monthly, Weekly, Daily and Yearly event management
*   GNOME Online Accounts integration
*   Mobile friendly

[Source](https://gitlab.com/floers/calendar-stuff/karlender/-/raw/main/App.toml)
