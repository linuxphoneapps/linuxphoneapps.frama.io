+++
title = "Kolorfill"
description = "Color filling game"
aliases = ["games/org.kde.kolorfill/"]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sune Vuorela",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "fedora_40", "fedora_41", "fedora_rawhide",]
freedesktop_categories = [ "KDE", "Qt", "Game", "LogicGame",]
programming_languages = [ "QML", "Cpp", "JavaScript",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://invent.kde.org/games/kolorfill"
homepage = "https://apps.kde.org/kolorfill/"
bugtracker = "https://invent.kde.org/games/kolorfill/-/issues/"
donations = ""
translations = ""
more_information = [ "https://pusling.com/blog/?p=505",]
summary_source_url = "https://invent.kde.org/games/kolorfill/-/raw/master/src/org.kde.kolorfill.appdata.xml"
screenshots = [ "https://i.imgur.com/iUCpI9I.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kolorfill"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kolorfill",]
appstream_xml_url = "https://invent.kde.org/games/kolorfill/-/raw/master/src/org.kde.kolorfill.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-02"
repo_created_date = "2020-05-16"
feed_entry_id = "https://linuxphoneapps.org/games/org.kde.kolorfill/"

+++

### Description

Kolorfill is a сolor filling game.

[Source](https://invent.kde.org/games/kolorfill/-/raw/master/src/org.kde.kolorfill.appdata.xml)
