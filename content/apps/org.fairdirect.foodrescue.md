+++
title = "Food Rescue App"
description = "Convergent mobile+desktop application to help decide if food is still edible."
aliases = []
date = 2020-08-29
updated = 2024-10-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "Matthias Ansorg <matthias@ansorgs.de>",]
categories = [ "food rescue",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "QML", "Cpp", "XSLT",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://github.com/fairdirect/foodrescue-app"
homepage = ""
bugtracker = "https://github.com/fairdirect/foodrescue-app/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/fairdirect/foodrescue-app"
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/fairdirect/foodrescue-app/master/src/images/applogo-1_orig.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.fairdirect.foodrescue"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/fairdirect/foodrescue-app/master/metadata-kde.xml"
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2020-11-05"
repo_created_date = "2020-06-02"

+++

### Description

Open source application to help assess if food is still edible. When you scan a food product’s barcode or search for a food category, the application will collect and show whatever it knows about assessing the edibility of this food item. The main innovation is to combine barcode scanning with food rescue information, which makes that information more accessible than through any existing solution.

[Source](https://github.com/fairdirect/foodrescue-app)

### Notice

Latest commit in November 2020.