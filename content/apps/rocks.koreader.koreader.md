+++
title = "KOReader"
description = "Ebook reader"
aliases = []
date = 2023-11-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KOReader Community",]
categories = [ "document viewer", "pdf viewer", "feed reader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "SDL",]
backends = [ "mupdf",]
services = [ "RSS",]
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Literature", "Office", "Viewer",]
programming_languages = [ "Lua",]
build_systems = [ "cmake",]
requires_internet = [ "recommends always",]
tags = []

[extra]
repository = "https://github.com/koreader/koreader"
homepage = "https://koreader.rocks/"
bugtracker = "https://github.com/koreader/koreader/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/koreader/"
more_information = [ "https://github.com/koreader/koreader/wiki",]
summary_source_url = "https://flathub.org/apps/rocks.koreader.KOReader"
screenshots = [ "https://github.com/koreader/koreader-artwork/raw/master/koreader-dictionary-framed.png", "https://github.com/koreader/koreader-artwork/raw/master/koreader-footnotes-framed.png", "https://github.com/koreader/koreader-artwork/raw/master/koreader-menu-framed.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/koreader/koreader/master/resources/koreader.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "rocks.koreader.KOReader"
scale_to_fit = ""
flathub = "https://flathub.org/apps/rocks.koreader.KOReader"
flatpak_link = "https://flathub.org/apps/rocks.koreader.KOReader.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "koreader",]
appstream_xml_url = "https://raw.githubusercontent.com/koreader/koreader/master/platform/common/koreader.metainfo.xml"
reported_by = "colinsane"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2013-03-24"

+++

### Description

KOReader is an ebook reader optimized for e-ink screens. It can open many formats and provides advanced text adjustments.

See below for a selection of its many features:

* Supports both fixed page formats (PDF, DjVu, CBT, CBZ) and reflowable e-book formats (EPUB, FB2, Mobi, DOC, CHM, TXT, HTML). Scanned PDF/DjVu documents can be reflowed. Special flow directions for reading double column PDFs and manga.
* Multi-lingual user interface optimized for e-ink screens. Highly customizable reader view with complete typesetting options. Multi-lingual hyphenation dictionaries are bundled in.
* Non-Latin script support for books, including the Hebrew, Arabic, Persian, Russian, Chinese, Japanese and Korean languages.
* Unique Book Map and Page Browser features to navigate your book.
* Special multi-page highlight mode with many local and online export options.
* Can synchronize your reading progress across all your KOReader running devices.
* Integrated with Calibre, Wallabag, Wikipedia, Google Translate and other content providers.

[Source](https://raw.githubusercontent.com/koreader/koreader/master/platform/common/koreader.metainfo.xml)