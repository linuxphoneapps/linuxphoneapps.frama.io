+++
title = "Paper Plane"
description = "Chat over Telegram on a modern and elegant client"
aliases = [ "apps/com.github.melix99.telegrand/",]
date = 2021-04-18
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Marco Melorio",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Telegram",]
services = [ "Telegram",]
packaged_in = [ "aur", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/paper-plane-developers/paper-plane"
homepage = "https://github.com/paper-plane-developers/paper-plane"
bugtracker = "https://github.com/paper-plane-developers/paper-plane/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/paper-plane"
more_information = []
summary_source_url = "https://github.com/paper-plane-developers/paper-plane"
screenshots = [ "https://github.com/paper-plane-developers/paper-plane/raw/main/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.PaperPlane"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "paper-plane",]
appstream_xml_url = "https://raw.githubusercontent.com/paper-plane-developers/paper-plane/main/data/app.drey.PaperPlane.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-08-18"
repo_created_date = "2021-02-07"

+++

### Description

Paper Plane is an alternative Telegram client.
It uses libadwaita for its user interface and strives to meet the design principles of the GNOME desktop.

Paper Plane is still under development and not yet feature-complete.
However, the following things are already working:

* The use of multiple accounts at the same time.
* Viewing text messages, images, stickers and files.
* Sending text messages and images.
* Replying to messages.
* Searching for groups and persons.

[Source](https://raw.githubusercontent.com/paper-plane-developers/paper-plane/main/data/app.drey.PaperPlane.metainfo.xml.in.in)

### Notice

WIP, in early beta, read the [readme's API key section](https://github.com/paper-plane-developers/paper-plane#telegram-api-credentials) regarding the risks of getting your account banned.