+++
title = "Klimbgrades"
description = "Quickly convert difficulty grades for rock climbing, lead and bouldering scales"
aliases = [ "apps/org.kde.klimbgrades.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "sports",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp", "JavaScript",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://invent.kde.org/utilities/klimbgrades"
homepage = "https://apps.kde.org/klimbgrades"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=klimbgrades"
donations = ""
translations = ""
more_information = [ "https://notmart.org/blog/2017/03/climbing-grades-a-kirigami-example-app/",]
summary_source_url = "https://invent.kde.org/utilities/klimbgrades/-/raw/master/org.kde.klimbgrades.appdata.xml"
screenshots = [ "https://lh3.googleusercontent.com/AptCINVC6I09l_PNO3t1x6YuQOA6JnSkxhbwlPAqjIQfvhqiZAyznUByVU0ZbPJdELw=h310-rw",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/klimbgrades/-/raw/master/src/klimbgrades.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.klimbgrades"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/klimbgrades/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/klimbgrades/-/raw/master/org.kde.klimbgrades.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.klimbgrades/"
latest_repo_commit = "2024-12-26"
repo_created_date = "2020-05-16"

+++

### Description

* Lead: French, YDS, UIAA, British Tech, British Adjectival
* Boulder: Fontainebleu, Hueco, B Grade

Example application to demonstrate KDE's Kirigami Qt-based framework for building cross-platform applications.

[Source](https://invent.kde.org/utilities/klimbgrades/-/raw/master/org.kde.klimbgrades.appdata.xml)