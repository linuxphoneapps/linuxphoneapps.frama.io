+++
title = "QField"
description = "A simplified touch optimized interface for QGIS"
aliases = [ "apps/qfield/",]
date = 2019-03-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "GPL-2.0-only",]
app_author = [ "opengisch",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Geography", "Maps",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://github.com/opengisch/QField"
homepage = "https://qfield.org/"
bugtracker = "https://github.com/opengisch/QField/issues"
donations = "https://github.com/opengisch"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/opengisch/QField/master/platform/linux/qfield.metainfo.xml"
screenshots = [ "https://qfield.org/images/title_tablet.jpg",]
screenshots_img = []
svg_icon_url = "https://github.com/opengisch/QField/raw/refs/heads/master/images/icons/qfield_logo.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.qfield.QField"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/opengisch/QField/master/platform/linux/qfield.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2014-11-26"

+++

### Description

QField focuses on efficiently getting GIS fieldwork done and exchanging data between the field and the office in a comfortable and user friendly way.
Built on top of the popular QGIS open source project, QField lets users consume fully configured projects in the field allowing for customized feature forms, map themes, print layouts, and more, bringing the power of QGIS at your fingertips.

[Source](https://raw.githubusercontent.com/opengisch/QField/master/platform/linux/qfield.metainfo.xml)