+++
title = "Upscaler"
description = "Upscale and enhance images"
aliases = []
date = 2023-11-18
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "The Upscaler Contributors",]
categories = [ "image processing",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Real-ESRGAN ncnn Vulkan",]
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Graphics", "ImageProcessing",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Upscaler"
homepage = "https://tesk.page/upscaler"
bugtracker = "https://gitlab.gnome.org/World/Upscaler/-/issues"
donations = "https://dir.floss.fund/view/funding/@tesk.page"
translations = "https://gitlab.gnome.org/World/Upscaler/po"
more_information = [ "https://matrix.to/#/#Upscaler:gnome.org", "https://gitlab.gnome.org/World/Upscaler/-/blob/main/PRESS.md",]
summary_source_url = "https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/screenshots/1.png", "https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/screenshots/2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.theevilskeleton.Upscaler"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.theevilskeleton.Upscaler"
flatpak_link = "https://flathub.org/apps/io.gitlab.theevilskeleton.Upscaler.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/Upscaler/-/raw/main/build-aux/io.gitlab.theevilskeleton.Upscaler.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "upscaler",]
appstream_xml_url = "https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-31"
repo_created_date = "2023-05-22"

+++

### Description

Upscaler enhances and enlarges images up to four times their original size, with special modes for cartoons, anime and photos.

[Source](https://gitlab.gnome.org/World/Upscaler/-/raw/main/data/io.gitlab.theevilskeleton.Upscaler.metainfo.xml.in.in)