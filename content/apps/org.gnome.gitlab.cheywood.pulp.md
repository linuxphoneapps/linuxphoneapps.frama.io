+++
title = "Pulp"
description = "Skim excessive feeds"
aliases = []
date = 2024-08-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Heywood",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "FreshRSS",]
services = [ "FreshRSS",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Feed", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/cheywood/Pulp"
homepage = "https://gitlab.gnome.org/cheywood/Pulp"
bugtracker = "https://gitlab.gnome.org/cheywood/Pulp/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.gitlab.cheywood.Pulp"
screenshots = [ "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/screenshots/desktop_article_list.png", "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/screenshots/desktop_article_list_with_sidebar.png", "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/screenshots/desktop_single_article.png", "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/screenshots/mobile.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.gitlab.cheywood.Pulp.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.gitlab.cheywood.Pulp"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gitlab.cheywood.Pulp"
flatpak_link = "https://flathub.org/apps/org.gnome.gitlab.cheywood.Pulp.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/build-aux/flatpak/org.gnome.gitlab.cheywood.Pulp.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/org.gnome.gitlab.cheywood.Pulp.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.gnome.gitlab.cheywood.pulp/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-07-24"

+++

### Description

Note: Today Pulp is in preview as a client for FreshRSS and Nextcloud News. Direct RSS access is planned for the future.

Pulp provides a workflow focused on reading through an excessive number of RSS feeds with the goal of regularly marking all read, resting in a state akin to an empty inbox.

Contributing to quickly parsing excessive articles:

* Translators: Part of metainfo description
* A large "mash roughly here" button to mark list read and continue
* Marking read while scrolling
* Ensuring full article titles and longer excerpts are visible in the article list
* Minimising everything else, in both UI clutter and functionality

The goal here isn't to be a typical RSS reader, please see the README (via the website link) for more about what Pulp is trying to do and what's possible with this preview. This preview will have some bugs, reporting those (via the Report an Issue link) is greatly appreciated 🙏

[Source](https://gitlab.gnome.org/cheywood/Pulp/-/raw/main/data/org.gnome.gitlab.cheywood.Pulp.metainfo.xml.in.in)