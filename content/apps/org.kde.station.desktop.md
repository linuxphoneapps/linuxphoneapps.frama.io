+++
title = "Station"
description = "Terminal Emulator"
aliases = [ "apps/org.kde.station/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Qt", "System", "TerminalEmulator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/maui/station"
homepage = "https://apps.kde.org/station"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=station"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/station/-/raw/master/org.kde.station.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/station/station-stable.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/maui-station/-/raw/master/src/assets/station.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.station.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-station", "station-terminal-emulator",]
appstream_xml_url = "https://invent.kde.org/maui/maui-station/-/raw/master/org.kde.station.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.station/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2019-04-21"

+++

### Description

Station is a convergent terminal emulator, ready for desktop and mobile devices using GNU/Linux.

[Source](https://invent.kde.org/maui/station/-/raw/master/org.kde.station.appdata.xml)