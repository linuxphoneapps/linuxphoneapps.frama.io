+++
title = "Quick Weather"
description = "Quick Weather QML Qt application"
aliases = []
date = 2022-04-12
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "bit-shift-io",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "QtQuick",]
backends = [ "bom.gov.au",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/bit-shift-io/qweather"
homepage = ""
bugtracker = "https://github.com/bit-shift-io/qweather/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/bit-shift-io/qweather"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "qweather",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2021-02-26"
repo_created_date = "2019-11-20"

+++

### Description

Quick Weather QML Qt application for desktop and mobile. Using BOM weather for Australia. [Source](https://github.com/bit-shift-io/qweather)

### Notice

For Australia only. Last commit 2021-02-26.