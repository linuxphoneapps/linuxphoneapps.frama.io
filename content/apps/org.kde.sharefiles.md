+++
title = "Nextcloud-Plasma-Mobile-login"
description = "A test project to create webview login for nextcloud accounts on Plasma Mobile."
aliases = []
date = 2020-03-02
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "cloud syncing",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Nextcloud",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
requires_internet = []
build_systems = [ "cmake",]
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile"
homepage = ""
bugtracker = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/issues/"
donations = ""
translations = ""
more_information = [ "https://orepoala.home.blog/2019/06/11/nextcloud-login-plugin-for-plamo/", "https://community.kde.org/GSoC/2019/StatusReports/ORePoala", "https://summerofcode.withgoogle.com/archive/2019/projects/6343810290810880/",]
summary_source_url = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile"
screenshots = [ "https://orepoala.home.blog/2019/06/11/nextcloud-login-plugin-for-plamo/",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.sharefiles"
scale_to_fit = "org.kde.sharefiles"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/rpatwal/nextcloud-plasma-mobile/-/raw/master/org.kde.sharefiles.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_aarch64_url = ""
appimage_x86_64_url = ""
appstream_xml_url = ""
repology = []
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.sharefiles/"
latest_repo_commit = "2022-11-21"
repo_created_date = "2019-05-22"

+++

### Notice

Last commit in July 2019.
