+++
title = "Audio Player"
description = "Play audio files"
aliases = [ "apps/com.vixalien.decibels/",]
date = 2023-11-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "audio player", "music player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "GStreamer",]
services = []
packaged_in = [ "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Audio", "AudioVideo", "Music",]
programming_languages = [ "TypeScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/Incubator/decibels/"
homepage = "https://apps.gnome.org/Decibels/"
bugtracker = "https://gitlab.gnome.org/GNOME/Incubator/decibels/-/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/decibels/"
more_information = [ "https://apps.gnome.org/Decibels/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/decibels/-/raw/main/data/org.gnome.Decibels.metainfo.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-48/decibels/playing-dark-x2.png", "https://static.gnome.org/appdata/gnome-48/decibels/playing-light-x2.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/Incubator/decibels/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Decibels.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Decibels"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Decibels"
flatpak_link = "https://flathub.org/apps/org.gnome.Decibels.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/Incubator/decibels/-/raw/main/build-aux/flatpak/org.gnome.Decibels.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "decibels",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/decibels/-/raw/main/data/org.gnome.Decibels.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.vixalien.decibels/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-03-11"

+++

### Description

An audio player that just plays audio files. It doesn't require an organized music library and won't overload you with tons of functionality.

Audio Player still offers advanced features such as:

* An elegant waveform of the track
* Adjustable playback speed
* Easy seek controls
* Playing multiple files at the same time

[Source](https://gitlab.gnome.org/GNOME/decibels/-/raw/main/data/org.gnome.Decibels.metainfo.xml.in.in)