+++
title = "MyGNUHealth"
description = "GNU Health Personal Health Record"
aliases = [ "apps/org.gnuhealth.my/",]
date = 2020-10-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNU Solidario",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kivy",]
backends = []
services = [ "GNU Health",]
packaged_in = [ "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "opensuse_tumbleweed",]
freedesktop_categories = [ "Office", "Science",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/gnuhealth/mygnuhealth/"
homepage = "https://www.gnuhealth.org"
bugtracker = "https://codeberg.org/gnuhealth/mygnuhealth/issues"
donations = "https://www.gnuhealth.org/donate/"
translations = ""
more_information = [ "https://www.gnuhealth.org/docs/mygnuhealth/",]
summary_source_url = "https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/raw-file/c6b4e9bd3c69/mygnuhealth/org.gnuhealth.mygnuhealth.metainfo.xml"
screenshots = [ "https://docs.gnuhealth.org/mygnuhealth/images/book_of_life_list.png", "https://docs.gnuhealth.org/mygnuhealth/images/mygnuhealth.png", "https://docs.gnuhealth.org/mygnuhealth/images/mygnuhealth_wide_bio.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.gnuhealth.mygnuhealth"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnuhealth.mygnuhealth"
flatpak_link = "https://flathub.org/apps/org.gnuhealth.mygnuhealth.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mygnuhealth",]
appstream_xml_url = "https://codeberg.org/gnuhealth/mygnuhealth/raw/branch/main/org.gnuhealth.mygnuhealth.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2024-03-14"

+++

### Description

Personal Health Record - PHR.

[Source](https://codeberg.org/gnuhealth/mygnuhealth/raw/branch/dev-2.2/org.gnuhealth.mygnuhealth.metainfo.xml)

### Notice

Can be installed using pip: pip3 install --user --upgrade MyGNUHealth.

MyGNUHealth 1.0 was implemented in Python with Kirigami; MyGNUHealth 2.0 is implemented in Python with a UI in Kivy.
The software is being rewritten in Go with Fyne for the UI for MyGNUHealth 3.