+++
title = "Diurnals"
description = "Get daily Todoist notifications"
aliases = []
date = 2024-10-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Brandon C",]
categories = [ "productivity", "task management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Todoist",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Audio", "AudioVideo",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/SSS-Says-Snek/diurnals"
homepage = "https://github.com/SSS-Says-Snek/diurnals"
bugtracker = "https://github.com/SSS-Says-Snek/diurnals/issues"
donations = ""
translations = "https://github.com/SSS-Says-Snek/diurnals/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.sss_says_snek.diurnals"
screenshots = [ "https://raw.githubusercontent.com/SSS-Says-Snek/diurnals/f02921501aad349da870164c8cbc9b7dbc82ad5d/screenshots/tasks-dark.png", "https://raw.githubusercontent.com/SSS-Says-Snek/diurnals/f02921501aad349da870164c8cbc9b7dbc82ad5d/screenshots/tasks-light.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/SSS-Says-Snek/diurnals/refs/heads/main/data/io.github.sss_says_snek.diurnals.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.sss_says_snek.diurnals"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.sss_says_snek.diurnals"
flatpak_link = "https://flathub.org/apps/io.github.sss_says_snek.diurnals.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/SSS-Says-Snek/diurnals/refs/heads/main/flatpak/io.github.sss_says_snek.diurnals.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "diurnals",]
appstream_xml_url = "https://raw.githubusercontent.com/SSS-Says-Snek/diurnals/refs/heads/main/data/io.github.sss_says_snek.diurnals.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.sss_says_snek.diurnals/"
latest_repo_commit = "2024-10-17"
repo_created_date = "2024-08-05"

+++


### Description

Not keeping up with upcoming and overdue tasks? Better track your tasks on Todoist with a popup that appears regularly.

Features:

* Customizable popup times
* Apply custom filters for tasks
* Manage your tasks and your time more efficiently

Note: Diurnals is not affiliated with or created by Doist

[Source](https://raw.githubusercontent.com/SSS-Says-Snek/diurnals/refs/heads/main/data/io.github.sss_says_snek.diurnals.metainfo.xml.in)