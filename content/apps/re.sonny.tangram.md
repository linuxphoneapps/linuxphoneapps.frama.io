+++
title = "Tangram"
description = "Browser for your pinned tabs"
aliases = []
date = 2021-06-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sonny Piers",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "webkit2gtk",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://tangram.sonny.re/source"
homepage = "https://tangram.sonny.re"
bugtracker = "https://tangram.sonny.re/feedback"
donations = "https://tangram.sonny.re/donate"
translations = "https://hosted.weblate.org/engage/tangram/"
more_information = [ "https://apps.gnome.org/Tangram/",]
summary_source_url = "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/desktop.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/re.sonny.tangram/1.png", "https://img.linuxphoneapps.org/re.sonny.tangram/2.png", "https://img.linuxphoneapps.org/re.sonny.tangram/3.png", "https://img.linuxphoneapps.org/re.sonny.tangram/4.png", "https://img.linuxphoneapps.org/re.sonny.tangram/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "re.sonny.Tangram"
scale_to_fit = "re.sonny.Tangram"
flathub = "https://flathub.org/apps/re.sonny.Tangram"
flatpak_link = "https://flathub.org/apps/re.sonny.Tangram.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/sonnyp/Tangram/main/re.sonny.Tangram.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tangram",]
appstream_xml_url = "https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-09-02"
repo_created_date = "2019-07-03"

+++

### Description

Tangram is a new kind of browser. It is designed to organize and run your Web applications.
Each tab is persistent and independent. You can set multiple tabs with different accounts for the same application.

Common use cases:

* Stay up to date with your favorite communities; Mastodon, Twitter, ...
* Merge all these chat applications into one; WhatsApp, Messenger, Telegram, ...
* Group your organization tools under one application; EMail, Calendar, ...
* One-stop for multiple sources of documentation or information

[Source](https://raw.githubusercontent.com/sonnyp/Tangram/main/data/appdata/re.sonny.Tangram.metainfo.xml)

### Notice

Perfect on phones since release 3.0. Used to be GTK3 before release 2.0.