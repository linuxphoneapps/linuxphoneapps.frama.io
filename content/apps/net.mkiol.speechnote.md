+++
title = "Speech Note"
description = "Notes with offline Speech to Text, Text to Speech and Machine Translation"
aliases = []
date = 2023-06-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michal Kosciesza",]
categories = [ "note taking", "text to speech", "speech to text",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick", "Silica",]
backends = [ "Coqui STT", "Vosk", "whisper.cpp", "espeak-ng", "MBROLA", "Piper", "RHVoice", "Coqui TTS",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "AudioVideo",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mkiol/dsnote"
homepage = "https://github.com/mkiol/dsnote"
bugtracker = "https://github.com/mkiol/dsnote/issues"
donations = "https://github.com/mkiol/dsnote#how-to-support"
translations = "https://app.transifex.com/mkiol/dsnote"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml.in"
screenshots = [ "https://gitlab.com/mkiol/dsnote/-/raw/5d8a190c182886f34a1a729bdbf09fd02acaf4e9/desktop/screenshots/speechnote-screenshot-dark-notepad.png", "https://gitlab.com/mkiol/dsnote/-/raw/5d8a190c182886f34a1a729bdbf09fd02acaf4e9/desktop/screenshots/speechnote-screenshot-dark-translator.png", "https://gitlab.com/mkiol/dsnote/-/raw/5d8a190c182886f34a1a729bdbf09fd02acaf4e9/desktop/screenshots/speechnote-screenshot-light-notepad.png", "https://gitlab.com/mkiol/dsnote/-/raw/5d8a190c182886f34a1a729bdbf09fd02acaf4e9/desktop/screenshots/speechnote-screenshot-light-translator.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/net.mkiol.speechnote/1.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/2.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/3.png", "https://img.linuxphoneapps.org/net.mkiol.speechnote/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "net.mkiol.SpeechNote"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.mkiol.SpeechNote"
flatpak_link = "https://flathub.org/apps/net.mkiol.SpeechNote.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2021-10-07"

+++

### Description

Speech Note let you take, read and translate notes in multiple languages.
It uses Speech to Text, Text to Speech and Machine Translation to do so.
Text and voice processing take place entirely offline, locally on your
computer, without using a network connection. Your privacy is always
respected. No data is sent to the Internet.

[Source](https://raw.githubusercontent.com/mkiol/dsnote/main/desktop/dsnote.metainfo.xml.in)