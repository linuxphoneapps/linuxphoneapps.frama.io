+++
title = "vgmms"
description = "vgtk-based SMS+MMS client"
aliases = []
date = 2020-09-12
updated = 2024-10-26

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "anteater",]
categories = [ "chat",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "oFono", "ModemManager", "mmsd-tng", "mmsd",]
services = [ "MMS",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://git.sr.ht/~anteater/vgmms"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~anteater/vgmms"
screenshots = [ "https://user-images.githubusercontent.com/65555601/111051950-cb6a7800-844e-11eb-8d90-56d6bcdb98e0.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.vgmms"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_sourcehut"
latest_repo_commit = "2024-03-05"

+++