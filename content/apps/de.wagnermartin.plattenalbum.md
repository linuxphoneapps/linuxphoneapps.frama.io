+++
title = "Plattenalbum"
description = "Connect to your music"
aliases = []
date = 2024-09-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martin Wagner",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "python3-mpd2",]
services = [ "mpd", "Music Player Daemon",]
packaged_in = [ "alpine_3_21", "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/SoongNoonien/plattenalbum"
homepage = "https://github.com/SoongNoonien/plattenalbum"
bugtracker = "https://github.com/SoongNoonien/plattenalbum/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/de.wagnermartin.Plattenalbum"
screenshots = [ "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/v2.2.0/screenshots/album_view.png", "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/v2.2.0/screenshots/main_window.png", "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/v2.2.0/screenshots/search.png", "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/v2.2.0/screenshots/small.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/refs/heads/master/data/de.wagnermartin.Plattenalbum.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = true
app_id = "de.wagnermartin.Plattenalbum"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.wagnermartin.Plattenalbum"
flatpak_link = "https://flathub.org/apps/de.wagnermartin.Plattenalbum.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/refs/heads/master/de.wagnermartin.Plattenalbum.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plattenalbum",]
appstream_xml_url = "https://raw.githubusercontent.com/SoongNoonien/plattenalbum/refs/heads/master/data/de.wagnermartin.Plattenalbum.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/de.wagnermartin.plattenalbum/"
latest_repo_commit = "2024-12-22"
repo_created_date = "2020-01-11"

+++

### Description

A client for the Music Player Daemon (MPD).

Browse your collection while viewing large album covers. Play your music without managing playlists.

[Source](https://raw.githubusercontent.com/SoongNoonien/plattenalbum/refs/heads/master/data/de.wagnermartin.Plattenalbum.metainfo.xml.in)

### Notice

Mobile-friendly since release 2.2.0.