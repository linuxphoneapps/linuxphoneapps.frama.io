+++
title = "Magarena"
description = "Single-player fantasy card game played against a computer opponent."
aliases = ["games/noappid.magarena.magarena/"]
date = 2021-03-14
updated = 2025-01-24

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = ["magarena",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "released",]
frameworks = []
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = ["Game", "CardGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/magarena/magarena"
homepage = "https://magarena.github.io/"
bugtracker = "https://github.com/magarena/magarena/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/magarena/magarena"
screenshots = [ "https://raw.githubusercontent.com/wiki/magarena/magarena/screenshots/maincombat.jpg", "https://raw.githubusercontent.com/wiki/magarena/magarena/screenshots/duel-decks.jpg", "https://raw.githubusercontent.com/wiki/magarena/magarena/screenshots/deck-editor-screen.jpg", "https://raw.githubusercontent.com/wiki/magarena/magarena/screenshots/tiled-deck-cards.jpg", "https://raw.githubusercontent.com/wiki/magarena/magarena/screenshots/card-explorer-screen.jpg", "https://raw.githubusercontent.com/wiki/magarena/magarena/screenshots/avatars.jpg",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "magarena",]
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.magarena.magarena/"
latest_repo_commit = "2023-04-24"
repo_created_date = "2014-01-26"

+++

### Description

 Magarena is a single-player fantasy card game played against a computer opponent. The rules for Magarena are based on the first modern collectible card game. Learn more about Magarena.

[Source](http://magarena.github.io/)
