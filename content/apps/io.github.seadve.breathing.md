+++
title = "Breathing"
description = "Relax and meditate"
aliases = []
date = 2021-08-07
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Dave Patrick Caberto",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/SeaDve/Breathing"
homepage = "https://github.com/SeaDve/Breathing"
bugtracker = "https://github.com/SeaDve/Breathing/issues"
donations = "https://seadve.github.io/donate/"
translations = "https://hosted.weblate.org/projects/seadve/breathing/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/SeaDve/Breathing/main/screenshots/screenshot1.png", "https://raw.githubusercontent.com/SeaDve/Breathing/main/screenshots/screenshot2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.seadve.Breathing"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.seadve.Breathing"
flatpak_link = "https://flathub.org/apps/io.github.seadve.Breathing.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "breathing",]
appstream_xml_url = "https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-18"
repo_created_date = "2021-05-13"

+++

### Description

Breathing is a simple application that guides your breathing pattern. This pattern is recommended by experts that will help ease your anxiety.

The main features of Breathing includes the following:

* 🌬️ Guide your breathing.
* 🌑 Change to a dark-mode with ease.
* 📱 Easy-to-use user interface.
* ⌨️ User-friendly keyboard shortcuts.

[Source](https://raw.githubusercontent.com/SeaDve/Breathing/main/data/io.github.seadve.Breathing.metainfo.xml.in)