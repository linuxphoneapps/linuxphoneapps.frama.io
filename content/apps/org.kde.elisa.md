+++
title = "Elisa"
description = "Play local music and listen to online radio"
aliases = []
date = 2021-03-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/multimedia/elisa"
homepage = "https://apps.kde.org/elisa"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=elisa"
donations = "https://www.kde.org/community/donations/?app=elisa&source=appdata"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/multimedia/elisa/-/raw/master/org.kde.elisa.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/elisa/elisa-mobile.png", "https://cdn.kde.org/screenshots/elisa/elisa.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/multimedia/elisa/-/raw/master/icons/sc-apps-elisa.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.elisa"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.elisa"
flatpak_link = "https://flathub.org/apps/org.kde.elisa.flatpakref"
flatpak_recipe = "https://invent.kde.org/multimedia/elisa/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "elisa",]
appstream_xml_url = "https://invent.kde.org/multimedia/elisa/-/raw/master/org.kde.elisa.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-10-08"

+++

### Description

The Elisa music player is developed by the KDE community and strives to be simple and pleasant to use.

With Elisa, you can browse your local music collection by genre, artist, album, or track, listen to online radio, create and manage playlists, display lyrics, and more.

Elisa supports your full KDE color scheme when used on the Plasma desktop, or else the standard light and dark modes.

Chill out with Party Mode, which puts your music's album art front and center.

[Source](https://invent.kde.org/multimedia/elisa/-/raw/master/org.kde.elisa.appdata.xml)