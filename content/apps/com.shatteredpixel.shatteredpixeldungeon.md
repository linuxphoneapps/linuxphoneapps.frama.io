+++
title = "Shattered Pixel Dungeon"
description = "Roguelike RPG, with pixel art graphics and lots of variety and replayability"
aliases = ["games/com.shatteredpixel.shatteredpixeldungeon/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "Evan Debenham",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/00-Evan/shattered-pixel-dungeon"
homepage = "https://shatteredpixel.com/"
bugtracker = "https://github.com/00-Evan/shattered-pixel-dungeon/issues"
donations = "https://www.patreon.com/ShatteredPixel"
translations = "https://www.transifex.com/shattered-pixel/shattered-pixel-dungeon/"
more_information = []
summary_source_url = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon"
screenshots = [ "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/screenshot1.png", "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/screenshot2.png", "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/screenshot3.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.shatteredpixel.shatteredpixeldungeon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon"
flatpak_link = "https://flathub.org/apps/com.shatteredpixel.shatteredpixeldungeon.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "shattered-pixel-dungeon",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/com.shatteredpixel.shatteredpixeldungeon.metainfo.xml"
reported_by = "Moxvallix"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-19"
repo_created_date = "2014-07-31"

+++

### Description

Shattered Pixel Dungeon is a Roguelike RPG, with pixel art graphics and lots of variety and replayability. Every game is unique, with four different playable characters, randomized levels and enemies, and over 150 items to collect and use. The game is simple to get into, but has lots of depth. Strategy is required if you want to win!

This game is based on the freely available source code of Pixel Dungeon. It began as a project to improve on the original game's quirks, but has since evolved into a separate game, with many unique features.

Shattered Pixel Dungeon includes:

* High replayability: Randomly generated levels, enemies, and items. No two games are the same!
* 4 hero classes: Warrior, Mage, Rogue, and Huntress. Each hero has a unique playstyle.
* 8 subclasses: Successful heroes can refine their skills to become more powerful.
* 5 distinct dungeon regions: each with their own enemies, traps, and quests.
* Over 150 different items: including powerful wands, rings, weapons, and armor.
* 50+ different enemies, 30 different traps, and 5 bosses to test your skills.
* Unique artifacts, which grow in power as you use them.
* Updates, with new content roughly once a month.

Shattered Pixel Dungeon is a 100% free game. There are no advertisements, microtransactions, or paywalls. An optional donation to support the game unlocks a couple of fun extras, but these are entirely cosmetic and do not affect gameplay.

Find an issue with the game? Have a suggestion? Then contact me! This game is a solo project, so the feedback of players is invaluable in improving the game. You can contact me at Evan@ShatteredPixel.com (My ability to respond to emails in languages other than English is limited.)

Shattered Pixel Dungeon is available in multiple languages thanks to support from the community. The translation project is hosted here: https://www.transifex.com/shattered-pixel/shattered-pixel-dungeon

Shattered Pixel Dungeon is open source software under the GPLv3 Licence. You can find the current source code here: https://github.com/00-Evan/shattered-pixel-dungeon

[Source](https://raw.githubusercontent.com/flathub/com.shatteredpixel.shatteredpixeldungeon/master/com.shatteredpixel.shatteredpixeldungeon.metainfo.xml)

### Notice

Forked by ebolalex to allow it to run. Best in fullscreen mode. Needs java.
