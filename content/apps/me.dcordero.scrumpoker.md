+++
title = "ScrumPoker"
description = "ScrumPoker is a simple and useful scrum planning poker App. It helps you by making your estimation meetings more effective."
aliases = []
date = 2019-02-16
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "dcordero",]
categories = [ "project management tool",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://github.com/dcordero/ScrumPoker"
homepage = ""
bugtracker = "https://github.com/dcordero/ScrumPoker/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/dcordero/ScrumPoker"
screenshots = [ "https://github.com/dcordero/ScrumPoker",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "me.dcordero.scrumpoker"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/dcordero/ScrumPoker/refs/heads/master/me.dcordero.scrumpoker.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2019-02-13"
repo_created_date = "2019-02-12"

+++