+++
title = "Decoder"
description = "Scan and generate QR codes"
aliases = []
date = 2021-01-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "qr code",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libaperture",]
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Scanning", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/decoder/"
homepage = "https://apps.gnome.org/Decoder/"
bugtracker = "https://gitlab.gnome.org/World/decoder/-/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/decoder/"
more_information = [ "https://blogs.gnome.org/msandova/2021/08/07/decoder-0-2-0-released/", "https://apps.gnome.org/Decoder/",]
summary_source_url = "https://flathub.org/apps/com.belmoussaoui.Decoder"
screenshots = [ "https://gitlab.gnome.org/World/decoder/raw/master/data/screenshots/screenshot1.png", "https://gitlab.gnome.org/World/decoder/raw/master/data/screenshots/screenshot2.png", "https://gitlab.gnome.org/World/decoder/raw/master/data/screenshots/screenshot3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.belmoussaoui.Decoder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Decoder"
flatpak_link = "https://flathub.org/apps/com.belmoussaoui.Decoder.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "decoder",]
appstream_xml_url = "https://gitlab.gnome.org/World/decoder/-/raw/master/data/com.belmoussaoui.Decoder.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-12-26"

+++

### Description

Fancy yet simple QR Codes scanner and generator.

Features:

* QR Code generation
* Scanning with a camera
* Scanning from a screenshot
* Parses and displays QR code content when possible

[Source](https://gitlab.gnome.org/World/decoder/-/raw/master/data/com.belmoussaoui.Decoder.metainfo.xml.in.in)

### Notice

Note: Use Megapixels to scan QR codes on your Linux Phone, and use Decoder as a QR Code decoder and creator - most Linux Phones do not expose the camera to every app yet.