+++
title = "Photos"
description = "Image Gallery"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-2.1-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Graphics", "Viewer",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/graphics/koko"
homepage = "https://apps.kde.org/koko"
bugtracker = "https://invent.kde.org/graphics/koko/-/issues"
donations = "https://kde.org/community/donations/?app=org.kde.koko"
translations = ""
more_information = [ "https://plasma-mobile.org/2024/03/01/plasma-6/#photos-koko",]
summary_source_url = "https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/koko/koko.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.koko"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.koko"
flatpak_link = "https://flathub.org/apps/org.kde.koko.flatpakref"
flatpak_recipe = "https://invent.kde.org/graphics/koko/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "koko",]
appstream_xml_url = "https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-05-16"

+++

### Description

An Image gallery application

[Source](https://invent.kde.org/graphics/koko/-/raw/master/org.kde.koko.appdata.xml)