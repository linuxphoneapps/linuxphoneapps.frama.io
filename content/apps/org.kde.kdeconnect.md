+++
title = "KDE Connect"
description = "Seamless connection of your devices"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "connectivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = [ "KDE Connect",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/kdeconnect-kde"
homepage = "https://kdeconnect.kde.org"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kdeconnect"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/network/kdeconnect-android",]
summary_source_url = "https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kdeconnect/gnome3.png", "https://cdn.kde.org/screenshots/kdeconnect/kcm.png", "https://cdn.kde.org/screenshots/kdeconnect/plasmoid.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/network/kdeconnect-kde/-/raw/master/icons/app/sc-apps-kdeconnect.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kdeconnect"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kdeconnect",]
appstream_xml_url = "https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-01-21"

+++

### Description

KDE Connect makes your phone and computer work better together: share your clipboard and access files across devices, turn your phone into a remote control for your computer, find your phone when it has slipped down the back of your couch, see your phone notifications from your computer, and much more!

[Source](https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml)