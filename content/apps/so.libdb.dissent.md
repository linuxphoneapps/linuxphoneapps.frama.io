+++
title = "Dissent"
description = "Tiny native Discord app"
aliases = [ "apps/noappid.diamondburned.gtkcord4/", "apps/xyz.diamondb.gtkcord4/", "apps/so.libdb.gtkcord4/",]
date = 2022-04-11
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "diamondburned",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Discord",]
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Chat", "InstantMessaging", "Network",]
programming_languages = [ "Go",]
build_systems = [ "go",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/diamondburned/dissent"
homepage = "https://github.com/diamondburned/dissent"
bugtracker = "https://github.com/diamondburned/dissent/issues"
donations = "https://github.com/sponsors/diamondburned"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/diamondburned/dissent/main/so.libdb.dissent.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/01.png", "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/02.png", "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/03.png", "https://raw.githubusercontent.com/diamondburned/dissent/main/.github/screenshots/04.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/diamondburned/dissent/main/internal/icons/hicolor/scalable/apps/so.libdb.dissent.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "so.libdb.dissent"
scale_to_fit = ""
flathub = "https://flathub.org/apps/so.libdb.dissent"
flatpak_link = "https://flathub.org/apps/so.libdb.dissent.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dissent",]
appstream_xml_url = "https://raw.githubusercontent.com/diamondburned/dissent/main/so.libdb.dissent.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/so.libdb.gtkcord4/"
latest_repo_commit = "2024-12-29"
repo_created_date = "2022-04-03"

+++

### Description

Dissent (previously gtkcord4) is a third-party Discord client designed
for a smooth, native experience on Linux desktops.

Built with the GTK4 and libadwaita for a modern look and feel, it
delivers your favorite Discord app in a lightweight and visually
appealing package.

Dissent offers a streamlined Discord experience, prioritizing simplicity
and speed over feature completeness on par with the official client.
Here's what you can expect:

* Text chat with complete Markdown and custom emoji support
* Guild folders and channel categories
* Tabbed chat interface
* Quick switcher for channels and servers
* Image and file uploads, previews, and downloads
* User theming via custom CSS
* Partial thread/forum support
* Partial message reaction support
* Partial AI summary support (provided by Discord)

It does not aim to support voice chat and other advanced features, as
these are best handled by the official client or the web app.

[Source](https://raw.githubusercontent.com/diamondburned/dissent/main/so.libdb.dissent.metainfo.xml)

### Notice

Warning: Use at your own risk. Using a third party Discord client may result in your account being banned.

gtkcord4 uses libadwaita since v0.0.10. [Check the gtkcord3 page](https://github.com/diamondburned/gtkcord3#logging-in) for information on how to obtain a Token to log in.

Pre-built Binaries are available on gtkcord4's CI which automatically builds each release for Linux x86_64 and aarch64.
See the [Releases](https://github.com/diamondburned/gtkcord4/releases) page for the binaries.