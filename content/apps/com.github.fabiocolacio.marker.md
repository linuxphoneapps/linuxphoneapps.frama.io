+++
title = "Marker"
description = "Powerful markdown editor for the GNOME desktop."
aliases = []
date = 2020-08-25
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "FSFAP",]
app_author = [ "fabiocolacio",]
categories = [ "writing",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/fabiocolacio/Marker"
homepage = "https://github.com/fabiocolacio/Marker"
bugtracker = "https://github.com/fabiocolacio/Marker/issues"
donations = ""
translations = "https://github.com/fabiocolacio/Marker/tree/master/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/fabiocolacio/Marker/master/data/com.github.fabiocolacio.marker.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/fabiocolacio/Marker/master/help/C/figures/scrot1.png", "https://raw.githubusercontent.com/fabiocolacio/Marker/master/help/C/figures/scrot2.png", "https://raw.githubusercontent.com/fabiocolacio/Marker/master/help/C/figures/sketcher.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.fabiocolacio.marker"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.fabiocolacio.marker"
flatpak_link = "https://flathub.org/apps/com.github.fabiocolacio.marker.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "marker",]
appstream_xml_url = "https://raw.githubusercontent.com/fabiocolacio/Marker/master/data/com.github.fabiocolacio.marker.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-09-11"
repo_created_date = "2017-08-16"

+++

### Description

Features:

* View and edit markdown documents
* Convert markdown documents to HTML
* Tex math rendering
* Support for mermaid diagrams
* Support for charter plots
* Syntax highlighting for code blocks
* Integrated sketch editor
* Exports to PDF, RTF, ODT, DOCx and LaTeX
* Custom CSS themes
* Custom syntax themes
* Figure captioning and numbering

[Source](https://raw.githubusercontent.com/fabiocolacio/Marker/master/data/com.github.fabiocolacio.marker.appdata.xml)