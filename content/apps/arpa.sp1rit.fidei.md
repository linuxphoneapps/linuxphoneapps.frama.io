+++
title = "Fidei"
description = "Bible reader"
aliases = []
date = 2024-08-15
updated = 2024-12-01

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Florian \"sp1rit\"",]
categories = [ "bible",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Zefania XML",]
packaged_in = []
freedesktop_categories = [ "GNOME", "GTK",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://github.com/sp1ritCS/fidei"
homepage = "https://github.com/sp1ritCS/fidei"
bugtracker = "https://github.com/sp1ritCS/fidei/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/sp1ritCS/fidei/master/data/arpa.sp1rit.Fidei.metainfo.xml"
screenshots = [ "https://media.githubusercontent.com/media/sp1ritCS/fidei/master/screenshots/desktop.png", "https://media.githubusercontent.com/media/sp1ritCS/fidei/master/screenshots/init-picker.png", "https://media.githubusercontent.com/media/sp1ritCS/fidei/master/screenshots/mobile.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/sp1ritCS/fidei/master/data/arpa.sp1rit.Fidei.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "arpa.sp1rit.Fidei"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/sp1ritCS/fidei/master/pkgs/flatpak/arpa.sp1rit.Fidei.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/sp1ritCS/fidei/master/data/arpa.sp1rit.Fidei.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/arpa.sp1rit.fidei/"
latest_repo_commit = "2024-11-09"
repo_created_date = "2022-07-23"

+++

### Description

Fidei is an offline GTK bible reader for GNOME making use of modern technologies like libadwaita.

Features include:

* Reads bibles with in Zefania XML format
* Caption/Heading support
* Support for proper capitalization (small-caps) of »Lord«

[Source](https://raw.githubusercontent.com/sp1ritCS/fidei/master/data/arpa.sp1rit.Fidei.metainfo.xml)

### Notice

Needs separate download of content.
[Unofficial APKBUILD](https://framagit.org/linmobapps/apkbuilds/-/raw/master/fidei/APKBUILD)