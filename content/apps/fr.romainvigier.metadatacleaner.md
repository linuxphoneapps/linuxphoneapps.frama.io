+++
title = "Metadata Cleaner"
description = "View and clean metadata in files"
aliases = []
date = 2021-01-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Romain Vigier",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "mat2",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.com/rmnvgr/metadata-cleaner/"
homepage = "https://metadatacleaner.romainvigier.fr"
bugtracker = "https://gitlab.com/rmnvgr/metadata-cleaner/-/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/metadata-cleaner/"
more_information = [ "https://apps.gnome.org/MetadataCleaner/",]
summary_source_url = "https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml"
screenshots = [ "https://metadatacleaner.romainvigier.fr/screenshots/1.png", "https://metadatacleaner.romainvigier.fr/screenshots/2.png", "https://metadatacleaner.romainvigier.fr/screenshots/3.png", "https://metadatacleaner.romainvigier.fr/screenshots/4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "fr.romainvigier.MetadataCleaner"
scale_to_fit = ""
flathub = "https://flathub.org/apps/fr.romainvigier.MetadataCleaner"
flatpak_link = "https://flathub.org/apps/fr.romainvigier.MetadataCleaner.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "metadata-cleaner", "python:metadata-cleaner",]
appstream_xml_url = "https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-27"
repo_created_date = "2020-11-11"

+++


### Description

Metadata within a file can tell a lot about you. Cameras record data about when and where a picture was taken and which camera was used. Office applications automatically add author and company information to documents and spreadsheets. This is sensitive information and you may not want to disclose it.

This tool allows you to view metadata in your files and to get rid of it, as much as possible.

[Source](https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml)