+++
title = "Clock"
description = "Keep time and set alarms"
aliases = []
date = 2020-08-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Clock", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/kclock"
homepage = "https://apps.kde.org/kclock/"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=KClock"
donations = "https://www.kde.org/community/donations/?app=kclock&source=appdata"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://apps.kde.org/kclock/", "https://plasma-mobile.org/2024/03/01/plasma-6/#clock",]
summary_source_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kclock/kclock-desktop-timer.png", "https://cdn.kde.org/screenshots/kclock/kclock-desktop-timezones.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-alarms.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-stopwatch.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-timers.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-timezones.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.kclock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kclock"
flatpak_link = "https://flathub.org/apps/org.kde.kclock.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kclock", "kde5-kclock",]
appstream_xml_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2020-04-12"

+++

### Description

A universal clock application for desktop and mobile. Clock includes a stopwatch and a world clock, and allows you to set multiple alarms and timers.

It contains alarm, timer, stopwatch and timezone functionalities. Alarms and timers are able to wake the device from suspend on Plasma.

[Source](https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml)