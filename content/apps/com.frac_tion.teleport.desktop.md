+++
title = "Teleport"
description = "Share files over the local network"
aliases = [ "apps/com.frac_tion.teleport/",]
date = 2020-10-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Julian Sparber",]
categories = [ "file transfer",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/jsparber/teleport"
homepage = "https://gitlab.gnome.org/jsparber/teleport"
bugtracker = "https://gitlab.gnome.org/jsparber/teleport/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/jsparber/teleport/-/raw/master/data/com.frac_tion.teleport.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/jsparber/teleport/raw/master/data/screenshots/notification.png", "https://gitlab.gnome.org/jsparber/teleport/raw/master/data/screenshots/window.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.frac_tion.teleport.desktop"
scale_to_fit = "teleport"
flathub = "https://flathub.org/apps/com.frac_tion.teleport"
flatpak_link = "https://flathub.org/apps/com.frac_tion.teleport.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/jsparber/teleport/-/raw/master/com.frac_tion.teleport.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "teleport-share",]
appstream_xml_url = "https://gitlab.gnome.org/jsparber/teleport/-/raw/master/data/com.frac_tion.teleport.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/com.frac_tion.teleport/"
latest_repo_commit = "2024-06-07"
repo_created_date = "2017-12-22"

+++


### Description

Teleport is a fast way to share files over the local network. It's
designed to be a replacement for using USB keys or emailing stuff to
yourself just to move them on another device on your desk.

[Source](https://gitlab.gnome.org/jsparber/teleport/-/raw/master/data/com.frac_tion.teleport.appdata.xml.in)

### Notice

Flatpak release was great after scale-to-fit, later versions seem to have libhandy (test required), requires avahi.