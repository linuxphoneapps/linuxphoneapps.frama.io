+++
title = "Kontrast"
description = "Check color contrast"
aliases = []
date = 2020-09-10
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "accessibility",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Graphics",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/accessibility/kontrast"
homepage = "https://kde.org/applications/en/kontrast"
bugtracker = "https://invent.kde.org/accessibility/kontrast/-/issues"
donations = "https://liberapay.com/Carl/"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kontrast/kontrast.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kontrast"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kontrast"
flatpak_link = "https://flathub.org/apps/org.kde.kontrast.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kontrast",]
appstream_xml_url = "https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-07-27"

+++

### Description

Kontrast is a color contrast checker and tells you if your color combinations are distinct enough to be readable and accessible.

[Source](https://invent.kde.org/accessibility/kontrast/-/raw/master/org.kde.kontrast.appdata.xml)