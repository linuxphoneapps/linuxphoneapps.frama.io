+++
title = "Scrum Poker"
description = "A small gtk app to help estimate the effort for a task."
aliases = []
date = 2021-05-30
updated = 2024-10-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "chefe",]
categories = [ "utilities", "development",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "make", "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/chefe/scrumpoker"
homepage = ""
bugtracker = "https://github.com/chefe/scrumpoker/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/chefe/scrumpoker"
screenshots = [ "https://pbs.twimg.com/media/E2ppFCtXwAYVQWL?format=jpg&name=large",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/chefe/scrumpoker/refs/heads/master/data/io.chefe.scrumpoker.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.chefe.scrumpoker"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/chefe/scrumpoker/refs/heads/master/data/io.chefe.scrumpoker.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2020-04-22"
repo_created_date = "2020-04-03"

+++