+++
title = "Notejot"
description = "Jot your ideas"
aliases = []
date = 2021-02-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lainsce/notejot/"
homepage = "https://github.com/lainsce/notejot/"
bugtracker = "https://github.com/lainsce/notejot/issues"
donations = "https://www.ko-fi.com/lainsce/"
translations = "https://github.com/lainsce/notejot/blob/main/po/README.md"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lainsce/notejot/main/data/io.github.lainsce.Notejot.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/lainsce/notejot/main/data/shot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.lainsce.notejot/1.png", "https://img.linuxphoneapps.org/io.github.lainsce.notejot/2.png", "https://img.linuxphoneapps.org/io.github.lainsce.notejot/3.png", "https://img.linuxphoneapps.org/io.github.lainsce.notejot/4.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lainsce.Notejot"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Notejot"
flatpak_link = "https://flathub.org/apps/io.github.lainsce.Notejot.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "notejot",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/notejot/main/data/io.github.lainsce.Notejot.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-07-04"
repo_created_date = "2017-05-23"

+++


### Description

A stupidly-simple notes application for any type of short term notes or ideas.

* 🟡 Color your notes in 8 different colors
* 📓 Classify them in notebooks
* 🔤 Format text to your liking
* 📌 Pin your most important ones

[Source](https://raw.githubusercontent.com/lainsce/notejot/main/data/io.github.lainsce.Notejot.metainfo.xml.in)