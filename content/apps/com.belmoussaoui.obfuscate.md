+++
title = "Obfuscate"
description = "Censor private information"
aliases = []
date = 2021-07-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "privacy", "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/obfuscate/"
homepage = "https://gitlab.gnome.org/World/obfuscate/"
bugtracker = "https://gitlab.gnome.org/World/obfuscate/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/obfuscate/"
more_information = [ "https://apps.gnome.org/Obfuscate/",]
summary_source_url = "https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/obfuscate/raw/master/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.belmoussaoui.Obfuscate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Obfuscate"
flatpak_link = "https://flathub.org/apps/com.belmoussaoui.Obfuscate.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-obfuscate",]
appstream_xml_url = "https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-23"
repo_created_date = "2019-08-12"

+++

### Description

Obfuscate lets you redact your private information from any image.

[Source](https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in)