+++
title = "Geoclue Stumbler"
description = "Submit Cell Tower/WiFi AP data to geoclue"
aliases = []
date = 2024-12-22
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "kop316",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "geoclue", "beacondb",]
services = [ "BeaconDB",]
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/kop316/geoclue-stumbler"
homepage = "https://gitlab.com/kop316/geoclue-stumbler"
bugtracker = "https://gitlab.com/kop316/geoclue-stumbler/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/org.kop316.stumbler.metainfo.xml.in"
screenshots = [ "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/metainfo/screenshot.png?inline=false", "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/metainfo/screenshot2.png?inline=false", "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/metainfo/screenshot3.png?inline=false",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/icons/hicolor/scalable/apps/org.kop316.stumbler.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kop316.stumbler"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/org.kop316.stumbler.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/org.kop316.stumbler.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kop316.stumbler/"
latest_repo_commit = "2025-01-11"
repo_created_date = "2024-12-02"

+++

### Description

Geoclue Stumbler is geoclue client in GTK4/Libadwaita. While running, it ensures
geoclue is active to submit cell tower/WiFi Access point data, provides the user
feedback on if geoclue can submit data.

[Source](https://gitlab.com/kop316/geoclue-stumbler/-/raw/main/data/org.kop316.stumbler.metainfo.xml.in)

### Notice

As of December 22th, 2024, geoclue needs to be [patched](https://gitlab.freedesktop.org/geoclue/geoclue/-/merge_requests/192) to successfully upload information to BeaconDB.