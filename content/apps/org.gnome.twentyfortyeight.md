+++
title = "2048"
description = "Obtain the 2048 tile"
aliases = ["games/org.gnome.twentyfortyeight/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "Arnaud Bonatti",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-2048"
homepage = "https://gitlab.gnome.org/GNOME/gnome-2048"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-2048/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-2048/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-2048/raw/master/data/screenshot.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.TwentyFortyEight"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.TwentyFortyEight"
flatpak_link = "https://flathub.org/apps/org.gnome.TwentyFortyEight.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-2048",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in"
reported_by = "Moxvallix"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2018-05-23"
feed_entry_id = "https://linuxphoneapps.org/games/org.gnome.twentyfortyeight/"

+++

### Description

Play the highly addictive 2048 game. 2048 is a clone of the popular
single-player puzzle game. Gameplay consists of joining numbers in a grid
and obtain the 2048 tile.

Use your keyboard's arrow keys to slide all tiles in the desired direction.
Be careful: all tiles slide to their farthest possible positions, you cannot
slide just one tile or one row or column. Tiles with the same value are
joined when slided one over the other.

With every new tile obtained you increase your score. If you think you can easily
get the 2048 tile, do not let it stop you, the game does not end there, you can
continue joining tiles and improving your score.

Originally created by Gabriele Cirulli, 2048 has gained much popularity
due to it being highly addictive. Cirulli's 2048 is in turn a clone of
the 1024 game and includes ideas from other clones.

[Source](https://gitlab.gnome.org/GNOME/gnome-2048/-/raw/master/data/org.gnome.TwentyFortyEight.appdata.xml.in)
