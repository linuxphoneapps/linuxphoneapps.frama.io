+++
title = "FreeTube"
description = "An Open Source YouTube app for privacy"
aliases = []
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Preston",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Electron",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "JavaScript",]
build_systems = [ "yarn",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/FreeTubeApp/FreeTube"
homepage = "https://freetubeapp.io/"
bugtracker = "https://github.com/FreeTubeApp/FreeTube/issues"
donations = "https://liberapay.com/FreeTube"
translations = "https://hosted.weblate.org/engage/free-tube/"
more_information = []
summary_source_url = "https://freetubeapp.io/"
screenshots = [ "https://raw.githubusercontent.com/FreeTubeApp/FreeTubeApp.io/master/src/images/FreeTube1.png", "https://raw.githubusercontent.com/FreeTubeApp/FreeTubeApp.io/master/src/images/FreeTube2.png", "https://raw.githubusercontent.com/FreeTubeApp/FreeTubeApp.io/master/src/images/FreeTube3.png", "https://raw.githubusercontent.com/FreeTubeApp/FreeTubeApp.io/master/src/images/FreeTube4.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/FreeTubeApp/FreeTube/refs/heads/development/_icons/icon.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.freetubeapp.FreeTube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.freetubeapp.FreeTube"
flatpak_link = "https://flathub.org/apps/io.freetubeapp.FreeTube.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/io.freetubeapp.FreeTube/refs/heads/master/io.freetubeapp.FreeTube.yml"
snapcraft = "https://snapcraft.io/freetube"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "freetube",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/io.freetubeapp.FreeTube/refs/heads/master/io.freetubeapp.FreeTube.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-02-28"

+++

### Description

FreeTube is an open source desktop YouTube player built with privacy in
mind. Use YouTube without advertisements and prevent Google from tracking
you with their cookies and JavaScript. Available for Windows, Mac &
Linux thanks to Electron.

Please note that FreeTube is currently in Beta. While it should work well
for most users, there are still bugs and missing features that need to be
addressed.

[Source](https://raw.githubusercontent.com/flathub/io.freetubeapp.FreeTube/refs/heads/master/io.freetubeapp.FreeTube.metainfo.xml)