+++
title = "Chess Clock"
description = "Time games of over-the-board chess"
aliases = [ "games/com.clarahobbs.chessclock/", "games/eu.fortysixandtwo.chessclock/"]
date = 2024-04-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos Ribeiro Tzaras",]
categories = [ "game", "board game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "BoardGame", "GNOME", "GTK", "Game",]
programming_languages = [ "meson",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/chess-clock"
homepage = "https://gitlab.gnome.org/World/chess-clock"
bugtracker = "https://gitlab.gnome.org/World/chess-clock/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/chess-clock/"
more_information = [ "https://apps.gnome.org/Chessclock/",]
summary_source_url = "https://flathub.org/eu.fortysixandtwo.chessclock"
screenshots = [ "https://clarahobbs.com/screenshots/chess-clock-0.6-1.png", "https://clarahobbs.com/screenshots/chess-clock-0.6-2.png", "https://clarahobbs.com/screenshots/chess-clock-0.6-3.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/chess-clock/-/raw/main/data/icons/hicolor/scalable/apps/eu.fortysixandtwo.chessclock.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "eu.fortysixandtwo.chessclock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/eu.fortysixandtwo.chessclock"
flatpak_link = "https://flathub.org/apps/eu.fortysixandtwo.chessclock.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/chess-clock/-/raw/main/eu.fortysixandtwo.chessclock.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "chess-clock",]
appstream_xml_url = "https://gitlab.gnome.org/World/chess-clock/-/raw/main/data/eu.fortysixandtwo.chessclock.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/games/com.clarahobbs.chessclock/"
latest_repo_commit = "2024-11-27"
repo_created_date = "2023-01-03"

+++


### Description

Chess Clock is a simple application to provide time control for over-the-board
chess games. Intended for mobile use, players select the time control settings
desired for their game, then the black player taps their clock to start white's
timer. After each player's turn, they tap the clock to start their opponent's,
until the game is finished or one of the clocks reaches zero.

[Source](https://gitlab.gnome.org/World/chess-clock/-/raw/main/data/eu.fortysixandtwo.chessclock.metainfo.xml.in)
