+++
title = "Parolottero"
description = "Word game similar to Boggle and Ruzzle"
aliases = [ "games/noappid.ltworf.parolottero/", "games/noappid.parolottero.parolottero/"]
date = 2023-01-08
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "ltworf",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://codeberg.org/parolottero/parolottero"
homepage = ""
bugtracker = "https://codeberg.org/parolottero/parolottero/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=NEwD4Rn_nPQ",]
summary_source_url = "https://codeberg.org/parolottero/parolottero"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/1.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/2.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/3.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/4.png",]
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "parolottero",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.ltworf.parolottero/"
latest_repo_commit = "2024-12-15"
repo_created_date = "2023-07-13"

+++

### Notice

Language files are at <https://codeberg.org/parolottero/parolottero-languages>
