+++
title = "SuperTux"
description = "A jump-and-run game starring Tux the Penguin"
aliases = ["games/org.supertuxproject.supertux/"]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The SuperTux Team",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "ActionGame", "ArcadeGame", "Game",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/SuperTux/supertux"
homepage = "https://www.supertux.org"
bugtracker = "https://github.com/SuperTux/supertux/issues"
donations = "https://www.supertux.org/donate.html"
translations = "https://github.com/SuperTux/supertux/wiki/Translation"
more_information = []
summary_source_url = "https://flathub.org/apps/org.supertuxproject.SuperTux"
screenshots = [ "https://www.supertux.org/images/0_6_0/0_6_0_14.png", "https://www.supertux.org/images/0_6_0/0_6_0_2.png", "https://www.supertux.org/images/0_6_0/0_6_0_3.png", "https://www.supertux.org/images/0_6_0/0_6_0_4.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.supertuxproject.SuperTux"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.supertuxproject.SuperTux"
flatpak_link = "https://flathub.org/apps/org.supertuxproject.SuperTux.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/supertux"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "supertux",]
appstream_xml_url = "https://raw.githubusercontent.com/SuperTux/supertux/master/org.supertuxproject.SuperTux.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2013-11-23"
feed_entry_id = "https://linuxphoneapps.org/games/org.supertuxproject.supertux/"

+++

### Description

Run and jump through multiple worlds, fight off enemies by
jumping on them, bumping them from below or tossing objects at
them, grabbing power-ups and other stuff on the way.

In addition to the Story Mode, there is a large amount of
community contributed levels available as add-ons or in the
forums. Everyone can contribute such content by using the
integrated level editor.

The Story: Penny Gets Captured!

Tux and Penny were out having a nice picnic on the ice fields
of Antarctica. Suddenly, everything was a blur as Tux was
knocked out!

When Tux wakes up, he finds that Penny is missing. Where she
lay before now lies a letter. “Tux, my arch enemy!” says the
letter. “I have captured your beautiful Penny and have taken
her to my fortress. The path to my fortress is littered with
my minions. Give up on the thought of trying to reclaim her,
you haven’t got a chance! -Nolok”

Tux looks and sees Nolok’s fortress in the distance.
Determined to save his beloved Penny, he begins his journey
through the Icy Island and beyond.

[Source](https://raw.githubusercontent.com/SuperTux/supertux/master/org.supertuxproject.SuperTux.metainfo.xml)
