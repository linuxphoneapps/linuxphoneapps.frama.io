+++
title = "Kongress"
description = "Conference companion"
aliases = []
date = 2020-02-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "conference companion",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Office",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/kongress"
homepage = "https://invent.kde.org/utilities/kongress"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kongress"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/kongress/",]
summary_source_url = "https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kongress/desktop.png", "https://cdn.kde.org/screenshots/kongress/mobile.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kongress"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kongress"
flatpak_link = "https://flathub.org/apps/org.kde.kongress.flatpakref"
flatpak_recipe = "https://invent.kde.org/utilities/kongress/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kongress",]
appstream_xml_url = "https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-01-04"

+++

### Description

Kongress is a companion application for conferences made by KDE

[Source](https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml)