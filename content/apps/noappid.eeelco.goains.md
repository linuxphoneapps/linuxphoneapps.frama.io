+++
title = "Goains"
description = "Workout tracking app"
aliases = []
date = 2024-09-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "Eeelco",]
categories = [ "fitness",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Wails",]
backends = [ "Go",]
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Go", "Svelte",]
build_systems = [ "Wails",]
requires_internet = [ "requires offline-only",]
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/Eeelco/goains"
homepage = ""
bugtracker = "https://github.com/Eeelco/goains/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "Eeelco"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.eeelco.goains/"
latest_repo_commit = "2025-01-03"
repo_created_date = "2024-03-01"

+++

### Description

Goains allows the user to create a plan for workouts at home or in the gym. When starting a workout session, it is then possible to track reps and weight. The app also functions as a rest timer