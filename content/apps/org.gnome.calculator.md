+++
title = "Calculator"
description = "Perform arithmetic, scientific or financial calculations"
aliases = []
date = 2021-09-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Calculator", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calculator"
homepage = "https://apps.gnome.org/Calculator"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calculator/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-calculator/"
more_information = [ "https://apps.gnome.org/Calculator/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/main/data/org.gnome.Calculator.metainfo.xml.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/calculator/advanced-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/basic-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/financial-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/keyboard-mode.png", "https://static.gnome.org/appdata/gnome-43/calculator/programming-mode.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Calculator.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Calculator"
flatpak_link = "https://flathub.org/apps/org.gnome.Calculator.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/main/org.gnome.Calculator.Devel.json"
snapcraft = "https://snapcraft.io/gnome-calculator"
snap_link = ""
snap_recipe = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/main/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-calculator",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/main/data/org.gnome.Calculator.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2018-05-22"

+++

### Description

Calculator is an application that solves mathematical equations.
Though it at first appears to be a simple calculator with only basic
arithmetic operations, you can switch into Advanced, Financial, or
Programming mode to find a surprising set of capabilities.

The Advanced calculator supports many operations, including:
logarithms, factorials, trigonometric and hyperbolic functions,
modulus division, complex numbers, random number generation, prime
factorization and unit conversions.

Financial mode supports several computations, including periodic interest
rate, present and future value, double declining and straight line
depreciation, and many others.

Programming mode supports conversion between common bases (binary, octal,
decimal, and hexadecimal), boolean algebra, one’s and two’s complementation,
character to character code conversion, and more.

[Source](https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/main/data/org.gnome.Calculator.metainfo.xml.in)

### Notice

Adaptive upstream since release 41, GTK4/libadwaita since release 42.