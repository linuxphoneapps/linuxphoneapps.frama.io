+++
title = "Fruit Credits"
description = "Keep plaintext accounts"
aliases = []
date = 2024-10-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Deniz Akşimşek",]
categories = [ "accounting",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Finance", "Office",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/dz4k/fruit-credits"
homepage = "https://fruitcredits.dz4k.com/"
bugtracker = "https://codeberg.org/dz4k/fruit-credits/issues"
donations = "https://buymeacoffee.com/dz4k"
translations = "https://codeberg.org/dz4k/fruit-credits/src/branch/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/com.dz4k.FruitCredits"
screenshots = [ "https://fruitcredits.dz4k.com/media/screenshot-add-txn.png", "https://fruitcredits.dz4k.com/media/screenshot-balance.png", "https://fruitcredits.dz4k.com/media/screenshot-register.png", "https://fruitcredits.dz4k.com/media/screenshot-splash.png",]
screenshots_img = []
svg_icon_url = "https://codeberg.org/dz4k/fruit-credits/raw/branch/main/data/icons/hicolor/scalable/apps/com.dz4k.FruitCredits.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "com.dz4k.FruitCredits"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.dz4k.FruitCredits"
flatpak_link = "https://flathub.org/apps/com.dz4k.FruitCredits.flatpakref"
flatpak_recipe = "https://codeberg.org/dz4k/fruit-credits/raw/branch/main/com.dz4k.FruitCredits.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/dz4k/fruit-credits/raw/branch/main/data/com.dz4k.FruitCredits.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/com.dz4k.fruitcredits/"
latest_repo_commit = "2024-11-05"
repo_created_date = "2024-09-08"

+++


### Description

Fruit Credits is a double-entry accounting app
that provides a quick and friendly way to log transactions and track your finances.
Based on the command-line tool hledger,
it stores all your transactions in a readable, editable text file.

* Enter transactions with the help of autocomplete
* View balances and history of transactions
* Make useful queries, e.g.

Fruit Credits works perfectly on small screens,
so it's perfect for recording purchases when you're out-and-about.
(As long as you have a Linux phone).

Fruit Credits bundles a copy of hledger you can use in the command line:

[Source](https://codeberg.org/dz4k/fruit-credits/raw/branch/main/data/com.dz4k.FruitCredits.metainfo.xml.in.in)