+++
title = "Smile"
description = "An emoji picker"
aliases = []
date = 2022-05-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Paderi",]
categories = [ "emoji picker",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/mijorus/smile"
homepage = "https://mijorus.it/projects/smile"
bugtracker = "https://github.com/mijorus/smile/issues"
donations = "https://ko-fi.com/mijorus"
translations = ""
more_information = []
summary_source_url = "https://github.com/mijorus/smile/blob/master/data/it.mijorus.smile.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/mijorus/smile/master/docs/screenshot10.png", "https://raw.githubusercontent.com/mijorus/smile/master/docs/screenshot11.png", "https://raw.githubusercontent.com/mijorus/smile/master/docs/screenshot4.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/it.mijorus.smile/1.png", "https://img.linuxphoneapps.org/it.mijorus.smile/2.png", "https://img.linuxphoneapps.org/it.mijorus.smile/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "it.mijorus.smile"
scale_to_fit = ""
flathub = "https://flathub.org/apps/it.mijorus.smile"
flatpak_link = "https://flathub.org/apps/it.mijorus.smile.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "smile",]
appstream_xml_url = "https://raw.githubusercontent.com/mijorus/smile/master/data/it.mijorus.smile.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-30"
repo_created_date = "2021-12-09"

+++

### Description

Smile is a simple emoji picker for linux with custom tags support

[Source](https://raw.githubusercontent.com/mijorus/smile/master/data/it.mijorus.smile.appdata.xml.in)

### Notice

Was GTK3 pre 2.0.