+++
title = "Vvave"
description = "Play your music collection"
aliases = [ "apps/org.kde.vvave.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Audio", "Player", "Qt",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/vvave"
homepage = "https://vvave.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=vvave"
donations = "https://www.kde.org/community/donations"
translations = ""
more_information = [ "https://medium.com/@temisclopeolimac/its-vvave-c3f83da90380",]
summary_source_url = "https://invent.kde.org/maui/vvave/-/raw/master/org.kde.vvave.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/vvave/vvave.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/vvave/-/raw/master/src/assets/vvave.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.vvave"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.vvave"
flatpak_link = "https://flathub.org/apps/org.kde.vvave.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "vvave",]
appstream_xml_url = "https://invent.kde.org/maui/vvave/-/raw/master/org.kde.vvave.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.vvave/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-04-19"

+++

### Description

Tiny Qt Music Player to keep your favorite songs at hand.

[Source](https://invent.kde.org/maui/vvave/-/raw/master/org.kde.vvave.appdata.xml)