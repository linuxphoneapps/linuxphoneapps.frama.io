+++
title = "Run Free!"
description = "Running log app built for Phosh with some basic analysis of your running schedule"
aliases = []
date = 2021-09-23
updated = 2024-10-12

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "greenbeast",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/greenbeast/run-free"
homepage = ""
bugtracker = "https://gitlab.com/greenbeast/run-free/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/run-free"
screenshots = [ "https://gitlab.com/greenbeast/run-free/-/tree/master/Screenshots",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "greenbeast"
updated_by = "check_via_git_api"
latest_repo_commit = "2021-10-12"
repo_created_date = "2021-09-10"

+++