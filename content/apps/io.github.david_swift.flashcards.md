+++
title = "Memorize"
description = "Study flashcards"
aliases = []
date = 2024-03-10
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "MIT",]
app_author = [ "david-swift",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Education", "GNOME",]
programming_languages = [ "Swift",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/david-swift/Memorize/"
homepage = "https://github.com/david-swift/Memorize"
bugtracker = "https://github.com/david-swift/Memorize/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.david_swift.Flashcards"
screenshots = [ "https://raw.githubusercontent.com/david-swift/Memorize/main/data/tutorials/Export.png", "https://raw.githubusercontent.com/david-swift/Memorize/main/data/tutorials/Import.png", "https://raw.githubusercontent.com/david-swift/Memorize/main/data/tutorials/Overview.png", "https://raw.githubusercontent.com/david-swift/Memorize/main/data/tutorials/Study.png", "https://raw.githubusercontent.com/david-swift/Memorize/main/data/tutorials/Test.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/david-swift/Memorize/main/data/icons/io.github.david_swift.Flashcards.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.david_swift.Flashcards"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.david_swift.Flashcards"
flatpak_link = "https://flathub.org/apps/io.github.david_swift.Flashcards.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/david-swift/Memorize/main/io.github.david_swift.Flashcards.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/david-swift/Memorize/main/data/io.github.david_swift.Flashcards.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-07-09"
repo_created_date = "2023-12-23"

+++


### Description

Memorize is a native GNOME app that stores your flashcard sets.
It enables you to create, edit, view, and study sets.
Use the test mode for creating a preparation exam.
You can easily import existing Quizlet sets.

[Source](https://raw.githubusercontent.com/david-swift/Memorize/main/data/io.github.david_swift.Flashcards.metainfo.xml)

### Notice

This app mostly fits the screen, except for  the 'Overview' screen.