+++
title = "Planify"
description = "Forget about forgetting things"
aliases = [ "apps/com.github.alainm23.planner/",]
date = 2020-08-26
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alain",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Todoist", "CalDAV",]
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Calendar", "Office", "ProjectManagement", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/alainm23/planify"
homepage = "https://useplanify.com/"
bugtracker = "https://github.com/alainm23/planify/issues"
donations = "https://www.patreon.com/alainm23"
translations = "https://github.com/alainm23/planify/tree/master/po#readme"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/alainm23/planify/master/data/io.github.alainm23.planify.appdata.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/alainm23/planify/master/data/resources/screenshot/screenshot-01.png", "https://raw.githubusercontent.com/alainm23/planify/master/data/resources/screenshot/screenshot-02.png", "https://raw.githubusercontent.com/alainm23/planify/master/data/resources/screenshot/screenshot-03.png", "https://raw.githubusercontent.com/alainm23/planify/master/data/resources/screenshot/screenshot-04.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.alainm23.planify"
scale_to_fit = "io.github.alainm23.planify"
flathub = "https://flathub.org/apps/io.github.alainm23.planify"
flatpak_link = "https://flathub.org/apps/io.github.alainm23.planify.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/io.github.alainm23.planify/master/io.github.alainm23.planify.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "elementary-planner", "planify",]
appstream_xml_url = "https://raw.githubusercontent.com/alainm23/planify/master/data/io.github.alainm23.planify.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-06"
repo_created_date = "2018-10-22"

+++

### Description

🚀️ Planify is here...

* 🚀️ Complete redesign of the UI.
* 🤚️ Drag and Order: Sort your tasks wherever you want.
* 💯️ Progress And Emoji indicator for each project.
* 💪️ Be more productive and organize your tasks by 'Sections'.
* 🌙️ Better integration with the dark theme.
* 🎉️ and much more.

☁️ Support for Todoist:

* Synchronize your Projects, Task and Sections thanks to Todoist.
* Support for Todoist offline: Work without an internet connection and when everything is reconnected it will be synchronized.

\* Planify not created by, affiliated with, or supported by Doist

💎️ Other features:

* 🔍️ Quick Find
* 🌙️ Night mode
* ⏰️ Reminders

[Source](https://raw.githubusercontent.com/alainm23/planify/master/data/io.github.alainm23.planify.appdata.xml.in.in)

### Notice

Previously GTK3/Granite and named Planner. Release 3.0.10 of Planner from Flathub fit the screen well.