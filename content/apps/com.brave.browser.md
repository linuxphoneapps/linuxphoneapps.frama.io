+++
title = "Brave"
description = "Fast Internet, AI, Adblock"
aliases = []
date = 2019-03-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Brave Software",]
categories = [ "web browser",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = []
backends = [ "chromium",]
services = []
packaged_in = [ "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = []
requires_internet = [ "recommends always",]
tags = []

[extra]
repository = "https://github.com/brave/brave-browser"
homepage = "https://brave.com"
bugtracker = "https://github.com/brave/brave-browser/issues"
donations = ""
translations = ""
more_information = [ "https://brave.com/about/", "https://framagit.org/mglapps/mglapps.frama.io/-/merge_requests/3",]
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/flathub/com.brave.Browser/refs/heads/master/media/new-tab.png", "https://raw.githubusercontent.com/flathub/com.brave.Browser/refs/heads/master/media/welcome.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/flathub/com.brave.Browser/refs/heads/master/brave_lion.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.brave.Browser"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.brave.Browser"
flatpak_link = "https://flathub.org/apps/com.brave.Browser.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = "https://snapcraft.io/brave"
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "brave",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.brave.Browser/refs/heads/master/com.brave.Browser.metainfo.xml"
reported_by = "MightyGNU"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.brave.browser/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2017-11-09"

+++

### Description

Brave is on a mission to fix the web by giving users a safer, faster and better browsing
experience while growing support for content creators through a new attention-based ecosystem
of rewards.

Browse faster by blocking ads and trackers that violate your privacy and cost you time and
money.

[Source](https://raw.githubusercontent.com/flathub/com.brave.Browser/refs/heads/master/com.brave.Browser.metainfo.xml)