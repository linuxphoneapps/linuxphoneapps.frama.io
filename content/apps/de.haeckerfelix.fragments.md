+++
title = "Fragments"
description = "Manage torrents"
aliases = []
date = 2019-03-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felix Häcker",]
categories = [ "bittorrent client",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "BitTorrent",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "FileTransfer", "P2P", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/Fragments"
homepage = "https://apps.gnome.org/Fragments/"
bugtracker = "https://gitlab.gnome.org/World/Fragments/issues"
donations = "https://liberapay.com/haecker-felix"
translations = "https://l10n.gnome.org/module/Fragments/"
more_information = [ "https://puri.sm/posts/fragments-app-for-the-librem-5/", "https://apps.gnome.org/Fragments/", "https://blogs.gnome.org/haeckerfelix/2022/02/07/the-road-to-fragments-2-0/",]
summary_source_url = "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/1.png", "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/2.png", "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/3.png", "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/screenshots/4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.haeckerfelix.Fragments"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.haeckerfelix.Fragments"
flatpak_link = "https://flathub.org/apps/de.haeckerfelix.Fragments.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fragments",]
appstream_xml_url = "https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2018-12-15"

+++

### Description

An easy to use BitTorrent client. Fragments can be used to transfer files via the BitTorrent peer-to-peer file-sharing protocol, such as videos, music or installation images for Linux distributions.

* Overview of all torrents grouped by status
* Schedule the order of downloads with a queue
* Automatic detection of torrent or magnet links from the clipboard
* Manage and access individual files of a torrent
* Connect to remote Fragments or Transmission sessions

Fragments uses the Transmission BitTorrent project under the hood.

[Source](https://gitlab.gnome.org/World/Fragments/-/raw/main/data/de.haeckerfelix.Fragments.metainfo.xml.in.in)

### Notice

Ported to GTK4/libadwaita for release 2.0.