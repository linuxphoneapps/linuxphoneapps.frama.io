+++
title = "GCompris"
description = "Educational game for children"
aliases = ["games/org.kde.gcompris/"]
date = 2021-03-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "educational game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Education", "Game", "KidsGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/gcompris"
homepage = "https://gcompris.net/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=gcompris"
donations = "https://gcompris.net/donate-en.html"
translations = "https://gcompris.net/wiki/Developer%27s_corner#Translation"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/gcompris/gcompris.png", "https://gcompris.net/screenshots_qt/large/chess_partyend.png", "https://gcompris.net/screenshots_qt/large/click_on_letter_up.png", "https://gcompris.net/screenshots_qt/large/clockgame.png", "https://gcompris.net/screenshots_qt/large/color_mix.png", "https://gcompris.net/screenshots_qt/large/colors.png", "https://gcompris.net/screenshots_qt/large/crane.png", "https://gcompris.net/screenshots_qt/large/enumerate.png", "https://gcompris.net/screenshots_qt/large/fifteen.png", "https://gcompris.net/screenshots_qt/large/hexagon.png", "https://gcompris.net/screenshots_qt/large/scalesboard.png", "https://gcompris.net/screenshots_qt/large/traffic.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.gcompris"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.gcompris"
flatpak_link = "https://flathub.org/apps/org.kde.gcompris.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gcompris",]
appstream_xml_url = "https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml"
reported_by = "skyrrd"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-05-16"
feed_entry_id = "https://linuxphoneapps.org/games/org.kde.gcompris/"

+++

### Description

GCompris is a high quality educational software suite, including a large number of activities for children aged 2 to 10.

Some of the activities are game orientated, but nonetheless
still educational.

Below you can find a list of categories with some of the
activities available in that category.

* computer discovery: keyboard, mouse, different mouse gestures, ...
* arithmetic: table memory, enumeration, mirror image, balance
  the scale, change giving, ...
* science: the canal lock, color mixing, gravity concept, ...
* games: memory, connect 4, tic tac toe, sudoku, hanoi tower, ...
* reading: reading practice, ...
* other: learn to tell time, the braille system, maze, music
  instruments, ...

Currently GCompris offers in excess of 100 activities and more
are being developed. GCompris is free software, that means that
you can adapt it to your own needs, improve it and, most
importantly, share it with children everywhere.

[Source](https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml)
