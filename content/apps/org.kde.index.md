+++
title = "Index"
description = "Manage your files"
aliases = [ "apps/org.kde.index.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "FileManager", "Qt", "System",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/index-fm"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/index-fm/-/issues"
donations = "https://www.kde.org/community/donations"
translations = ""
more_information = [ "https://medium.com/@temisclopeolimac/index-overview-b2ddcb16534f",]
summary_source_url = "https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/index/index.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.index"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.index"
flatpak_link = "https://flathub.org/apps/org.kde.index.flatpakref"
flatpak_recipe = "https://invent.kde.org/maui/index-fm/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "index-fm",]
appstream_xml_url = "https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.index/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-04-19"

+++

### Description

Index allows you to navigate your computer and preview multimedia files.

[Source](https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml)

### Notice

Index 3.* has issues with scrolling in Alpine/postmarketOS. This is fixed in Index 4.0.0.