+++
title = "Contacts"
description = "Manage your contacts"
aliases = []
date = 2019-02-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "ContactManagement", "Office",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-contacts"
homepage = "https://apps.gnome.org/Contacts/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-contacts/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-contacts/"
more_information = [ "https://apps.gnome.org/Contacts/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.metainfo.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/contacts/contacts-edit.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-empty.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-filled.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-selection.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-setup.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Contacts"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Contacts"
flatpak_link = "https://flathub.org/apps/org.gnome.Contacts.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-contacts",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2018-01-24"

+++

### Description

Contacts keeps and organize your contacts information. You can create,
edit, delete and link together pieces of information about your contacts.
Contacts aggregates the details from all your sources providing a
centralized place for managing your contacts.

Contacts will also integrate with online address books and automatically
link contacts from different online sources.

[Source](https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.metainfo.xml.in.in)

### Notice

Used GTK3/libhandy before 42, supports .vcf import since 43.