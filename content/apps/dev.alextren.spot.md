+++
title = "Spot"
description = "Listen to music on Spotify"
aliases = []
date = 2021-02-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alexandre Trendel",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "librespot",]
services = [ "Spotify",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "AudioVideo", "Music",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/xou816/spot"
homepage = "https://github.com/xou816/spot"
bugtracker = "https://github.com/xou816/spot/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/xou816/spot/master/data/dev.alextren.Spot.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/xou816/spot/master/data/appstream/1.png", "https://raw.githubusercontent.com/xou816/spot/master/data/appstream/2.png", "https://raw.githubusercontent.com/xou816/spot/master/data/appstream/3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "dev.alextren.Spot"
scale_to_fit = "spot"
flathub = "https://flathub.org/apps/dev.alextren.Spot"
flatpak_link = "https://flathub.org/apps/dev.alextren.Spot.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/xou816/spot/development/dev.alextren.Spot.development.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "spot-client",]
appstream_xml_url = "https://raw.githubusercontent.com/xou816/spot/master/data/dev.alextren.Spot.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-11-29"
repo_created_date = "2020-05-07"

+++

### Description

Listen to music on Spotify.

Current features:

* playback control (play/pause, prev/next, seeking)
* play queue with shuffle option
* selection mode: easily browse and select mutliple tracks to queue them
* browse your saved albums and playlists
* search albums and artists
* view an artist's releases
* view users' playlists
* credentials management with Secret Service
* MPRIS integration

[Source](https://raw.githubusercontent.com/xou816/spot/master/data/dev.alextren.Spot.appdata.xml)

### Notice

Spotify Premium only, GTK4/libadwaita since 0.2.0.