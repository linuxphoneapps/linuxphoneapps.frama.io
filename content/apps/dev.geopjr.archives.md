+++
title = "Archives"
description = "Create and view web archives"
aliases = []
date = 2024-08-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos \"GeopJr\" Paterakis",]
categories = [ "web archival",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Archiving", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GeopJr/Archives/"
homepage = "https://archives.geopjr.dev/"
bugtracker = "https://gitlab.gnome.org/GeopJr/Archives/-/issues"
donations = "https://geopjr.dev/donate"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/dev.geopjr.Archives"
screenshots = [ "https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/data/screenshots/screenshot-1.png", "https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/data/screenshots/screenshot-2.png", "https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/data/screenshots/screenshot-3.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/data/icons/color.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "dev.geopjr.Archives"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.geopjr.Archives"
flatpak_link = "https://flathub.org/apps/dev.geopjr.Archives.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/build-aux/dev.geopjr.Archives.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/data/dev.geopjr.Archives.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/dev.geopjr.archives/"
latest_repo_commit = "2025-01-09"
repo_created_date = "2024-08-02"

+++

### Description

Every day, countless of websites go offline for good. High server costs, government censorship, venture capitalism are some of the threats, many of your favorite websites are facing.

Archives allows you to archive any website, including its assets, into a self-contained hybrid HTML/ZIP. These HTML files can be viewed in any browser (or in-app), be shared or be extracted like a regular ZIP file. Archives can additionally view WARC, WACZ, HAR and SWF files.

[Source](https://gitlab.gnome.org/GeopJr/Archives/-/raw/main/data/dev.geopjr.Archives.metainfo.xml.in)