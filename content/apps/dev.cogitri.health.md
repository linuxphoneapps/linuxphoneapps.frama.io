+++
title = "Health"
description = "Track your fitness goals"
aliases = []
date = 2021-01-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rasmus “Cogitri” Thomsen",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Google Fit",]
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "MedicalSoftware", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/Health"
homepage = "https://gitlab.gnome.org/World/Health"
bugtracker = "https://gitlab.gnome.org/World/Health/issues"
donations = ""
translations = "https://l10n.gnome.org/module/health/"
more_information = [ "https://apps.gnome.org/Health/",]
summary_source_url = "https://gitlab.gnome.org/World/Health/-/raw/master/data/dev.Cogitri.Health.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Health/raw/master/docs/screenshot_activities.png", "https://gitlab.gnome.org/World/Health/raw/master/docs/screenshot_main.png", "https://gitlab.gnome.org/World/Health/raw/master/docs/screenshot_steps.png", "https://gitlab.gnome.org/World/Health/raw/master/docs/screenshot_weight.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "dev.Cogitri.Health"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.Cogitri.Health"
flatpak_link = "https://flathub.org/apps/dev.Cogitri.Health.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/Health/-/raw/master/dev.Cogitri.Health.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "health", "gnome-health",]
appstream_xml_url = "https://gitlab.gnome.org/World/Health/-/raw/master/data/dev.Cogitri.Health.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-02"
repo_created_date = "2020-09-27"

+++

### Description

Health can visualize how many steps you’ve made daily,
your weight development over time and your daily activities.
Data can be synched from Google Fit or manually entered into Health.
Afterwards, it can be viewed and edited in Health.

[Source](https://gitlab.gnome.org/World/Health/-/raw/master/data/dev.Cogitri.Health.metainfo.xml.in.in)