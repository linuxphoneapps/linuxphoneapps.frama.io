+++
title = "Ptyxis"
description = "Container-oriented terminal"
aliases = []
date = 2024-05-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Christian Hergert",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "System", "TerminalEmulator",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/chergert/ptyxis"
homepage = "https://gitlab.gnome.org/chergert/ptyxis"
bugtracker = "https://gitlab.gnome.org/chergert/ptyxis/issues"
donations = ""
translations = "https://gitlab.gnome.org/chergert/ptyxis/-/tree/main/po"
more_information = [ "https://blogs.gnome.org/chergert/2024/05/15/ptyxis-on-flathub/", "https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/395",]
summary_source_url = "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/org.gnome.Ptyxis.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/change-behavior.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/columns-and-rows.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/containers-menu.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/edit-profile.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/integrated-dark-mode.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/palette-selector.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/rename-tab.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/shortcut-editing.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/ssh-tracking.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/sudo-tracking.png", "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/screenshots/tab-overview.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/icons/ptyxis.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "app.devsuite.Ptyxis"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.devsuite.Ptyxis"
flatpak_link = "https://flathub.org/apps/app.devsuite.Ptyxis.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/org.gnome.Ptyxis.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ptyxis",]
appstream_xml_url = "https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/org.gnome.Ptyxis.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-11-09"

+++

### Description

Ptyxis is a terminal for GNOME that focuses on ease-of-use in a
world of containers.

Features:

* Remembers current container when opening a new tab with support for podman, toolbox, distrobox, and more
* Configurable keyboard shortcuts
* Modern interface which integrates the palette with the window styling
* User-installable color palettes
* Support for preferences profiles with container integration
* Integrated color palettes with light and dark support
* Tabbed interface with tab overviews
* Efficient foreground process tracking denoting sudo and SSH
* Support for transparent terminal backgrounds
* Separate process mode for terminal-based applications
* Support for pinned tabs and saved sessions
* Terminal tabs are run within separate cgroups
* Terminal inspector to help when writing terminal-based applications

[Source](https://gitlab.gnome.org/chergert/ptyxis/-/raw/main/data/org.gnome.Ptyxis.metainfo.xml.in.in)