+++
title = "Secrets"
description = "Manage your passwords"
aliases = []
date = 2019-02-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Falk Alexander Seidl",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "pykeepass",]
services = [ "KeePass",]
packaged_in = [ "alpine_3_20", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/secrets"
homepage = "https://apps.gnome.org/Secrets/"
bugtracker = "https://gitlab.gnome.org/World/secrets/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/Secrets/"
more_information = [ "https://apps.gnome.org/Secrets/",]
summary_source_url = "https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/screenshot-1.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/screenshot-2.png", "https://gitlab.gnome.org/World/secrets/-/raw/master/screenshots/screenshot-3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.World.Secrets"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.Secrets"
flatpak_link = "https://flathub.org/apps/org.gnome.World.Secrets.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-passwordsafe", "secrets",]
appstream_xml_url = "https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-08-13"

+++

### Description

Secrets is a password manager which makes use of the KeePass v.4 format. It integrates perfectly with the GNOME desktop and provides an easy and uncluttered interface for the management of password databases.

[Source](https://gitlab.gnome.org/World/secrets/-/raw/master/data/org.gnome.World.Secrets.metainfo.xml.in.in)

### Notice

Previously based on GTK3/libhandy and called GNOME PasswordSafe. Secrets additionally supports two-factor authentication.