+++
title = "Futify"
description = "Native qml spotify client."
aliases = []
date = 2020-11-07
updated = 2024-10-26

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "futify",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick", "LomiriComponents",]
backends = []
services = [ "Spotify",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network", "Audio", "Player",]
programming_languages = [ "QML", "Go",]
build_systems = [ "clickable", "go",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/frenchutouch/futify"
homepage = ""
bugtracker = "https://gitlab.com/frenchutouch/futify/-/issues/"
donations = ""
translations = ""
more_information = [ "https://open-store.io/app/futify.frenchutouch",]
summary_source_url = "https://gitlab.com/frenchutouch/futify/"
svg_icon_url = "https://gitlab.com/frenchutouch/futify/-/raw/master/assets/logo.svg"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-10-21"
repo_created_date = "2020-04-14"

+++

### Notice

Ubuntu Touch Spotify client, can be run on other Mobile Linux distribution too.