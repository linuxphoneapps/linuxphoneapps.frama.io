+++
title = "FeedReader"
description = "RSS client for various webservices"
aliases = []
date = 2020-10-15
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan Lukas Gernert",]
categories = [ "feed reader",]
mobile_compatibility = [ "2",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = [ "TT-RSS", "Feedly", "Feedin", "Fresh Rss", "Inoreader", "The Old Reader", "Nextcloud News", "RSS",]
packaged_in = [ "aur", "debian_11", "devuan_4_0", "pureos_byzantium",]
freedesktop_categories = [ "GTK", "Network", "Feed",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/jangernert/FeedReader"
homepage = ""
bugtracker = "https://github.com/jangernert/FeedReader/issues"
donations = "https://app.bountysource.com/teams/jangernert-feedreader/issues"
translations = "https://hosted.weblate.org/projects/feedreader/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/jangernert/FeedReader/master/data/org.gnome.FeedReader.appdata.xml.in"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.FeedReader"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "feedreader",]
appstream_xml_url = "https://raw.githubusercontent.com/jangernert/FeedReader/master/data/org.gnome.FeedReader.appdata.xml.in"
reported_by = "ula"
updated_by = "check_via_github_api"
latest_repo_commit = "2020-05-23"
repo_created_date = "2015-08-21"

+++

### Description

FeedReader is a program designed to complement an already existing web-based RSS reader account.


Currently supported services:


* Feedbin
* Feedly
* FreshRSS
* InoReader
* Local RSS
* Nextcloud/ownCloud
* The Old Reader
* Tiny Tiny RSS


It combines all the advantages of web based services like syncing across all your devices with everything you expect
 from
 a modern desktop program: Desktop notifications, fast search and filtering, tagging, sharing to "read-it-later"
 services
 like Pocket and Instapaper, handy keyboard shortcuts and a database that keeps all your old articles as long as you
 like.


FeedReader also allows you to save articles to read-it-later with the supported services for now:


* Instapaper
* Pocket
* Wallabag


It also let you share any article with your friends using:


* Email
* Twitter
* Telegram

[Source](https://raw.githubusercontent.com/jangernert/FeedReader/master/data/org.gnome.FeedReader.appdata.xml.in)

### Notice

Not maintained any more, the developer is now making NewsFlash. Scale-to-fit necessary and even with the last pane of is not completely displayed and some crash, Launch and configuration works. Archived as the project has been archived. It has been continued as Communique for Elementary OS, but as that does not target mobile devices, Communique has not been added.