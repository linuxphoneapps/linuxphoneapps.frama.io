+++
title = "Alligator"
description = "RSS feed reader"
aliases = []
date = 2020-08-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Feed", "KDE", "Network", "News", "Qt",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/network/alligator"
homepage = "https://apps.kde.org/alligator"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Alligator"
donations = "https://kde.org/community/donations"
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/alligator/alligator-desktop.png", "https://cdn.kde.org/screenshots/alligator/alligator-mobile.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.alligator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.alligator"
flatpak_link = "https://flathub.org/apps/org.kde.alligator.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "alligator", "kde5-alligator",]
appstream_xml_url = "https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-02-07"

+++

### Description

Alligator is a convergent, cross-platform feed reader, supporting standard RSS/Atom feeds

[Source](https://invent.kde.org/network/alligator/-/raw/master/org.kde.alligator.appdata.xml)