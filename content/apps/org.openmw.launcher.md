+++
title = "OpenMW"
description = "Reimplementation of The Elder Scrolls III: Morrowind"
aliases = [ "games/org.openmw.openmw/", "games/org.openmw.launcher/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "OpenMW Contributors",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium",]
freedesktop_categories = [ "Game", "RolePlaying",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/OpenMW/openmw"
homepage = "https://openmw.org"
bugtracker = "https://gitlab.com/OpenMW/openmw/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml"
screenshots = [ "https://wiki.openmw.org/images/0.40_Screenshot-Balmora_3.png", "https://wiki.openmw.org/images/Openmw_0.11.1_launcher_1.png", "https://wiki.openmw.org/images/Screenshot_Vivec_seen_from_Ebonheart_0.35.png", "https://wiki.openmw.org/images/Screenshot_mournhold_plaza_0.35.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.openmw.launcher"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.openmw.OpenMW"
flatpak_link = "https://flathub.org/apps/org.openmw.OpenMW.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "openmw",]
appstream_xml_url = "https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml"
reported_by = "preflex"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/org.openmw.openmw/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-06-16"

+++

### Description

OpenMW is a new engine for 2002's Game of the Year, The Elder Scrolls 3: Morrowind.

It aims to be a fully playable (and improved!), open source implementation of the game's engine and functionality (including mods).

You will still need the original game data to play OpenMW.

[Source](https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml)
