+++
title = "Xash3D"
description = "Custom Gold Source engine rewritten from scratch."
aliases = ["games/noappid.fwgs.xash3d-fwgs/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "fwgs",]
categories = [ "game engine",]
mobile_compatibility = [ "1",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Game", "Shooter",]
programming_languages = [ "C",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/FWGS/xash3d-fwgs"
homepage = "https://xash.su/xash3d.html"
bugtracker = "https://github.com/FWGS/xash3d-fwgs/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/FWGS/xash3d-fwgs"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/FWGS/xash3d-fwgs/master/scripts/flatpak/su.xash.Engine.Compat.i386.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "xash3d-fwgs",]
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.fwgs.xash3d-fwgs/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-04-02"

+++
