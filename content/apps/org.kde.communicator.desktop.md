+++
title = "Communicator"
description = "Contacts manager"
aliases = [ "apps/org.kde.communicator/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Qt", "Office", "ContactManagement",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/maui/communicator"
homepage = "https://apps.kde.org/communicator"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=communicator"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/communicator/-/raw/master/org.kde.communicator.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/communicator/communicator.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/maui-communicator/-/raw/master/src/assets/communicator.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.communicator.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "communicator",]
appstream_xml_url = "https://invent.kde.org/maui/maui-communicator/-/raw/master/org.kde.communicator.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.communicator/"
latest_repo_commit = "2024-12-31"
repo_created_date = "2019-04-21"

+++

### Description

Communicator is a contacts manager.

[Source](https://invent.kde.org/maui/communicator/-/raw/master/org.kde.communicator.appdata.xml)