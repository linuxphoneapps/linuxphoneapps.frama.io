+++
title = "High Tide"
description = "Sail away"
aliases = []
date = 2025-01-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nokse",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "python3-tidalapi",]
services = [ "Tidal",]
packaged_in = []
freedesktop_categories = [ "Music",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Nokse22/high-tide"
homepage = "https://github.com/Nokse22/high-tide"
bugtracker = "https://github.com/Nokse22/high-tide/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Nokse22/high-tide/refs/heads/master/data/io.github.nokse22.HighTide.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Nokse22/HighTide/master/data/resources/screenshot%201.png", "https://raw.githubusercontent.com/Nokse22/HighTide/master/data/resources/screenshot%202.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Nokse22/high-tide/refs/heads/master/data/icons/hicolor/scalable/apps/io.github.nokse22.HighTide.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.nokse22.HighTide"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/Nokse22/high-tide/refs/heads/master/io.github.nokse22.HighTide.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Nokse22/high-tide/refs/heads/master/data/io.github.nokse22.HighTide.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.nokse22.hightide/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2023-09-22"

+++

### Description

TIDAL client made for GNOME

[Source](https://raw.githubusercontent.com/Nokse22/high-tide/refs/heads/master/data/io.github.nokse22.HighTide.appdata.xml.in)

### Notice

Prebuilt flatpaks can be found under [Actions](https://github.com/Nokse22/high-tide/actions) - click on the latest sucessful build and scroll down to Artifacts.