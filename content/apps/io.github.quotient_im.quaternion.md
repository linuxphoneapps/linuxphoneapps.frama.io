+++
title = "Quaternion"
description = "Qt-based client for Matrix networks"
aliases = [ "apps/com.github.quaternion/",]
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-4.0",]
app_author = [ "The Quotient Project",]
categories = [ "messenger",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = []
backends = []
services = [ "Matrix",]
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "InstantMessaging", "Network",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/quotient-im/Quaternion"
homepage = "https://github.com/quotient-im/Quaternion"
bugtracker = "https://github.com/quotient-im/Quaternion/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/quotient-im/Quaternion/refs/heads/dev/linux/io.github.quotient_im.Quaternion.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/quotient-im/Quaternion/master/Screenshot1.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/quotient-im/Quaternion/refs/heads/dev/icons/quaternion/sources/sc-apps-quaternion.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.quotient_im.Quaternion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.quaternion"
flatpak_link = "https://flathub.org/apps/com.github.quaternion.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "quaternion",]
appstream_xml_url = "https://raw.githubusercontent.com/quotient-im/Quaternion/refs/heads/dev/linux/io.github.quotient_im.Quaternion.appdata.xml"
reported_by = "linmob"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.github.quaternion/"
latest_repo_commit = "2024-12-31"
repo_created_date = "2016-01-21"

+++

### Description

Quaternion is a cross-platform desktop IM client for the Matrix protocol.

[Source](https://raw.githubusercontent.com/quotient-im/Quaternion/refs/heads/dev/linux/io.github.quotient_im.Quaternion.appdata.xml)
