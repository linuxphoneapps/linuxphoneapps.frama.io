+++
title = "Caffeine"
description = "Calculate your coffee"
aliases = []
date = 2024-10-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Konstantin Tutsch",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/konstantintutsch/Caffeine"
homepage = "https://konstantintutsch.com/Caffeine"
bugtracker = "https://github.com/konstantintutsch/Caffeine/issues"
donations = ""
translations = "https://github.com/konstantintutsch/Caffeine/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/com.konstantintutsch.Caffeine"
screenshots = [ "https://github.com/konstantintutsch/Caffeine/raw/refs/heads/main/data/com.konstantintutsch.Caffeine.Screenshot.Dark.png", "https://github.com/konstantintutsch/Caffeine/raw/refs/heads/main/data/com.konstantintutsch.Caffeine.Screenshot.Light.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/konstantintutsch/Caffeine/refs/heads/main/data/icons/com.konstantintutsch.Caffeine.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "com.konstantintutsch.Caffeine"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.konstantintutsch.Caffeine"
flatpak_link = "https://flathub.org/apps/com.konstantintutsch.Caffeine.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/konstantintutsch/Caffeine/refs/heads/main/build-aux/com.konstantintutsch.Caffeine.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/konstantintutsch/Caffeine/refs/heads/main/data/com.konstantintutsch.Caffeine.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.konstantintutsch.caffeine/"
latest_repo_commit = "2024-12-21"
repo_created_date = "2024-10-01"

+++

### Description

Caffeine is a utility for coffee enthusiasts. It's sole purpose is to calculate the extraction ratio of a coffee.

For example, an espresso brewed from 10g of coffee ground weighing 18g would have an extraction ratio of 1 : 1.8.

[Source](https://raw.githubusercontent.com/konstantintutsch/Caffeine/refs/heads/main/data/com.konstantintutsch.Caffeine.metainfo.xml.in.in)
