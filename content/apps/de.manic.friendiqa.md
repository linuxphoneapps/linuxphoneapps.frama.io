+++
title = "Friendiqa"
description = "Qt client for the social network Friendica"
aliases = []
date = 2022-03-29
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "lubuwest",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Friendica",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "Feed",]
programming_languages = [ "QML", "JavaScript", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.friendi.ca/lubuwest/Friendiqa"
homepage = "https://friendiqa.ma-nic.de/"
bugtracker = "https://git.friendi.ca/lubuwest/Friendiqa/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.friendi.ca/lubuwest/Friendiqa/raw/branch/master/src/assets/de.manic.friendiqa.metainfo.xml"
screenshots = [ "https://friendiqa.ma-nic.de/Screenshots/EventsTab.jpg", "https://friendiqa.ma-nic.de/Screenshots/PhotoTab.jpg", "https://friendiqa.ma-nic.de/ScreenshotsDesktop/Screenshot_Desktop_Timeline_small.png",]
screenshots_img = []
svg_icon_url = "https://git.friendi.ca/lubuwest/Friendiqa/raw/branch/master/src/assets/de.manic.Friendiqa.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "de.manic.friendiqa"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "friendiqa",]
appstream_xml_url = "https://git.friendi.ca/lubuwest/Friendiqa/raw/branch/master/src/assets/de.manic.friendiqa.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-05-03"
repo_created_date = "2018-07-20"

+++

### Description

Qt based client for the Friendica Social Network. Tabs for news (incl. Direct Messages), friends, photos and events.

[Source](https://git.friendi.ca/lubuwest/Friendiqa/raw/branch/master/src/assets/de.manic.friendiqa.metainfo.xml)