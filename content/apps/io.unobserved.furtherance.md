+++
title = "Furtherance"
description = "Time tracker"
aliases = [ "apps/com.lakoliu.furtherance/",]
date = 2022-03-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ricky Kresslein",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "iced",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/unobserved-io/Furtherance"
homepage = "https://furtherance.app"
bugtracker = "https://github.com/unobserved-io/Furtherance/issues"
donations = "https://www.paypal.com/donate/?hosted_button_id=TLYY8YZ424VRL"
translations = "https://hosted.weblate.org/projects/furtherance/translations/"
more_information = []
summary_source_url = "https://flathub.org/apps/io.unobserved.furtherance"
screenshots = [ "https://unobserved.io/assets/screenshots/furtherance/linux/1000x657/History.png", "https://unobserved.io/assets/screenshots/furtherance/linux/1000x657/Report.png", "https://unobserved.io/assets/screenshots/furtherance/linux/1000x657/Settings-Pomodoro.png", "https://unobserved.io/assets/screenshots/furtherance/linux/1000x657/Shortcuts.png", "https://unobserved.io/assets/screenshots/furtherance/linux/1000x657/Timer.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.unobserved.furtherance"
scale_to_fit = "com.lakoliu.Furtherance"
flathub = "https://flathub.org/apps/io.unobserved.furtherance"
flatpak_link = "https://flathub.org/apps/io.unobserved.furtherance.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/lakoliu/Furtherance/main/com.lakoliu.Furtherance.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "furtherance",]
appstream_xml_url = "https://raw.githubusercontent.com/unobserved-io/Furtherance/refs/heads/main/assets/linux/io.unobserved.furtherance.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.lakoliu.furtherance/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2022-02-18"

+++

### Description

Furtherance is a time tracking application that respects your privacy.
It allows you to track time spent on different activities without worrying about your data being captured and sold.

[Source](https://raw.githubusercontent.com/unobserved-io/Furtherance/refs/heads/main/assets/linux/io.unobserved.furtherance.appdata.xml)

### Notice

Used to be GTK4/libadwaita before 24.9-beta. Since switching to iced/24.10, it fits the screen well, but the rendering is a bit broken - the app has a two column layout that does not adapt to narrow screen-widths.
