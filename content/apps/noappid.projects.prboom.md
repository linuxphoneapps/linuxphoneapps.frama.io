+++
title = "PrBoom"
description = "PrBoom is the culmination of years of work by various people and projects on the Doom source code."
aliases = ["games/noappid.projects.prboom/"]
date = 2021-03-14
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = ["prboom",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "inactive",]
frameworks = [ "SDL",]
backends = []
services = []
packaged_in = [ "aur", "fedora_40", "fedora_41", "fedora_rawhide",]
freedesktop_categories = ["Game", "Shooter"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://sourceforge.net/projects/prboom/"
homepage = "https://prboom.sourceforge.net/"
bugtracker = "https://sourceforge.net/p/prboom/bugs/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://prboom.sourceforge.net/about.html"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "prboom",]
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.projects.prboom/"

+++
