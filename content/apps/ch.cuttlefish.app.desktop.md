+++
title = "Cuttlefish"
description = "Client for PeerTube, the federated video hosting service"
aliases = [ "apps/ch.cuttlefish.app/",]
date = 2021-01-23
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "artectrex",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "PeerTube",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "AudioVideo", "Player",]
programming_languages = [ "Cpp",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.shinice.net/artectrex/Cuttlefish"
homepage = "https://cuttlefish.ch"
bugtracker = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/raw/master/data/ch.cuttlefish.app.appdata.xml.in"
screenshots = [ "https://cuttlefish.ch/screenshots/1.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/raw/master/data/cuttlefish_logo.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "ch.cuttlefish.app.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/raw/master/ch.cuttlefish.app.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.shinice.net/artectrex/Cuttlefish/-/raw/master/data/ch.cuttlefish.app.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/ch.cuttlefish.app/"
latest_repo_commit = "2021-11-14"
repo_created_date = "2020-10-09"

+++

### Description

Client for PeerTube, the federated video hosting service

[Source](https://gitlab.shinice.net/artectrex/Cuttlefish/-/raw/master/data/ch.cuttlefish.app.appdata.xml.in)

### Notice

It's very much WIP and seems to be inactive since November 2021.