+++
title = "Kalm"
description = "Breathing techniques"
aliases = []
date = 2023-10-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "education", "health",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/kalm"
homepage = "https://apps.kde.org/kalm"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kalm"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalm/general.png", "https://cdn.kde.org/screenshots/kalm/info.png", "https://cdn.kde.org/screenshots/kalm/menu.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.kalm"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kalm"
flatpak_link = "https://flathub.org/apps/org.kde.kalm.flatpakref"
flatpak_recipe = "https://invent.kde.org/utilities/kalm/-/raw/master/.flatpak-manifest.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalm",]
appstream_xml_url = "https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml"
reported_by = "plata"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2023-06-08"

+++

### Description

Kalm can teach you different breathing techniques.

[Source](https://invent.kde.org/utilities/kalm/-/raw/master/org.kde.kalm.appdata.xml)