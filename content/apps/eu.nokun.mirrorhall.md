+++
title = "Mirror Hall"
description = "Use Linux devices as second screens"
aliases = []
date = 2024-09-26
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Raffaele T.",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/r/mirrorhall"
homepage = "https://nokun.eu/mirrorhall"
bugtracker = "https://gitlab.gnome.org/r/mirrorhall/-/issues"
donations = ""
translations = ""
more_information = [ "https://notes.nokun.eu/post/2024-09-22-mirrorhall/",]
summary_source_url = "https://flathub.org/apps/eu.nokun.MirrorHall"
screenshots = [ "https://gitlab.gnome.org/r/mirrorhall/-/raw/master/data/screenshots/mirror.png", "https://gitlab.gnome.org/r/mirrorhall/-/raw/master/data/screenshots/stream.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/r/mirrorhall/-/raw/master/data/icons/hicolor/scalable/apps/eu.nokun.MirrorHall.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "eu.nokun.MirrorHall"
scale_to_fit = ""
flathub = "https://flathub.org/apps/eu.nokun.MirrorHall"
flatpak_link = "https://flathub.org/apps/eu.nokun.MirrorHall.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/r/mirrorhall/-/raw/master/eu.nokun.MirrorHall.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/r/mirrorhall/-/raw/master/data/eu.nokun.MirrorHall.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/eu.nokun.mirrorhall/"
latest_repo_commit = "2024-12-26"
repo_created_date = "2024-05-18"

+++

### Description

Mirror Hall is an experimental app that aims to turn any Linux device into a
second monitor.

It is designed on both desktop and mobile devices, and supports both
hardware-accelerated and software-based encoding to stream video efficiently.

Mirror Hall makes use of a new, experimental GNOME feature to create virtual
monitors, and requires UDP traffic to be allowed by the network and firewall.
Support for other desktops will be added once a standardized API exists.

[Source](https://gitlab.gnome.org/r/mirrorhall/-/raw/master/data/eu.nokun.MirrorHall.appdata.xml.in)