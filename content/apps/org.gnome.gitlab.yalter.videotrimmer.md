+++
title = "Video Trimmer"
description = "Trim videos quickly"
aliases = []
date = 2020-10-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ivan Molodetskikh",]
categories = [ "video editing",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "ffmpeg",]
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
homepage = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
bugtracker = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/video-trimmer/"
more_information = [ "https://apps.gnome.org/VideoTrimmer/",]
summary_source_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
screenshots = [ "https://gitlab.gnome.org/YaLTeR/video-trimmer/uploads/e840fa093439348448007197d07c8033/image.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/data/icons/hicolor/scalable/apps/org.gnome.gitlab.YaLTeR.VideoTrimmer.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.gitlab.YaLTeR.VideoTrimmer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.VideoTrimmer"
flatpak_link = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.VideoTrimmer.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/build-aux/org.gnome.gitlab.YaLTeR.VideoTrimmer.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "video-trimmer",]
appstream_xml_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-04-25"

+++

### Description

Video Trimmer cuts out a fragment of a video given the start and end timestamps. The video is never re-encoded, so the process is very fast and does not reduce the video quality.

[Source](https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in)