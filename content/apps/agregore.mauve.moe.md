+++
title = "Agregore Browser"
description = "A minimal browser for the distributed web"
aliases = []
date = 2021-05-16
updated = 2024-12-01

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "agregoreweb",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Electron",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "JavaScript",]
build_systems = [ "yarn",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/AgregoreWeb/agregore-browser"
homepage = "https://agregore.mauve.moe/"
bugtracker = "https://github.com/AgregoreWeb/agregore-browser/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=TnYKvOQB0ts", "https://github.com/AgregoreWeb/agregore-browser/issues/103",]
summary_source_url = "https://github.com/AgregoreWeb/agregore-browser"
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/AgregoreWeb/agregore-browser/refs/heads/master/build/icon.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "agregore.mauve.moe"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "agregore-browser",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-11-18"
repo_created_date = "2020-06-12"

+++

### Notice

Can be built as described on https://github.com/AgregoreWeb/agregore-browser/issues/103