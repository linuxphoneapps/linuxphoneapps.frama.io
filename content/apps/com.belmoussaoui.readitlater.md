+++
title = "Read It Later"
description = "Save and read web articles"
aliases = []
date = 2020-08-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Wallabag",]
services = [ "Wallabag",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Network", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "recommends always", "requires first-run",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/read-it-later"
homepage = "https://gitlab.gnome.org/World/read-it-later"
bugtracker = "https://gitlab.gnome.org/World/read-it-later/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/read-it-later/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/read-it-later/-/raw/master/data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/read-it-later/raw/master/data/resources/screenshots/screenshot1.png", "https://gitlab.gnome.org/World/read-it-later/raw/master/data/resources/screenshots/screenshot2.png", "https://gitlab.gnome.org/World/read-it-later/raw/master/data/resources/screenshots/screenshot3.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/read-it-later/-/raw/master/data/icons/hicolor/scalable/apps/com.belmoussaoui.ReadItLater.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.belmoussaoui.ReadItLater"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.ReadItLater"
flatpak_link = "https://flathub.org/apps/com.belmoussaoui.ReadItLater.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/read-it-later/-/raw/master/build-aux/com.belmoussaoui.ReadItLater.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "read-it-later",]
appstream_xml_url = "https://gitlab.gnome.org/World/read-it-later/-/raw/master/data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-15"
repo_created_date = "2019-10-16"

+++

### Description

Read It Later, is a simple Wallabag client. It has the basic features to manage your articles.

* Add new articles
* Archive an article
* Delete an article
* Favorite an article

It also comes with a nice on eyes reader mode that supports code syntax highlighting and a dark mode.

[Source](https://gitlab.gnome.org/World/read-it-later/-/raw/master/data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in)

### Notice

GTK3/libhandy before 0.3.0.