+++
title = "Samegame"
description = "A fun game"
aliases = [ "games/org.kde.samegame/", "games/org.kde.plasma.samegame/"]
date = 2020-09-07
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sebastian Kügler <sebas@kde.org>",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "LogicGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-samegame"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/plasma-samegame/-/issues/"
donations = "https://www.kde.org/donate.php?app=org.kde.plasma.samegame"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-samegame/-/raw/master/org.kde.samegame.appdata.xml"
screenshots = [ "https://linmob.net/pinephone-building-plasma-mobile-apps-from-the-aur/20200906_22h15m48s_grim.jpg",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.plasma.samegame"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-samegame/-/raw/master/org.kde.samegame.appdata.xml"
reported_by = "1peter10"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/org.kde.samegame/"
latest_repo_commit = "2023-10-22"
repo_created_date = "2020-05-16"

+++
