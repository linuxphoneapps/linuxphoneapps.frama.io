+++
title = "Recorder"
description = "Audio recorder"
aliases = []
date = 2020-02-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "audio recorder",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Audio", "KDE", "Recorder", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/utilities/krecorder"
homepage = "https://invent.kde.org/utilities/krecorder"
bugtracker = "https://invent.kde.org/plasma-mobile/krecorder/-/issues"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#recorder",]
summary_source_url = "https://invent.kde.org/utilities/krecorder/-/raw/master/org.kde.krecorder.appdata.xml"
screenshots = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-1.png", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-2.png", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-3.png", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-4.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.krecorder"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/krecorder/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "krecorder",]
appstream_xml_url = "https://invent.kde.org/utilities/krecorder/-/raw/master/org.kde.krecorder.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2019-12-25"

+++

### Description

Recorder is a simple, cross-platform audio recording application.

[Source](https://invent.kde.org/utilities/krecorder/-/raw/master/org.kde.krecorder.appdata.xml)