+++
title = "splitcat"
description = "Splitcat application packaged as a Flatpak."
aliases = []
date = 2024-10-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "Ivan Janjić",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Dart", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vogonwann/splitcat"
homepage = "https://github.com/vogonwann/splitcat"
bugtracker = "https://github.com/vogonwann/splitcat/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/packaging/linux/lol.janjic.Splitcat.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/screenshots/custom.png", "https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/screenshots/merge.png", "https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/screenshots/preset.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = "https://github.com/vogonwann/splitcat/blob/main/linux/flutter/assets/icons/128x128/icon.png"
all_features_touch = true
intended_for_mobile = false
app_id = "lol.janjic.Splitcat"
scale_to_fit = ""
flathub = "https://flathub.org/apps/lol.janjic.Splitcat"
flatpak_link = "https://flathub.org/apps/lol.janjic.Splitcat.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/lol.janjic.Splitcat.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/packaging/linux/lol.janjic.Splitcat.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/lol.janjic.splitcat/"
latest_repo_commit = "2024-12-10"
repo_created_date = "2024-09-28"

+++

### Description

Simple file splitting and merging application with predefined presets for popular apps.

[Source](https://raw.githubusercontent.com/vogonwann/splitcat/refs/heads/main/packaging/linux/lol.janjic.Splitcat.appdata.xml)