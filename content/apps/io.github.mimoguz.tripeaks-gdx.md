+++
title = "TriPeaks"
description = "A Tri Peaks solitaire game"
aliases = ["games/io.github.mimoguz.tripeaks-gdx/"]
date = 2025-01-21
updated = 2025-01-21

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Oguz Tas",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = []
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Game", "CardGame",]
programming_languages = [ "Kotlin",]
build_systems = [ "Gradle",]
requires_internet = [ "requires offline-only",]
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/mimoguz/tripeaks-gdx"
homepage = ""
bugtracker = "https://github.com/mimoguz/tripeaks-gdx/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/tripeaks-gdx/Bundle/"
more_information = []
summary_source_url = "https://github.com/mimoguz/tripeaks-gdx"
screenshots = [ "https://dl.flathub.org/media/io/github/mimoguz.TriPeaks-GDX/0c6e31c0a70f85c7a0d16729a935eb76/screenshots/image-1_orig.png", "https://dl.flathub.org/media/io/github/mimoguz.TriPeaks-GDX/0c6e31c0a70f85c7a0d16729a935eb76/screenshots/image-2_orig.png", "https://dl.flathub.org/media/io/github/mimoguz.TriPeaks-GDX/0c6e31c0a70f85c7a0d16729a935eb76/screenshots/image-3_orig.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.mimoguz.TriPeaks-GDX"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.mimoguz.TriPeaks-GDX"
flatpak_link = "https://flathub.org/apps/io.github.mimoguz.TriPeaks-GDX.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "Maxnet"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/io.github.mimoguz.tripeaks-gdx/"
latest_repo_commit = "2025-01-19"
repo_created_date = "2021-02-14"

+++
