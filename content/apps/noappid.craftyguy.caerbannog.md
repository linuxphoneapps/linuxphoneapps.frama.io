+++
title = "caerbannog"
description = "Frontend for password-store"
aliases = []
date = 2021-02-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "craftyguy",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "pass",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~craftyguy/caerbannog"
latest_repo_commit = "2024-10-14"
homepage = ""
bugtracker = "https://todo.sr.ht/~craftyguy/caerbannog"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~craftyguy/caerbannog"
screenshots = [ "https://fosstodon.org/@linmob/105686222789395866",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~craftyguy/caerbannog/blob/master/data/icons/hicolor/scalable/apps/net.craftyguy.caerbannog.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "caerbannog",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_repology"
repo_created_date = "2020-12-13"

+++