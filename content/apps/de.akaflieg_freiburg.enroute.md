+++
title = "Enroute Flight Navigation"
description = "Flight Navigation App"
aliases = []
date = 2020-12-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Akaflieg Freiburg",]
categories = [ "flight navigation",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Maps", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Akaflieg-Freiburg/enroute"
homepage = "https://akaflieg-freiburg.github.io/enroute/"
bugtracker = "https://github.com/Akaflieg-Freiburg/enroute/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/de.akaflieg_freiburg.enroute"
screenshots = [ "https://akaflieg-freiburg.github.io/enroute/assets/images/00-Desktop.png", "https://akaflieg-freiburg.github.io/enroute/assets/images/01-Desktop.png", "https://akaflieg-freiburg.github.io/enroute/assets/images/02-Desktop.png", "https://akaflieg-freiburg.github.io/enroute/assets/images/03-Desktop.png", "https://akaflieg-freiburg.github.io/enroute/assets/images/04-Desktop.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Akaflieg-Freiburg/enroute/refs/heads/main/metadata/de.akaflieg_freiburg.enroute.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "de.akaflieg_freiburg.enroute"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.akaflieg_freiburg.enroute"
flatpak_link = "https://flathub.org/apps/de.akaflieg_freiburg.enroute.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "enroute",]
appstream_xml_url = "https://raw.githubusercontent.com/Akaflieg-Freiburg/enrouteText/refs/heads/master/desktop/linux/de.akaflieg_freiburg.enroute.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-08"
repo_created_date = "2019-12-23"

+++

### Description

Enroute Flight Navigation is a flight navigation app for VFR pilots. The
app runs primarily on mobile devices. It is free, open source, 100%
non-commercial and does not collect user data. This desktop version of the
app is hardly useful for actual navigation, but might be used to plan your
next flight on the big screen.

[Source](https://raw.githubusercontent.com/Akaflieg-Freiburg/enrouteText/refs/heads/master/desktop/linux/de.akaflieg_freiburg.enroute.appdata.xml.in)