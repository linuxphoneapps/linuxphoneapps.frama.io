+++
title = "Ruqola"
description = "Rocket Chat Client"
aliases = []
date = 2020-02-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "chat",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = []
services = [ "Rocket.Chat",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "InstantMessaging", "Network",]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://invent.kde.org/network/ruqola"
homepage = "https://apps.kde.org/ruqola"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=ruqola"
donations = "https://www.kde.org/community/donations/?app=ruqola&source=appdata"
translations = ""
more_information = [ "https://community.kde.org/GSoC/2017/StatusReports/Vasudha", "https://vasudhamathur.wordpress.com/2017/08/25/ruqola-installation/",]
summary_source_url = "https://invent.kde.org/network/ruqola/-/raw/master/src/data/org.kde.ruqola.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/ruqola/ruqola.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.ruqola"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ruqola"
flatpak_link = "https://flathub.org/apps/org.kde.ruqola.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ruqola",]
appstream_xml_url = "https://invent.kde.org/network/ruqola/-/raw/master/src/data/org.kde.ruqola.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.ruqola/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-05-16"

+++

### Description

Ruqola is a client for Rocket.Chat.

It supports RC features:

* Direct Message
* group channel
* channel
* Support autotranslate (when RC has it)
* Configuring room notification
* Configuring room
* Thread message support
* Discussion room
* Teams room support
* Configuring own account
* Registering new account
* Support multi account
* Search message in room
* Show mention/attachment/Pinned message/start message
* Support emoji
* Video support
* Configure administrator support (add/remove roles, add/remove users, add/remove permissions, configure rooms etc.)
* Support OTR (in progress)
* support gif image
* Add support for uploading attachment files
* Show unread message information
* Allow to block/unblock users
* Auto-away
* Two-factor authentication via TOTP
* Two-factor authentication via email
* Administrator support (Server Info, Rooms, Custom User Status, Custom Sounds, Custom Emoji, Users, Invites, View Log, Permissions, Roles, Oauth), Administrator settings (Message, Account, Retention Policy, File Upload, Encryption)
* Export Messages
* Video Conference

Extra features:

* Allow to reply directly to message from notification (kde features)
* Drag and drop image from ruqola to website or local folder
* Store message in local database so we can search in offline
* Mark All Channels are read
* Add notification history
* New RocketChat Server Information Support
* Manage Device (RC >= 5.0)
* Manage Personal Access Token
* Add support for local translation
* Add support for text auto-correction

[Source](https://invent.kde.org/network/ruqola/-/raw/master/src/data/org.kde.ruqola.appdata.xml)

### Notice

This was added back in the day for it's Kirigami variant, which has [since been abandonned](https://invent.kde.org/network/ruqola/-/commit/5460f61a52932f93916a0006def9b58f5c84ff81).
If you are a RocketChat user, please help us evaluate how well the current app works on a Mobile Linux device!