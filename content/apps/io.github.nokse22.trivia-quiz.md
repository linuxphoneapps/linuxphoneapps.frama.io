+++
title = "Trivia Quiz"
description = "Respond to endless questions"
aliases = ["games/io.github.nokse22.trivia-quiz/"]
date = 2024-02-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nokse",]
categories = [ "educational game", "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Open Trivia Database",]
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "ArcadeGame", "Game",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Nokse22/trivia-quiz"
homepage = "https://github.com/Nokse22/trivia-quiz"
bugtracker = "https://github.com/Nokse22/trivia-quiz/issues"
donations = "https://ko-fi.com/nokse22"
translations = "https://github.com/Nokse22/trivia-quiz/tree/master/po"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.nokse22.trivia-quiz"
screenshots = [ "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/resources/screenshot%201.png", "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/resources/screenshot%202.png", "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/resources/screenshot%203.png", "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/resources/screenshot%204.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/icons/hicolor/scalable/apps/io.github.nokse22.trivia-quiz.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.nokse22.trivia-quiz"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.nokse22.trivia-quiz"
flatpak_link = "https://flathub.org/apps/io.github.nokse22.trivia-quiz.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/io.github.nokse22.trivia-quiz.json"
snapcraft = "https://snapcraft.io/trivia-quiz"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "trivia-quiz",]
appstream_xml_url = "https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/io.github.nokse22.trivia-quiz.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-09-19"
repo_created_date = "2023-08-21"

+++


### Description

This app let's you respond to an endless stream of questions.
It uses Open Trivia database.
Questions are only available in English.

[Source](https://raw.githubusercontent.com/Nokse22/trivia-quiz/master/data/io.github.nokse22.trivia-quiz.metainfo.xml.in)
