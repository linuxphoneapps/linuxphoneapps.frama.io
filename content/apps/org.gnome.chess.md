+++
title = "GNOME Chess"
description = "Play the classic two-player board game of chess"
aliases = ["games/org.gnome.chess/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "BoardGame", "Game",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-chess/"
homepage = "https://gitlab.gnome.org/GNOME/gnome-chess/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-chess/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://discourse.gnome.org/tag/i18n"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Chess"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/main/data/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Chess.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Chess"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Chess"
flatpak_link = "https://flathub.org/apps/org.gnome.Chess.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-chess",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/main/data/org.gnome.Chess.appdata.xml.in"
reported_by = "Moxvallix"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-05-22"
feed_entry_id = "https://linuxphoneapps.org/games/org.gnome.chess/"

+++

### Description

GNOME Chess is a simple chess game. You can play against your computer at
three different difficulty levels, or against a friend at your computer.

Computer chess enthusiasts will appreciate GNOME Chess’s compatibility with
nearly all modern computer chess engines, and its ability to detect several
popular engines automatically if installed.

[Source](https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/main/data/org.gnome.Chess.appdata.xml.in)
