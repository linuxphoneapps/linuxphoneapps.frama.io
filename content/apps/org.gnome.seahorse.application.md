+++
title = "Passwords and Keys"
description = "Manage your passwords and encryption keys"
aliases = []
date = 2021-06-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "key management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "DesktopSettings", "Security", "Utility",]
programming_languages = [ "C", "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/seahorse"
homepage = "https://wiki.gnome.org/Apps/Seahorse"
bugtracker = "https://gitlab.gnome.org/GNOME/seahorse/issues"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/seahorse/-/raw/main/data/org.gnome.seahorse.Application.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/seahorse/raw/main/data/screenshot-default.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/seahorse/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.seahorse.Application.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.seahorse.Application"
scale_to_fit = "seahorse"
flathub = "https://flathub.org/apps/org.gnome.seahorse.Application"
flatpak_link = "https://flathub.org/apps/org.gnome.seahorse.Application.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "seahorse",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/seahorse/-/raw/main/data/org.gnome.Seahorse.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-05-22"

+++

### Description

Passwords and Keys is a GNOME application for managing encryption keys.

With Passwords and Keys you can create and manage PGP keys, create and manage SSH keys,
publish and retrieve keys from key servers, cache your passphrase so you
don’t have to keep typing it and backup your keys and keyring.

[Source](https://gitlab.gnome.org/GNOME/seahorse/-/raw/main/data/org.gnome.seahorse.Application.appdata.xml.in.in)

### Notice

Almost fits the screen perfectly with release 40, just the "Find Remote Keys" and the "Preferences" page don't work in portrait, but are fine in landscape.
