+++
title = "Uno Calculator"
description = "Linux port of the Microsoft Windows calculator app"
aliases = []
date = 2024-10-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Uno Platform",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "libcosmic", "iced",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Csharp", "Cpp",]
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/unoplatform/calculator/"
homepage = "https://platform.uno/uno-calculator/"
bugtracker = "https://github.com/unoplatform/calculator/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/uno.platform.uno-calculator"
screenshots = [ "https://dashboard.snapcraft.io/site_media/appmedia/2020/09/Linux-calc-01.png", "https://dashboard.snapcraft.io/site_media/appmedia/2020/09/Linux-calc-02.png", "https://uno-website-assets.s3.amazonaws.com/wp-content/uploads/2022/03/23133038/UnoCal_Linux.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/cosmic-utils/calculator/refs/heads/main/res/icons/hicolor/scalable/apps/icon.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "uno.platform.uno-calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/uno.platform.uno-calculator"
flatpak_link = "https://flathub.org/apps/uno.platform.uno-calculator.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/uno.platform.uno-calculator/refs/heads/master/uno.platform.uno-calculator.yaml"
snapcraft = "https://snapcraft.io/uno-calculator"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "uno-calculator",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/uno.platform.uno-calculator/refs/heads/master/uno.platform.uno-calculator.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/uno.platform.uno-calculator/"
latest_repo_commit = "2024-12-05"
repo_created_date = "2019-05-09"

+++

### Description

The open-source, Windows Calculator receives a cross-platform makeover from Uno Platform and now runs on Linux in addition to already running on Android, iOS, macOS and Web (WebAssembly)
This is a port of the offical Snap version.

[Source](https://raw.githubusercontent.com/flathub/uno.platform.uno-calculator/refs/heads/master/uno.platform.uno-calculator.appdata.xml)
