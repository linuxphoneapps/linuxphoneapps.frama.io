+++
title = "Mobile Config for Firefox"
description = "Mobile and privacy friendly configuration for Firefox (distro-independent)"
aliases = []
date = 2024-11-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "CSS",]
backends = [ "Android Translation Layer",]
services = []
packaged_in = [ "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "manjaro_stable", "manjaro_unstable", "postmarketos_master", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "CSS", "Javascript", "Shell",]
build_systems = [ "make",]
requires_internet = []
tags = [ "no icon",]

[extra]
repository = "https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox"
homepage = "https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox"
bugtracker = "https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/raw/master/org.postmarketos.mobile_config_firefox.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.mobile_config_firefox"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mobile-config-firefox", "firefox-esr-mobile-config",]
appstream_xml_url = "https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/raw/master/org.postmarketos.mobile_config_firefox.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.postmarketos.mobile_config_firefox/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2020-08-06"

+++

### Description

UI optimizations to allow using Firefox on mobile devices (i.e. small touch screen), including two finger zoom and enforcing of mobile user agent. Privacy enhancements such as disabled search suggestions, disabled Firefox studies, disabled Telemetry and DuckDuckGo as default search engine.

[Source](https://gitlab.postmarketos.org/postmarketOS/mobile-config-firefox/-/raw/master/org.postmarketos.mobile_config_firefox.metainfo.xml)

### Notice

Some distributions ship a patched mobile-config-firefox:

- [Debian patches](https://salsa.debian.org/DebianOnMobile-team/firefox-esr-mobile-config/-/tree/debian/latest/debian/patches)
- [PureOS patches](https://source.puri.sm/Librem5/debs/firefox-esr-mobile-config/-/tree/pureos/byzantium/debian/patches)
