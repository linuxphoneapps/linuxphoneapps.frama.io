+++
title = "Folio"
description = "Beautiful markdown note-taking app"
aliases = []
date = 2024-03-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Greg Ross",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "fedora_rawhide", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "TextEditor", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/toolstack/Folio"
homepage = "https://github.com/toolstack/Folio"
bugtracker = "https://github.com/toolstack/Folio/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/toolstack/Folio/main/data/app.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-desktop-dark.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-desktop.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-mobile.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-preferences.png", "https://raw.githubusercontent.com/toolstack/Folio/main/meta/folio-three-pane-mode.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/toolstack/Folio/main/data/icons/hicolor/scalable/apps/com.toolstack.Folio.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.toolstack.Folio"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.toolstack.Folio"
flatpak_link = "https://flathub.org/apps/com.toolstack.Folio.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/toolstack/Folio/main/com.toolstack.Folio.json"
snapcraft = "https://snapcraft.io/folio"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/toolstack/Folio/main/snap/snapcraft.yaml"
appimage_x86_64_url = "https://github.com/toolstack/Folio/releases"
appimage_aarch64_url = ""
repology = [ "folio",]
appstream_xml_url = "https://raw.githubusercontent.com/toolstack/Folio/main/data/app.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-04"
repo_created_date = "2024-02-11"

+++

### Description

Create notebooks and take notes in markdown

Some features include:

* Almost WYSIWYG markdown rendering
* Searchable through GNOME search
* Highlight and strikethrough text formatting
* App themeing based on notebook color
* Trash can
* Markdown document
* Optional line numbers
* Optional auto save
* Open links with Control-Click
* Link to other notes in Folio
* Automatically create links for bare URL's and e-mail addresses

[Source](https://raw.githubusercontent.com/toolstack/Folio/main/data/app.metainfo.xml.in)

### Notice

Folio is a maintained fork of [Paper](https://linuxphoneapps.org/apps/io.posidon.paper/).