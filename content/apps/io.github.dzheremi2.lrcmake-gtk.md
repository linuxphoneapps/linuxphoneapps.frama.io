+++
title = "Chronograph"
description = "Sync lyrics of your loved songs"
aliases = []
date = 2025-01-20
updated = 2025-01-20

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Dzheremi",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "lrclib.net",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Audio", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Dzheremi2/Chronograph/"
homepage = "https://github.com/Dzheremi2/Chronograph/"
bugtracker = "https://github.com/Dzheremi2/Chronograph/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/lrcmake/chronograph/"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.dzheremi2.lrcmake-gtk"
screenshots = [ "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/libB.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/libW.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/lrclibB.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/lrclibW.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/syncB.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/syncCB.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/syncCW.png", "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/docs/screenshots/syncW.png",]
screenshots_img = []
svg_icon_url = "https://github.com/Dzheremi2/Chronograph/blob/master/data/icons/hicolor/scalable/apps/io.github.dzheremi2.lrcmake-gtk.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.dzheremi2.lrcmake-gtk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.dzheremi2.lrcmake-gtk"
flatpak_link = "https://flathub.org/apps/io.github.dzheremi2.lrcmake-gtk.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/io.github.dzheremi2.lrcmake-gtk.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/data/io.github.dzheremi2.lrcmake-gtk.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.dzheremi2.lrcmake-gtk/"
latest_repo_commit = "2025-01-11"
repo_created_date = "2024-11-02"

+++

### Description

This app helps you to contribute to the large database of synced lyrics with beautiful GTK4 and LibAdwaita interface. It supports music in ogg, flac, mp3 and wav formats

[Source](https://raw.githubusercontent.com/Dzheremi2/Chronograph/refs/heads/master/data/io.github.dzheremi2.lrcmake-gtk.metainfo.xml.in)