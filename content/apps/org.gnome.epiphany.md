+++
title = "Web"
description = "Browse the web"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "webkit2gtk",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/epiphany"
homepage = "https://apps.gnome.org/Epiphany"
bugtracker = "https://gitlab.gnome.org/GNOME/epiphany/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/epiphany"
more_information = [ "https://puri.sm/posts/end-of-year-librem-5-update/", "https://apps.gnome.org/Epiphany/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/main/data/org.gnome.Epiphany.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/epiphany/raw/HEAD/data/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.epiphany/1.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/2.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/3.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/4.png",]
svg_icon_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Epiphany.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Epiphany"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Epiphany"
flatpak_link = "https://flathub.org/apps/org.gnome.Epiphany.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/org.gnome.Epiphany.json"
snapcraft = "https://snapcraft.io/epiphany"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "epiphany-browser",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/main/data/org.gnome.Epiphany.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-05-25"

+++

### Description

The web browser for GNOME, featuring tight integration with the desktop
and a simple and intuitive user interface that allows you to focus on your
web pages. If you’re looking for a simple, clean, beautiful view of the
web, this is the browser for you.

Web is often referred to by its code name, Epiphany.

[Source](https://gitlab.gnome.org/GNOME/epiphany/-/raw/main/data/org.gnome.Epiphany.appdata.xml.in.in)

### Notice

GTK3/libhandy before release 44.