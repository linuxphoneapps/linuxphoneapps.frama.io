+++
title = "Ear Tag"
description = "Edit audio file tags"
aliases = []
date = 2022-09-08
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "knuxify",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_unstable", "devuan_unstable", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Audio", "AudioVideo", "AudioVideoEditing",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/eartag/"
homepage = "https://gitlab.gnome.org/World/eartag"
bugtracker = "https://gitlab.gnome.org/World/eartag/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/eartag/"
more_information = [ "https://apps.gnome.org/EarTag/",]
summary_source_url = "https://gitlab.gnome.org/World/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in"
screenshots = [ "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-1_orig.png", "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-2_orig.png", "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-3_orig.png", "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-4_orig.png", "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-5_orig.png", "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-6_orig.png", "https://dl.flathub.org/media/app/drey/EarTag/877865aa7ae434a5416f9ad6a795166c/screenshots/image-7_orig.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.EarTag"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.EarTag"
flatpak_link = "https://flathub.org/apps/app.drey.EarTag.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "eartag",]
appstream_xml_url = "https://gitlab.gnome.org/World/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-02-10"

+++

### Description

Ear Tag is a simple audio file tag editor. It is primarily geared towards making quick edits or bulk-editing tracks in albums/EPs. Unlike other tagging programs, Ear Tag does not require the user to set up a music library folder. It can:

* Edit tags of MP3, WAV, M4A, FLAC, OGG and WMA files
* Modify metadata of multiple files at once
* Rename files using information from present tags
* Identify files using AcoustID

Network access is only used for the "Identify selected files" option.

[Source](https://gitlab.gnome.org/World/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in)