+++
title = "Mt"
description = "A terminal written in Rust and gtk-rs"
aliases = []
date = 2021-01-17
updated = 2024-10-12

[taxonomies]
project_licenses = [ "BSD-3-Clause",]
metadata_licenses = []
app_author = [ "miridyan",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "System", "TerminalEmulator",]
programming_languages = [ "Rust",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/mkditto/mt"
homepage = ""
bugtracker = "https://gitlab.com/mkditto/mt/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://fosstodon.org/@linmob/105572758025488468",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/mkditto/mt/-/raw/main/data/icons/scalable/apps/com.gitlab.miridyan.Mt.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.miridyan.Terminal"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mt",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2022-09-03"
repo_created_date = "2019-07-31"

+++

### Notice

Last commit September 18th, 2021.