+++
title = "Turn On"
description = "Turn on devices in your network"
aliases = []
date = 2024-10-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sebastian Wiesner",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/swsnr/turnon"
homepage = "https://github.com/swsnr/turnon"
bugtracker = "https://github.com/swsnr/turnon/discussions/categories/ideas-issues"
donations = ""
translations = "https://translate.codeberg.org/engage/de-swsnr-turnon/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/resources/de.swsnr.turnon.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/screenshots/edit-device.png", "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/screenshots/list-of-devices.png", "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/screenshots/list-of-discovered-devices.png", "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/screenshots/start-page.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/resources/icons/scalable/apps/de.swsnr.turnon.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "de.swsnr.turnon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.swsnr.turnon"
flatpak_link = "https://flathub.org/apps/de.swsnr.turnon.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/flatpak/de.swsnr.turnon.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/resources/de.swsnr.turnon.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/de.swsnr.turnon/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-09-13"

+++

### Description

Conveniently turn on devices in your network, such as a NAS or a gaming console,
using Wake On LAN (WoL).

Features:

* Add devices to turn on.
* Discover devices in the local network.
* Monitor device status.
* Turn on devices with magic Wake On LAN (WoL) packets.
* Turn on devices from GNOME Shell search.
* Turn on devices from the command line.

[Source](https://raw.githubusercontent.com/swsnr/turnon/refs/heads/main/resources/de.swsnr.turnon.metainfo.xml.in)
