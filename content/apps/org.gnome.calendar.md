+++
title = "Calendar"
description = "Manage your schedule"
aliases = []
date = 2022-09-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Calendar", "Core", "Office",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calendar"
homepage = "https://apps.gnome.org/Calendar/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calendar/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-calendar/"
more_information = [ "https://apps.gnome.org/Calendar/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/appdata/org.gnome.Calendar.appdata.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/calendar/calendar-event.png", "https://static.gnome.org/appdata/gnome-43/calendar/calendar-month.png", "https://static.gnome.org/appdata/gnome-43/calendar/calendar-week.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.calendar/1.png", "https://img.linuxphoneapps.org/org.gnome.calendar/2.png", "https://img.linuxphoneapps.org/org.gnome.calendar/3.png", "https://img.linuxphoneapps.org/org.gnome.calendar/4.png", "https://img.linuxphoneapps.org/org.gnome.calendar/5.png",]
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Calendar.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Calendar"
scale_to_fit = "org.gnome.Calendar"
flathub = "https://flathub.org/apps/org.gnome.Calendar"
flatpak_link = "https://flathub.org/apps/org.gnome.Calendar.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/build-aux/flatpak/org.gnome.Calendar.json"
snapcraft = "https://snapcraft.io/gnome-calendar"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-calendar",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/src/appdata/org.gnome.Calendar.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2017-11-24"

+++

### Description

GNOME Calendar is an elegant tool to help you plan and stay on top of your schedule for
upcoming days and weeks, whether your months are filled with meetings and appointments,
public events, travel logistics, or other commitments in your life.

Easily create and manage unlimited calendars and events with GNOME Calendar's infinite
scrolling month view, week timetable, and scheduling list view.

Integrating the latest GNOME technologies and design best practices, GNOME Calendar builds on
top of the mature Evolution data server to manage large quantities of events and to provide
integration with other applications. You can add calendars from various online calendaring
services (like NextCloud, Google Calendar, and CalDAV / WebDAV servers), enabling you to
synchronize your events across multiple devices and platforms. GNOME Calendar also supports
local and offline calendaring, so you can use it anywhere and anytime while travelling
(or after civilization's collapse.)

[Source](https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/src/appdata/org.gnome.Calendar.metainfo.xml.in.in)

### Notice

With release 43, this app has been redesigned to be mobile friendly. In initial testing (flathub), it misses this goal by a few pixels (see first screenshot). Previously, Purism had created an adaptive fork of GNOME Calendar 41 for the Librem 5. With GNOME 45, it fits the phone screen without issues.