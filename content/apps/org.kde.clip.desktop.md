+++
title = "Clip"
description = "Video Player"
aliases = [ "apps/org.maui.clip/", "apps/org.kde.clip/",]
date = 2020-12-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = [ "mpv",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Player", "Qt", "Video",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/maui/clip/"
homepage = "https://mauikit.org/apps/clip/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=clip"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/maui-clip/-/raw/master/org.kde.clip.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/clip/clip-stable.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/maui-clip/-/raw/master/src/assets/clip.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.clip.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-clip",]
appstream_xml_url = "https://invent.kde.org/maui/maui-clip/-/raw/master/org.kde.clip.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.clip/"
latest_repo_commit = "2024-12-31"
repo_created_date = "2020-04-15"

+++

### Description

Clip is a video player and collection manager.

[Source](https://invent.kde.org/maui/maui-clip/-/raw/master/org.kde.clip.appdata.xml)