+++
title = "Play Timer"
description = "Native-feeling timers"
aliases = []
date = 2024-11-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "Artem Suprun",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Go",]
build_systems = [ "go",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/efogdev/mpris-timer"
homepage = "https://github.com/efogdev/mpris-timer"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.efogdev.mpris-timer"
screenshots = [ "https://github.com/user-attachments/assets/3c8d7055-e10b-4730-8468-590ab080f643", "https://github.com/user-attachments/assets/55396255-d1e1-4c49-9201-b8c9aab0b93f", "https://github.com/user-attachments/assets/77d107f2-7454-4f36-9ca2-580df93c56c1", "https://github.com/user-attachments/assets/926d47ef-afa2-4190-aada-a09bdb499f59", "https://github.com/user-attachments/assets/cb680714-5b62-4044-adda-b17794d59a5a",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/efogdev/mpris-timer/refs/heads/main/internal/ui/res/icon.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.efogdev.mpris-timer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.efogdev.mpris-timer"
flatpak_link = "https://flathub.org/apps/io.github.efogdev.mpris-timer.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/efogdev/mpris-timer/refs/heads/main/io.github.efogdev.mpris-timer.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/efogdev/mpris-timer/refs/heads/main/misc/io.github.efogdev.mpris-timer.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.efogdev.mpris-timer/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-09-23"

+++

### Description

Timer app with seamless integration accomplished by pretending to be a media player.
Ultimately, serves the only purpose — to start a timer quickly and efficiently.
Notifications included!

Play Timer aims to be as keyboard friendly as possible.
Use navigation keys (arrows, tab, shift+tab, space, enter) or start inputting numbers right away.

The app is CLI-friendly, try `flatpak run io.github.efogdev.mpris-timer -help`!

[Source](https://raw.githubusercontent.com/efogdev/mpris-timer/refs/heads/main/misc/io.github.efogdev.mpris-timer.metainfo.xml)