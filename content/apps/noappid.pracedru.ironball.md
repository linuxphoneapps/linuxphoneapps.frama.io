+++
title = "Ironball"
description = "Mobile game inspired by the classic Speedball game for Amiga/C64 etc."
aliases = ["games/noappid.pracedru.ironball/"]
date = 2019-02-16
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = ["pracedru",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "Web technologies",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/pracedru/ironball"
homepage = "https://www.pracedru.dk:8888/index.html"
bugtracker = "https://github.com/pracedru/ironball/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/pracedru/ironball/"
screenshots = [ "https://github.com/pracedru/ironball/",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.pracedru.ironball/"
latest_repo_commit = "2019-02-22"
repo_created_date = "2018-08-12"

+++
