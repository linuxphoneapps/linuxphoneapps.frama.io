+++
title = "Bluetooth Connect"
description = "Scan, add, edit and connect bluetooth devices on your linux phone"
aliases = [ "apps/noappid.andym48.pinephone_btconnect/",]
date = 2023-09-22
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "AndyM48",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Tk",]
backends = [ "bluez/bluez-utils",]
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Tcl",]
build_systems = [ "custom",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/AndyM48/pinephone_btconnect"
homepage = ""
bugtracker = "https://gitlab.com/AndyM48/pinephone_btconnect/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/AndyM48/pinephone_btconnect"
screenshots = [ "https://gitlab.com/AndyM48/pinephone_btconnect/-/raw/main/screenshots/list.png", "https://gitlab.com/AndyM48/pinephone_btconnect/-/raw/main/screenshots/edit.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "AndyM48"
updated_by = "check_via_git_api"
latest_repo_commit = "2023-11-15"
repo_created_date = "2023-09-22"

+++

### Description

- Check that bluetooth is started

- List known bluetooth devices

- Connect a bluetooth device

- Scan for and pair bluetooth devices

- Edit devices to trust, block, disconnect and remove devices.