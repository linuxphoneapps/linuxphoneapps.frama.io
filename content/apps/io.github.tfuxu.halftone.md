+++
title = "Halftone"
description = "Dither your images"
aliases = []
date = 2023-11-18
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "tfuxu",]
categories = [ "image processing",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "imagemagick",]
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Graphics", "ImageProcessing", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/tfuxu/Halftone"
homepage = "https://github.com/tfuxu/halftone"
bugtracker = "https://github.com/tfuxu/halftone/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/io.github.tfuxu.Halftone.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-1.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-2.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-3.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-4.png", "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/screenshots/halftone-5.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.tfuxu.Halftone"
scale_to_fit = "io.github.tfuxu.Halftone"
flathub = "https://flathub.org/apps/io.github.tfuxu.Halftone"
flatpak_link = "https://flathub.org/apps/io.github.tfuxu.Halftone.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/tfuxu/Halftone/main/build-aux/flatpak/io.github.tfuxu.Halftone.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "halftone",]
appstream_xml_url = "https://raw.githubusercontent.com/tfuxu/Halftone/main/data/io.github.tfuxu.Halftone.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-10"
repo_created_date = "2023-04-29"

+++


### Description

A simple Libadwaita app for lossy image compression using dithering technique.
Give your images a pixel art-like style and reduce the file size in the process with Halftone.

[Source](https://raw.githubusercontent.com/tfuxu/Halftone/main/data/io.github.tfuxu.Halftone.metainfo.xml.in.in)