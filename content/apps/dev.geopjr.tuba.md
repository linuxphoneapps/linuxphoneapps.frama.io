+++
title = "Tuba"
description = "Browse the Fediverse"
aliases = [ "apps/dev.geopjr.tooth/",]
date = 2022-11-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos \"GeopJr\" Paterakis",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Mastodon", "Akkoma", "GoToSocial",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "editors choice", "GNOME Circle",]

[extra]
repository = "https://github.com/GeopJr/Tuba/"
homepage = "https://tuba.geopjr.dev/"
bugtracker = "https://github.com/GeopJr/Tuba/issues"
donations = "https://geopjr.dev/donate"
translations = "https://hosted.weblate.org/engage/tuba/"
more_information = [ "https://fosstodon.org/@bragefuglseth/113030003231359426", "https://apps.gnome.org/Tuba/",]
summary_source_url = "https://github.com/GeopJr/Tuba"
screenshots = [ "https://media.githubusercontent.com/media/GeopJr/Tuba/main/data/screenshots/screenshot-1.png", "https://media.githubusercontent.com/media/GeopJr/Tuba/main/data/screenshots/screenshot-2.png", "https://media.githubusercontent.com/media/GeopJr/Tuba/main/data/screenshots/screenshot-3.png", "https://media.githubusercontent.com/media/GeopJr/Tuba/main/data/screenshots/screenshot-4.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/GeopJr/Tuba/refs/heads/main/data/icons/color.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "dev.geopjr.Tuba"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.geopjr.Tuba"
flatpak_link = "https://flathub.org/apps/dev.geopjr.Tuba.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/GeopJr/Tuba/main/build-aux/dev.geopjr.Tuba.Devel.json"
snapcraft = "https://snapcraft.io/tuba"
snap_link = "https://nightly.link/GeopJr/Tuba/workflows/build/main/snap-aarch64.zip"
snap_recipe = "https://raw.githubusercontent.com/GeopJr/Tuba/main/build-aux/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tuba",]
appstream_xml_url = "https://raw.githubusercontent.com/GeopJr/Tuba/main/data/dev.geopjr.Tuba.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-04"
repo_created_date = "2022-11-13"

+++

### Description

Explore the federated social web with Tuba for GNOME. Stay connected to your favorite communities, family and friends with support for popular Fediverse platforms like Mastodon, GoToSocial, Akkoma & more!

The Fediverse is a decentralized social network that consists of multiple interconnected platforms and communities, allowing users to communicate and share content across different networks and servers. It promotes user privacy and data ownership, and offers an alternative to centralized social media platforms.

[Source](https://raw.githubusercontent.com/GeopJr/Tuba/main/data/dev.geopjr.Tuba.metainfo.xml.in)

### Notice

Continuation of [Tootle](https://linuxphoneapps.org/apps/com.github.bleakgrey.tootle/), was called Tooth before its initial release.