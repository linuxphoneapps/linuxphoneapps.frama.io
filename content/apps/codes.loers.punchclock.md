+++
title = "Punchclock"
description = "Track time for your tasks."
aliases = []
date = 2023-02-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Florian Loers",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "Office", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo", "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "Unsupported custom Appstream Metadata format",]

[extra]
repository = "https://gitlab.com/floers/punchclock"
homepage = "https://www.gitlab.com/floers/punchclock"
bugtracker = "https://www.gitlab.com/floers/punchclock"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/codes.loers.Punchclock"
screenshots = [ "https://gitlab.com/floers/punchclock/-/raw/main/screenshots/large.png", "https://gitlab.com/floers/punchclock/-/raw/main/screenshots/small.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/codes.loers.punchclock/1.png", "https://img.linuxphoneapps.org/codes.loers.punchclock/2.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "codes.loers.Punchclock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/codes.loers.Punchclock"
flatpak_link = "https://flathub.org/apps/codes.loers.Punchclock.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "script"
latest_repo_commit = "2024-03-05"
repo_created_date = "2022-12-13"

+++

### Description

Track time for your tasks.

[Source](https://gitlab.com/floers/punchclock/-/raw/main/App.toml)