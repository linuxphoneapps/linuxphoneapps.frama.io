+++
title = "1010! Klooni"
description = "libGDX game based on the original 1010!"
aliases = ["games/dev.lonami.klooni/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = ["moxvallix",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = ["Game", "LogicGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/moxvallix/LinMobGames"
homepage = "https://lonami.dev/klooni/"
bugtracker = "https://github.com/moxvallix/LinMobGames/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/LonamiWebs/Klooni1010"
screenshots = [ "https://f-droid.org/en/packages/dev.lonami.klooni/",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "dev.lonami.klooni"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "klooni1010",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/dev.lonami.klooni/"
latest_repo_commit = "2021-08-25"
repo_created_date = "2021-05-06"

+++

### Notice

Patched to run by Moxvallix.
