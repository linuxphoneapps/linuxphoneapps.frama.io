+++
title = "Gapless"
description = "Play your music elegantly"
aliases = []
date = 2022-06-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nanling",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_21", "alpine_edge", "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Audio", "AudioVideo", "Music",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/neithern/g4music"
homepage = "https://gitlab.gnome.org/neithern/g4music"
bugtracker = "https://gitlab.gnome.org/neithern/g4music/issues"
donations = ""
translations = "https://l10n.gnome.org/module/g4music"
more_information = []
summary_source_url = "https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/neithern/screenshots/-/raw/main/g4music/albums.png", "https://gitlab.gnome.org/neithern/screenshots/-/raw/main/g4music/playing.png", "https://gitlab.gnome.org/neithern/screenshots/-/raw/main/g4music/playlist.png", "https://gitlab.gnome.org/neithern/screenshots/-/raw/main/g4music/window.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.neithern.g4music"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.neithern.g4music"
flatpak_link = "https://flathub.org/apps/com.github.neithern.g4music.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "g4music", "gapless",]
appstream_xml_url = "https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2022-06-06"

+++

### Description

Gapless (AKA: G4Music) is a light weight music player written in GTK4, focuses on large music collection.

Features

* Supports most music file types, Samba and any other remote protocols (depends on GIO and GStreamer).
* Fast loading and parsing thousands of music files in very few seconds, monitor local changes.
* Low memory usage for large music collection with album covers (embedded and external), no thumbnail caches to store.
* Group and sorts by album/artist/title, shuffle list, full-text searching.
* Fluent adaptive user interface for different screen (Desktop, Tablet, Mobile).
* Gaussian blurred cover as background, follows GNOME light/dark mode.
* Supports creating and editing playlists, drag cover to change order or add to another playlist.
* Supports drag and drop with other apps.
* Supports audio peaks visualizer.
* Supports gapless playback.
* Supports normalizing volume with ReplayGain.
* Supports specified audio sink.
* Supports MPRIS control.

[Source](https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.metainfo.xml.in)