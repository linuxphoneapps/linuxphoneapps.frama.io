+++
title = "KleverNotes"
description = "Take and manage your notes"
aliases = []
date = 2024-01-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "KDE",]
categories = [ "note taking", "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_41", "fedora_rawhide", "flathub", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Office",]
programming_languages = [ "C", "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/office/klevernotes"
homepage = "https://invent.kde.org/office/klevernotes"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=KleverNotes"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/klevernotes/",]
summary_source_url = "https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/klevernotes/main_note_desktop.png", "https://cdn.kde.org/screenshots/klevernotes/main_note_mobile.png", "https://cdn.kde.org/screenshots/klevernotes/painting.png", "https://cdn.kde.org/screenshots/klevernotes/todo_desktop.png",]
screenshots_img = []
all_features_touch = false
svg_icon_url = "https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.svg"
intended_for_mobile = false
app_id = "org.kde.klevernotes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.klevernotes"
flatpak_link = "https://flathub.org/apps/org.kde.klevernotes.flatpakref"
flatpak_recipe = "https://invent.kde.org/office/klevernotes/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "klevernotes",]
appstream_xml_url = "https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2023-03-11"

+++

### Description

KleverNotes is a note taking and management application for your mobile and desktop devices. It uses markdown and allow you to preview your content.

[Source](https://invent.kde.org/office/klevernotes/-/raw/master/org.kde.klevernotes.metainfo.xml)

### Notice

KDE incubator project. [Unofficial APKBUILD](https://framagit.org/linmobapps/apkbuilds/-/blob/master/klevernotes/APKBUILD).