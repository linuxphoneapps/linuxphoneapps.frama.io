+++
title = "Concessio"
description = "Understand file permissions"
aliases = []
date = 2024-08-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ronnie Nissan",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "System", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ronniedroid/concessio"
homepage = "https://github.com/ronniedroid/concessio"
bugtracker = "https://github.com/ronniedroid/concessio/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.ronniedroid.concessio"
screenshots = [ "https://raw.githubusercontent.com/ronniedroid/concessio/main/data/screenshots/help-page.png", "https://raw.githubusercontent.com/ronniedroid/concessio/main/data/screenshots/main-page.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/ronniedroid/concessio/main/data/icons/io.github.ronniedroid.concessio.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.ronniedroid.concessio"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.ronniedroid.concessio"
flatpak_link = "https://flathub.org/apps/io.github.ronniedroid.concessio.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/ronniedroid/concessio/main/io.github.ronniedroid.concessio.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/ronniedroid/concessio/main/data/io.github.ronniedroid.concessio.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.ronniedroid.concessio/"
latest_repo_commit = "2024-11-25"
repo_created_date = "2024-07-22"

+++


### Description

Concessio helps you understand and convert between unix permissions representations

* Convert between symbolic and numeric representations of UNIX file permissions
* Use toggle buttons to update the symbolic and numeric fields
* Open a file to read it's permissions and convert them
* Open the help dialog to read and understand the UNIX permissions system

[Source](https://raw.githubusercontent.com/ronniedroid/concessio/main/data/io.github.ronniedroid.concessio.metainfo.xml)