+++
title = "boxclip"
description = "\"Boxclip\", a 2D platformer engine, with an emphasis on interactive map editing"
aliases = [ "games/noappid.glitchapp.boxclip-mobile/", "games/noappid.glitchapp.spinny-the-runner/"]
date = 2024-07-27
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "glitchapp",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release",]
frameworks = [ "SDL2",]
backends = [ "LÖVE2D",]
services = []
packaged_in = []
freedesktop_categories = [ "Game", "ActionGame",]
programming_languages = [ "Lua",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://codeberg.org/glitchapp/Spinny-the-runner"
homepage = "https://glitchapp.codeberg.page/_boxclip/"
bugtracker = "https://codeberg.org/glitchapp/Spinny-the-runner/issues"
donations = ""
translations = ""
more_information = [ "https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3Cd01a5ad1-06e4-4d34-854d-dba5171b3f5d@disroot.org%3E",]
summary_source_url = "https://codeberg.org/glitchapp/boxclip-mobile"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/glitchapp/Spinny-the-runner/raw/branch/main/flatpak/flatpak-org.flatpak.Spinny.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "glichapp"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.glitchapp.boxclip-mobile/"
latest_repo_commit = "2025-01-05"
repo_created_date = "2025-01-05"

+++

### Description

Boxclip is a 2D platformer engine, with an emphasis on interactive map editing. Built using the LÖVE2D framework. Maps can be created with the custom built-in map editor. Simply drop and place entities into the world to create a level.

[Source](https://glitchapp.codeberg.page/_boxclip/)




### Notice

The [repo contains an APKBUILD to make building for postmarketOS/Alpine more easy](https://codeberg.org/glitchapp/boxclip-mobile/src/branch/main/apk/APKBUILD).
