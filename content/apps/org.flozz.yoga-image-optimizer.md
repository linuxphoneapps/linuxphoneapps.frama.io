+++
title = "YOGA Image Optimizer"
description = "Convert and optimize JPEG, PNG and WebP images"
aliases = []
date = 2021-07-22
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Fabien LOISON",]
categories = [ "utilities",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Graphics", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/flozz/yoga-image-optimizer"
homepage = "https://yoga.flozz.org/"
bugtracker = "https://github.com/flozz/yoga-image-optimizer/issues"
donations = "https://github.com/flozz/yoga-image-optimizer#supporting-this-project"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flozz/yoga-image-optimizer/master/linuxpkg/org.flozz.yoga-image-optimizer.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/flozz/yoga-image-optimizer/master/screenshot.png", "https://yoga.flozz.org/images/v1.2.0/yoga-image-optimizer_v1.2_optimizing.png", "https://yoga.flozz.org/images/v1.2.0/yoga-image-optimizer_v1.2_output-options.png", "https://yoga.flozz.org/images/v1.2.0/yoga-image-optimizer_v1.2_settings.png", "https://yoga.flozz.org/images/v1.2.0/yoga-image-optimizer_v1.2_transformations-options.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/flozz/yoga-image-optimizer/refs/heads/master/yoga_image_optimizer/data/images/icon.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.flozz.yoga-image-optimizer"
scale_to_fit = "org.flozz.yoga-image-optimizer"
flathub = "https://flathub.org/apps/org.flozz.yoga-image-optimizer"
flatpak_link = "https://flathub.org/apps/org.flozz.yoga-image-optimizer.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/org.flozz.yoga-image-optimizer/refs/heads/master/org.flozz.yoga-image-optimizer.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/flozz/yoga-image-optimizer/master/linuxpkg/org.flozz.yoga-image-optimizer.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-10-29"
repo_created_date = "2021-02-21"

+++


### Description

YOGA Image Optimizer is a free tool to convert and optimize images. It currently
supports JPEG, PNG and WebP as output formats and much more image formats
are supported as input.

With YOGA, you will be able to save about 30 % of space on JPEGs and 20 % on PNGs.
On WebP images, you will only save few percents... but converting a
JPEG to a lossy WebP can reduce image size to a half and converting
a PNG to a lossy WebP can save you 35 % on average.

Please note that image optimization can be slow and may require a lot of RAM, especially
for JPEGs. For example, the JPEG encoder needs 300 MB of RAM and 3 min
per Mpix (PNG and WebP encoders are less resource hungry).

[Source](https://raw.githubusercontent.com/flozz/yoga-image-optimizer/master/linuxpkg/org.flozz.yoga-image-optimizer.metainfo.xml)