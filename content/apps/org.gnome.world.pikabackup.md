+++
title = "Pika Backup"
description = "Keep your data safe"
aliases = []
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Small Mammal Collective",]
categories = [ "backup", "system utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "borg",]
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed",]
freedesktop_categories = [ "Archiving", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/pika-backup/"
homepage = "https://apps.gnome.org/PikaBackup/"
bugtracker = "https://gitlab.gnome.org/World/pika-backup/-/issues"
donations = "https://opencollective.com/pika-backup"
translations = "https://l10n.gnome.org/module/pika-backup/"
more_information = [ "https://apps.gnome.org/PikaBackup/",]
summary_source_url = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/pika-backup/uploads/5c55d06042a0ffe9e6b7b9f0d3fd43cc/screenshot-1.png", "https://gitlab.gnome.org/World/pika-backup/uploads/d817a95f0e8dc9cf335609259eddef74/screenshot-2.png", "https://gitlab.gnome.org/World/pika-backup/uploads/e02e4444094858631bdd537cbfb2b6ba/screenshot-3.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.World.PikaBackup"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.PikaBackup"
flatpak_link = "https://flathub.org/apps/org.gnome.World.PikaBackup.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/build-aux/org.gnome.World.PikaBackup.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pika-backup",]
appstream_xml_url = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-06"
repo_created_date = "2020-04-23"

+++

### Description

Doing backups the easy way. Plugin your USB drive and let the Pika do the rest for you.

* Create backups locally and remotely
* Set a schedule for regular backups
* Save time and disk space because Pika Backup does not need to copy known data again
* Encrypt your backups
* List created archives and browse through their contents
* Recover files or folders via your file browser

Pika Backup is designed to save your personal data and does not support complete system recovery. Pika Backup is powered by the well-tested BorgBackup software.

[Source](https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in)