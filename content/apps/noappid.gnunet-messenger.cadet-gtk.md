+++
title = "cadet-gtk"
description = "A GTK based GUI for the CADET subsystem of GNUnet."
aliases = []
date = 2020-10-11
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "gnunet-messenger",]
categories = [ "chat",]
mobile_compatibility = [ "needs testing",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "GNUnet CADET",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "P2P",]
programming_languages = [ "C",]
build_systems = [ "custom",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.com/gnunet-messenger/cadet-gtk"
homepage = ""
bugtracker = "https://gitlab.com/gnunet-messenger/cadet-gtk/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/gnunet-messenger/cadet-gtk"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "cadet-gtk",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2021-12-16"
repo_created_date = "2020-04-12"

+++