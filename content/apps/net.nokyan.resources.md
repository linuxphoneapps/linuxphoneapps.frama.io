+++
title = "Resources"
description = "Keep an eye on system resources"
aliases = []
date = 2023-11-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "nokyan",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "System",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/nokyan/resources"
homepage = "https://apps.gnome.org/app/net.nokyan.Resources/"
bugtracker = "https://github.com/nokyan/resources/issues"
donations = ""
translations = "https://github.com/nokyan/resources/tree/main/po"
more_information = [ "https://apps.gnome.org/Resources/",]
summary_source_url = "https://raw.githubusercontent.com/nokyan/resources/main/data/net.nokyan.Resources.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/1.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/2.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/3.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/4.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/5.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/6.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/7.png", "https://raw.githubusercontent.com/nokyan/resources/main/data/resources/screenshots/8.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/nokyan/resources/main/data/icons/net.nokyan.Resources.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "net.nokyan.Resources"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.nokyan.Resources"
flatpak_link = "https://flathub.org/apps/net.nokyan.Resources.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/nokyan/resources/main/build-aux/net.nokyan.Resources.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "resources",]
appstream_xml_url = "https://raw.githubusercontent.com/nokyan/resources/main/data/net.nokyan.Resources.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2022-08-29"

+++

### Description

Resources allows you to check the utilization of your system resources and control your running processes and apps. It’s designed to be user-friendly and feel right at home on a modern desktop by using GNOME’s libadwaita.

Resources supports monitoring the following components:

* CPU
* Memory
* GPU
* Network Interfaces
* Storage Devices
* Batteries

[Source](https://raw.githubusercontent.com/nokyan/resources/main/data/net.nokyan.Resources.metainfo.xml.in.in)