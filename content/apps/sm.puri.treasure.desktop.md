+++
title = "Treasure"
description = "An application example, implementing a simple guessing game."
aliases = [ "games/sm.puri.treasure/", "games/sm.puri.treasure.desktop/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = ["david.boddie",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://source.puri.sm/david.boddie/treasure"
homepage = ""
bugtracker = "https://source.puri.sm/david.boddie/treasure/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/david.boddie/treasure"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "sm.puri.treasure.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://source.puri.sm/david.boddie/treasure/-/raw/master/data/sm.puri.treasure.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/sm.puri.treasure/"
latest_repo_commit = "2019-03-21"
repo_created_date = "2018-12-02"

+++

### Description



An application example, implementing a simple guessing game.

[Source](https://source.puri.sm/david.boddie/treasure)
