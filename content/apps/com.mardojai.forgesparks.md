+++
title = "Forge Sparks"
description = "Get Git forges notifications"
aliases = []
date = 2024-02-11
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rafael Mardojai CM",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Forgejo", "Gitea", "GitHub",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Network",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/rafaelmardojai/forge-sparks"
homepage = "https://github.com/rafaelmardojai/forge-sparks"
bugtracker = "https://github.com/rafaelmardojai/forge-sparks/issues"
donations = "https://mardojai.com/donate/"
translations = "https://hosted.weblate.org/engage/forge-sparks/"
more_information = [ "https://apps.gnome.org/ForgeSparks/",]
summary_source_url = "https://flathub.org/apps/com.mardojai.ForgeSparks"
screenshots = [ "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/screenshots/1.png", "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/screenshots/2.png", "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/screenshots/3.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/com.mardojai.ForgeSparks.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.mardojai.ForgeSparks"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.mardojai.ForgeSparks"
flatpak_link = "https://flathub.org/apps/com.mardojai.ForgeSparks.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/com.mardojai.ForgeSparksDevel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/com.mardojai.ForgeSparks.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-28"
repo_created_date = "2022-11-16"

+++

### Description

Simple notifier app with support for GitLab, Github, Gitea and Forgejo.

[Source](https://raw.githubusercontent.com/rafaelmardojai/forge-sparks/main/data/com.mardojai.ForgeSparks.metainfo.xml.in.in)