+++
title = "ThiefMD"
description = "The markdown editor worth stealing."
aliases = []
date = 2020-11-03
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "MIT",]
app_author = [ "kmwallio",]
categories = [ "text editor",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Office", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/kmwallio/ThiefMD"
homepage = "https://thiefmd.com"
bugtracker = "https://github.com/kmwallio/thiefmd/issues"
donations = "https://github.com/sponsors/kmwallio"
translations = "https://poeditor.com/join/project?hash=iQkE5oTIOV"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml"
screenshots = [ "https://thiefmd.com/images/grammar-notes.png", "https://thiefmd.com/images/preview.png", "https://thiefmd.com/images/theme_preferences.png", "https://thiefmd.com/images/thief_window.png", "https://thiefmd.com/images/thiefmd-screenplay.png", "https://thiefmd.com/images/write-good.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/kmwallio/ThiefMD/refs/heads/master/data/icons/128/com.github.kmwallio.thiefmd.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.kmwallio.thiefmd"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.kmwallio.thiefmd"
flatpak_link = "https://flathub.org/apps/com.github.kmwallio.thiefmd.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/kmwallio/ThiefMD/refs/heads/master/flatpak/com.github.kmwallio.thiefmd.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "thiefmd",]
appstream_xml_url = "https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-04-06"
repo_created_date = "2018-06-12"

+++

### Description

Keep your Markdown managed. Write epic tales, a novel, that screen play, keep a journal, or finally write that book report.

ThiefMD is a Markdown Editor providing an easy way to organize, format, and compile your markdown documents. When you're ready to Publish, go to ePub, PDF, Office, Ghost, WordPress, Write-Freely, or more.

Import from and Export to Office, ePUB, HTML, and more. ThiefMD is your one stop shop for making and managing markdown mischief.

Working on a screenplay? ThiefMD can handle and combine Fountain files as well. Write your novel and prepare for that movie deal all from the same app.

[Source](https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml)

### Notice

The start dialog does not fit the screen properly, the editor does.