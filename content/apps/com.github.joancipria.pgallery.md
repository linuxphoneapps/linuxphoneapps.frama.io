+++
title = "PGallery"
description = "A dead simple photo gallery made for Pinephone. Built with Vala and Gtk"
aliases = []
date = 2020-12-12
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "joancipria",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/joancipria/pgallery"
homepage = ""
bugtracker = "https://github.com/joancipria/pgallery/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/joancipria/pgallery"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.joancipria.pgallery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2020-12-22"
repo_created_date = "2020-11-30"

+++

### Notice

Archived on Oct 4, 2021.