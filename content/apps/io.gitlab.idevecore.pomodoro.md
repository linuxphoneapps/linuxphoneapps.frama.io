+++
title = "Pomodoro"
description = "Pomodoro is a productivity-focused timer"
aliases = []
date = 2023-12-04
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ideve Core",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Clock", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/idevecore/pomodoro"
homepage = "https://gitlab.com/idevecore/pomodoro"
bugtracker = "https://gitlab.com/idevecore/pomodoro/-/issues"
donations = "https://ko-fi.com/idevecore"
translations = "https://hosted.weblate.org/engage/pomodoro"
more_information = []
summary_source_url = "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/io.gitlab.idevecore.Pomodoro.appdata.xml.in.in"
screenshots = [ "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/01.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/02.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/03.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/04.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/05.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/06.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/07.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/08.png", "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/screenshots/09.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = false
app_id = "io.gitlab.idevecore.Pomodoro"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://flathub.org/apps/io.gitlab.idevecore.Pomodoro.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pomodoro-gtk",]
appstream_xml_url = "https://gitlab.com/idevecore/pomodoro/-/raw/main/data/io.gitlab.idevecore.Pomodoro.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-09-08"
repo_created_date = "2023-06-02"

+++

### Description

Pomodoro is a timer utility with rules, ideal for better productivity.

[Source](https://gitlab.com/idevecore/pomodoro/-/raw/main/data/io.gitlab.idevecore.Pomodoro.appdata.xml.in.in)

### Notice

Development is "currently on hold with no expected date for resumption" according to the apps [README](https://gitlab.com/idevecore/pomodoro#important-notice).