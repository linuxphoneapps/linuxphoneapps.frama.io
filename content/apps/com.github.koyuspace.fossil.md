+++
title = "Fossil"
description = "A simple GTK Gopher/Gemini client written in Vala"
aliases = []
date = 2021-05-13
updated = 2024-10-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "koyuspace",]
categories = [ "gemini browser", "gopher browser",]
mobile_compatibility = [ "4",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/koyuspace/fossil"
homepage = ""
bugtracker = "https://github.com/koyuspace/fossil/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/koyuspace/fossil"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/twitter_screenshots/com.github.koyuspace.fossil/1.jpg", "https://img.linuxphoneapps.org/twitter_screenshots/com.github.koyuspace.fossil/2.jpg", "https://img.linuxphoneapps.org/twitter_screenshots/com.github.koyuspace.fossil/3.jpg", "https://img.linuxphoneapps.org/twitter_screenshots/com.github.koyuspace.fossil/4.jpg",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.koyuspace.fossil"
scale_to_fit = "com.github.koyuspace.fossil"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2021-11-11"
repo_created_date = "2021-03-23"

+++

### Notice
Runs fine after scale-to-fit. Its origin/the project it has been forked from, dragonstone, scales perfectly out of the box, so I recommend it over Fossil.