+++
title = "explosive-c4"
description = "Connect4 clone"
aliases = ["games/noappid.ltworf.explosive-c4/"]
date = 2024-09-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "Salvo 'LtWorf' Tomaselli",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "pureos_landing",]
freedesktop_categories = [ "Game", "Qt", "LogicGame",]
programming_languages = [ "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://codeberg.org/ltworf/explosive-c4"
homepage = "https://codeberg.org/ltworf/explosive-c4"
bugtracker = "https://codeberg.org/ltworf/explosive-c4/issues"
donations = ""
translations = ""
more_information = [ "https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3C6368532.DvuYhMxLoT@galatea%3E", "https://www.youtube.com/watch?v=oBcFPTqldt8",]
summary_source_url = "https://codeberg.org/ltworf/explosive-c4"
screenshots = []
screenshots_img = []
svg_icon_url = "https://codeberg.org/ltworf/explosive-c4/raw/branch/master/src/extras/high/explosive-c4.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "explosive-c4",]
appstream_xml_url = ""
reported_by = "ltworf"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.ltworf.explosive-c4/"
latest_repo_commit = "2024-12-03"
repo_created_date = "2023-10-18"

+++

### Description

Four in a row game
 The classic four in a row game.
 Two players can play on the same device, or it can be played
 against an AI.

 It is similar to kfourinline but optimised for mobile devices
 with touch screens and slower CPUs.

[Source](https://codeberg.org/ltworf/explosive-c4/raw/branch/master/debian/control)
