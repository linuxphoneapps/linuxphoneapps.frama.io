+++
title = "Nimlem"
description = "An linux mobile first GTK4 client for Lemmy"
aliases = []
date = 2024-11-11
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Boorb",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "lemmy",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Nim",]
build_systems = [ "nimble",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://codeberg.org/Boorb/Nimlem"
homepage = ""
bugtracker = "https://codeberg.org/Boorb/Nimlem/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/Boorb/Nimlem"
screenshots = [ "https://codeberg.org/Boorb/Nimlem/raw/branch/main/resources/screenshots/post.png", "https://codeberg.org/Boorb/Nimlem/raw/branch/main/resources/screenshots/posts.png",]
screenshots_img = []
svg_icon_url = "https://codeberg.org/Boorb/Nimlem/raw/branch/main/resources/icons/nimlem-icon.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.codeberg.boorb.Nimlem"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://codeberg.org/Boorb/Nimlem/raw/branch/main/org.codeberg.boorb.Nimlem.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.codeberg.boorb.nimlem/"
latest_repo_commit = "2024-12-03"
repo_created_date = "2024-03-26"

+++

### Description

Nimlem is a linux client for Lemmy written in nim, using GTK4 / Libadwaita.

 ⚠️ WARNING: Nimlem is still in an very early development stage, there are still alot of bugs and missing features! ⚠️️

[Source](https://codeberg.org/Boorb/Nimlem)


### Notice

Does not work with lemmy.ml due to what apparently are ssl connection issues.