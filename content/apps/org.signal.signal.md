+++
title = "Signal Desktop"
description = "Private messenger"
aliases = []
date = 2022-04-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Signal Foundation",]
categories = [ "chat",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "Electron",]
backends = []
services = [ "Signal",]
packaged_in = [ "alpine_edge", "arch", "aur", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "snapcraft",]
freedesktop_categories = [ "Chat", "InstantMessaging", "Network",]
programming_languages = [ "TypeScript", "JavaScript",]
build_systems = [ "yarn",]
requires_internet = []
tags = [ "flathub x86_64 only",]

[extra]
repository = "https://github.com/signalapp/Signal-Desktop"
homepage = "https://signal.org/"
bugtracker = "https://github.com/flathub/org.signal.Signal/issues"
donations = "https://signal.org/donate/"
translations = ""
more_information = [ "https://signalflatpak.github.io/signal/", "https://community.signalusers.org/t/signal-desktop-on-arm64-aarch64/9001", "https://web.archive.org/web/20230921192857/https://wiki.mobian.org/doku.php?id=signaldesktop", "https://wiki.mobian.org/doku.php?id=signal", "https://github.com/signalapp/Signal-Desktop/issues/3904#issuecomment-585861062",]
summary_source_url = "https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml"
screenshots = [ "https://signal.org/assets/images/screenshots/download-desktop-windows.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.signal.signal/1.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.signal.Signal"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.signal.Signal"
flatpak_link = "https://flathub.org/apps/org.signal.Signal.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/signal-desktop"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "signal-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2014-02-24"

+++

### Description

To use the Signal desktop app, Signal must first be installed on your phone.

Millions of people use Signal every day for free and instantaneous communication
anywhere in the world. Send and receive high-fidelity messages, participate in
HD voice/video calls, and explore a growing set of new features that help you
stay connected. Signal’s advanced privacy-preserving technology is always enabled,
so you can focus on sharing the moments that matter with the people who matter to you.

Signal is an independent 501c3 nonprofit. The complete source code for
the Signal clients and the Signal server is available on GitHub.

This flatpak is maintained by the Flathub community, and is not necessarily
endorsed or officially maintained by the upstream developers.

[Source](https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml)

### Notice

Official Signal app for desktops, worked pretty well at some point, now requires adjustments via "View" -> "Zoom out". Sadly, Signal does not provide official ARM64/aarch64 binaries, so that unofficial builds must do: Builds for Debian are listed on the [Mobian Wiki](https://web.archive.org/web/20230921192857/https://wiki.mobian.org/doku.php?id=signaldesktop), and there's also a distribution-independent [flatpak build](https://signalflatpak.github.io/signal) ([releases](https://github.com/signalflatpak/signal/releases)) that should work on every distro. Be sure to examine these before use if your thread model requires it.