+++
title = "Vakzination"
description = "Manage your vaccination certificates"
aliases = []
date = 2021-07-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "KDE",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "fedora_40", "fedora_41", "fedora_rawhide", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/pim/vakzination"
homepage = "https://invent.kde.org/plasma-mobile/vakzination"
bugtracker = "https://invent.kde.org/plasma-mobile/vakzination/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/pim/vakzination/-/raw/master/org.kde.vakzination.metainfo.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.vakzination"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/pim/vakzination/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "vakzination",]
appstream_xml_url = "https://invent.kde.org/pim/vakzination/-/raw/master/org.kde.vakzination.metainfo.xml"
reported_by = "myxor"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-28"
repo_created_date = "2021-07-11"

+++

### Description

Manage your digital health certificates like vaccination, test, and recovery certificates.

[Source](https://invent.kde.org/pim/vakzination/-/raw/master/org.kde.vakzination.metainfo.xml)