+++
title = "fWallet"
description = "A beautiful cross-platform wallet application for your transport tickets, discount cards and subscriptions."
aliases = []
date = 2024-02-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "EUPL-1.2",]
metadata_licenses = [ "FSFAP",]
app_author = [ "TheOneWithTheBraid",]
categories = [ "wallet",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Flutter",]
backends = [ "pkpass",]
services = [ "pkpass",]
packaged_in = [ "alpine_edge", "aur",]
freedesktop_categories = [ "Office",]
programming_languages = [ "Dart", "Cpp",]
build_systems = [ "cmake", "flutter",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/TheOneWithTheBraid/f_wallet"
homepage = "https://gitlab.com/TheOneWithTheBraid/f_wallet"
bugtracker = "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/fwallet/"
more_information = []
summary_source_url = "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/linux/business.braid.f_wallet.metainfo.xml"
screenshots = [ "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/assets/screenshots/desktop/dark.png?ref_type=heads", "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/assets/screenshots/desktop/light.png?ref_type=heads", "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/assets/screenshots/mobile/dark.png?ref_type=heads", "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/assets/screenshots/mobile/light.png?ref_type=heads",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/assets/logo/logo-circle.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "business.braid.f_wallet"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fwallet",]
appstream_xml_url = "https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/linux/business.braid.f_wallet.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-09-23"

+++

### Description

A beautiful cross-platform wallet application for your transport tickets, discount cards and subscriptions.


* easily import PkPass-files and show them on purpose
* beautifully design
* no trackers, no Google, no proprietary dependencies.

[Source](https://gitlab.com/TheOneWithTheBraid/f_wallet/-/raw/main/linux/business.braid.f_wallet.metainfo.xml)

### Notice

CI generated Debian packages for aarch64 are [available](https://gitlab.com/TheOneWithTheBraid/f_wallet/-/pipelines/1085252503)