+++
title = "Keychain"
description = "Manage your passwords"
aliases = []
date = 2024-08-17
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami", "QtQuick",]
backends = [ "keepassxc",]
services = [ "KeePass",]
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "C", "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/carlschwan/keychain"
homepage = ""
bugtracker = "https://invent.kde.org/carlschwan/keychain/-/issues"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2024/07/04/keychain-development-update-yubikey-support/", "https://carlschwan.eu/2024/07/01/initial-work-on-keychain/",]
summary_source_url = "https://invent.kde.org/carlschwan/keychain/-/raw/master/org.kde.keychain.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = "https://invent.kde.org/carlschwan/keychain/-/raw/master/org.kde.keychain.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.keychain"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/carlschwan/keychain/-/raw/master/org.kde.kdepasswords.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/keychain/-/raw/master/org.kde.keychain.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.keychain/"
latest_repo_commit = "2024-11-07"
repo_created_date = "2024-05-25"

+++

### Description

A password manager

[Source](https://invent.kde.org/carlschwan/keychain/-/raw/master/org.kde.keychain.metainfo.xml)

### Notice

In early WIP stages but workable (and relatively fast). [Unofficial APKBUILD](https://framagit.org/linmobapps/apkbuilds/-/raw/master/plasma-keychain/APKBUILD)