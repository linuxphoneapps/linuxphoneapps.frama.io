+++
title = "wake-mobile"
description = "Proof-of-concept alarm app that uses systemd timers to wake up the system"
aliases = []
date = 2021-03-24
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "alarm clock",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Utility", "Clock",]
programming_languages = [ "Python", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://gitlab.gnome.org/kailueke/wake-mobile"
homepage = ""
bugtracker = "https://gitlab.gnome.org/kailueke/wake-mobile/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/kailueke/wake-mobile"
screenshots = [ "https://gitlab.gnome.org/kailueke/wake-mobile",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.gitlab.kailueke.WakeMobile"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "wake-mobile",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-06-26"
repo_created_date = "2021-01-03"

+++

### Notice
Just really simple, working Proof-of-Concept, easy to make install and uninstall.