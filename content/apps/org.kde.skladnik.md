+++
title = "Skladnik"
description = "Sokoban Game"
aliases = ["games/org.kde.skladnik/"]
date = 2024-09-23
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = [ "kdegames",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/games/skladnik"
homepage = "https://apps.kde.org/skladnik"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=skladnik"
donations = "https://www.kde.org/community/donations/?app=skladnik&source=appdata"
translations = ""
more_information = [ "https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3C5503500.31r3eYUQgx@galatea%3E",]
summary_source_url = "https://invent.kde.org/games/skladnik/-/raw/master/src/org.kde.skladnik.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/skladnik/skladnik.png",]
screenshots_img = []
non_svg_icon_url = "https://invent.kde.org/games/skladnik/-/raw/master/logo.png"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.skladnik"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "skladnik",]
appstream_xml_url = "https://invent.kde.org/games/skladnik/-/raw/master/src/org.kde.skladnik.metainfo.xml"
reported_by = "ltworf"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/org.kde.skladnik/"
latest_repo_commit = "2025-01-09"
repo_created_date = "2020-05-16"

+++

### Description

Skladnik is an implementation of the Japanese warehouse keeper game “sokoban”.

The idea is that you are a warehouse keeper trying to push crates to their proper locations in a warehouse. The problem is that you cannot pull the crates or step over them. If you are not careful, some of the crates can get stuck in wrong places and/or block your way.

[Source](https://invent.kde.org/games/skladnik/-/raw/master/src/org.kde.skladnik.metainfo.xml)

### Notice

According to the [submission notice](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3C5503500.31r3eYUQgx@galatea%3E), the game works fine on mobile, especially with this [patch](https://invent.kde.org/games/skladnik/-/merge_requests/11).
