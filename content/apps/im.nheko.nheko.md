+++
title = "nheko"
description = "Desktop client for the Matrix protocol"
aliases = [ "apps/io.github.nhekoreborn.nheko/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nheko Reborn",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Matrix",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "InstantMessaging", "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Nheko-Reborn/nheko"
homepage = "https://github.com/Nheko-Reborn/nheko"
bugtracker = "https://github.com/Nheko-Reborn/nheko/issues"
donations = ""
translations = "https://weblate.nheko.im/projects/nheko/"
more_information = []
summary_source_url = "https://github.com/mujx/nheko"
screenshots = [ "https://nheko-reborn.github.io/images/screenshots/Start.png", "https://nheko-reborn.github.io/images/screenshots/chat.png", "https://nheko-reborn.github.io/images/screenshots/login.png", "https://nheko-reborn.github.io/images/screenshots/mobile.png", "https://nheko-reborn.github.io/images/screenshots/settings.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/1.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/2.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/3.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/4.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/5.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/6.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/7.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "im.nheko.Nheko"
scale_to_fit = "nheko"
flathub = "https://flathub.org/apps/im.nheko.Nheko"
flatpak_link = "https://flathub.org/apps/im.nheko.Nheko.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Nheko-Reborn/nheko/master/im.nheko.Nheko.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "nheko",]
appstream_xml_url = "https://raw.githubusercontent.com/Nheko-Reborn/nheko/master/resources/nheko.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.nhekoreborn.nheko/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2019-01-10"

+++

### Description

The motivation behind the project is to provide a native
desktop app for Matrix that feels more like a mainstream
chat app.

[Source](https://raw.githubusercontent.com/Nheko-Reborn/nheko/master/resources/nheko.appdata.xml.in)