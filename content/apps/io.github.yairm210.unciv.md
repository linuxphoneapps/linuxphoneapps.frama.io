+++
title = "Unciv"
description = "Turn-based strategy game"
aliases = ["games/io.github.yairm210.unciv/"]
date = 2021-03-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Yair Morgenstern",]
categories = [ "game",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/yairm210/Unciv"
homepage = "https://github.com/yairm210/Unciv/"
bugtracker = "https://github.com/yairm210/Unciv/issues"
donations = ""
translations = "https://yairm210.github.io/Unciv/Other/Translating/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/PolicyScreen.png", "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/TechTree.png", "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/TradeScreen.png", "https://raw.githubusercontent.com/yairm210/Unciv/master/extraImages/Screenshots/WorldScreen.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.yairm210.unciv"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.yairm210.unciv"
flatpak_link = "https://flathub.org/apps/io.github.yairm210.unciv.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "unciv",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
reported_by = "Nob0dy73"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2017-11-21"
feed_entry_id = "https://linuxphoneapps.org/games/io.github.yairm210.unciv/"

+++

### Description

A reimplementation of the most famous civilization-building game ever—fast, small, no ads, free forever!

Build your civilization, research technologies, expand your cities and defeat your foes!

The world awaits! Will you build your civilization into an empire that will stand the test of time?

[Source](https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml)

### Notice

The resolution is pretty messed up with the text bordering on unreadable, but otherwise running fine
