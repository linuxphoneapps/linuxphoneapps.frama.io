+++
title = "Peruse"
description = "Comic Book Reader"
aliases = [ "apps/org.kde.peruse.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Graphics", "Viewer",]
programming_languages = [ "Cpp", "QML", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/peruse"
homepage = "https://peruse.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=peruse"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml"
screenshots = [ "https://peruse.kde.org/screenshots/peruse-book-details.jpg", "https://peruse.kde.org/screenshots/peruse-reading-sidebar.jpg", "https://peruse.kde.org/screenshots/peruse-welcome.jpg",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/graphics/peruse/-/raw/master/src/app/icon/sc-apps-peruse.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.peruse"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.peruse"
flatpak_link = "https://flathub.org/apps/org.kde.peruse.flatpakref"
flatpak_recipe = "https://invent.kde.org/graphics/peruse/-/raw/master/.flatpak-manifest.json"
snapcraft = "https://snapcraft.io/peruse"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "peruse",]
appstream_xml_url = "https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.peruse/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-05-16"

+++

### Description

There are many ways to read comic books, and one of those that has become more
common in recent years is on a computer. Peruse was created as a way to make
exactly that as easy and pleasant as possible, and to simply get out of the
way and let you read your comic books. One could say that it allows you to
peruse your comic books at your leisure, but that would be a silly way of
putting it - so, peruse your comic books at your leisure!

One of the ways in which Peruse ensures your reading experience is as pleasant
and effortless as possible is to allow you to simply pick up where you last left
off. The welcome page in the application (first screenshot below) shows you your
most recently read comics, and shows your progress through them. To pick up
where you left off, simply tap the top left item and you will be taken right to
the page you were reading before you had to stop.

Features:

* Comic Book Archive (cbz, cbr, cb7, cbt, cba)
* Portable Document Format (pdf)
* ePub Books (epub)
* DeVice Independent files (dvi)
* DeJaVu (djvu)
* Compiled Help (chm)

[Source](https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml)