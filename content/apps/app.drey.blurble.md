+++
title = "Blurble"
description = "Word guessing game"
aliases = [ "games/app.drey.blurble/",]
date = 2022-07-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Vojtěch Perník",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Blurble"
homepage = "https://gitlab.gnome.org/World/Blurble"
bugtracker = "https://gitlab.gnome.org/World/Blurble/-/issues"
donations = "https://paypal.me/pervoj"
translations = "https://l10n.gnome.org/module/Blurble"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Blurble"
screenshots = [ "https://gitlab.gnome.org/World/Blurble/-/raw/v0.4.0/data/screenshots/screenshot-1.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.Blurble"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Blurble"
flatpak_link = "https://flathub.org/apps/app.drey.Blurble.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "blurble",]
appstream_xml_url = "https://gitlab.gnome.org/World/Blurble/-/raw/main/data/blurble.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-07"
repo_created_date = "2022-07-06"

+++

### Description

Solve the riddle until you run out of guesses!

The game is a clone of Wordle and made with localization in mind.

[Source](https://gitlab.gnome.org/World/Blurble/-/raw/main/data/blurble.appdata.xml.in.in)
