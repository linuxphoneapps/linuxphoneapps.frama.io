+++
title = "< polycule >"
description = "A geeky and efficient [matrix] client for power users"
aliases = []
date = 2024-10-11
updated = 2025-01-13

[taxonomies]
project_licenses = [ "EUPL-1.2",]
metadata_licenses = [ "FSFAP",]
app_author = [ "The one with the braid",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Flutter",]
backends = []
services = [ "Matrix",]
packaged_in = [ "alpine_edge", "aur",]
freedesktop_categories = [ "Chat", "IRCClient", "InstantMessaging", "Network", "Office", "Telephony", "VideoConference",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]
requires_internet = [ "recommends always", "requires first-run",]
tags = []

[extra]
repository = "https://gitlab.com/polycule_client/polycule.git"
homepage = "https://gitlab.com/polycule_client/polycule"
bugtracker = "https://gitlab.com/polycule_client/polycule/-/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/polycule"
more_information = []
summary_source_url = "https://gitlab.com/polycule_client/polycule/-/raw/main/linux/business.braid.polycule.metainfo.xml"
screenshots = [ "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/01.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/02.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/03.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/04.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/05.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/06.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/07.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/08.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/09.png?ref_type=heads", "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/screenshots/mobile/linux/dark/10.png?ref_type=heads",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/polycule_client/polycule/-/raw/main/assets/logo/logo-circle.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "business.braid.polycule"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "polycule",]
appstream_xml_url = "https://gitlab.com/polycule_client/polycule/-/raw/main/linux/business.braid.polycule.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/business.braid.polycule/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-01-16"

+++

### Description

Beep boop and I had too much time during boring work meetings. Using this client as
a small piece to practice some matrix related stuff.

I'm especially considering to experiment with Sliding Sync and Flutter Linux-native integrations.

* keyboard optimized
* no matrix.org !
* fast and efficient
* terminal style design
* cross-platform

[Source](https://gitlab.com/polycule_client/polycule/-/raw/main/linux/business.braid.polycule.metainfo.xml)