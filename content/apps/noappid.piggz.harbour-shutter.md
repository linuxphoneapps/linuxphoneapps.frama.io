+++
title = "Shutter"
description = "Shutter is a camera application which exposes all available camera parameters to the user."
aliases = []
date = 2024-09-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "piggz",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami", "QtQuick",]
backends = [ "libcamera",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Graphics", "Photography",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/piggz/harbour-shutter"
homepage = "https://github.com/piggz/harbour-shutter/"
bugtracker = "https://github.com/piggz/harbour-shutter/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/piggz/harbour-shutter"
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/piggz/harbour-shutter/refs/heads/main/harbour-shutter.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "harbour-shutter",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.piggz.harbour-shutter/"
latest_repo_commit = "2024-12-09"
repo_created_date = "2023-01-17"

+++

### Description

Shutter is a camera application for Sailfish which exposes all available camera parameters to the user. The app uses libcamera to interface with the device's cameras.

[Source](https://github.com/piggz/harbour-shutter)




### Notice

Primarily made for Sailfish OS, ported to Ubuntu Touch, but can also run on other distributionshttps://raw.githubusercontent.com/piggz/harbour-shutter/refs/heads/main/harbour-shutter.svg.