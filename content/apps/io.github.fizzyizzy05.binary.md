+++
title = "Binary"
description = "Convert numbers between bases"
aliases = []
date = 2024-09-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Isabelle Jackson",]
categories = [ "utilities", "calculator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/fizzyizzy05/binary"
homepage = "https://apps.gnome.org/Binary/"
bugtracker = "https://github.com/fizzyizzy05/binary/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/binary/"
more_information = [ "https://apps.gnome.org/Binary/",]
summary_source_url = "https://flathub.org/apps/io.github.fizzyizzy05.binary"
screenshots = [ "https://github.com/fizzyizzy05/binary/raw/main/img/binary-101010-decimal-42.png", "https://github.com/fizzyizzy05/binary/raw/main/img/decimal-1201-hexadecimal-4b1.png", "https://github.com/fizzyizzy05/binary/raw/main/img/octal-17-hexadecimal-f.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/fizzyizzy05/binary/refs/heads/main/data/icons/hicolor/scalable/apps/io.github.fizzyizzy05.binary.svg"
all_features_touch = false
intended_for_mobile = true
app_id = "io.github.fizzyizzy05.binary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.fizzyizzy05.binary"
flatpak_link = "https://flathub.org/apps/io.github.fizzyizzy05.binary.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/fizzyizzy05/binary/refs/heads/main/io.github.fizzyizzy05.binary.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "binary",]
appstream_xml_url = "https://raw.githubusercontent.com/fizzyizzy05/binary/refs/heads/main/data/io.github.fizzyizzy05.binary.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.fizzyizzy05.binary/"
latest_repo_commit = "2025-01-05"
repo_created_date = "2023-12-16"

+++

### Description

A small and simple app used to convert between different hexadecimal and binary numbers

[Source](https://raw.githubusercontent.com/fizzyizzy05/binary/refs/heads/main/data/io.github.fizzyizzy05.binary.metainfo.xml.in)