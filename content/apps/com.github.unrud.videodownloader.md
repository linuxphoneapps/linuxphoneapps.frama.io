+++
title = "Video Downloader"
description = "Download web videos"
aliases = []
date = 2020-10-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Unrud",]
categories = [ "video downloader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp",]
services = []
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Unrud/video-downloader"
homepage = "https://github.com/Unrud/video-downloader"
bugtracker = "https://github.com/Unrud/video-downloader/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/video-downloader/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Unrud/video-downloader/v0.12.21/screenshots/1.png", "https://raw.githubusercontent.com/Unrud/video-downloader/v0.12.21/screenshots/2.png", "https://raw.githubusercontent.com/Unrud/video-downloader/v0.12.21/screenshots/3.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Unrud/video-downloader/refs/heads/master/data/com.github.unrud.VideoDownloader.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.unrud.VideoDownloader"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.unrud.VideoDownloader"
flatpak_link = "https://flathub.org/apps/com.github.unrud.VideoDownloader.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "video-downloader",]
appstream_xml_url = "https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-29"
repo_created_date = "2019-06-06"

+++

### Description

Download videos from websites with an easy-to-use interface.
Provides the following features:

* Convert videos to MP3
* Supports password-protected and private videos
* Download single videos or whole playlists
* Automatically selects a video format based on your quality demands

Based on yt-dlp.

[Source](https://raw.githubusercontent.com/Unrud/video-downloader/master/data/com.github.unrud.VideoDownloader.metainfo.xml.in)

### Notice

Was GTK3/libadwaita before release 0.10.0.