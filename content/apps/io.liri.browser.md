+++
title = "Liri Browser"
description = "Liri Browser is a cross-platform, Material Design Web browser, currently in  alpha  stage. "
aliases = []
date = 2019-02-01
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "lirios",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "WebBrowser",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/lirios/browser"
homepage = "https://liri.io/apps/browser/"
bugtracker = "https://github.com/lirios/browser/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://liri.io/apps/browser/"
screenshots = [ "https://liri.io/apps/browser/",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/lirios/browser/refs/heads/develop/res/icons/scalable/apps/io.liri.Browser.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.Browser"
scale_to_fit = "io.liri.Browser"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/lirios/browser/refs/heads/develop/dist/flatpak/io.liri.Browser.json"
snapcraft = ""
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/lirios/browser/refs/heads/develop/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "liri-browser",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_github_api"
latest_repo_commit = "2024-06-28"
repo_created_date = "2016-09-22"

+++