+++
title = "Jami"
description = "Privacy-oriented voice, video, chat, and conference platform"
aliases = []
date = 2022-04-26
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "savoirfairelinux",]
categories = [ "chat", "telephony", "communication", "productivity",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "QtQuick 6",]
backends = []
services = [ "Jami", "SIP",]
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Chat", "FileTransfer", "InstantMessaging", "Network", "P2P",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://git.jami.net/savoirfairelinux/jami-client-qt"
homepage = "https://jami.net/"
bugtracker = "https://git.jami.net/savoirfairelinux/jami-client-qt/issues"
donations = "https://jami.net/whydonate/"
translations = "https://www.transifex.com/savoirfairelinux/jami"
more_information = [ "https://jami.net/help/",]
summary_source_url = "https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml"
screenshots = [ "https://dl.jami.net/media-resources/screenshots/jami_linux_audiovideo.png", "https://dl.jami.net/media-resources/screenshots/jami_linux_screenshare.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/net.jami.jami/1.png", "https://img.linuxphoneapps.org/net.jami.jami/2.png", "https://img.linuxphoneapps.org/net.jami.jami/3.png", "https://img.linuxphoneapps.org/net.jami.jami/4.png", "https://img.linuxphoneapps.org/net.jami.jami/5.png", "https://img.linuxphoneapps.org/net.jami.jami/6.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "net.jami.Jami"
scale_to_fit = "net.jami.jami-qt"
flathub = "https://flathub.org/apps/net.jami.Jami"
flatpak_link = "https://flathub.org/apps/net.jami.Jami.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "jami-qt",]
appstream_xml_url = "https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/net.jami.Jami.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-08-07"

+++

### Description

Jami [1], a GNU package, is software for universal and
distributed peer-to-peer communication that respects the
freedom and privacy of its users.

Jami is the simplest and easiest way to connect with people
(and devices) with instant messaging, audio and video calls
over the Internet and LAN/WAN intranets.

Jami is a free/libre, end-to-end encrypted, and private
communication platform.

Jami – which used to be known as Ring – is also an
open-source alternative (to Facebook Messenger, Signal,
Skype, Teams, Telegram, TikTok, Viber, WhatsApp, Zoom) that
prioritizes the privacy of its users.

Jami has a professional-looking design and is available for
a wide range of platforms. Unlike the alternatives, calls
using Jami are directly between users as it does not use
servers to handle calls.

This gives the greatest privacy as the distributed nature
of Jami means your calls are only between participants.

One-to-one and group conversations with Jami are enhanced
with: instant messaging; audio and video calling;
recording and sending audio and video messages;
file transfers; screen sharing; and, location sharing.

Jami can also function as a SIP client.

Jami has multiple extensions [2] available:
Audio Filter; Auto Answer; Green Screen; Watermark; and,
Whisper Transcript.

Jami can be easily deployed in organizations with the
“Jami Account Management Server” (JAMS) [3], allowing users
to connect with their corporate credentials or create local
accounts. JAMS allows you to manage your own Jami community
while taking advantage of Jami’s distributed network
architecture.

Jami is available for GNU/Linux, Windows, macOS, iOS,
Android, and Android TV, making Jami an interoperable and
cross-platform communication framework.

Manage multiple SIP accounts, Jami accounts and JAMS
accounts with the Jami client installed on one or multiple
devices.

Jami is free, unlimited, private, advertising free,
compatible, fast, autonomous, and anonymous.

[1] https://jami.net/

[2] https://jami.net/extensions/

[3] https://jami.biz/

[Source](https://git.jami.net/savoirfairelinux/jami-client-qt/-/raw/master/extras/data/jami.appdata.xml)

### Notice

Arch ARM and flatpak builds were evaluated. It fits the screen mostly okay without scale-to-fit for the chat and phone UI after setting [recommended env vars](https://img.linuxphoneapps.org/net.jami.jami/flatseal.png) (you won't see what you type), scale-to-fit makes the app behave worse in many circumstances. Voice calls work, the button to hang up is invisible, just tap the center of the lower half, chat while video/audio makes the window far two wide (somewhat usable in landscape). Video calls don't work out of the box but may work after setting up camera/video pipelines properly.
