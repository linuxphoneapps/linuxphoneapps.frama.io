+++
title = "BadWolf"
description = "Minimalist and privacy-oriented web browser based on WebKitGTK"
aliases = []
date = 2022-08-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "BSD-3-Clause", "CC-BY-SA-4.0",]
metadata_licenses = []
app_author = [ "git",]
categories = [ "web browser",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "fedora_40", "fedora_41", "fedora_rawhide", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "C",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "manual status maintenance",]

[extra]
repository = "https://hacktivis.me/git/badwolf/"
homepage = "https://hacktivis.me/projects/badwolf"
bugtracker = "https://todo.sr.ht/~lanodan/badwolf"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://hacktivis.me/projects/badwolf"
screenshots = [ "https://hacktivis.me/projects/badwolf",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "me.hacktivis.badwolf"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "badwolf",]
appstream_xml_url = ""
reported_by = "lanodan"
updated_by = "check_via_repology"
latest_repo_commit = "2024-06-04"
repo_created_date = "2019-01-07"

+++

### Notice

Note on mobile compatibility: Fits nicely in on the pinephone with a custom [GTK stylesheet](https://hacktivis.me/git/dotfiles/file/.config/gtk-3.0/gtk.css.html) allowing to go down to 306×97 px instead of 399×111 px with the defaults provided by GTK.