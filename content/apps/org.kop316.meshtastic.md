+++
title = "Meshtastic Client"
description = "Graphically control meshtastic devices"
aliases = []
date = 2024-10-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Talbot",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "python-meshtastic",]
services = [ "Meshtastic",]
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/kop316/gtk-meshtastic-client"
homepage = "https://gitlab.com/kop316/gtk-meshtastic-client"
bugtracker = "https://gitlab.com/kop316/gtk-meshtastic-client/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kop316/gtk-meshtastic-client/-/raw/main/data/org.kop316.meshtastic.metainfo.xml.in"
screenshots = [ "https://gitlab.com/kop316/gtk-meshtastic-client/-/raw/main/data/screenshot/dark_mode.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/kop316/gtk-meshtastic-client/-/raw/main/data/icons/hicolor/scalable/apps/org.kop316.meshtastic.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "org.kop316.meshtastic"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/kop316/gtk-meshtastic-client/-/raw/main/org.kop316.meshtastic.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/kop316/gtk-meshtastic-client/-/raw/main/data/org.kop316.meshtastic.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kop316.meshtastic/"
latest_repo_commit = "2025-01-04"
repo_created_date = "2024-10-03"

+++

### Description

This is a GUI client to control a meshtastic device over Serial, Bluetooth,
or TCP/IP. It runs on top of the meshtastic Python API

[Source](https://gitlab.com/kop316/gtk-meshtastic-client/-/raw/main/data/org.kop316.meshtastic.metainfo.xml.in)