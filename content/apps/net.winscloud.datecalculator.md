+++
title = "Date Calculator"
description = "The go-to minimal date calculator"
aliases = []
date = 2024-03-06
updated = 2024-12-01

[taxonomies]
project_licenses = [ "LGPL-2.1-only",]
metadata_licenses = [ "MIT",]
app_author = [ "Win and Amy",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://github.com/WinsDominoes/dateapp"
homepage = "https://git.winscloud.net/WinsDominoes/dateapp"
bugtracker = "https://git.winscloud.net/WinsDominoes/dateapp/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/WinsDominoes/dateapp/main/linux/net.winscloud.DateCalculator.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/WinsDominoes/dateapp/main/assets/screenshots/startscreen-linux.png",]
screenshots_img = []
svg_icon_url = "https://github.com/WinsDominoes/dateapp/raw/main/assets/app_icons/net.winscloud.DateCalculator.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "net.winscloud.DateCalculator"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://github.com/AtiusAmy/net.winscloud.DateCalculator/releases/"
flatpak_recipe = "https://raw.githubusercontent.com/AtiusAmy/net.winscloud.DateCalculator/main/net.winscloud.DateCalculator.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/WinsDominoes/dateapp/main/linux/net.winscloud.DateCalculator.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-07-19"
repo_created_date = "2024-01-22"

+++


### Description

Date Calculator is an app to calculate differences between dates.

* Simple to use!
* Fast and responsive.
* Calculate differences between dates, with months, years, and weeks given.
* Find which day is a certain amount of dates away.
* Compatible with dynamic color themes.
* Open Source and ad-free.

[Source](https://raw.githubusercontent.com/WinsDominoes/dateapp/main/linux/net.winscloud.DateCalculator.appdata.xml)

### Notice

ARM64 flatpak available on <https://github.com/AtiusAmy/net.winscloud.DateCalculator/releases/>