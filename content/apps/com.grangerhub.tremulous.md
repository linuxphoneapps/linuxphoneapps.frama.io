+++
title = "Tremulous"
description = "Aliens vs Humans, First Person Shooter game with elements of Real Time Strategy"
aliases = [ "games/com.grangerhub.tremulous.desktop/", "games/com.grangerhub.tremulous/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "Dark Legion Development and GrangerHub",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "pre-release",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "fedora_40", "fedora_41", "fedora_rawhide", "flathub",]
freedesktop_categories = [ "ActionGame", "Game", "Shooter", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/darklegion/tremulous"
homepage = "https://grangerhub.com"
bugtracker = "https://github.com/darklegion/tremulous/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/tremulous/a.png", "https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/tremulous/b.png", "https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/tremulous/c.png", "https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/tremulous/d.png", "https://raw.githubusercontent.com/hughsie/fedora-appstream/master/screenshots-extra/tremulous/e.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.grangerhub.Tremulous"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.grangerhub.Tremulous"
flatpak_link = "https://flathub.org/apps/io.github.grangerhub.Tremulous.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tremulous",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml"
reported_by = "-Euso-"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/games/com.grangerhub.tremulous.desktop/"
latest_repo_commit = "2024-06-02"
repo_created_date = "2013-01-03"

+++


### Description

Tremulous is a free, open source game that blends a team based FPS with
elements of an RTS.

Players can choose from 2 unique races, aliens and humans.
Players on both teams are able to build working structures in-game like an
RTS.

[Source](https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml)
