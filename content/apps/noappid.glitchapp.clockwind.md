+++
title = "Clockwind"
description = "a platform game with time travel mechanichs forked from https://alesan99.itch.io/clockwind"
aliases = ["games/noappid.glitchapp.clockwind/"]
date = 2024-10-05
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "glitchapp",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = [ "LÖVE2D",]
services = []
packaged_in = []
freedesktop_categories = [ "Game", "ActionGame"]
programming_languages = [ "Lua",]
build_systems = []
requires_internet = [ "requires offline-only",]
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://codeberg.org/glitchapp/clockwind"
homepage = "https://codeberg.org/glitchapp/clockwind"
bugtracker = "https://codeberg.org/glitchapp/clockwind/issues"
donations = "https://glitchapp.codeberg.page/img/ZCashQr.webp"
translations = ""
more_information = []
summary_source_url = "https://github.com/Azzla/chess-engine"
screenshots = [ "https://codeberg.org/glitchapp/clockwind/media/branch/main/screenshots/sc1.webp",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "",]
appstream_xml_url = ""
reported_by = "glitchapp"
updated_by = "1Maxnet1"
latest_repo_commit = "2024-10-05"
repo_created_date = "2024-10-05"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.glitchapp.clockwind/"

+++

### Description

The mechanic of this game is time travel. Make your way through 4 levels utilizing the passage of time in different ways. This may involve making trees grow or turning a car in to a flying car.

[Source](https://codeberg.org/glitchapp/clockwind)

### Notice

An .apk for postmarketOS (and a .rpm for Sailfish OS) can be found under [releases](https://codeberg.org/glitchapp/clockwind/releases).
