+++
title = "Minuet"
description = "Music Education Software"
aliases = [ "apps/org.kde.minuet.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "education",]
mobile_compatibility = [ "2",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Education", "KDE", "Music", "Qt",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/minuet"
homepage = "https://minuet.kde.org"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=minuet"
donations = "https://www.kde.org/community/donations/?app=minuet&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/minuet/minuet.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/education/minuet/-/raw/master/src/app/icons/128-apps-minuet.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.minuet"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.minuet"
flatpak_link = "https://flathub.org/apps/org.kde.minuet.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "minuet",]
appstream_xml_url = "https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.minuet/"
latest_repo_commit = "2025-01-09"
repo_created_date = "2020-04-25"

+++

### Description

Minuet is an application for music education. It features a set of ear training exercises regarding intervals, chords, scales and more.

[Source](https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.appdata.xml)

### Notice

Does not really work on mobile.