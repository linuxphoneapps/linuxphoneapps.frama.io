+++
title = "Papers"
description = "Read documents"
aliases = []
date = 2024-06-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "GFDL-1.3-only", "GPL-2.0-or-later",]
app_author = [ "The GNOME Project",]
categories = [ "document viewer", "pdf viewer",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "poppler",]
services = []
packaged_in = [ "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "2DGraphics", "Graphics", "Office", "VectorGraphics", "Viewer",]
programming_languages = [ "C", "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/Incubator/papers/"
homepage = "https://apps.gnome.org/Papers"
bugtracker = "https://gitlab.gnome.org/GNOME/Incubator/papers/-/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/papers/"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Papers"
screenshots = [ "https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-1.png", "https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-2.png", "https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-3.png", "https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-4.png", "https://gitlab.gnome.org/GNOME/Incubator/papers/raw/main/data/screenshots/papers-5.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/Incubator/papers/-/raw/main/data/icons/scalable/apps/org.gnome.Papers.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Papers"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Papers"
flatpak_link = "https://flathub.org/apps/org.gnome.Papers.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/Incubator/papers/-/raw/main/build-aux/flatpak/org.gnome.Papers.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-papers",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/Incubator/papers/-/raw/main/data/org.gnome.Papers.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2022-03-29"

+++

### Description

A document viewer for the GNOME desktop. You can view, search or annotate documents in many different formats.

Papers supports documents in: PDF, DjVu, TIFF, and Comic Books archives (CBR, CBT, CBZ, CB7).

[Source](https://gitlab.gnome.org/GNOME/Incubator/papers/-/raw/main/data/org.gnome.Papers.metainfo.xml.in.in)

### Notice

46.1 does not fit on mobile yet, current (16-06-2024) nighly flatpak does.