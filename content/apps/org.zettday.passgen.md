+++
title = "Passgen"
description = "Password generator written using Qt Quick Controls 2"
aliases = []
date = 2019-03-19
updated = 2024-10-26

[taxonomies]
project_licenses = [ "MIT and others",]
metadata_licenses = []
app_author = [ "zettdaymond",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "C", "QML", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://github.com/zettdaymond/passgen"
homepage = ""
bugtracker = "https://github.com/zettdaymond/passgen/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/zettdaymond/passgen"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.zettday.passgen"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
repo_created_date = "2016-11-10"
latest_repo_commit = "2016-11-10"

+++

### Description

Passgen - unique password generator written using Qt Quick Controls 2. Generates password based on (web) service name and master-password. Any inputted data or generated password doesn't be stored. [Source](https://github.com/zettdaymond/passgen)
