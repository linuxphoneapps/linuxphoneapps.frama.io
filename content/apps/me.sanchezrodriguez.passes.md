+++
title = "Passes"
description = "Manage your digital passes"
aliases = []
date = 2022-03-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Pablo Sánchez Rodríguez",]
categories = [ "wallet",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "pkpass",]
services = [ "pkpass", "espass",]
packaged_in = [ "alpine_3_21", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/pablo-s/passes"
homepage = "https://github.com/pablo-s/passes"
bugtracker = "https://github.com/pablo-s/passes/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/pablo-s/passes/main/data/me.sanchezrodriguez.passes.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/pablo-s/passes/main/data/screenshots/passes.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "me.sanchezrodriguez.passes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.sanchezrodriguez.passes"
flatpak_link = "https://flathub.org/apps/me.sanchezrodriguez.passes.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "passes",]
appstream_xml_url = "https://raw.githubusercontent.com/pablo-s/passes/main/data/me.sanchezrodriguez.passes.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2022-01-27"

+++

### Description

Passes is a handy app that helps you manage all your digital passes effortlessly. With Passes, you can conveniently store your boarding passes, coupons, loyalty cards, event tickets, and more, all in PKPass or esPass format.

Moreover, the app seamlessly adjusts to different screen sizes, allowing you to access your passes on various devices, whether it's a desktop computer or a mobile phone.

Stop wasting time searching through your email or printing out your digital passes. Download Passes now and keep all your passes in one convenient location.

[Source](https://raw.githubusercontent.com/pablo-s/passes/main/data/me.sanchezrodriguez.passes.metainfo.xml.in)