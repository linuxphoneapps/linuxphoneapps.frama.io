+++
title = "Backup"
description = "postmarketOS Backup and restore tool"
aliases = []
date = 2021-06-23
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "postmarketOS Developers",]
categories = [ "backup", "system utilities",]
mobile_compatibility = [ "5",]
status = [ "archived", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/postmarketOS/postmarketos-backup"
homepage = "https://gitlab.com/postmarketOS/postmarketos-backup"
bugtracker = "https://gitlab.com/postmarketOS/postmarketos-backup/-/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=4neYnrLgTy8",]
summary_source_url = "https://gitlab.com/postmarketOS/postmarketos-backup/-/raw/master/data/org.postmarketos.Backup.appdata.xml"
screenshots = [ "http://brixitcdn.net/metainfo/backup.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/postmarketOS/postmarketos-backup/-/raw/master/data/org.postmarketos.Backup.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.postmarketos.Backup"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/postmarketOS/postmarketos-backup/-/raw/master/data/org.postmarketos.Backup.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2022-04-30"
repo_created_date = "2021-06-07"

+++

### Description
A backup application that integrates with apk to quickly backup and restore your system
 state in postmarketOS

[Source](https://gitlab.com/postmarketOS/postmarketos-backup/-/raw/master/data/org.postmarketos.Backup.appdata.xml)