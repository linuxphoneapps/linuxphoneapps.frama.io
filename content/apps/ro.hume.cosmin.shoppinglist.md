+++
title = "Shopping List"
description = "A shopping list application for GNU/Linux mobile devices"
aliases = []
date = 2022-05-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Cosmin Humeniuc",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/cosmin/shopping-list"
homepage = "https://cosmin.hume.ro/project/shopping-list-gtk/"
bugtracker = "https://gitlab.gnome.org/cosmin/shopping-list/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/cosmin/shopping-list"
screenshots = [ "https://gitlab.gnome.org/cosmin/shopping-list/raw/23d82af6fa9855df8522bf922af42a8af7dfa68d/data/screenshots/screenshot1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/ro.hume.cosmin.shoppinglist/1.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "ro.hume.cosmin.ShoppingList"
scale_to_fit = ""
flathub = "https://flathub.org/apps/ro.hume.cosmin.ShoppingList"
flatpak_link = "https://flathub.org/apps/ro.hume.cosmin.ShoppingList.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/cosmin/shopping-list/-/raw/master/ro.hume.cosmin.ShoppingList.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/cosmin/shopping-list/-/raw/master/data/ro.hume.cosmin.ShoppingList.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-05-18"
repo_created_date = "2022-05-07"

+++


### Description

Shopping List makes it easy to add items to a list,
check the item off, and remove it from the list.

[Source](https://gitlab.gnome.org/cosmin/shopping-list/-/raw/master/data/ro.hume.cosmin.ShoppingList.metainfo.xml.in)