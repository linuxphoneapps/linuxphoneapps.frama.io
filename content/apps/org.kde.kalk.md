+++
title = "Calculator"
description = "A feature rich calculator"
aliases = []
date = 2020-03-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = [ "Math.js",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Calculator", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake", "ninja",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/kalk"
homepage = "https://apps.kde.org/kalk/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Kalk"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2024/03/01/plasma-6/#calculator",]
summary_source_url = "https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kalk/kalk.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.kalk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kalk"
flatpak_link = "https://flathub.org/apps/org.kde.kalk.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kalk",]
appstream_xml_url = "https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-06-09"

+++

### Description

Kalk is a convergent calculator for Plasma.

[Source](https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml)