+++
title = "Numberstation"
description = "TOTP authenticator application"
aliases = []
date = 2021-03-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "martijnbraam",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/numberstation"
latest_repo_commit = "2024-01-29"
homepage = "https://sr.ht/~martijnbraam/numberstation"
bugtracker = "https://todo.sr.ht/~martijnbraam/numberstation"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/numberstation/blob/master/data/org.postmarketos.Numberstation.appdata.xml"
screenshots = [ "http://brixitcdn.net/metainfo/numberstation.png",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~martijnbraam/numberstation/blob/master/data/org.postmarketos.Numberstation.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.Numberstation"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "numberstation",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/numberstation/blob/master/data/org.postmarketos.Numberstation.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
repo_created_date = "2021-03-23"

+++

### Description

A TOTP authenticator application like Google Authenticator and Gnome Authenticator

[Source](https://git.sr.ht/~martijnbraam/numberstation/blob/master/data/org.postmarketos.Numberstation.appdata.xml)