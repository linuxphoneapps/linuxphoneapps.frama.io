+++
title = "Crow Translate"
description = "Application that allows you to translate and speak text using Mozhi"
aliases = [ "apps/io.crow_translate.crowtranslate/",]
date = 2021-02-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = [ "Mozhi",]
services = [ "Google Translate", "bing translate", "DeepL", "Yandex translate", "DuckDuckGo translate", "LibreTranslate", "MyMemory", "Reverso",]
packaged_in = [ "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Office", "Translation",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/office/crow-translate"
homepage = "https://apps.kde.org/crowtranslate"
bugtracker = "https://bugs.kde.org"
donations = ""
translations = "https://l10n.kde.org"
more_information = []
summary_source_url = "https://flathub.org/apps/org.kde.CrowTranslate"
screenshots = [ "https://invent.kde.org/websites/product-screenshots/-/raw/master/crow-translate/main-mobile-portrait.png", "https://invent.kde.org/websites/product-screenshots/-/raw/master/crow-translate/main-mobile.png", "https://invent.kde.org/websites/product-screenshots/-/raw/master/crow-translate/main.png", "https://invent.kde.org/websites/product-screenshots/-/raw/master/crow-translate/settings-mobile.png", "https://invent.kde.org/websites/product-screenshots/-/raw/master/crow-translate/settings.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.CrowTranslate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.CrowTranslate"
flatpak_link = "https://flathub.org/apps/org.kde.CrowTranslate.flatpakref"
flatpak_recipe = "https://invent.kde.org/office/crow-translate/-/raw/master/.flatpak-manifest.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "crow-translate",]
appstream_xml_url = "https://invent.kde.org/office/crow-translate/-/raw/master/data/org.kde.CrowTranslate.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.crow_translate.crowtranslate/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2024-06-09"

+++

### Description

Application written in C++ / Qt that allows you to translate and speak text using Mozhi.

Features:

* Multiple translation engines provided by Mozhi (some instances can disable specific engines)\*
* Translate and speak text from screen or selection
* Highly customizable shortcuts
* Command-line interface with rich options
* D-Bus API
* Available for Linux and Windows

\*While Mozhi acts as a proxy to protect your privacy, the third-party services it uses may store and analyze the text you send.

[Source](https://invent.kde.org/office/crow-translate/-/raw/master/data/org.kde.CrowTranslate.metainfo.xml)

### Notice

You can force portrait mode in Settings -> General -> Main window orientation.