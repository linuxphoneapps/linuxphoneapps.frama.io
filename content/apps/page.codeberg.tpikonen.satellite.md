+++
title = "Satellite"
description = "Check your GPS reception and save your tracks"
aliases = [ "apps/org.codeberg.tpikonen.satellite/",]
date = 2021-08-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Teemu Ikonen",]
categories = [ "geography",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "ModemManager", "gnss-share",]
services = []
packaged_in = [ "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Geoscience", "Monitor", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/tpikonen/satellite"
homepage = "https://codeberg.org/tpikonen/satellite"
bugtracker = "https://codeberg.org/tpikonen/satellite/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/tpikonen/satellite"
screenshots = [ "https://tpikonen.codeberg.page/satellite/img/screenshot-default.png", "https://tpikonen.codeberg.page/satellite/img/screenshot-expanded.png", "https://tpikonen.codeberg.page/satellite/img/screenshot-landscape-dark.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "page.codeberg.tpikonen.satellite"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.codeberg.tpikonen.satellite"
flatpak_link = "https://flathub.org/apps/page.codeberg.tpikonen.satellite.flatpakref"
flatpak_recipe = "https://codeberg.org/tpikonen/satellite/raw/branch/main/flatpak/page.codeberg.tpikonen.satellite.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "satellite-gnss",]
appstream_xml_url = "https://codeberg.org/tpikonen/satellite/raw/branch/main/data/appdata.xml"
reported_by = "tpikonen"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-03"
repo_created_date = "2021-07-30"

+++

### Description

Satellite displays global navigation satellite system (GNSS: that's GPS, Galileo, Glonass etc.) data obtained from an NMEA source in your device. Currently the ModemManager and gnss-share APIs are supported.

You can use it to check the navigation satellite signal strength and see your speed, coordinates and other parameters once a fix is obtained. It can also save GPX-tracks.

[Source](https://codeberg.org/tpikonen/satellite/raw/branch/main/data/appdata.xml)