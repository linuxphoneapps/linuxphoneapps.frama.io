+++
title = "Fonts"
description = "View fonts on your system"
aliases = []
date = 2024-05-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-font-viewer/"
homepage = "https://apps.gnome.org/FontViewer/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-font-viewer/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-font-viewer/"
more_information = [ "https://apps.gnome.org/FontViewer/",]
summary_source_url = "https://flathub.org/apps/org.gnome.font-viewer"
screenshots = [ "https://static.gnome.org/appdata/gnome-46/fonts/fonts-main.png", "https://static.gnome.org/appdata/gnome-46/fonts/fonts-preview.png", "https://static.gnome.org/appdata/gnome-46/fonts/fonts-search.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-font-viewer/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.font-viewer.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.font-viewer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.font-viewer"
flatpak_link = "https://flathub.org/apps/org.gnome.font-viewer.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-font-viewer/-/raw/main/build-aux/flatpak/org.gnome.font-viewerDevel.json"
snapcraft = "https://snapcraft.io/gnome-font-viewer"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-font-viewer",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-font-viewer/-/raw/main/data/org.gnome.font-viewer.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2018-05-23"

+++

### Description

Fonts shows you the fonts installed on your computer for your use as
thumbnails.
Selecting any thumbnails shows the full view of how the font would look under
various sizes.

Fonts also supports installing new font files downloaded in the .ttf
and other formats.
Fonts may be installed only for your use or made available to all users on the computer.

[Source](https://gitlab.gnome.org/GNOME/gnome-font-viewer/-/raw/main/data/org.gnome.font-viewer.appdata.xml.in)