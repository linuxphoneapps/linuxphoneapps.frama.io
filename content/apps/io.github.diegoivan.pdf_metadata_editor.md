+++
title = "Paper Clip"
description = "Edit PDF document metadata"
aliases = []
date = 2024-02-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Diego Iván",]
categories = [ "office",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Office", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/Diego-Ivan/Paper-Clip"
homepage = "https://github.com/Diego-Ivan/Paper-Clip"
bugtracker = "https://github.com/Diego-Ivan/Paper-Clip/issues"
donations = ""
translations = "https://github.com/Diego-Ivan/Paper-Clip/tree/main/po"
more_information = [ "https://apps.gnome.org/PdfMetadataEditor/",]
summary_source_url = "https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor"
screenshots = [ "https://raw.githubusercontent.com/Diego-Ivan/pdf-metadata-editor/main/data/screenshots/01.png", "https://raw.githubusercontent.com/Diego-Ivan/pdf-metadata-editor/main/data/screenshots/02.png", "https://raw.githubusercontent.com/Diego-Ivan/pdf-metadata-editor/main/data/screenshots/03.png", "https://raw.githubusercontent.com/Diego-Ivan/pdf-metadata-editor/main/data/screenshots/04.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Diego-Ivan/Paper-Clip/main/data/icons/hicolor/scalable/apps/io.github.diegoivan.pdf_metadata_editor.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.diegoivan.pdf_metadata_editor"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor"
flatpak_link = "https://flathub.org/apps/io.github.diegoivan.pdf_metadata_editor.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Diego-Ivan/Paper-Clip/main/io.github.diegoivan.pdf_metadata_editor.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "paper-clip",]
appstream_xml_url = "https://raw.githubusercontent.com/Diego-Ivan/Paper-Clip/main/data/io.github.diegoivan.pdf_metadata_editor.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-09-10"
repo_created_date = "2023-01-28"

+++


### Description

Edit the title, author, keywords and more details of your PDF documents

[Source](https://raw.githubusercontent.com/Diego-Ivan/Paper-Clip/main/data/io.github.diegoivan.pdf_metadata_editor.appdata.xml.in)