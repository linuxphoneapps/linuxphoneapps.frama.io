+++
title = "Add Water"
description = "Keep Firefox in fashion"
aliases = []
date = 2024-10-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "qwery",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = []

[extra]
repository = "https://github.com/largestgithubuseronearth/addwater"
homepage = "https://addwater.qwery.dev/"
bugtracker = "https://github.com/largestgithubuseronearth/addwater/issues"
donations = "https://paypal.me/RafaelMardojaiCM"
translations = "https://github.com/largestgithubuseronearth/addwater/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/dev.qwery.AddWater"
screenshots = [ "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/tags/rc1.1/docs/image-previews/banner-dark-noff.png", "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/tags/rc1.1/docs/image-previews/banner-light-noff.png", "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/tags/rc1.1/docs/image-previews/preview-dark.png", "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/tags/rc1.1/docs/image-previews/preview-light.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/heads/main/data/icons/hicolor/scalable/apps/dev.qwery.AddWater.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "dev.qwery.AddWater"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.qwery.AddWater"
flatpak_link = "https://flathub.org/apps/dev.qwery.AddWater.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/heads/main/build-aux/dev.qwery.AddWater.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "addwater",]
appstream_xml_url = "https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/heads/main/data/dev.qwery.AddWater.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/dev.qwery.addwater/"
latest_repo_commit = "2024-12-13"
repo_created_date = "2024-07-25"

+++

### Description

A utility app to easily install the Firefox GNOME Theme\* and automatically update it in the background. This theme keeps Firefox fashionable within the GNOME design ecosystem, and provides many helpful features to customize the interface to make browsing even more pleasant:

* Supports light and dark style
* Hide Tab Bar when only one tab is open
* Use an OLED-friendly pitch black instead of grey
* Customize Tab Bar position, position of controls, alignment, and more
* Hide redundant microphone, webcam, screen share indicators
* Supports Flatpak, Snap, Librewolf, and Floorp variants of Firefox

\*The Firefox GNOME Theme itself is independently developed and maintained by Rafael Mardojai CM.

The Firefox browser is not included and must be downloaded separately. This project is not affiliated or endorsed by the Mozilla Foundation. Firefox is a trademark of the Mozilla Foundation in the U.S. and other countries.

[Source](https://raw.githubusercontent.com/largestgithubuseronearth/addwater/refs/heads/main/data/dev.qwery.AddWater.metainfo.xml.in)