+++
title = "Plasma Mobile Settings"
description = "Settings application for Plasma Mobile."
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "Plasma Mobile Developers",]
categories = [ "settings",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Settings", "DesktopSettings",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-settings"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/plasma-settings/-/issues/"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/plasma-mobile/plasma-settings",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-settings"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.mobile.plasmasettings"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-settings", "kde5-plasma-settings",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2018-12-27"

+++