+++
title = "Clapper"
description = "Lean back and enjoy videos"
aliases = []
date = 2021-05-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rafał Dzięgiel",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Rafostar/clapper"
homepage = "https://rafostar.github.io/clapper"
bugtracker = "https://github.com/Rafostar/clapper/issues"
donations = "https://liberapay.com/Clapper"
translations = ""
more_information = [ "https://briandaniels.me/2021/07/06/hardware-accelerated-video-playback-on-the-pinephone-with-clapper.html",]
summary_source_url = "https://raw.githubusercontent.com/Rafostar/clapper/master/src/bin/clapper-app/data/metainfo/com.github.rafostar.Clapper.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot_01.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot_02.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot_03.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot_04.png", "https://raw.githubusercontent.com/wiki/Rafostar/clapper/media/screenshot_05.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Rafostar/clapper/refs/heads/master/doc/reference/clapper-gtk/images/clapper-logo.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.rafostar.Clapper"
scale_to_fit = "com.github.rafostar.Clapper"
flathub = "https://flathub.org/apps/com.github.rafostar.Clapper"
flatpak_link = "https://flathub.org/apps/com.github.rafostar.Clapper.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Rafostar/clapper/refs/heads/master/pkgs/flatpak/com.github.rafostar.Clapper-nightly.json"
snapcraft = "https://snapcraft.io/clapper"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "clapper",]
appstream_xml_url = "https://raw.githubusercontent.com/Rafostar/clapper/master/src/bin/clapper-app/data/metainfo/com.github.rafostar.Clapper.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-08-29"

+++

### Description

Clapper is a modern media player designed for simplicity and ease of use.
Powered by GStreamer and built for the GNOME desktop environment using
GTK4 toolkit, it has a clean and stylish interface that lets you focus
on enjoying your favorite videos.

Clapper uses a playback queue where you can add multiple media files.
Think of it like a playlist that you can build. You can easily reorder
items or remove them from the queue with a simple drag and drop operation.

[Source](https://raw.githubusercontent.com/Rafostar/clapper/master/src/bin/clapper-app/data/metainfo/com.github.rafostar.Clapper.metainfo.xml)

### Notice

Supports hardware accelerated playback on PinePhone and Librem 5.