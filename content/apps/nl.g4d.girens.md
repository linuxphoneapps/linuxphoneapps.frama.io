+++
title = "Girens for Plex"
description = "Watch your Plex content"
aliases = []
date = 2019-09-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gerben Droogers",]
categories = [ "media",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Plex",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Audio", "AudioVideo", "Player", "Video",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/tijder/girens"
homepage = "https://gitlab.gnome.org/tijder/girens"
bugtracker = "https://gitlab.gnome.org/tijder/girens/issues"
donations = ""
translations = "https://hosted.weblate.org/projects/girens/girens/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/screenshots/Albumscreen.jpg", "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/screenshots/Homescreen.jpg", "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/screenshots/Showscreen.jpg",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/icons/hicolor/scalable/apps/nl.g4d.Girens.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "nl.g4d.Girens"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.g4d.Girens"
flatpak_link = "https://flathub.org/apps/nl.g4d.Girens.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/tijder/girens/-/raw/master/nl.g4d.Girens.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "girens",]
appstream_xml_url = "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-28"
repo_created_date = "2018-12-15"

+++

### Description

Girens is a Plex media player client. You can watch and listen to your music, shows and movies with this GTK app.

[Source](https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in)