+++
title = "Haguichi"
description = "Manage your Hamachi networks"
aliases = []
date = 2025-01-20
updated = 2025-01-20

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Stephen Brandt",]
categories = [ "network",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "hamachi",]
services = [ "hamachi",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "Network", "P2P",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = []

[extra]
repository = "https://github.com/ztefn/haguichi"
homepage = "https://haguichi.net"
bugtracker = "https://github.com/ztefn/haguichi/issues"
donations = "https://haguichi.net/donate/"
translations = "https://translations.launchpad.net/haguichi"
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.ztefn.haguichi"
screenshots = [ "https://haguichi.net/appdata/screenshot-1.png", "https://haguichi.net/appdata/screenshot-2.png", "https://haguichi.net/appdata/screenshot-3.png", "https://haguichi.net/appdata/screenshot-4.png", "https://haguichi.net/appdata/screenshot-5.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/ztefn/haguichi/refs/heads/master/data/icons/hicolor/scalable/apps/com.github.ztefn.haguichi.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.ztefn.haguichi"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.ztefn.haguichi"
flatpak_link = "https://flathub.org/apps/com.github.ztefn.haguichi.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/ztefn/haguichi/refs/heads/master/com.github.ztefn.haguichi.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/ztefn/haguichi/refs/heads/master/data/com.github.ztefn.haguichi.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.github.ztefn.haguichi/"
latest_repo_commit = "2025-01-07"
repo_created_date = "2016-10-09"

+++

### Description

Haguichi makes it a breeze to join, create and administer Hamachi networks.

Features:

* Searchable, sortable and collapsible network list
* Sidebar for easy access to details and actions
* Indicator support for quick access to primary actions
* Desktop notifications to stay up to date of network events
* Commands that can be completely customized
* Backup and restore of your Hamachi configuration folder

[Source](https://raw.githubusercontent.com/ztefn/haguichi/refs/heads/master/data/com.github.ztefn.haguichi.metainfo.xml.in.in)

### Notice

It seems to work fine, but as it needs a hamachi network to properly evaluate, help with full evaluation is needed.
