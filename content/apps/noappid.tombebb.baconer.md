+++
title = "Baconer"
description = "Kirigami/ QT Reddit Client"
aliases = []
date = 2021-06-06
updated = 2024-09-02

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "tombebb",]
categories = [ "social media",]
mobile_compatibility = [ "4",]
status = [ "gone",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = [ "Reddit",]
packaged_in = []
freedesktop_categories = [ "Network", "Qt", "WebBrowser",]
programming_languages = [ "QML", "Cpp", "JavaScript",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://web.archive.org/web/20210613211515/httpss://github.com/TomBebb/Baconer/"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://twitter.com/linuxphoneapps/status/1403135178856349696",]
summary_source_url = "https://web.archive.org/web/20210613211515/httpss://github.com/TomBebb/Baconer/"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "script"

+++

### Notice

The repo has been deleted some time between October 2023 and September 2024 - it's gone :-(.

WIP, hard to rate: Fits the screen, but rendering of parts of the layout is bad on small screens and text disappears (see Screenshots).
