+++
title = "Qwertone"
description = "Turns your PC into musical instrument"
aliases = []
date = 2021-02-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andrii Zymohliad",]
categories = [ "musical tool",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo", "Education",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/azymohliad/qwertone"
homepage = "https://gitlab.com/azymohliad/qwertone"
bugtracker = "https://gitlab.com/azymohliad/qwertone/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/azymohliad/qwertone/-/raw/master/res/com.gitlab.azymohliad.Qwertone.appdata.xml"
screenshots = [ "https://i.postimg.cc/hjTJqy82/screenshot.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.azymohliad.Qwertone"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.azymohliad.Qwertone"
flatpak_link = "https://flathub.org/apps/com.gitlab.azymohliad.Qwertone.flatpakref"
flatpak_recipe = "https://gitlab.com/azymohliad/qwertone/-/raw/master/package/flatpak/flathub/com.gitlab.azymohliad.Qwertone.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "qwertone",]
appstream_xml_url = "https://gitlab.com/azymohliad/qwertone/-/raw/master/res/com.gitlab.azymohliad.Qwertone.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-30"
repo_created_date = "2019-03-23"

+++

### Description

Qwertone is a very simple music synthesizer that relies on usual QWERTY keyboard for input.

[Source](https://gitlab.com/azymohliad/qwertone/-/raw/master/res/com.gitlab.azymohliad.Qwertone.appdata.xml)

### Notice

Inactive since 2021-11-21.