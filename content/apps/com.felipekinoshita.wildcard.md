+++
title = "Wildcard"
description = "Test your regular expressions"
aliases = []
date = 2024-05-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Development",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Wildcard"
homepage = "https://gitlab.gnome.org/World/Wildcard"
bugtracker = "https://gitlab.gnome.org/World/Wildcard/-/issues"
donations = "https://ko-fi.com/fkinoshita"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.felipekinoshita.Wildcard"
screenshots = [ "https://gitlab.gnome.org/World/Wildcard/-/raw/main/data/screenshots/dark.png", "https://gitlab.gnome.org/World/Wildcard/-/raw/main/data/screenshots/preview.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/Wildcard/-/raw/main/data/icons/hicolor/scalable/apps/com.felipekinoshita.Wildcard.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.felipekinoshita.Wildcard"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.felipekinoshita.Wildcard"
flatpak_link = "https://flathub.org/apps/com.felipekinoshita.Wildcard.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/Wildcard/-/raw/main/build-aux/com.felipekinoshita.Wildcard.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "wildcard",]
appstream_xml_url = "https://gitlab.gnome.org/World/Wildcard/-/raw/main/data/com.felipekinoshita.Wildcard.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-15"
repo_created_date = "2023-10-20"

+++


### Description

Wildcard gives you a nice and simple to use interface to test/practice
regular expressions.

[Source](https://gitlab.gnome.org/World/Wildcard/-/raw/main/data/com.felipekinoshita.Wildcard.metainfo.xml.in.in)