+++
title = "Qt Virtual Keyboard"
description = "The Qt Virtual Keyboard project provides an input framework and reference keyboard frontend for Qt 5 on Linux Desktop/X11, Windows Desktop, and Boot2Qt targets."
aliases = []
date = 2019-02-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Qt Company",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "no icon", "manual status maintenance",]

[extra]
repository = "https://code.qt.io/cgit/qt/qtvirtualkeyboard.git"
homepage = "https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html"
screenshots = [ "https://doc.qt.io/qt-5/qtvirtualkeyboard-basic-example.html",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git"
latest_repo_commit = "2024-11-29"
repo_created_date = "2013-11-29"

+++
