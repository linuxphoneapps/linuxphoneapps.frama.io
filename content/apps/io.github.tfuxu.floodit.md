+++
title = "Flood It"
description = "Flood the board"
aliases = ["games/io.github.tfuxu.floodit/"]
date = 2024-10-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "tfuxu",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "BoardGame", "Game", "StrategyGame",]
programming_languages = [ "Go",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/tfuxu/floodit"
homepage = "https://github.com/tfuxu/floodit"
bugtracker = "https://github.com/tfuxu/floodit/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.tfuxu.floodit"
screenshots = [ "https://raw.githubusercontent.com/tfuxu/floodit/master/data/screenshots/screenshot-2.png", "https://raw.githubusercontent.com/tfuxu/floodit/master/data/screenshots/screenshot-3.png", "https://raw.githubusercontent.com/tfuxu/floodit/master/data/screenshots/screenshot-4.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/tfuxu/floodit/refs/heads/master/data/icons/hicolor/scalable/apps/io.github.tfuxu.floodit.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.tfuxu.floodit"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.tfuxu.floodit"
flatpak_link = "https://flathub.org/apps/io.github.tfuxu.floodit.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/tfuxu/floodit/refs/heads/master/build-aux/flatpak/io.github.tfuxu.floodit.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/tfuxu/floodit/refs/heads/master/data/io.github.tfuxu.floodit.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/games/io.github.tfuxu.floodit/"
latest_repo_commit = "2024-11-23"
repo_created_date = "2024-09-12"

+++


### Description

Flood It is a game with the simple premise of flooding the entire board with one color in the least amount of moves possible.
Challenge yourself with this simple, yet addictive strategy game, where you need to flood-it as efficiently as you can!

[Source](https://raw.githubusercontent.com/tfuxu/floodit/refs/heads/master/data/io.github.tfuxu.floodit.metainfo.xml.in.in)
