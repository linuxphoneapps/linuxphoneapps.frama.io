+++
title = "Video Player"
description = "Play local and remote videos"
aliases = [ "apps/org.kde.mobile.vplayer/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge",]
freedesktop_categories = [ "Qt", "KDE", "Video", "Player",]
programming_languages = [ "JavaScript", "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/jbbgameich/videoplayer"
homepage = ""
bugtracker = "https://invent.kde.org/jbbgameich/videoplayer/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/jbbgameich/videoplayer/-/raw/master/org.kde.mobile.vplayer.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.vplayer"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "plasma-videoplayer",]
appstream_xml_url = "https://invent.kde.org/jbbgameich/videoplayer/-/raw/master/org.kde.mobile.vplayer.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_repology"
latest_repo_commit = "2022-02-01"
repo_created_date = "2018-12-19"

+++

### Description

This application also has support for watching videos from youtube, the local storage or streams.

[Source](https://invent.kde.org/jbbgameich/videoplayer/-/raw/master/org.kde.mobile.vplayer.appdata.xml)

### Notice

Last commit in February 2020.