+++
title = "Lock"
description = "Process data with GnuPG"
aliases = []
date = 2024-10-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Konstantin Tutsch",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "GPG",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/konstantintutsch/Lock"
homepage = "https://konstantintutsch.com/Lock"
bugtracker = "https://github.com/konstantintutsch/Lock/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/Lock/"
more_information = []
summary_source_url = "https://flathub.org/apps/com.konstantintutsch.Lock"
screenshots = [ "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Dark.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.File.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Generate.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Key.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Light.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Showcase.1Text.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Showcase.2Select.png", "https://github.com/konstantintutsch/Lock/raw/refs/heads/main/data/com.konstantintutsch.Lock.Screenshot.Showcase.3Encrypted.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/konstantintutsch/Lock/refs/heads/main/data/icons/com.konstantintutsch.Lock.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = false
app_id = "com.konstantintutsch.Lock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.konstantintutsch.Lock"
flatpak_link = "https://flathub.org/apps/com.konstantintutsch.Lock.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/konstantintutsch/Lock/refs/heads/main/build-aux/com.konstantintutsch.Lock.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/konstantintutsch/Lock/refs/heads/main/data/com.konstantintutsch.Lock.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.konstantintutsch.lock/"
latest_repo_commit = "2025-01-04"
repo_created_date = "2024-10-11"

+++

### Description

Lock is a graphical front-end for GnuPG (GPG) making use of a beautiful LibAdwaita GUI.

Process text and files:

* Encryption
* Decryption
* Signing
* Verification

Manage your GnuPG keyring:

* Generate new key pairs
* Import keys
* Export public keys
* View expire dates
* Update expire dates of expired keys
* Remove keys

[Source](https://raw.githubusercontent.com/konstantintutsch/Lock/refs/heads/main/data/com.konstantintutsch.Lock.metainfo.xml.in.in)
