+++
title = "Quicky in search of Mickey"
description = "In a beautiful magical forest magic live animals. Among them are our friends Quicky and Mickey. But once Mickey got lost. What happened to him? Quicky will have to find his brother and unravel the mysteries of the magic forest."
aliases = ["games/net.overmy.adventure/"]
date = 2019-03-17
updated = 2025-01-07

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = ["cyberbach",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "AdventureGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/cyberbach/Adventure"
homepage = ""
bugtracker = "https://github.com/cyberbach/Adventure/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=net.overmy.adventure"
screenshots = [ "https://play.google.com/store/apps/details?id=net.overmy.adventure",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.overmy.adventure"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/net.overmy.adventure/"
latest_repo_commit = "2018-04-17"
repo_created_date = "2018-03-07"

+++
