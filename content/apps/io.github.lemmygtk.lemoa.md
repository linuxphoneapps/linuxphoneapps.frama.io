+++
title = "Lemoa"
description = "Gtk client for Lemmy"
aliases = []
date = 2023-08-21
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bnyro",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Lemmy",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lemmygtk/lemoa"
homepage = "https://github.com/lemmygtk/lemoa"
bugtracker = "https://github.com/lemmygtk/lemoa/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lemmygtk/lemoa/main/data/io.github.lemmygtk.lemoa.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/lemmygtk/lemoa/main/data/screenshots/community.png", "https://raw.githubusercontent.com/lemmygtk/lemoa/main/data/screenshots/posts.png", "https://raw.githubusercontent.com/lemmygtk/lemoa/main/data/screenshots/user.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lemmygtk.lemoa"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lemoa",]
appstream_xml_url = "https://raw.githubusercontent.com/lemmygtk/lemoa/main/data/io.github.lemmygtk.lemoa.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2024-02-12"
repo_created_date = "2023-06-18"

+++

### Description

A native Gtk client for the Lemmyverse

[Source](https://raw.githubusercontent.com/lemmygtk/lemoa/main/data/io.github.lemmygtk.lemoa.metainfo.xml.in)

### Notice

Not fully adaptive - get's too wide while browsing Lemmy.
Archived on February 12th, 2024.