+++
title = "OpenTodoList"
description = "Todo list and note taking application"
aliases = []
date = 2021-06-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martin Höher",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Nextcloud Notes", "OwnCloud Notes", "WebDAV", "Dropbox",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/rpdev/opentodolist"
homepage = "https://opentodolist.rpdev.net/"
bugtracker = "https://gitlab.com/rpdev/opentodolist/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.appdata.xml"
screenshots = [ "https://opentodolist.rpdev.net/images/appstream/library_page.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/rpdev/opentodolist/-/raw/development/assets/OpenTodoList-Icon.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "net.rpdev.OpenTodoList"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.rpdev.OpenTodoList"
flatpak_link = "https://flathub.org/apps/net.rpdev.OpenTodoList.flatpakref"
flatpak_recipe = "https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "opentodolist",]
appstream_xml_url = "https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2015-03-08"

+++

### Description

OpenTodoList is a todo list and note taking application. It is
available for Linux, macOS and Windows as well as on Android.
Your information can be shared across devices to keep everything
up date date, where ever you go.

* Organize todo lists, notes and other items in libraries
* Synchronize out-of-the-box with NextCloud, ownCloud and other WebDAV
  servers
* Use sync clients of your choice to synchronize with arbitrary
  services
* Schedule items by setting due dates on them
* Use Markdown in item titles and descriptions to format text
  as you like

[Source](https://gitlab.com/rpdev/opentodolist/-/raw/development/net.rpdev.OpenTodoList.metainfo.xml)