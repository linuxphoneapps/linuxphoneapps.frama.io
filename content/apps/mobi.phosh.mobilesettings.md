+++
title = "Phosh Mobile Settings"
description = "Advanced settings for Mobile devices using Phosh"
aliases = [ "apps/org.sigxcpu.mobilesettings/",]
date = 2022-06-29
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Phosh developers",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/guidog/phosh-mobile-settings"
homepage = "https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings"
bugtracker = "https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/guido.gunther/phosh-mobile-settings"
screenshots = [ "https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/-/raw/main/screenshots/compositor.png?inline=false", "https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/-/raw/main/screenshots/feedback.png?inline=false", "https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/-/raw/main/screenshots/panels.png?inline=false",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/1.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/2.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/3.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/4.png", "https://img.linuxphoneapps.org/org.sigxcpu.mobilesettings/5.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "mobi.phosh.MobileSettings"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "phosh-mobile-settings",]
appstream_xml_url = "https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/-/raw/main/data/mobi.phosh.MobileSettings.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.sigxcpu.mobilesettings/"
latest_repo_commit = "2024-12-31"
repo_created_date = "2023-12-17"

+++

### Description

This app allows you to configure some aspects of Phosh that would otherwise
require command line usage.

[Source](https://gitlab.gnome.org/World/Phosh/phosh-mobile-settings/-/raw/main/data/mobi.phosh.MobileSettings.metainfo.xml.in)

### Notice

No more manual fiddling with feedbackd and no more APP ID guessing for scale-to-fit.