+++
title = "Alpaca"
description = "Chat with local AI models"
aliases = []
date = 2024-08-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jeffry Samuel Eduarte Rojas",]
categories = [ "llm chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Ollama",]
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Chat", "Development", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Jeffser/Alpaca"
homepage = "https://jeffser.com/alpaca/"
bugtracker = "https://github.com/Jeffser/Alpaca/issues"
donations = "https://github.com/sponsors/Jeffser"
translations = "https://github.com/Jeffser/Alpaca/discussions/153"
more_information = []
summary_source_url = "https://flathub.org/apps/com.jeffser.Alpaca"
screenshots = [ "https://jeffser.com/images/alpaca/screenie1.png", "https://jeffser.com/images/alpaca/screenie2.png", "https://jeffser.com/images/alpaca/screenie3.png", "https://jeffser.com/images/alpaca/screenie4.png", "https://jeffser.com/images/alpaca/screenie5.png", "https://jeffser.com/images/alpaca/screenie6.png", "https://jeffser.com/images/alpaca/screenie7.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Jeffser/Alpaca/main/data/icons/hicolor/scalable/apps/com.jeffser.Alpaca.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.jeffser.Alpaca"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.jeffser.Alpaca"
flatpak_link = "https://flathub.org/apps/com.jeffser.Alpaca.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Jeffser/Alpaca/main/com.jeffser.Alpaca.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "alpaca",]
appstream_xml_url = "https://raw.githubusercontent.com/Jeffser/Alpaca/main/data/com.jeffser.Alpaca.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.jeffser.alpaca/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-05-11"

+++

### Description

An Ollama client

Features

* Built in Ollama instance
* Talk to multiple models in the same conversation
* Pull and delete models from the app
* Have multiple conversations
* Image recognition (Only available with compatible models)
* Plain text documents recognition
* Import and export chats
* Append YouTube transcripts to the prompt
* Append text from a website to the prompt
* PDF recognition

Disclaimer

This project is not affiliated at all with Ollama, I'm not responsible for any damages to your device or software caused by running code given by any models.

[Source](https://raw.githubusercontent.com/Jeffser/Alpaca/main/data/com.jeffser.Alpaca.metainfo.xml.in)