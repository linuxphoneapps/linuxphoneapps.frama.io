+++
title = "KStars"
description = "Desktop Planetarium"
aliases = []
date = 2020-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "scientific",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_experimental", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Astronomy", "Education", "Science",]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://invent.kde.org/education/kstars"
homepage = "https://kstars.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=kstars"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/kstars"
screenshots = [ "https://cdn.kde.org/screenshots/kstars/kstars_ekos.png", "https://cdn.kde.org/screenshots/kstars/kstars_interesting.png", "https://cdn.kde.org/screenshots/kstars/kstars_main.png", "https://cdn.kde.org/screenshots/kstars/kstars_planner.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kstars"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kstars"
flatpak_link = "https://flathub.org/apps/org.kde.kstars.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kstars",]
appstream_xml_url = "https://invent.kde.org/education/kstars/-/raw/master/org.kde.kstars.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.kstars/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-05-16"

+++

### Description

KStars is freely licensed, open source, cross-platform Astronomy Software by KDE.

It provides an accurate graphical simulation of the night sky, from any location on Earth, at any date and time. The display includes up to 100 million stars, 13,000 deep-sky objects,all 8 planets, the Sun and Moon, and thousands of comets, asteroids, supernovae, and satellites.

For students and teachers, it supports adjustable simulation speeds in order to view phenomena that happen over long timescales, the KStars Astrocalculator to predict conjunctions, and many common astronomical calculations.

For the amateur astronomer, it provides an observation planner, a sky calendar tool, and an FOV editor to calculate field of view of equipment and display them. Find out interesting objects in the "What's up Tonight" tool, plot altitude vs. time graphs for any object, print high-quality sky charts, and gain access to lots of information and resources to help you explore the universe!

Included with KStars is Ekos astrophotography suite, a complete astrophotography solution that can control all INDI devices including numerous telescopes, CCDs, DSLRs, focusers, filters, and a lot more. Ekos supports highly accurate tracking using online and offline astrometry solver, autofocus and autoguiding capabilities, and capture of single or multiple images using the powerful built in sequence manager.

Astronomical Catalogs:

* Default catalog consisting of stars to magnitude 8
* Extra catalogs consisting of 100 million stars to magnitude 16
* Downloadable catalogs including Messier Images, Abell Planetary Nebulae, Sharpless Catalog, Lynds Dark Nebula Catalog
* Corrections for precession, nutation and atmospheric refraction
* Tools for retrieval of data from online databases

[Source](https://invent.kde.org/education/kstars/-/raw/master/org.kde.kstars.appdata.xml)

### Notice

Originally added as it had a mobile-friendly Kirigami variant at some point.