+++
title = "Superjumper"
description = "Super Jumper is a very simple Doodle Jump clone."
aliases = ["games/com.badlogicgames.superjumper/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "libGDX"]
categories = [ "demo", "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/libgdx/libgdx-demo-superjumper"
homepage = ""
bugtracker = "https://github.com/libgdx/libgdx-demo-superjumper/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/libgdx/libgdx-demo-superjumper"
screenshots = [ "https://github.com/libgdx/libgdx-demo-superjumper",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.badlogicgames.superjumper"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/com.badlogicgames.superjumper/"
latest_repo_commit = "2022-03-22"
repo_created_date = "2014-04-01"

+++
