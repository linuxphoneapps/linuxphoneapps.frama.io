+++
title = "Tokodon"
description = "Browse the Fediverse"
aliases = []
date = 2021-05-29
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Mastodon",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/network/tokodon"
homepage = "https://apps.kde.org/tokodon/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Tokodon"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/", "https://plasma-mobile.org/2022/04/26/plasma-mobile-gear-22-04/#tokodon", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#tokodon", "https://kde.org/announcements/megarelease/6/#tokodon",]
summary_source_url = "https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/tokodon/tokodon-desktop.png", "https://cdn.kde.org/screenshots/tokodon/tokodon-notifications.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.tokodon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.tokodon"
flatpak_link = "https://flathub.org/apps/org.kde.tokodon.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "tokodon",]
appstream_xml_url = "https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2021-05-08"

+++

### Description

Tokodon is a Mastodon client. It allows you to interact with the Fediverse community.

[Source](https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml)