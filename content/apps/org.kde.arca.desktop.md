+++
title = "Arca"
description = "Archive manager"
aliases = []
date = 2024-09-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community"]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = [ "MauiKit Archiver",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "manjaro_stable", "manjaro_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Archiving", "Compression", "KDE", "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/arca"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/arca/-/issues"
donations = "https://www.kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/arca/-/raw/master/data/org.kde.arca.appdata.xml"
screenshots = [ "https://mauikit.org/wp-content/uploads/2022/11/arca.jpg",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/arca/-/raw/master/src/assets/logo.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.arca.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "arca",]
appstream_xml_url = "https://invent.kde.org/maui/arca/-/raw/master/data/org.kde.arca.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.arca.desktop/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-01-04"

+++

### Description

Browse, create and manage archives.

[Source](https://invent.kde.org/maui/arca/-/raw/master/data/org.kde.arca.appdata.xml)
