+++
title = "Errands"
description = "Manage your tasks"
aliases = []
date = 2023-04-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Vlad Krupinskii",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "CalDAV",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/mrvladus/Errands/"
homepage = "https://github.com/mrvladus/Errands"
bugtracker = "https://github.com/mrvladus/Errands/issues"
donations = "https://github.com/mrvladus/Errands/blob/main/DONATIONS.md"
translations = "https://github.com/mrvladus/Errands/blob/main/TRANSLATIONS.md"
more_information = [ "https://apps.gnome.org/List/",]
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/mrvladus/Errands/main/screenshots/main.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.mrvladus.list/1.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/2.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/3.png", "https://img.linuxphoneapps.org/io.github.mrvladus.list/4.png",]
svg_icon_url = "https://github.com/mrvladus/Errands/raw/main/data/icons/hicolor/scalable/apps/io.github.mrvladus.List.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.mrvladus.List"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.mrvladus.List"
flatpak_link = "https://flathub.org/apps/io.github.mrvladus.List.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/mrvladus/Errands/main/io.github.mrvladus.List.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/mrvladus/Errands/main/data/io.github.mrvladus.List.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-04-04"

+++

### Description

Todo application for those who prefer simplicity.

Features:

* Multiple task lists support
* Add, remove, edit tasks and sub-tasks
* Mark task and sub-tasks as completed
* Add accent color for each task
* Sync tasks with Nextcloud or other CalDAV providers
* Drag and Drop support
* Import .ics files

[Source](https://raw.githubusercontent.com/mrvladus/Errands/main/data/io.github.mrvladus.List.metainfo.xml.in.in)