+++
title = "MartianRun"
description = "A 2D running game built with libGDX."
aliases = ["games/com.gamestudio24.martianrun/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = []
app_author = ["wmora",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ActionGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/wmora/martianrun"
homepage = ""
bugtracker = "https://github.com/wmora/martianrun/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/wmora/martianrun"
screenshots = [ "https://play.google.com/store/apps/details?id=com.gamestudio24.cityescape.android&utm_source=global_co&utm_medium=prtnr&utm_content=Mar2515&utm_campaign=PartBadge&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.gamestudio24.martianrun"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/com.gamestudio24.martianrun/"
latest_repo_commit = "2016-05-02"
repo_created_date = "2014-06-07"

+++
