+++
title = "Password"
description = "Password calculator and generator"
aliases = [ "apps/org.emilien.password/",]
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emilien Lescoute",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/elescoute/password"
homepage = "https://gitlab.com/elescoute/password"
bugtracker = "https://gitlab.com/elescoute/password/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/elescoute/password/-/raw/main/data/io.gitlab.elescoute.password.metainfo.xml.in"
screenshots = [ "https://gitlab.com/elescoute/password/raw/main/data/appdata/screenshot01.png", "https://gitlab.com/elescoute/password/raw/main/data/appdata/screenshot02.png", "https://gitlab.com/elescoute/password/raw/main/data/appdata/screenshot03.png", "https://gitlab.com/elescoute/password/raw/main/data/appdata/screenshot04.png", "https://gitlab.com/elescoute/password/raw/main/data/appdata/screenshot05.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/elescoute/password/-/raw/main/data/icons/hicolor/scalable/apps/io.gitlab.elescoute.password.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.elescoute.password"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.elescoute.password"
flatpak_link = "https://flathub.org/apps/io.gitlab.elescoute.password.flatpakref"
flatpak_recipe = "https://gitlab.com/elescoute/password/-/raw/main/build-aux/flatpak/io.gitlab.elescoute.password.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "password-for-gnome-vala",]
appstream_xml_url = "https://gitlab.com/elescoute/password/-/raw/main/data/io.gitlab.elescoute.password.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/org.emilien.password/"
latest_repo_commit = "2024-11-19"
repo_created_date = "2024-05-05"

+++


### Description

Password is a password calculator and random generator for Gnome written in Gtk/Vala.

This app calculates strong unique passwords for each alias and passphrase combination.
No need to remember dozens of passwords any longer and no need for a password manager!

The calculator can use MD5, SHA1, SHA256 and SHA512 hash algorithms in combination with HMAC.

Compatible with Linux Phone using Phosh (PinePhone, Librem 5).

[Source](https://gitlab.com/elescoute/password/-/raw/main/data/io.gitlab.elescoute.password.metainfo.xml.in)

### Notice

Was GTK3/libhandy before release 1.4.0.