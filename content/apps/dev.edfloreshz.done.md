+++
title = "Done"
description = "To-do lists reimagined"
aliases = []
date = 2024-02-13
updated = 2024-12-01

[taxonomies]
project_licenses = [ "MPL-2.0",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Eduardo Flores",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Microsoft Todo",]
packaged_in = [ "aur", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Database", "Office",]
programming_languages = [ "Rust", "Fluent",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/edfloreshz/done"
homepage = ""
bugtracker = "https://github.com/edfloreshz/done/issues"
donations = "https://github.com/sponsors/edfloreshz"
translations = "https://github.com/edfloreshz/done/tree/main/i18n"
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/about.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/content-dark.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/content-light.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/sidebar-dark.png", "https://raw.githubusercontent.com/edfloreshz/done/main/data/resources/screenshots/sidebar-light.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/done-devs/done/main/data/icons/dev.edfloreshz.Done.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "dev.edfloreshz.Done"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://flathub.org/apps/dev.edfloreshz.Done.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/done-devs/done/main/build-aux/dev.edfloreshz.Done.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "done",]
appstream_xml_url = "https://raw.githubusercontent.com/done-devs/done/main/data/dev.edfloreshz.Done.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-04-19"
repo_created_date = "2022-03-18"

+++

### Description

The ultimate task management solution for seamless organization and efficiency.


Done is a user\-friendly app that allows you to effortlessly consolidate your existing task providers into a single application for optimal productivity and organization.


Current features:


* Create tasks and lists
* Add task service providers
* Edit your tasks and lists
* Delete tasks and lists

[Source](https://raw.githubusercontent.com/done-devs/done/main/data/dev.edfloreshz.Done.metainfo.xml.in.in)

### Notice

Archived on April 19, 2024.
Did not fit the screen without scale-to-fit, being a few pixels too wide.