+++
title = "Disks"
description = "Disk management utility for GNOME"
aliases = []
date = 2020-12-12
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-disk-utility"
homepage = "https://wiki.gnome.org/Apps/Disks"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/DiskUtility/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-additional-partition-options.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-application-menu.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-delete-selected-partition.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-drive-options.png", "https://gitlab.gnome.org/GNOME/gnome-disk-utility/raw/HEAD/data/screenshots/disks-main.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.DiskUtility.svg?ref_type=heads"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.DiskUtility"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/main/flatpak/org.gnome.DiskUtility.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-disk-utility",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2018-05-23"

+++

### Description

Disks provides an easy way to inspect, format, partition, and configure disks
and block devices.

Using Disks, you can view SMART data, manage devices, benchmark physical
disks, and image USB sticks.

[Source](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in)