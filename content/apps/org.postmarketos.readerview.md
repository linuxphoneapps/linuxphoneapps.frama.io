+++
title = "Readerview"
description = "A mobile-ready gpu-accelerated gemini browser."
aliases = []
date = 2021-05-13
updated = 2024-10-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "gemini browser",]
mobile_compatibility = [ "3",]
status = [ "released", "inactive", ]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/readerview"
latest_repo_commit = "2021-12-20"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://sr.ht/~martijnbraam/Readerview/"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1392920004442591233",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~martijnbraam/readerview/blob/master/org.postmarketos.readerview.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.readerview"
scale_to_fit = "org.postmarketos.readerview"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
repo_created_date = "2020-10-24"

+++

### Notice

Part of the UI vanish when on Gemini sites, even with scale-to-fit applied.
