+++
title = "Unfettered Keyboard"
description = "The Unfettered Keyboard is an on-screen keyboard for Linux mobile."
aliases = []
date = 2024-08-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "flamingradian",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = []
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/flamingradian/unfettered-keyboard"
homepage = "https://gitlab.com/flamingradian/unfettered-keyboard"
bugtracker = "https://gitlab.com/flamingradian/unfettered-keyboard/-/issues"
donations = ""
translations = ""
more_information = [ "https://social.sineware.ca/@flamingradian/112935825486540292",]
summary_source_url = "https://gitlab.com/flamingradian/unfettered-keyboard"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.flamingradian.unfettered-keyboard/"
latest_repo_commit = "2024-12-27"
repo_created_date = "2024-04-16"

+++

### Description

On-screen keyboard for "Ctrl+Shift+N curl -OL http://localhost:8080/index.html" that works on Linux Mobile (Wayland)

[Source](https://gitlab.com/flamingradian/unfettered-keyboard)

### Notice

Check the [readme](https://gitlab.com/flamingradian/unfettered-keyboard/-/blob/main/README.md) for how to set this up on Phosh, Plasma Mobile and Sxmo.