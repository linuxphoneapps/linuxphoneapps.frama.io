+++
title = "Paper"
description = "Take notes in Markdown"
aliases = []
date = 2022-05-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Zagura",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "TextEditor", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/posidon_software/paper"
homepage = "https://gitlab.com/posidon_software/paper"
bugtracker = "https://gitlab.com/posidon_software/paper/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/posidon_software/paper"
screenshots = [ "https://zagura.one/dev/paper/screenshot/22_0801_desktop.png", "https://zagura.one/dev/paper/screenshot/22_0801_desktop_minimal.png", "https://zagura.one/dev/paper/screenshot/22_0801_mobile.png", "https://zagura.one/dev/paper/screenshot/22_0801_mobile_list.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "io.posidon.Paper"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.posidon.Paper"
flatpak_link = "https://flathub.org/apps/io.posidon.Paper.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "paper-note",]
appstream_xml_url = "https://gitlab.com/posidon_software/paper/-/raw/main/data/app.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-11"
repo_created_date = "2022-01-24"

+++

### Description

Create notebooks, take notes in markdown

Features:

* Almost WYSIWYG markdown rendering
* Searchable through GNOME search
* Highlight & Strikethrough text formatting
* App recoloring based on notebook color
* Trash can

[Source](https://gitlab.com/posidon_software/paper/-/raw/main/data/app.metainfo.xml.in)