+++
title = "blabber"
description = "QML Kirigami based Mastodon client"
aliases = []
date = 2019-02-01
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "err4nt",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Network",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/err4nt/blabber"
homepage = ""
bugtracker = "https://github.com/err4nt/blabber/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/err4nt/blabber"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_github_api"
latest_repo_commit = "2017-09-04"
repo_created_date = "2017-09-04"

+++

### Notice

Last commit September 2017.