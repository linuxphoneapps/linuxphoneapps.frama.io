+++
title = "Kana"
description = "Learn Japanese characters"
aliases = []
date = 2024-02-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Education",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/fkinoshita/Kana"
homepage = "https://gitlab.gnome.org/fkinoshita/Kana"
bugtracker = "https://gitlab.gnome.org/fkinoshita/Kana/-/issues"
donations = "https://ko-fi.com/fkinoshita"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.felipekinoshita.Kana"
screenshots = [ "https://gitlab.gnome.org/fkinoshita/Kana/-/raw/main/data/screenshots/dark.png", "https://gitlab.gnome.org/fkinoshita/Kana/-/raw/main/data/screenshots/light.png", "https://gitlab.gnome.org/fkinoshita/Kana/-/raw/main/data/screenshots/welcome.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/fkinoshita/kana/-/raw/main/data/icons/hicolor/scalable/apps/com.felipekinoshita.Kana.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "com.felipekinoshita.Kana"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.felipekinoshita.Kana"
flatpak_link = "https://flathub.org/apps/com.felipekinoshita.Kana.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/fkinoshita/kana/-/raw/main/build-aux/com.felipekinoshita.Kana.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kana",]
appstream_xml_url = "https://gitlab.gnome.org/fkinoshita/kana/-/raw/main/data/com.felipekinoshita.Kana.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-08-08"
repo_created_date = "2023-10-25"

+++


### Description

Hone your Japanese skills by matching romanized characters to their correct hiragana and katakana counterparts.

[Source](https://gitlab.gnome.org/fkinoshita/kana/-/raw/main/data/com.felipekinoshita.Kana.metainfo.xml.in.in)

### Notice

It does not show japanese characters out of the box on postmarketOS, see [related issue](https://gitlab.gnome.org/fkinoshita/kana/-/issues/12)