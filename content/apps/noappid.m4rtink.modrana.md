+++
title = "modRana"
description = "ModRana is a flexible GPS navigation system for mobile devices."
aliases = []
date = 2021-07-29
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "m4rtink",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Utility", "Maps",]
programming_languages = [ "Python", "QML",]
build_systems = [ "qmake", "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/M4rtinK/modrana"
homepage = "https://modrana.org/trac"
bugtracker = "https://github.com/M4rtinK/modrana/issues/"
donations = ""
translations = ""
more_information = [ "https://modrana.org/trac#History", "https://twitter.com/linuxphoneapps/status/1420817607112933377",]
summary_source_url = "https://github.com/M4rtinK/modrana"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = "modrana"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "modrana",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2022-01-02"
repo_created_date = "2012-03-22"

+++

### Notice

Great app, it really should be packaged in more distributions.To help with this, I created a PKGBUILD script that builds the current state from git: https://framagit.org/linmobapps/pkgbuilds/-/tree/main/modrana-git . Does not fit the screen after first launch for me, but toggling the upwards pointing arrow for full screen fixes this for me.