+++
title = "Solanum"
description = "Balance working time and break time"
aliases = []
date = 2020-10-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Christopher Davis",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/Solanum/"
homepage = "https://apps.gnome.org/Solanum"
bugtracker = "https://gitlab.gnome.org/World/Solanum/-/issues"
donations = "https://patreon.com/chrisgnome"
translations = "https://l10n.gnome.org/module/Solanum/"
more_information = [ "https://apps.gnome.org/Solanum/",]
summary_source_url = "https://gitlab.gnome.org/World/Solanum/-/raw/main/data/org.gnome.Solanum.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Solanum/-/raw/main/data/screenshots/screenshot-1.png", "https://gitlab.gnome.org/World/Solanum/-/raw/main/data/screenshots/screenshot-2.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/Solanum/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Solanum.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Solanum"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Solanum"
flatpak_link = "https://flathub.org/apps/org.gnome.Solanum.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "solanum-pomodoro",]
appstream_xml_url = "https://gitlab.gnome.org/World/Solanum/-/raw/main/data/org.gnome.Solanum.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-29"
repo_created_date = "2020-09-03"

+++

### Description

Solanum is a time tracking app that uses the pomodoro technique. Work in 4 sessions,
with breaks in between each session and one long break after all 4.

[Source](https://gitlab.gnome.org/World/Solanum/-/raw/main/data/org.gnome.Solanum.appdata.xml.in.in)