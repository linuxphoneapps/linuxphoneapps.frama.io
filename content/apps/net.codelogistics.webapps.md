+++
title = "Web Apps"
description = "Install websites as apps"
aliases = []
date = 2024-05-20
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Satvik Patwardhan",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "webkit2gtk",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/eyekay/webapps"
homepage = "https://codeberg.org/eyekay/webapps"
bugtracker = "https://codeberg.org/eyekay/webapps/issues"
donations = ""
translations = "https://translate.codeberg.org/engage/web-apps//"
more_information = []
summary_source_url = "https://flathub.org/apps/net.codelogistics.webapps"
screenshots = [ "https://codelogistics.net/webapps4.png", "https://codelogistics.net/webapps5.png", "https://codelogistics.net/webapps6.png",]
screenshots_img = []
svg_icon_url = "https://codeberg.org/eyekay/webapps/raw/branch/main/data/icons/hicolor/scalable/apps/net.codelogistics.webapps.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "net.codelogistics.webapps"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.codelogistics.webapps"
flatpak_link = "https://flathub.org/apps/net.codelogistics.webapps.flatpakref"
flatpak_recipe = "https://codeberg.org/eyekay/webapps/raw/branch/main/net.codelogistics.webapps.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/eyekay/webapps/raw/branch/main/data/net.codelogistics.webapps.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2023-07-31"

+++

### Description

Install websites as desktop apps, so that they appear in their own windows separate from any browsers installed.
This is similar to the "Install as App" feature found in popular web browsers.

It uses an internal browser isolated from the system browser, which can optionally be used in incognito mode.
Links to websites other than the one installed open in a new browser window.

[Source](https://codeberg.org/eyekay/webapps/raw/branch/main/data/net.codelogistics.webapps.appdata.xml.in)