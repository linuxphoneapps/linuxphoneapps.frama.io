+++
title = "Showtime"
description = "Watch without distraction"
aliases = []
date = 2024-06-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "kramo",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "alpine_edge", "arch", "aur", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Player", "Utility", "Video",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/Incubator/showtime"
homepage = "https://apps.gnome.org/Showtime/"
bugtracker = "https://gitlab.gnome.org/GNOME/Incubator/showtime/-/issues"
donations = ""
translations = "https://l10n.gnome.org/module/showtime/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/Incubator/showtime/-/raw/main/data/org.gnome.Showtime.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/Incubator/showtime/-/raw/main/data/screenshots/1.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/Incubator/showtime/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Showtime.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Showtime"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Showtime"
flatpak_link = "https://flathub.org/apps/org.gnome.Showtime.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/Incubator/showtime/-/raw/main/build-aux/flatpak/org.gnome.Showtime.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "showtime",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/Incubator/showtime/-/raw/main/data/org.gnome.Showtime.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-05-01"

+++

### Description

Play your favorite movies and video files without hassle. Showtime features simple playback controls that fade out of your way when you're watching, fullscreen, adjustable playback speed, multiple language and subtitle tracks, and screenshots — everything you need for a straightforward viewing experience.

[Source](https://gitlab.gnome.org/GNOME/Incubator/showtime/-/raw/main/data/org.gnome.Showtime.metainfo.xml.in)