+++
title = "Lorem"
description = "Generate placeholder text"
aliases = []
date = 2024-05-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maximiliano Sandoval",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/design/lorem"
homepage = "https://apps.gnome.org/Lorem/"
bugtracker = "https://gitlab.gnome.org/World/design/lorem/issues"
donations = ""
translations = "https://l10n.gnome.org/module/lorem/"
more_information = [ "https://apps.gnome.org/Lorem/",]
summary_source_url = "https://flathub.org/apps/org.gnome.design.Lorem"
screenshots = [ "https://gitlab.gnome.org/World/design/lorem/-/raw/master/screenshots/main_window.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/design/lorem/-/raw/master/data/icons/org.gnome.design.Lorem.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.design.Lorem"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.design.Lorem"
flatpak_link = "https://flathub.org/apps/org.gnome.design.Lorem.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/design/lorem/-/raw/master/build-aux/org.gnome.design.Lorem.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lorem",]
appstream_xml_url = "https://gitlab.gnome.org/World/design/lorem/-/raw/master/data/org.gnome.design.Lorem.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2021-08-03"

+++

### Description

Simple app to generate the well-known Lorem Ipsum placeholder text.

[Source](https://gitlab.gnome.org/World/design/lorem/-/raw/master/data/org.gnome.design.Lorem.metainfo.xml.in.in)