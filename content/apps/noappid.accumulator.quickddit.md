+++
title = "Quickddit"
description = "A free and open source Reddit client for mobile phones."
aliases = []
date = 2020-12-22
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "accumulator",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "QtQuick",]
backends = []
services = [ "reddit",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "News",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/accumulator/Quickddit"
homepage = ""
bugtracker = "https://github.com/accumulator/Quickddit/issues/"
donations = ""
translations = ""
more_information = [ "https://linmob.net/2020/10/06/reddit-clients-for-mobile-linux.html",]
summary_source_url = "https://open-store.io/app/quickddit"
screenshots = [ "https://fosstodon.org/@linmob/105424788379486065",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/accumulator/Quickddit/refs/heads/master/ubuntu-touch/Icons/quickddit.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "quickddit",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2023-03-06"
repo_created_date = "2015-06-12"

+++

### Description

Quickddit is a free and open source Reddit client for mobile phones. Quickddit is not an official client of Reddit and is **not** affiliated with Reddit in any way.

Quickddit is developed using Qt and currently available for MeeGo Harmattan (Qt 4.7.4), Sailfish OS (Qt 5.6) and Ubuntu-touch (Qt 5.9). The logic part is developed in C++ while the UI is developed in QML.

The Harmattan port has been left at 1.0.0 feature level, and will not receive new features.

### Notice

Archived on July 8th, 2024.