+++
title = "Blanket"
description = "Listen to ambient sounds"
aliases = []
date = 2020-09-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Rafael Mardojai CM",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/rafaelmardojai/blanket"
homepage = "https://apps.gnome.org/Blanket"
bugtracker = "https://github.com/rafaelmardojai/blanket/issues"
donations = "https://rafaelmardojai.com/donate/"
translations = "https://hosted.weblate.org/engage/blanket/"
more_information = [ "https://apps.gnome.org/Blanket/",]
summary_source_url = "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/brand/screenshot-1-dark.png", "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/brand/screenshot-1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.rafaelmardojai.Blanket"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.rafaelmardojai.Blanket"
flatpak_link = "https://flathub.org/apps/com.rafaelmardojai.Blanket.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "blanket",]
appstream_xml_url = "https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2020-08-25"

+++

### Description

Improve focus and increase your productivity by listening to different ambient sounds

Also can help you to fall asleep in a noisy environment.

Features:

* Save presets
* Add custom sounds
* Auto start in background
* MPRIS integration

Included Sounds in the App:

* Birds
* Boat
* City
* Coffee Shop
* Fireplace
* Pink Noise
* Rain
* Summer Night
* Storm
* Stream
* Train
* Waves
* White Noise
* Wind

[Source](https://raw.githubusercontent.com/rafaelmardojai/blanket/master/data/com.rafaelmardojai.Blanket.metainfo.xml.in)

### Notice

GTK3/libhandy before 0.6.0.