+++
title = "Marble Maps"
description = "Find your way"
aliases = [ "apps/org.kde.marble.maps/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_experimental", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Geography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/education/marble"
homepage = "https://marble.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=marble"
donations = "https://www.kde.org/community/donations/?app=marble&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.marble.maps.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "marble",]
appstream_xml_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.marble.maps/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-05-16"

+++

### Description

OpenStreetMap Navigation.

[Source](https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml)

### Notice

Marble maps is sadly not working well, at least on wayland.