+++
title = "Poland can into Space"
description = "‘Poland can into Space’ is a simple Android game based on LibGDX framework"
aliases = ["games/pl.sanszo.pcis/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "NPOSL-3.0",]
metadata_licenses = []
app_author = ["jsandomierz",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/JSandomierz/pcis"
homepage = ""
bugtracker = "https://github.com/JSandomierz/pcis/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/JSandomierz/pcis"
screenshots = [ "https://user-images.githubusercontent.com/10513420/41747762-7094deb2-75af-11e8-903c-c0c3ab9a6f43.jpg",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "pl.sanszo.pcis"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/pl.sanszo.pcis/"
latest_repo_commit = "2018-08-05"
repo_created_date = "2018-06-20"

+++

### Description

"Poland can into Space" is a simple Android game based on LibGDX framework. The game uses also the Box2D Physics Engine.

In the game, player takes control of Polandball and helps him to reach high altitude and gives him opportunity to GET INTO SPACE!

[Source](https://github.com/JSandomierz/pcis)
