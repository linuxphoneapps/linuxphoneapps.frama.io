+++
title = "Fairy Tale"
description = "Read and manage your comics collection."
aliases = []
date = 2020-10-11
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
homepage = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
bugtracker = "https://gitlab.gnome.org/belmoussaoui/fairytale/issues"
donations = "https://liberapay.com/bielmoussaoui"
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
screenshots = [ "https://gitlab.gnome.org/belmoussaoui/fairytale/raw/master/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/data/icons/com.belmoussaoui.FairyTale.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.belmoussaoui.FairyTale"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/build-aux/com.belmoussaoui.FairyTaleDevel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/data/com.belmoussaoui.FairyTale.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2020-04-07"
repo_created_date = "2019-05-22"

+++

### Description
Fairy Tale is a comics/manga library manager. It ships with a minimal reader and a discovery page.

[Source](https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/data/com.belmoussaoui.FairyTale.appdata.xml.in.in)

### Notice

Last commit in August 2019.