+++
title = "Transito"
description = "FOSS data-provider-agnostic (GTFS) public transportation app"
aliases = []
date = 2024-07-13
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Miles Alan",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Gio",]
backends = [ "mobroute",]
services = []
packaged_in = [ "alpine_edge",]
freedesktop_categories = [ "Maps",]
programming_languages = [ "Go",]
build_systems = [ "go",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~mil/transito"
latest_repo_commit = "2024-11-15"
homepage = ""
bugtracker = "https://todo.sr.ht/~mil/mobroute-tickets"
donations = ""
translations = ""
more_information = [ "https://git.sr.ht/~mil/transito/tree/master/doc/doc_transito_userguide.md", "https://git.sr.ht/~mil/transito/tree/master/doc/doc_transito_screenshots.md", "https://git.sr.ht/~mil/transito/tree/master/doc/doc_transito_build_from_source.md", "https://git.sr.ht/~mil/transito/tree/master/doc/doc_transito_development.md", "https://git.sr.ht/~mil/mobroute/tree/HEAD/doc/doc_contribute.md",]
summary_source_url = "https://git.sr.ht/~mil/transito"
screenshots = [ "https://git.sr.ht/~mil/transito/blob/master/doc/transito_screenshot_routesteps.png", "https://git.sr.ht/~mil/transito/blob/master/doc/transito_screenshot_routemap.png", "https://git.sr.ht/~mil/transito/blob/master/doc/transito_screenshot_search.png", "https://git.sr.ht/~mil/transito/blob/master/doc/transito_screenshot_config.png", "https://git.sr.ht/~mil/transito/tree/master/doc/doc_transito_development.md",]
screenshots_img = []
non_svg_icon_url = "https://git.sr.ht/~mil/transito/blob/master/assets/transito_128x128.png"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "transito",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
repo_created_date = "2023-08-31"

+++

### Description

Point-to-point directions without comprising user freedoms or privacy.

Transito is a FOSS data-provider-agnostic public transportation app that let's you route between locations using openly available public GTFS feeds. Utilizing the Mobroute Go Library, the Transito app lets you performs routing calculations offline / right on your phone (e.g. no network calls once data is initially fetched). Overall, Transito aims to be an opensource alternative to proprietary routing apps (such as Google Maps, etc.) to get users from point-a to point-b via public transit without comprising privacy or user freedoms. It works in many well-connected metros which have publicly available GTFS data, to name a few: Lisbon, NYC, Brussels, Krakow, and Bourges (check for compatability with your metro here).

The app is currently in active development but generally usable on Android & Linux and looking for more users & testers.

[Source](https://git.sr.ht/~mil/transito)
