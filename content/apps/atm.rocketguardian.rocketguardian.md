+++
title = "Rocket Guardian"
description = "Rocket Guardian is a android game about a guardian that must protect a city from the falling zombies."
aliases = ["games/atm.rocketguardian.RocketGuardian/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = ["atorresm",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/atorresm/rocket-guardian"
homepage = ""
bugtracker = "https://gitlab.com/atorresm/rocket-guardian/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/atorresm/rocket-guardian"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "atm.rocketguardian.RocketGuardian"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/atm.rocketguardian.rocketguardian/"
latest_repo_commit = "2019-01-24"
repo_created_date = "2017-01-17"

+++
