+++
title = "PassUi"
description = "PassUi is a free and open source graphical user interface for the standard unix password manager written in python, with small screens in mind."
aliases = []
date = 2021-02-06
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "micke",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "WxWidgets",]
backends = [ "pass",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "no icon",]

[extra]
repository = "https://code.smolnet.org/micke/passui"
homepage = ""
bugtracker = "https://code.smolnet.org/micke/passui/issues/"
donations = "https://liberapay.com/micke/donate"
translations = ""
more_information = []
summary_source_url = "https://code.smolnet.org/micke/passui"
screenshots = [ "https://code.smolnet.org/micke/passui/raw/branch/main/screenshots/first.png", "https://code.smolnet.org/micke/passui/raw/branch/main/screenshots/second.png", "https://code.smolnet.org/micke/passui/raw/branch/main/screenshots/third.png", "https://code.smolnet.org/micke/passui/raw/branch/main/screenshots/fourth.png", "https://code.smolnet.org/micke/passui/raw/branch/main/screenshots/fifth.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2022-07-01"
repo_created_date = "2021-04-21"

+++