+++
title = "Curtail"
description = "Compress your images"
aliases = []
date = 2021-05-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Hugo Posnic",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Graphics",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/Huluti/Curtail"
homepage = "https://github.com/Huluti/Curtail"
bugtracker = "https://github.com/Huluti/Curtail/issues"
donations = "https://liberapay.com/hugoposnic"
translations = "https://github.com/Huluti/Curtail/tree/master/po"
more_information = [ "https://apps.gnome.org/Curtail/",]
summary_source_url = "https://raw.githubusercontent.com/Huluti/Curtail/master/data/com.github.huluti.Curtail.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Huluti/Curtail/master/data/screenshots/screen1.png", "https://raw.githubusercontent.com/Huluti/Curtail/master/data/screenshots/screen2.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Huluti/Curtail/refs/heads/master/data/icons/hicolor/scalable/apps/com.github.huluti.Curtail.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.huluti.Curtail"
scale_to_fit = "curtail"
flathub = "https://flathub.org/apps/com.github.huluti.Curtail"
flatpak_link = "https://flathub.org/apps/com.github.huluti.Curtail.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Huluti/Curtail/refs/heads/master/com.github.huluti.Curtail.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "curtail",]
appstream_xml_url = "https://raw.githubusercontent.com/Huluti/Curtail/master/data/com.github.huluti.Curtail.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-10-03"

+++

### Description

Optimize your images with Curtail, a useful image compressor that supports PNG, JPEG, WebP and SVG file types.

It supports both lossless and lossy compression modes with an option to whether keep or not metadata of images.

[Source](https://raw.githubusercontent.com/Huluti/Curtail/master/data/com.github.huluti.Curtail.appdata.xml.in)