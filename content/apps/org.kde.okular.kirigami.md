+++
title = "Okular Mobile"
description = "Document Viewer"
aliases = [ "apps/org.kde.mobile.okular/", "apps/org.kde.okular.kirigami.desktop/",]
date = 2019-02-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GFDL-1.3", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "document viewer", "pdf viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "KDE", "Office", "Qt", "Viewer",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/graphics/okular"
homepage = "https://okular.kde.org/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=okular"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/okular/okular-mobile.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.okular.kirigami"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.okular.kirigami"
flatpak_link = "https://flathub.org/apps/org.kde.okular.kirigami.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "okular",]
appstream_xml_url = "https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.okular.kirigami/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-06-20"

+++

### Description

Okular is a universal document viewer developed by KDE. Okular works on multiple platforms, including but not limited to Linux, Windows, Mac OS X, \*BSD, etc.

Features:

* Supported Formats: PDF, PS, Tiff, CHM, DjVu, Images, DVI, XPS, Fiction Book, Comic Book, Plucker, EPub, Fax
* Sidebar with contents, thumbnails, reviews and bookmarks
* Annotations support

[Source](https://invent.kde.org/graphics/okular/-/raw/master/mobile/app/org.kde.okular.kirigami.appdata.xml)