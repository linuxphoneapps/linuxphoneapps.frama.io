+++
title = "Entangle"
description = "Tethered Camera Control & Capture"
aliases = []
date = 2020-09-10
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Entangle Photo project",]
categories = [ "camera utility",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/entangle/entangle"
homepage = "https://entangle-photo.org/"
bugtracker = "https://gitlab.com/entangle/entangle/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/entangle/entangle/-/raw/master/src/org.entangle_photo.Manager.metainfo.xml"
screenshots = [ "https://entangle-photo.org/appdata/en_US/screenshots/camera-manager.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.entangle_photo.Manager"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.entangle_photo.Manager"
flatpak_link = "https://flathub.org/apps/org.entangle_photo.Manager.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "entangle",]
appstream_xml_url = "https://gitlab.com/entangle/entangle/-/raw/master/src/org.entangle_photo.Manager.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-09"
repo_created_date = "2015-03-13"

+++

### Description

Entangle is a program used to control digital cameras
that are connected to the computer via USB.

Entangle can trigger the camera shutter to capture new images.
When supported by the camera, a continuously updating preview
of the scene can be displayed prior to capture. Images will be
downloaded and displayed as they are captured by the camera.
Entangle also allows the settings of the camera to be changed
from the computer.

Entangle is compatible with most DSLR cameras from Nikon and
Canon, some of their compact camera models, and a variety of
cameras from other manufacturers.

[Source](https://gitlab.com/entangle/entangle/-/raw/master/src/org.entangle_photo.Manager.metainfo.xml)

### Notice

It seems to scale just fine outside of the settings menu, but I do not have a camera to test with scale-to-fit is likely a good idea.