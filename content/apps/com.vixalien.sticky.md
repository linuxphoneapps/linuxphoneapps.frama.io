+++
title = "Sticky Notes"
description = "Pin notes to your desktop"
aliases = []
date = 2025-02-10

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Angelo Verlain",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "TypeScript",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vixalien/sticky"
homepage = "https://github.com/vixalien/sticky"
bugtracker = "https://github.com/vixalien/sticky/issues"
donations = "https://www.buymeacoffee.com/vixalien"
translations = "https://github.com/vixalien/sticky/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/com.vixalien.sticky"
screenshots = [ "https://raw.githubusercontent.com/vixalien/sticky/v0.1.1/data/resources/screenshots/note.png", "https://raw.githubusercontent.com/vixalien/sticky/v0.1.1/data/resources/screenshots/notes.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/vixalien/sticky/refs/heads/main/data/icons/hicolor/scalable/apps/com.vixalien.sticky.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "com.vixalien.sticky"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.vixalien.sticky"
flatpak_link = "https://flathub.org/apps/com.vixalien.sticky.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/vixalien/sticky/refs/heads/main/build-aux/flatpak/com.vixalien.sticky.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "sticky-notes",]
appstream_xml_url = "https://raw.githubusercontent.com/vixalien/sticky/refs/heads/main/data/com.vixalien.sticky.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = ""
feed_entry_id = "https://linuxphoneapps.org/apps/com.vixalien.sticky/"
latest_repo_commit = "2024-10-30"
repo_created_date = "2023-02-27"

+++

### Description

Sticky Notes is a simple sticky notes app for GNOME. It allows you to create
notes and format them.

Current features:

* simple formatting (bold, italic, underline and strikethrough)
* notes are restored if they were open when the application was closed
* changing color of notes
* dark theme support

[Source](https://raw.githubusercontent.com/vixalien/sticky/refs/heads/main/data/com.vixalien.sticky.appdata.xml.in.in)
