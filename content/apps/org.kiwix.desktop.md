+++
title = "Kiwix"
description = "View offline content"
aliases = []
date = 2020-12-12
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Kiwix Foundation",]
categories = [ "education",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Education",]
programming_languages = [ "JavaScript", "Cpp",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/kiwix/kiwix-desktop"
homepage = "https://www.kiwix.org/"
bugtracker = "https://github.com/kiwix/kiwix-desktop"
donations = "https://www.kiwix.org/support/"
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/kiwix/kiwix-desktop/master/resources/org.kiwix.desktop.appdata.xml"
screenshots = [ "https://upload.wikimedia.org/wikipedia/commons/3/38/Kiwix-desktop_showing_its_local_Library.png", "https://upload.wikimedia.org/wikipedia/commons/3/3e/Kiwix_desktop_reading_a_TED_ZIM_file.png", "https://upload.wikimedia.org/wikipedia/commons/c/cd/Kiwix-desktop_showing_the_remote_Library.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kiwix.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kiwix.desktop"
flatpak_link = "https://flathub.org/apps/org.kiwix.desktop.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kiwix-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/kiwix/kiwix-desktop/master/resources/org.kiwix.desktop.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2017-12-03"

+++

### Description

Kiwix is an offline reader - meaning that it allows you to check text or video that is normally only available on the internet.

We turn various online educational contents (such as Wikipedia, for example) into ZIM files, and these can be opened by Kiwix even if you have no connectivity whatsoever.

[Source](https://raw.githubusercontent.com/kiwix/kiwix-desktop/master/resources/org.kiwix.desktop.appdata.xml)

### Notice

Some menu items are not accessible, but it mostly works.