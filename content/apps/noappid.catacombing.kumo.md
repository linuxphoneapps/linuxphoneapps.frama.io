+++
title = "Kumo"
description = "A Wayland Mobile Browser"
aliases = []
date = 2024-12-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Catacombing",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = []
backends = [ "wpewebkit",]
services = []
packaged_in = []
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "Rust", "C",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/catacombing/kumo"
homepage = ""
bugtracker = "https://github.com/catacombing/kumo/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/catacombing/kumo"
screenshots = []
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/catacombing/kumo/refs/heads/master/logo.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.catacombing.kumo/"
latest_repo_commit = "2025-01-09"
repo_created_date = "2024-02-24"

+++

### Description

Kumo is a web browser with a UI focused on portrait-mode touchscreen mobile devices. It is optimized to run on low-end hardware with a limited battery life.

Features

Kumo is a UI written around Safari's WebKit browser engine, so they should be comparable in behavior and performance.

The following noteworthy features are implemented:

- Built-in adblocker
- Whitelist-based cookie policy
- Shell-like URI completion
- Session recovery
- Tab groups


[Source](https://github.com/catacombing/kumo)




### Notice

Only available for Arch Linux ARM-based distributions through [a special repo](https://github.com/catacombing/packaging).
