+++
title = "Komikku"
description = "Read your favorite manga"
aliases = []
date = 2020-08-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-4.0",]
app_author = [ "Valéry Febvre",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "fedora_40", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Graphics", "Network", "Viewer",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://codeberg.org/valos/Komikku"
homepage = "https://apps.gnome.org/Komikku/"
bugtracker = "https://codeberg.org/valos/Komikku/issues"
donations = "https://valos.gitlab.io/Komikku/#sponsor"
translations = "https://hosted.weblate.org/projects/komikku"
more_information = [ "https://apps.gnome.org/Komikku/",]
summary_source_url = "https://gitlab.com/valos/Komikku/-/raw/main/data/info.febvre.Komikku.appdata.xml.in.in"
screenshots = [ "https://gitlab.com/valos/Komikku/raw/main/screenshots/card-chapters.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/card-info.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/categories-editor.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/explorer.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/history.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/library-with-categories.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/library.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/preferences.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/reader-mobile.png", "https://gitlab.com/valos/Komikku/raw/main/screenshots/reader.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "info.febvre.Komikku"
scale_to_fit = ""
flathub = "https://flathub.org/apps/info.febvre.Komikku"
flatpak_link = "https://flathub.org/apps/info.febvre.Komikku.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "komikku",]
appstream_xml_url = "https://gitlab.com/valos/Komikku/-/raw/main/data/info.febvre.Komikku.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2024-01-15"

+++

### Description

With its simple, elegant and adaptive interface, this manga reader allows you to search, sort and read all your favorite manga with ease.

Why you will love Komikku?

* Online and offline reading from dozens of servers
* Support for locally stored manga (in CBZ or CBR formats)
* RTL, LTR, Vertical and Webtoon reading modes
* Support several types of navigation: keyboard arrow keys, right and left navigation layout via mouse click or tapping (touchpad/touch screen), mouse wheel, 2-fingers swipe gesture (touchpad), swipe gesture (touch screen)
* Categories to organize your library
* Automatic update of manga
* Automatic download of new chapters
* Reading history
* Light and dark themes

[Source](https://gitlab.com/valos/Komikku/-/raw/main/data/info.febvre.Komikku.appdata.xml.in.in)

### Notice

Was GTK3/libhandy before v1.0.0.