+++
title = "Merkuro Mail"
description = "Read and write emails"
aliases = []
date = 2024-01-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "email",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami", "QtQuick",]
backends = [ "Akonadi",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Email",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/pim/merkuro/-/tree/master/src/mail"
homepage = "https://apps.kde.org/merkuro.mail/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=merkuro"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/merkuro.mail/",]
summary_source_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/mail/org.kde.merkuro.mail.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/merkuro/mail.png",]
screenshots_img = []
non_svg_icon_url = "https://invent.kde.org/pim/merkuro/-/raw/master/icons/128-apps-org.kde.merkuro.mail.png"
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.merkuro.mail"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "merkuro",]
appstream_xml_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/mail/org.kde.merkuro.mail.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2021-02-10"

+++

### Description

Merkuro Mail is a free and open source email client. Merkuro Mail is part of the Merkuro groupware suite and is currently in beta. For now, it only allows you to read your emails.

[Source](https://invent.kde.org/pim/merkuro/-/raw/master/src/mail/org.kde.merkuro.mail.metainfo.xml)

### Notice

To set up an account, make sure to launch the app from terminal with `env QT_QUICK_CONTROLS_MOBILE=0 merkuro-mail`.

To get a launcher, run "cp /usr/share/appplications/org.kde.merkuro.mail.desktop ~/.local/share/applications/ and remove the "NoDisplay=true" line at the end of the file (or change true to false).