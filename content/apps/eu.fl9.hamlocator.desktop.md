+++
title = "hamlocator"
description = "Displays a maidenhead locator for current location"
aliases = [ "apps/eu.fl9.hamlocator/",]
date = 2023-04-17
updated = 2024-10-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michał Rudowicz",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libgeoclue",]
services = []
packaged_in = []
freedesktop_categories = [ "GNOME", "GTK",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~michalr/hamlocator"
latest_repo_commit = "2024-04-24"
homepage = "https://sr.ht/~michalr/hamlocator/"
bugtracker = "https://todo.sr.ht/~michalr/hamlocator"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~michalr/hamlocator/blob/main/data/eu.fl9.hamlocator.appdata.xml.in"
screenshots = [ "https://git.sr.ht/~michalr/hamlocator/blob/main/media/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/eu.fl9.hamlocator/1.png", "https://img.linuxphoneapps.org/eu.fl9.hamlocator/2.png",]
svg_icon_url = "https://git.sr.ht/~michalr/hamlocator/blob/main/data/icons/hicolor/scalable/apps/eu.fl9.hamlocator.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "eu.fl9.hamlocator.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://git.sr.ht/~michalr/hamlocator/blob/main/eu.fl9.hamlocator.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://git.sr.ht/~michalr/hamlocator/blob/main/data/eu.fl9.hamlocator.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git"
feed_entry_id = "https://linuxphoneapps.org/apps/eu.fl9.hamlocator/"
repo_created_date = "2022-05-11"

+++

### Description

Display a maidenhead locator

[Source](https://git.sr.ht/~michalr/hamlocator/blob/main/data/eu.fl9.hamlocator.appdata.xml.in)
