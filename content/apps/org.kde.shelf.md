+++
title = "Shelf"
description = "Browse and view your documents"
aliases = [ "apps/org.maui.shelf/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "document and ebook manager",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Qt", "Office", "Viewer",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/maui/maui-shelf"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/maui-shelf/-/issues"
donations = "https://kde.org/donate/"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/maui-shelf/-/raw/master/org.kde.shelf.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/shelf/shelf.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/maui-shelf/-/raw/master/src/assets/shelf.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.shelf"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-shelf", "shelf",]
appstream_xml_url = "https://invent.kde.org/maui/maui-shelf/-/raw/master/org.kde.shelf.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2019-04-21"

+++

### Description
Shelf is a simple PDF and EPUB document browser and viewer.

[Source](invent.kde.org/maui/maui-shelf/-/raw/master/org.kde.shelf.metainfo.xml)
