+++
title = "Workflow"
description = "Screen time monitor app based on ActivityWatch"
aliases = []
date = 2020-09-23
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Raffaele T.",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://gitlab.com/nokun/workflow"
homepage = "https://gitlab.com/cunidev/workflow"
bugtracker = "https://gitlab.com/cunidev/workflow/issues"
donations = "https://paypal.me/tranquillini"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/nokun/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in"
screenshots = [ "https://i.imgur.com/Uk3XUQU.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/nokun/workflow/-/raw/master/data/icons/hicolor/scalable/apps/com.gitlab.cunidev.Workflow.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.gitlab.cunidev.Workflow"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/nokun/workflow/-/raw/master/com.gitlab.cunidev.Workflow.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/nokun/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2023-06-06"
repo_created_date = "2019-09-19"

+++


### Description

Workflow is a basic Linux screen time monitor app.

It is based on ActivityWatch, which is not included and should be installed and running following the instructions on their website.

[Source](https://gitlab.com/nokun/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in)

### Notice

Marked EOL / unmaintained on June 7th, 2023.