+++
title = "DayKountdown"
description = "A simple date countdown app written for KDE Plasma, using Kirigami."
aliases = []
date = 2021-04-29
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Plasma Mobile Developers",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://invent.kde.org/utilities/daykountdown"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/daykountdown/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/utilities/daykountdown"
screenshots = []
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/daykountdown/-/raw/master/org.kde.daykountdown.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.daykountdown"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "daykountdown",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-07"
repo_created_date = "2021-02-09"

+++

### Description

A date countdown app written in QML/Kirigami and C++, for use with KDE Plasma on Linux.

The app counts the days towards a date of your choice.

It allows you to pick a date, provide a name, and a short description for your countdowns.

## Features

- Adding and removing countdowns
- Compatibility with both desktop and mobile form factors
- Import and export countdowns from .JSON files
- Sorting by date, name, and creation time
- Sync with your KDE Plasma calendars and easily add events from there into your countdowns

[Source](https://invent.kde.org/utilities/daykountdown)