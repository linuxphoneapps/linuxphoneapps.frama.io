+++
title = "Stellarium"
description = "Desktop Planetarium"
aliases = []
date = 2020-10-27
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Stellarium team",]
categories = [ "entertainment",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Education", "Science",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Stellarium/stellarium"
homepage = "https://www.stellarium.org/"
bugtracker = "https://github.com/Stellarium/stellarium/issues"
donations = "https://opencollective.com/stellarium"
translations = "https://app.transifex.com/stellarium/stellarium/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml"
screenshots = [ "https://stellarium.org/img/screenshots/0.10-constellations.jpg", "https://stellarium.org/img/screenshots/0.10-from-mars.jpg", "https://stellarium.org/img/screenshots/0.10-orion-nebula.jpg", "https://stellarium.org/img/screenshots/0.10-planets.jpg", "https://www.stellarium.org/img/screenshots/0.10-constellation-art-on.jpg",]
screenshots_img = []
non_svg_icon_url = "https://github.com/Stellarium/stellarium/blob/master/data/icons/128x128/stellarium.png"
all_features_touch = false
intended_for_mobile = false
app_id = "org.stellarium.Stellarium"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.stellarium.Stellarium"
flatpak_link = "https://flathub.org/apps/org.stellarium.Stellarium.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/stellarium-daily"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "stellarium",]
appstream_xml_url = "https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2014-04-13"

+++

### Description

Stellarium renders 3D photo-realistic skies in real time with OpenGL. It displays stars, constellations, planets and nebulae, and has many other features including multiple panoramic landscapes, fog, light pollution simulation and a built-in scripting engine.

Stellarium comes with a star catalogue of about 600 thousand stars and it is possible to download extra catalogues with up to 210 million stars.

Stellarium has multiple sky cultures - see the constellations from the traditions of Polynesian, Inuit, Navajo, Korean, Lakota, Egyptian and Chinese astronomers, as well as the traditional Western constellations.

It is also possible to visit other planets in the solar system - see what the sky looked like to the Apollo astronauts, or what the rings of Saturn looks like from Titan.

[Source](https://raw.githubusercontent.com/Stellarium/stellarium/master/data/org.stellarium.Stellarium.appdata.xml)

### Notice

For a good mobile experience, maybe look into running the [Ubuntu Touch variant of Stellarium](https://open-store.io/app/me.lduboeuf.stellarium).