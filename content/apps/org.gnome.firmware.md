+++
title = "Firmware"
description = "Install firmware on devices"
aliases = []
date = 2022-04-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Richard Hughes",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "fwupd",]
services = [ "Linux Vendor Firmware Service",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/gnome-firmware"
homepage = "https://gitlab.gnome.org/World/gnome-firmware"
bugtracker = "https://gitlab.gnome.org/World/gnome-firmware/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-firmware/"
more_information = [ "https://dylanvanassche.be/blog/2022/pinephone-modem-upgrade/", "https://linmob.net/easily-upgrading-pinephone-pro-modem-firmware/",]
summary_source_url = "https://gitlab.gnome.org/World/gnome-firmware"
screenshots = [ "https://gitlab.gnome.org/World/gnome-firmware/raw/master/data/appdata/ss-devices.png", "https://gitlab.gnome.org/World/gnome-firmware/raw/master/data/appdata/ss-releases.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.firmware/1.png", "https://img.linuxphoneapps.org/org.gnome.firmware/2.png", "https://img.linuxphoneapps.org/org.gnome.firmware/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Firmware"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Firmware"
flatpak_link = "https://flathub.org/apps/org.gnome.Firmware.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-firmware", "gnome-firmware-updater",]
appstream_xml_url = "https://gitlab.gnome.org/World/gnome-firmware/-/raw/master/data/appdata/org.gnome.Firmware.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-04-24"

+++

### Description

A power-user tool that allows updating, reinstalling and downgrading of firmware on
devices supported by fwupd.

[Source](https://gitlab.gnome.org/World/gnome-firmware/-/raw/master/data/appdata/org.gnome.Firmware.metainfo.xml.in)