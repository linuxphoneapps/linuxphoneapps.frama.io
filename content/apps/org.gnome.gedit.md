+++
title = "gedit"
description = "Text editor"
aliases = [ "apps/org.gnome.gedit.desktop/",]
date = 2020-12-12
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The gedit team",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "TextEditor", "Utility",]
programming_languages = [ "C", "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/gedit/gedit"
homepage = "https://gedit-text-editor.org/"
bugtracker = "https://gedit-text-editor.org/reporting-bugs.html"
donations = ""
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/gedit/gedit/-/raw/master/data/org.gnome.gedit.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/swilmet/gedit-extra/-/raw/main/screenshots/screenshot.png",]
screenshots_img = []
non_svg_icon_url = "https://gitlab.gnome.org/World/gedit/gedit/-/raw/master/data/icons/org.gnome.gedit-256x256.png"
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.gedit"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gedit"
flatpak_link = "https://flathub.org/apps/org.gnome.gedit.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gedit",]
appstream_xml_url = "https://gitlab.gnome.org/World/gedit/gedit/-/raw/master/data/org.gnome.gedit.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.gnome.gedit/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-05-22"

+++

### Description

gedit is an easy-to-use and general-purpose text editor. Its development
started in 1998, at the beginnings of the GNOME project, with a good
integration with that desktop environment.

You can use it to write simple notes and documents, or you can enable more
advanced features that are useful for software development.

[Source](https://gitlab.gnome.org/World/gedit/gedit/-/raw/master/data/org.gnome.gedit.metainfo.xml.in)