+++
title = "Settings"
description = "Utility to configure the GNOME desktop"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "settings",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-control-center/"
homepage = "https://apps.gnome.org/Settings/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-control-center/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-control-center/"
more_information = [ "https://apps.gnome.org/Settings/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-control-center/-/raw/main/shell/appdata/org.gnome.Settings.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-control-center/raw/HEAD/shell/appdata/screenshot-appearance.png", "https://gitlab.gnome.org/GNOME/gnome-control-center/raw/HEAD/shell/appdata/screenshot-keyboard.png", "https://gitlab.gnome.org/GNOME/gnome-control-center/raw/HEAD/shell/appdata/screenshot-mouse.png", "https://gitlab.gnome.org/GNOME/gnome-control-center/raw/HEAD/shell/appdata/screenshot-sound.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-control-center/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Settings.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Settings"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pureos-gnome-settings", "manjaro-gnome-settings", "gnome-settings-daemon",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-control-center/-/raw/main/shell/appdata/org.gnome.Settings.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-02-08"

+++

### Description

Settings is the primary interface for configuring your system.

[Source](https://gitlab.gnome.org/GNOME/gnome-control-center/-/raw/main/shell/appdata/org.gnome.Settings.appdata.xml.in)