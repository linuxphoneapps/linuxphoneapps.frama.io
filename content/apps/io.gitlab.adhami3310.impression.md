+++
title = "Impression"
description = "Create bootable drives"
aliases = []
date = 2024-05-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Khaleel Al-Adhami",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.com/adhami3310/Impression"
homepage = "https://apps.gnome.org/Impression/"
bugtracker = "https://gitlab.com/adhami3310/Impression/-/issues"
donations = "https://opencollective.com/impression"
translations = "https://gitlab.com/adhami3310/Impression/-/tree/main/po"
more_information = [ "https://apps.gnome.org/Impression/",]
summary_source_url = "https://flathub.org/apps/io.gitlab.adhami3310.Impression"
screenshots = [ "https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/0.png", "https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/1.png", "https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/2.png", "https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/screenshots/3.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/adhami3310/Impression/-/raw/main/data/resources/icons/hicolor/scalable/apps/io.gitlab.adhami3310.Impression.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.adhami3310.Impression"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.adhami3310.Impression"
flatpak_link = "https://flathub.org/apps/io.gitlab.adhami3310.Impression.flatpakref"
flatpak_recipe = "https://gitlab.com/adhami3310/Impression/-/raw/main/io.gitlab.adhami3310.Impression.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "impression",]
appstream_xml_url = "https://gitlab.com/adhami3310/Impression/-/raw/main/data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-25"
repo_created_date = "2023-06-01"

+++

### Description

Write disk images to your drives with ease. Select an image, insert your drive, and you're
good to go! Impression is a useful tool for both avid distro-hoppers and casual computer
users.

[Source](https://gitlab.com/adhami3310/Impression/-/raw/main/data/io.gitlab.adhami3310.Impression.metainfo.xml.in.in)