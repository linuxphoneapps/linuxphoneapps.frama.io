+++
title = "Shipments"
description = "Postal package tracking application"
aliases = []
date = 2021-12-01
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "martijnbraam",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/shipments"
latest_repo_commit = "2022-07-16"
homepage = "https://sr.ht/~martijnbraam/shipments"
bugtracker = "https://todo.sr.ht/~martijnbraam/shipments"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml"
screenshots = [ "http://brixitcdn.net/metainfo/shipments.png",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.postmarketos.Shipments"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "shipments",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
repo_created_date = "2021-11-25"

+++

### Description

Application to track your shipments. Supported carriers:

* 4PX
* DHL (needs api code)
* InPost
* PostNL
* Russian Post
* UPS

[Source](https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml)