+++
title = "Whale"
description = "File manager"
aliases = [ "apps/org.kde.whale.desktop/",]
date = 2021-12-18
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "System", "FileTools", "FileManager",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "not packaged yet",]

[extra]
repository = "https://invent.kde.org/carlschwan/whale"
homepage = "https://invents.kde.org/carlschwan/whale"
bugtracker = "https://invent.kde.org/carlschwan/whale/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/",]
summary_source_url = "https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.whale"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.whale/"
latest_repo_commit = "2024-11-20"
repo_created_date = "2020-06-25"

+++

### Description

A file manager for mobile devices.

[Source](https://invent.kde.org/carlschwan/whale/-/raw/master/org.kde.whale.appdata.xml)

### Notice

WIP fun project, worked well when tested on 2021-12-18.
