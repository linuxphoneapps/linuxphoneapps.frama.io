+++
title = "Console"
description = "Terminal Emulator"
aliases = []
date = 2020-10-07
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "System", "TerminalEmulator",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/console"
homepage = "https://apps.gnome.org/Console/"
bugtracker = "https://gitlab.gnome.org/GNOME/console/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/console/"
more_information = [ "https://apps.gnome.org/Console/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/console/-/raw/main/data/org.gnome.Console.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/console/raw/gnome-47/data/screenshots/01-Terminal.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Console"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-console",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/console/-/raw/main/data/org.gnome.Console.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2019-04-08"

+++

### Description

A simple user-friendly terminal emulator for the GNOME desktop.

[Source](https://gitlab.gnome.org/GNOME/console/-/raw/main/data/org.gnome.Console.metainfo.xml.in.in)

### Notice

Console was previously called KingsCross / kgx. From GNOME 42 onwards, this is GNOME's default terminal. GTK3/libhandy before release 43.