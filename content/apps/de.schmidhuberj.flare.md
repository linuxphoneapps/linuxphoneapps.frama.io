+++
title = "Flare"
description = "Chat with your friends on Signal"
aliases = []
date = 2022-10-08
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "presage",]
services = [ "Signal",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Chat", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "recommends always", "requires first-run",]
tags = []

[extra]
repository = "https://gitlab.com/schmiddi-on-mobile/flare"
homepage = "https://mobile.schmidhuberj.de/flare"
bugtracker = "https://gitlab.com/schmiddi-on-mobile/flare/-/issues"
donations = "https://gitlab.com/schmiddi-on-mobile/flare#donate"
translations = "https://hosted.weblate.org/engage/schmiddi-on-mobile/"
more_information = []
summary_source_url = "https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml"
screenshots = [ "https://gitlab.com/Schmiddiii/flare/-/raw/master/data/screenshots/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.schmidhuberj.Flare"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.Flare"
flatpak_link = "https://flathub.org/apps/de.schmidhuberj.Flare.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/de.schmidhuberj.Flare/master/de.schmidhuberj.Flare.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "flare-signal-client",]
appstream_xml_url = "https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2022-06-30"

+++

### Description

Flare is an unofficial app for Signal. It is still in development and doesn't include all the features that the official Signal apps do. More information can be found on its feature roadmap.

Please note that using this application will probably worsen your security compared to using official Signal applications.
Use with care when handling sensitive data.
Look at the projects README for more information about security.

[Source](https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/de.schmidhuberj.Flare.metainfo.xml.in.in)

### Notice

Native Signal Desktop alternative, needs [signal-cli](https://github.com/AsamK/signal-cli) or an Android/iOS app as primary signal client. See [this toot for signal-cli paring instructions](https://fosstodon.org/@schmiddionmobile/110865217789552548).