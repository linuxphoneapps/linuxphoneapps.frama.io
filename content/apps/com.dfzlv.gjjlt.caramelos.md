+++
title = "Caramelos"
description = "Caramelos is an Android Game made with Libgdx"
aliases = ["games/com.dfzlv.gjjlt.caramelos/"]
date = 2019-02-01
updated = 2025-01-07

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = ["luarca84",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "LogicGame"]
programming_languages = [ "Java",]
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/luarca84/Caramelos"
homepage = ""
bugtracker = "https://github.com/luarca84/Caramelos/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/luarca84/Caramelos"
screenshots = [ "https://play.google.com/store/apps/details?id=com.dfzlv.gjjlt.caramelos",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.dfzlv.gjjlt.caramelos"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/com.dfzlv.gjjlt.caramelos/"
latest_repo_commit = "2017-11-08"
repo_created_date = "2017-09-20"

+++

### Notice

Last commit 2017-11-08.
