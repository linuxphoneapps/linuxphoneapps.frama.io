+++
title = "GZDoom"
description = "Game engine for Classic Doom"
aliases = ["games/org.zdoom.gzdoom/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "ZDoom team",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub", "gentoo", "gnuguix", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed",]
freedesktop_categories = [ "Game", "Shooter",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ZDoom/gzdoom"
homepage = "https://zdoom.org/index"
bugtracker = "https://github.com/coelckers/gzdoom"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.zdoom.GZDoom"
screenshots = [ "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_01.png", "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_02.png", "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_03.png", "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/images/image_04.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.zdoom.GZDoom"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.zdoom.GZDoom"
flatpak_link = "https://flathub.org/apps/org.zdoom.GZDoom.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gzdoom",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/org.zdoom.GZDoom.appdata.xml"
reported_by = "-Euso-"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2013-06-23"
feed_entry_id = "https://linuxphoneapps.org/games/org.zdoom.gzdoom/"

+++

### Description

GZDoom is a source port for the modern era, supporting current hardware and operating systems and sporting a vast array of user options. Make Doom your own again!

In addition to Doom, GZDoom supports Heretic, Hexen, Strife, Chex Quest, and fan-created games like Harmony and Hacx. Meet the entire idTech 1 family!

Experience mind-bending user-created mods, made possible by ZDoom's advanced mapping features and the new ZScript language. Or make a mod of your own!

* Can play all Doom engine games, including Ultimate Doom, Doom II, Heretic, Hexen, Strife, and more
* Supports all the editing features of Hexen. (ACS, hubs, new map format, etc.)
* Supports most of the Boom editing features
* Features complete translations of Doom, Heretic, Hexen, Strife and other games into over ten different languages with Unicode support for Latin, Cyrillic, and Hangul so far
* All Doom limits are gone
* Several softsynths for MUS and MIDI playback, including an OPL softsynth for an authentic “oldschool” flavor
* High resolutions
* Quake-style console and key bindings
* Crosshairs
* Free look (look up/down)
* Jumping, crouching, swimming, and flying
* Up to 8 player network games using UDP/IP, including team-based gameplay
* Support for the Bloodbath announcer from the classic Monolith game Blood
* Walk over/under monsters and other things

Commercial data files are required to run the supported games. For more info about all supported games and their data files, see: Help -> List of supported games.

With Flatpak, all file-access is restricted to ~/.var/app/org.zdoom.GZDoom/.config/gzdoom for privacy reasons. You'll have to manually place your IWADs and PWADS there, or you should use an application like Flatseal to give GZDoom additional permissions.

[Source](https://raw.githubusercontent.com/flathub/org.zdoom.GZDoom/master/org.zdoom.GZDoom.appdata.xml)
