+++
title = "phom"
description = "Quick hack to have a virtual mouse"
aliases = [ "apps/guido.gunther.phom.md",]
date = 2020-11-07
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Guido Günther",]
categories = [ "virtual input device",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.gnome.org/guidog/phom"
homepage = ""
bugtracker = "https://gitlab.gnome.org/guidog/phom/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/guidog/phom"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2023-08-21"
repo_created_date = "2022-07-14"

+++

### Description

Phom is a simple virtual mouse for phosh. It can be used to
 control the mouse cursor on an external screen.

 If you're not working phosh's convergence features then this
 package is likely not very useful to you.

[Source](https://gitlab.gnome.org/guidog/phom/-/raw/main/debian/control)

### Notice

Debian/PureOS packages are to be found as pipeline build artifacts on <https://gitlab.gnome.org/guidog/phom/-/pipelines>