+++
title = "Pikasso"
description = "Drawing Program"
aliases = []
date = 2021-02-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPl-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "drawing",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Graphics", "KDE", "Qt", "RasterGraphics",]
programming_languages = [ "Cpp", "Rust", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "no icon",]

[extra]
repository = "https://invent.kde.org/graphics/pikasso"
homepage = ""
bugtracker = "https://invent.kde.org/graphics/pikasso/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/01/25/pikasso-a-simple-drawing-application-in-qtquick-with-rust/",]
summary_source_url = "https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.pikasso"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pikasso",]
appstream_xml_url = "https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-02"
repo_created_date = "2021-01-20"

+++

### Description

Pikasso is a simple drawing program for Plasma and Plasma Mobile.

Pikasso provides a few interesting features:

* Free form drawing
* Draw rectangles and ellipses
* SVG export
* Undo

[Source](https://invent.kde.org/graphics/pikasso/-/raw/master/org.kde.pikasso.appdata.xml)