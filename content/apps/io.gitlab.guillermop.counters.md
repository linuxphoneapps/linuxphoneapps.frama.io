+++
title = "Counters"
description = "Keep track of anything"
aliases = []
date = 2024-11-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Guillermo Peña",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "TypeScript", "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/guillermop/Counters"
homepage = "https://gitlab.com/guillermop/counters/"
bugtracker = "https://gitlab.com/guillermop/counters/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.gitlab.guillermop.Counters"
screenshots = [ "https://gitlab.com/guillermop/counters/-/raw/main/data/screenshots/screenshot1.png", "https://gitlab.com/guillermop/counters/-/raw/main/data/screenshots/screenshot2.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/guillermop/Counters/-/raw/main/data/icons/hicolor/scalable/apps/io.gitlab.guillermop.Counters.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.guillermop.Counters"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.guillermop.Counters"
flatpak_link = "https://flathub.org/apps/io.gitlab.guillermop.Counters.flatpakref"
flatpak_recipe = "https://gitlab.com/guillermop/Counters/-/raw/main/build-aux/flatpak/io.gitlab.guillermop.Counters.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/guillermop/Counters/-/raw/main/data/io.gitlab.guillermop.Counters.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/io.gitlab.guillermop.counters/"
latest_repo_commit = "2024-11-18"
repo_created_date = "2023-03-26"

+++


### Description

Simple counting application that lets you create counters to keep track of anything you want.

Features:

* Multiple counters
* Set and track goals
* Automatically resets every day, week, etc.
* Basic streak counting
* Export your data into a CSV file

[Source](https://gitlab.com/guillermop/Counters/-/raw/main/data/io.gitlab.guillermop.Counters.appdata.xml.in.in)