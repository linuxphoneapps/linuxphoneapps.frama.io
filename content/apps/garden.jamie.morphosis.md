+++
title = "Morphosis"
description = "Convert your documents"
aliases = []
date = 2024-11-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jamie Gravendeel",]
categories = [ "office", "document conversion",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "pandoc",]
services = []
packaged_in = [ "arch", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/morphosis"
homepage = "https://gitlab.gnome.org/World/morphosis"
bugtracker = "https://gitlab.gnome.org/World/morphosis/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/garden.jamie.Morphosis"
screenshots = [ "https://gitlab.gnome.org/World/morphosis/-/raw/v1.4.1/data/screenshots/1.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/morphosis/-/raw/main/data/icons/hicolor/scalable/apps/garden.jamie.Morphosis.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "garden.jamie.Morphosis"
scale_to_fit = ""
flathub = "https://flathub.org/apps/garden.jamie.Morphosis"
flatpak_link = "https://flathub.org/apps/garden.jamie.Morphosis.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/morphosis/-/raw/main/flatpak/garden.jamie.Morphosis.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "morphosis",]
appstream_xml_url = "https://gitlab.gnome.org/World/morphosis/-/raw/main/data/garden.jamie.Morphosis.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/garden.jamie.morphosis/"
latest_repo_commit = "2024-11-11"
repo_created_date = "2024-05-09"

+++


### Description

Use Morphosis to easily convert your text documents to other formats.

Supported export formats:

* PDF
* Markdown
* reStructuredText
* LaTeX
* HTML
* Microsoft Word (.docx)
* OpenOffice/LibreOffice (.odt)
* Rich Text Format (.rtf)
* EPUB
* AsciiDoc

[Source](https://gitlab.gnome.org/World/morphosis/-/raw/main/data/garden.jamie.Morphosis.metainfo.xml.in)
