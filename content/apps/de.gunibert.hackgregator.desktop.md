+++
title = "Hackgregator"
description = "Read Hacker News"
aliases = [ "apps/de.gunibert.hackgregator/",]
date = 2020-09-23
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Günther Wagner",]
categories = [ "news",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "News",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/gunibert/hackgregator"
homepage = "https://gitlab.com/gunibert/hackgregator"
bugtracker = "https://gitlab.com/gunibert/hackgregator/-/issues/"
donations = ""
translations = ""
more_information = [ "https://web.archive.org/web/20220623110901/httpss://www.gwagner.dev/hackgregator-rewritten-in-rust/",]
summary_source_url = "https://gitlab.com/gunibert/hackgregator/-/raw/master/hackgregator/data/de.gunibert.Hackgregator.appdata.xml"
screenshots = [ "https://gunibert.de/cloud/index.php/s/2GJpN7x3HZMsLa2/preview", "https://gunibert.de/cloud/index.php/s/4XiYkk427MSi3bX/preview", "https://gunibert.de/cloud/index.php/s/NTHNq8ecAoJjzNR/preview", "https://gunibert.de/cloud/index.php/s/mXtnzPCf7LtaWGX/preview",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.gunibert.Hackgregator.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.gunibert.Hackgregator"
flatpak_link = "https://flathub.org/apps/de.gunibert.Hackgregator.flatpakref"
flatpak_recipe = "https://gitlab.com/gunibert/hackgregator/-/raw/master/hackgregator/build-aux/de.gunibert.Hackgregator.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "hackgregator",]
appstream_xml_url = "https://gitlab.com/gunibert/hackgregator/-/raw/master/hackgregator/data/de.gunibert.Hackgregator.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/de.gunibert.hackgregator/"
latest_repo_commit = "2024-05-09"
repo_created_date = "2019-09-23"

+++


### Description

Hackgregator is a Hacker News reader application for Gnome.

[Source](https://gitlab.com/gunibert/hackgregator/-/raw/master/hackgregator/data/de.gunibert.Hackgregator.appdata.xml)

### Notice

Based on Python and GTK3/libhandy before 0.4.0.