+++
title = "Bavarder"
description = "Chit-chat with an AI"
aliases = []
date = 2023-05-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "0xMRTT",]
categories = [ "llm chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "ChatGPT",]
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/Bavarder/Bavarder"
homepage = "https://bavarder.codeberg.page"
bugtracker = "https://codeberg.org/Bavarder/Bavarder/issues"
donations = ""
translations = "https://translate.codeberg.org/engage/bavarder/"
more_information = []
summary_source_url = ""
screenshots = [ "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/data/screenshots/preferences.png", "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/data/screenshots/preview.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.bavarder.bavarder/1.png", "https://img.linuxphoneapps.org/io.github.bavarder.bavarder/2.png",]
svg_icon_url = "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/data/icons/hicolor/scalable/apps/io.github.Bavarder.Bavarder.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.Bavarder.Bavarder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.Bavarder.Bavarder"
flatpak_link = "https://flathub.org/apps/io.github.Bavarder.Bavarder.flatpakref"
flatpak_recipe = "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/build-aux/flatpak/io.github.Bavarder.Bavarder.json"
snapcraft = "https://snapcraft.io/bavarder"
snap_link = ""
snap_recipe = "https://codeberg.org/Bavarder/Bavarder/raw/branch/main/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "bavarder",]
appstream_xml_url = "https://raw.githubusercontent.com/Bavarder/Bavarder/main/data/io.github.Bavarder.Bavarder.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-03"
repo_created_date = "2023-04-27"

+++

### Description

Chit-chat with an AI

[Source](https://raw.githubusercontent.com/Bavarder/Bavarder/main/data/io.github.Bavarder.Bavarder.appdata.xml.in.in)