+++
title = "Usage"
description = "Analyse your system"
aliases = []
date = 2024-04-02
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Usage developers",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GNOME", "GTK", "System", "Monitor",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-usage"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-usage/issues"
donations = "https://liberapay.com/camelCaseNick/"
translations = "https://l10n.gnome.org/module/gnome-usage/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-usage/-/raw/main/data/org.gnome.Usage.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-usage/raw/main/screenshots/screenshot-performance-memory-view.png", "https://gitlab.gnome.org/GNOME/gnome-usage/raw/main/screenshots/screenshot-performance-view.png", "https://gitlab.gnome.org/GNOME/gnome-usage/raw/main/screenshots/screenshot-storage-view.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-usage/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Usage.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Usage"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-usage/-/raw/main/org.gnome.Usage.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-usage",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-usage/-/raw/main/data/org.gnome.Usage.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2017-09-27"

+++

### Description

Usage helps with simple to understand ways to analyze how the system uses
its resources.

* Easy access to which app uses what amount of processing power or memory,
  helps find performance issues.
* The storage view provides insight to simplify cleaning up unnecessary
  leftover files.

[Source](https://gitlab.gnome.org/GNOME/gnome-usage/-/raw/main/data/org.gnome.Usage.metainfo.xml.in.in)

### Notice

Before release 45, it was using GTK3/libhandy and mobile friendly since 3.38.