+++
title = "Save the bunny!"
description = "Save the bunny! is a turn-based puzzle game."
aliases = ["games/es.seastorm.merlin/"]
date = 2019-03-17
updated = 2025-01-07

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = []
app_author = ["pabloalba",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "LogicGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/pabloalba/savethebunny"
homepage = ""
bugtracker = "https://github.com/pabloalba/savethebunny/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=es.seastorm.merlin"
screenshots = [ "https://play.google.com/store/apps/details?id=es.seastorm.merlin",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "es.seastorm.merlin"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/es.seastorm.merlin/"
latest_repo_commit = "2019-08-21"
repo_created_date = "2016-12-29"

+++
