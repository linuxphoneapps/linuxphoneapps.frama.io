+++
title = "Calculator"
description = "Beyond the equation"
aliases = []
date = 2024-10-13
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Eduardo Flores",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "libcosmic", "iced",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust", "Python",]
build_systems = [ "cargo",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/cosmic-utils/calculator"
homepage = "https://github.com/cosmic-utils/calculator"
bugtracker = "https://github.com/cosmic-utils/calculator/issues"
donations = "https://ko-fi.com/edfloreshz"
translations = "https://github.com/cosmic-utils/calculator/tree/main/i18n"
more_information = []
summary_source_url = "https://flathub.org/apps/dev.edfloreshz.Calculator"
screenshots = [ "https://raw.githubusercontent.com/cosmic-utils/calculator/main/res/screenshots/window-dark.png", "https://raw.githubusercontent.com/cosmic-utils/calculator/main/res/screenshots/window-light.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/cosmic-utils/calculator/refs/heads/main/res/icons/hicolor/scalable/apps/icon.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = true
app_id = "dev.edfloreshz.Calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.edfloreshz.Calculator"
flatpak_link = "https://flathub.org/apps/dev.edfloreshz.Calculator.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/cosmic-utils/calculator/refs/heads/main/dev.edfloreshz.Calculator.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "cosmic-ext-calculator",]
appstream_xml_url = "https://raw.githubusercontent.com/cosmic-utils/calculator/refs/heads/main/res/metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/dev.edfloreshz.calculator/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-08-15"

+++

### Description

A calculator app with infinite possibilities

* Exceptional performance and responsiveness.

[Source](https://raw.githubusercontent.com/cosmic-utils/calculator/refs/heads/main/res/metainfo.xml)
