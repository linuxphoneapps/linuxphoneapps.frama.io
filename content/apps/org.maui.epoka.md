+++
title = "Epoka"
description = "Simple clock app in development, supporting stopwatch and timer until now"
aliases = []
date = 2019-10-22
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "pontaoski",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/pontaoski/epoka"
homepage = ""
bugtracker = "https://github.com/pontaoski/epoka/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://t.me/mauiproject/1508",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.maui.epoka"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_github_api"
latest_repo_commit = "2020-01-03"
repo_created_date = "2019-10-07"

+++

### Notice

No commits since January 2020, no longer runs after build (submodules no longer recursable).