+++
title = "KTuberling"
description = "A simple constructor game suitable for children and adults alike"
aliases = ["games/org.kde.ktuberling/"]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Game", "KidsGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/games/ktuberling"
homepage = "https://apps.kde.org/ktuberling/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=ktuberling"
donations = "https://www.kde.org/community/donations/?app=ktuberling&source=appdata"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/ktuberling/ktuberling.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.ktuberling"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ktuberling"
flatpak_link = "https://flathub.org/apps/org.kde.ktuberling.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ktuberling",]
appstream_xml_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-05-16"
feed_entry_id = "https://linuxphoneapps.org/games/org.kde.ktuberling/"

+++

### Description

KTuberling a simple constructor game suitable for children and adults alike. The idea of the game is based around a once popular doll making concept.

[Source](https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml)
