+++
title = "gPodder-adaptive"
description = "Media aggregator and podcast client for mobile and desktop alike"
aliases = [ "apps/org.gpodder.gpodder/",]
date = 2021-02-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The gPodder Team",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo", "FileTransfer", "Network", "News",]
programming_languages = [ "Python",]
build_systems = [ "make",]
requires_internet = []
tags = [ "manual status maintenance",]

[extra]
repository = "https://github.com/gpodder/gpodder"
homepage = "https://www.gpodder.org"
bugtracker = "https://github.com/gpodder/gpodder/issues"
donations = ""
translations = ""
more_information = [ "https://wiki.postmarketos.org/wiki/GPodder",]
summary_source_url = "https://raw.githubusercontent.com/gpodder/gpodder/adaptive/share/metainfo/org.gpodder.gpodder-adaptive.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot1.png", "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot2.png", "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot3.png", "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot4.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/gpodder/gpodder/refs/heads/adaptive/share/icons/hicolor/scalable/apps/gpodder-adaptive.svg"
all_features_touch = false
intended_for_mobile = true
app_id = "org.gpodder.gpodder-adaptive"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gpodder.gpodder-adaptive"
flatpak_link = "https://flathub.org/apps/org.gpodder.gpodder-adaptive.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/org.gpodder.gpodder-adaptive.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gpodder-adaptive", "gpodder",]
appstream_xml_url = "https://raw.githubusercontent.com/gpodder/gpodder/adaptive/share/metainfo/org.gpodder.gpodder-adaptive.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-07"
repo_created_date = "2011-04-28"

+++

### Description

gPodder lets you manage your Podcast subscriptions, discover new content and download episodes to your devices.

You can also take advantage of the service gpodder.net, which lets you sync subscriptions, playback progress and starred episodes.

This is a version of gPodder with an adaptive interface meant for both touchscreen devices, like phones and tablets, and desktop and laptop computers.

[Source](https://raw.githubusercontent.com/gpodder/gpodder/adaptive/share/metainfo/org.gpodder.gpodder-adaptive.appdata.xml)

### Notice

GPodder adaptive became a branch of the main project, and has been scaling just fine since.