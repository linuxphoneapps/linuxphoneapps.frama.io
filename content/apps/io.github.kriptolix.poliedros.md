+++
title = "Poliedros"
description = "Poliedros is a multi-type dice roller."
aliases = []
date = 2024-11-23
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Diego C Sampaio",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/kriptolix/Poliedros"
homepage = "https://github.com/kriptolix/Poliedros"
bugtracker = "https://github.com/kriptolix/Poliedros/issues"
donations = ""
translations = "https://github.com/kriptolix/Poliedros/tree/master/po"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.kriptolix.Poliedros"
screenshots = [ "https://github.com/kriptolix/Poliedros/blob/master/data/screenshots/dark-basic-no-sidebar.png?raw=true", "https://github.com/kriptolix/Poliedros/blob/master/data/screenshots/dark-basic-sidebar.png?raw=true", "https://github.com/kriptolix/Poliedros/blob/master/data/screenshots/dark-colapsed-sidebar.png?raw=true", "https://github.com/kriptolix/Poliedros/blob/master/data/screenshots/dark-command-sidebar.png?raw=true",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.kriptolix.Poliedros"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.kriptolix.Poliedros"
flatpak_link = "https://flathub.org/apps/io.github.kriptolix.Poliedros.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/kriptolix/Poliedros/refs/heads/master/io.github.kriptolix.Poliedros.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/kriptolix/Poliedros/refs/heads/master/data/io.github.kriptolix.Poliedros.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.kriptolix.poliedros/"
latest_repo_commit = "2024-11-18"
repo_created_date = "2024-10-07"

+++


### Description

Poliedros is a multi-type dice roller. Basic mode allows you to make quick rolls using just the mouse, while Command mode allows for more complex rolls and combinations.

[Source](https://raw.githubusercontent.com/kriptolix/Poliedros/refs/heads/master/data/io.github.kriptolix.Poliedros.metainfo.xml.in)