+++
title = "Notae"
description = "Take notes easily"
aliases = []
date = 2024-01-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "snapcraft",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/notae"
homepage = "https://invent.kde.org/fhek/notae"
bugtracker = "https://invent.kde.org/fhek/notae/-/issues"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/notae/",]
summary_source_url = "https://invent.kde.org/utilities/notae/-/raw/master/org.kde.notae.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/notae/-/raw/master/org.kde.notae.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.notae"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/notae/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/notae/-/raw/master/org.kde.notae.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2023-01-11"

+++

### Description

Notae lets you focus on what's important.

[Source](https://invent.kde.org/utilities/notae/-/raw/master/org.kde.notae.metainfo.xml)

### Notice

Pre-release, not packaged for any mobile distribution yet. Needs to be evaluated.