+++
title = "Organizer"
description = "A utility to organize your files into neat categories"
aliases = []
date = 2019-02-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/aviwad/organizer"
homepage = "https://gitlab.gnome.org/aviwad/organizer"
bugtracker = "https://gitlab.gnome.org/aviwad/organizer/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/aviwad/organizer/-/raw/master/data/org.librehunt.Organizer.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/aviwad/organizer/raw/master/screenshots/homescreen.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/aviwad/organizer/-/raw/master/data/icons/hicolor/scalable/apps/org.librehunt.Organizer.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.librehunt.Organizer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.librehunt.Organizer"
flatpak_link = "https://flathub.org/apps/org.librehunt.Organizer.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/aviwad/organizer/-/raw/master/org.librehunt.Organizer.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "organizer",]
appstream_xml_url = "https://gitlab.gnome.org/aviwad/organizer/-/raw/master/data/org.librehunt.Organizer.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
latest_repo_commit = "2021-06-16"
repo_created_date = "2019-01-26"

+++


### Description

Organizer shifts your files according to their filetype, making file organizing a breeze!

Since the app is brand new, the features are a little limited:

* Comprehensive detection for filetype
* One click organization
* Constantly improving, just inform the developer! (in the link below)

[Source](https://gitlab.gnome.org/aviwad/organizer/-/raw/master/data/org.librehunt.Organizer.appdata.xml.in)

### Notice

Latest commit March 31st, 2019.