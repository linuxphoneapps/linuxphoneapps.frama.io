+++
title = "Biblioteca"
description = "Read GNOME documentation offline"
aliases = []
date = 2023-11-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Akshay Warrier",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_unstable",]
freedesktop_categories = [ "Development",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/workbenchdev/Biblioteca"
homepage = "https://github.com/workbenchdev/Biblioteca"
bugtracker = "https://github.com/workbenchdev/Biblioteca/issues"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/Biblioteca/",]
summary_source_url = "https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/app.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/screenshot.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.Biblioteca"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Biblioteca"
flatpak_link = "https://flathub.org/apps/app.drey.Biblioteca.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "biblioteca",]
appstream_xml_url = "https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/app.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-08"
repo_created_date = "2023-10-29"

+++


### Description

Biblioteca lets you browse and read GNOME documentation. Among other things, Biblioteca comes with

* Offline documentation
* Web browsing
* Tabs
* Dark mode support
* Fuzzy search
* Mobile / adaptive

[Source](https://raw.githubusercontent.com/workbenchdev/Biblioteca/main/data/app.metainfo.xml.in)