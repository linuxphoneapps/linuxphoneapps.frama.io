+++
title = "Frog"
description = "Extract text from images"
aliases = []
date = 2022-11-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Tender Owl",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "tesseract",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Office", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/tenderowl/frog/"
homepage = "https://getfrog.app"
bugtracker = "https://github.com/tenderowl/frog/issues"
donations = "https://www.buymeacoffee.com/tenderowl/"
translations = "https://hosted.weblate.org/projects/frog/default/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/tenderowl/frog/master/data/screenshots/frog-window-dark.png", "https://raw.githubusercontent.com/tenderowl/frog/master/data/screenshots/frog-window-decoded.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.tenderowl.frog/1.png", "https://img.linuxphoneapps.org/com.github.tenderowl.frog/2.png", "https://img.linuxphoneapps.org/com.github.tenderowl.frog/3.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.tenderowl.frog"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.tenderowl.frog"
flatpak_link = "https://flathub.org/apps/com.github.tenderowl.frog.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-06"
repo_created_date = "2021-07-04"

+++


### Description

Extract text from images, websites, videos, and QR codes by taking a picture of the source.

[Source](https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in)