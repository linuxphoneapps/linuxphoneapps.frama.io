+++
title = "Marknote"
description = "Write down your thoughts"
aliases = []
date = 2024-01-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Office",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/office/marknote"
homepage = "https://apps.kde.org/marknote"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=Marknote"
donations = "https://kde.org/community/donations/?app=marknote"
translations = ""
more_information = [ "https://apps.kde.org/marknote/", "https://carlschwan.eu/2024/05/17/marknote-1.2/",]
summary_source_url = "https://invent.kde.org/office/marknote/-/raw/master/org.kde.marknote.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/marknote/marknote.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.marknote"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.marknote"
flatpak_link = "https://flathub.org/apps/org.kde.marknote.flatpakref"
flatpak_recipe = "https://invent.kde.org/office/marknote/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "marknote",]
appstream_xml_url = "https://invent.kde.org/office/marknote/-/raw/master/org.kde.marknote.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-03-24"

+++

### Description

Marknote lets you create rich text notes and easily organise them into notebooks. You can personalise your notebooks by choosing an icon and accent color for each one, making it easy to distinguish between them and keep your notes at your fingertips. Your notes are saved as Markdown files in your Documents folder, making it easy to use your notes outside of Marknote as well as inside the app.

[Source](https://invent.kde.org/office/marknote/-/raw/master/org.kde.marknote.metainfo.xml)

### Notice

Releases up to 1.2.0 did not really work on mobile (notes could not be viewed/edited).