+++
title = "Kairo"
description = "Sport Training Timer Application"
aliases = [ "apps/org.kde.kairo/",]
date = 2021-02-03
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-2.0-only", "GPL-3.0-only", "LicenseRef-KDE-Accepted-GPL",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community"]
categories = [ "timer",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Clock", "KDE", "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "no icon",]

[extra]
repository = "https://invent.kde.org/utilities/kairo"
homepage = "https://community.kde.org/Kairo"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=kairo"
donations = "https://www.kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kairo/kairo_blue_timer.png", "https://cdn.kde.org/screenshots/kairo/kairo_free_timer.png", "https://cdn.kde.org/screenshots/kairo/kairo_timers_list.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kairo.desktop"
scale_to_fit = ""
svg_icon_url = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kairo",]
appstream_xml_url = "https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.kairo/"
latest_repo_commit = "2024-05-30"
repo_created_date = "2020-05-16"

+++

### Description

An application to help doing sport exercises to train (bodybuilding, stretching, ...) using timers to help you.

[Source](https://invent.kde.org/utilities/kairo/-/raw/master/org.kde.kairo.appdata.xml)
