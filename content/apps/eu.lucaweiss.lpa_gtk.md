+++
title = "eSIM Manager"
description = "Download and manage eSIM profiles"
aliases = []
date = 2024-06-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Luca Weiss",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "lpac",]
services = []
packaged_in = [ "alpine_3_21", "alpine_edge",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/lucaweiss/lpa-gtk"
homepage = "https://codeberg.org/lucaweiss/lpa-gtk"
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://lucaweiss.eu/post/2024-06-24-esim-manager-for-mobile-linux/", "https://fosstodon.org/@z3ntu/112671374903329787",]
summary_source_url = "https://codeberg.org/lucaweiss/lpa-gtk/raw/branch/main/data/eu.lucaweiss.lpa_gtk.metainfo.xml"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = false
app_id = "eu.lucaweiss.lpa_gtk"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lpa-gtk",]
appstream_xml_url = "https://codeberg.org/lucaweiss/lpa-gtk/raw/branch/main/data/eu.lucaweiss.lpa_gtk.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-06"
repo_created_date = "2024-06-21"

+++

### Description

A LPA (Local Profile Assistant) app to manage an eSIM/eUICC in your device.

Designed for Linux phones using Phosh/GNOME Mobile.

[Source](https://codeberg.org/lucaweiss/lpa-gtk/raw/branch/main/data/eu.lucaweiss.lpa_gtk.metainfo.xml)