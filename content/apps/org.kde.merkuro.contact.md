+++
title = "Merkuro Contact"
description = "Manage your contacts with speed and ease"
aliases = []
date = 2023-11-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = [ "Akonadi",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "ContactManagement", "KDE", "Office", "Qt",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/pim/merkuro/-/tree/master/src/contacts"
homepage = "https://apps.kde.org/merkuro.contact/"
bugtracker = "https://invent.kde.org/pim/merkuro/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/contacts/org.kde.merkuro.contact.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/merkuro/contact.png", "https://cdn.kde.org/screenshots/merkuro/contact_editor.png",]
screenshots_img = []
non_svg_icon_url = "https://invent.kde.org/pim/merkuro/-/raw/master/icons/128-apps-org.kde.merkuro.contact.png"
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.merkuro.contact"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "merkuro",]
appstream_xml_url = "https://invent.kde.org/pim/merkuro/-/raw/master/src/contacts/org.kde.merkuro.contact.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2021-02-10"

+++

### Description

Merkuro contact book application.

[Source](https://invent.kde.org/pim/merkuro/-/raw/master/src/contacts/org.kde.merkuro.contact.metainfo.xml)