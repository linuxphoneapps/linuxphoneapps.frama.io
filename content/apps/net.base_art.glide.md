+++
title = "Glide"
description = "Play movies and audio files"
aliases = [ "apps/net.baseart.glide/",]
date = 2020-11-07
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC-BY-3.0",]
app_author = [ "Philippe Normand",]
categories = [ "video player",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/philn/glide"
homepage = "https://github.com/philn/glide"
bugtracker = "https://github.com/philn/glide/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/philn/glide/main/data/net.base_art.Glide.metainfo.xml"
screenshots = [ "https://github.com/philn/glide/raw/main/audio-screenshot.png", "https://github.com/philn/glide/raw/main/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/philn/glide/main/data/net.base_art.Glide.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "net.base_art.Glide"
scale_to_fit = "glide"
flathub = "https://flathub.org/apps/net.base_art.Glide"
flatpak_link = "https://flathub.org/apps/net.base_art.Glide.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/philn/glide/main/build-aux/net.base_art.Glide.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "glide-media-player", "glide-player",]
appstream_xml_url = "https://raw.githubusercontent.com/philn/glide/main/data/net.base_art.Glide.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/net.baseart.glide/"
latest_repo_commit = "2024-12-24"
repo_created_date = "2017-12-25"

+++

### Description

Glide is a simple and minimalistic media player relying on GStreamer for the multimedia support
and GTK for the user interface. Glide should be able to play any multimedia format supported by
GStreamer, locally or remotely hosted.

Once Glide is running you can use some menus to switch the subtitle and audio
tracks, play, pause, seek and switch the window to fullscreen.

[Source](https://raw.githubusercontent.com/philn/glide/main/data/net.base_art.Glide.metainfo.xml)

### Notice

GTK3 before 0.6.0.