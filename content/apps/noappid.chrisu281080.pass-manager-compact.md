+++
title = "pass-manager-compact"
description = "Compact GUI for pass to be useable for smartphones"
aliases = []
date = 2021-02-06
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "chrisu281080",]
categories = [ "password manager",]
mobile_compatibility = [ "needs testing",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = [ "pass",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/chrisu281080/pass-manager-compact"
homepage = ""
bugtracker = "https://gitlab.com/chrisu281080/pass-manager-compact/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/chrisu281080/pass-manager-compact"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = true
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-09-09"
repo_created_date = "2019-12-20"

+++

### Description

This a compact GUI for pass (https://www.passwordstore.org/). It’s designed to be used for the Librem5 (or other Linux Smartphones with GTK). But it can also be used on normal screens.

It supports the extension pass-otp (https://github.com/tadfisher/pass-otp
and the extension pass-tomb (https://github.com/roddhjav/pass-tomb) [Source](https://gitlab.com/chrisu281080/pass-manager-compact/-/blob/master/README.md)

### Notice

Binary for Debian can be downloaded from the [repo](https://gitlab.com/chrisu281080/pass-manager-compact/-/tree/master/deb).

#helpwanted, testing needed
