+++
title = "KDE Itinerary"
description = "Digital travel assistant"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "travel", "public transport",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Office", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/pim/itinerary"
homepage = "https://apps.kde.org/itinerary"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?format=guided&product=KDE+Itinerary"
donations = ""
translations = ""
more_information = [ "https://www.volkerkrause.eu/2018/08/25/kde-itinerary-overview.html", "https://www.volkerkrause.eu/2019/01/26/kde-itinerary-december-january-2019.html", "https://community.kde.org/KDE_PIM/KDE_Itinerary", "https://kde.org/announcements/megarelease/6/#itinerary",]
summary_source_url = "https://invent.kde.org/pim/itinerary"
screenshots = [ "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-boardingpass.png", "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-statistics.png", "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-timeline.png", "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-train-station-map-details.png", "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-train-station-map-with-rental-bike-data.png", "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-train-station-platform-map.png", "https://cdn.kde.org/screenshots/itinerary/kde-itinerary-vehicle-layout.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/pim/itinerary/-/raw/master/src/app/sc-apps-org.kde.itinerary.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.itinerary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.itinerary"
flatpak_link = "https://flathub.org/apps/org.kde.itinerary.flatpakref"
flatpak_recipe = "https://invent.kde.org/pim/itinerary/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "itinerary",]
appstream_xml_url = "https://invent.kde.org/pim/itinerary/-/raw/master/src/app/org.kde.itinerary.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-01-21"

+++

### Description

KDE Itinerary is a digital travel assistant with a priority on protecting your privacy.

Features:

* Timeline view of a unified travel itinerary with automatic trip grouping.
* Supports train, bus and flight bookings as well as hotel, restaurant, event and rental car reservations.
* Boarding pass management.
* Supports ticket management for multi-traveler and multi-ticket bookings.
* Automatic booking data extraction from various input formats, performed locally on your device.
* Real-time delay and platform change information for trains.
* Weather forecast for destination along your trip.
* Full control over all online access.
* Selection of alternative train connections on unbound tickets or on missed connections.
* Local ground transportation navigation between elements of your itinerary.
* Train coach layout view (for some operators only).
* Train station and airport per-floor maps based on OpenStreetMap data.
* Available dock-based or free-floating rental bikes can be displayed on the train station map.
* Personal travel statistics to monitor environmental impact.

KDE Itinerary works best alongside KMail's itinerary extraction plug-in and KDE Connect, or Nextcloud Hub and DavDroid.

[Source](https://invent.kde.org/pim/itinerary/-/raw/master/src/app/org.kde.itinerary.appdata.xml)