+++
title = "QMLKonsole"
description = "Mobile terminal application"
aliases = [ "apps/org.kde.mobile.qmlkonsole/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "System", "TerminalEmulator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/qmlkonsole"
homepage = "https://apps.kde.org/qmlkonsole"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=QMLKonsole"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/plasma-mobile/qmlkonsole/-/raw/master/org.kde.qmlkonsole.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/qmlkonsole/main.png", "https://cdn.kde.org/screenshots/qmlkonsole/settings.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.qmlkonsole"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.qmlkonsole"
flatpak_link = "https://flathub.org/apps/org.kde.qmlkonsole.flatpakref"
flatpak_recipe = "https://invent.kde.org/plasma-mobile/qmlkonsole/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "qmlkonsole", "kde5-qmlkonsole",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/qmlkonsole/-/raw/master/org.kde.qmlkonsole.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2018-12-23"

+++

### Description

Terminal application offering additional keyboard buttons useful on touch devices.

[Source](https://invent.kde.org/plasma-mobile/qmlkonsole/-/raw/master/org.kde.qmlkonsole.appdata.xml)