+++
title = "Endeavour"
description = "Manage your tasks"
aliases = []
date = 2022-10-04
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jamie Murphy",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/World/Endeavour"
homepage = "https://gitlab.gnome.org/World/Endeavour"
bugtracker = "https://gitlab.gnome.org/World/Endeavour/issues"
donations = ""
translations = "https://l10n.gnome.org/module/Endeavour/"
more_information = [ "https://feaneron.com/2022/06/21/giving-up-on-gnome-to-do/",]
summary_source_url = "https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/World/Endeavour/raw/main/data/appdata/no-tasks.png", "https://gitlab.gnome.org/World/Endeavour/raw/main/data/appdata/task-list.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.todo/1.png", "https://img.linuxphoneapps.org/org.gnome.todo/2.png", "https://img.linuxphoneapps.org/org.gnome.todo/3.png", "https://img.linuxphoneapps.org/org.gnome.todo/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Todo"
scale_to_fit = "org.gnome.todo"
flathub = "https://flathub.org/apps/org.gnome.Todo"
flatpak_link = "https://flathub.org/apps/org.gnome.Todo.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-todo", "endeavour",]
appstream_xml_url = "https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-25"
repo_created_date = "2017-10-16"

+++


### Description

Endeavour is a task management application designed for simplicity. Save and order your todos.
Manage multiple todo lists. And more

[Source](https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in)

### Notice

Previously known as GNOME To Do. Endeavour upstream is not yet adaptive, but Mobian has been shipping an [adaptive build](https://salsa.debian.org/Mobian-team/packages/gnome-todo) for a long time, with a [patchset](https://salsa.debian.org/Mobian-team/packages/gnome-todo/-/tree/mobian/debian/patches) maintained up to 41 release.