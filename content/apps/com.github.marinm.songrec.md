+++
title = "SongRec"
description = "An open-source, unofficial Shazam client for Linux, written in Rust."
aliases = []
date = 2022-06-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Marin",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = [ "shazam",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Audio", "AudioVideo", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/marin-m/SongRec"
homepage = "https://github.com/marin-m/SongRec"
bugtracker = "https://github.com/marin-m/SongRec/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.marinm.songrec"
screenshots = [ "https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/Screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.marinm.songrec/1.png", "https://img.linuxphoneapps.org/com.github.marinm.songrec/2.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.marinm.songrec"
scale_to_fit = "songrec"
flathub = "https://flathub.org/apps/com.github.marinm.songrec"
flatpak_link = "https://flathub.org/apps/com.github.marinm.songrec.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "songrec",]
appstream_xml_url = "https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/rootfs/usr/share/metainfo/com.github.marinm.songrec.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-09-29"
repo_created_date = "2020-08-13"

+++

### Description

Features:

- Recognize audio from an arbitrary audio file.

- Recognize audio from the microphone.

- Usage from both GUI and command line (for the file recognition part).

- Provide an history of the recognized songs on the GUI, exportable to CSV.

- Continuous song detection from the microphone, with the ability to choose your input device.

- Ability to recognize songs from your speakers rather than your microphone (on compatible PulseAudio setups).

[Source](https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/rootfs/usr/share/metainfo/com.github.marinm.songrec.metainfo.xml)