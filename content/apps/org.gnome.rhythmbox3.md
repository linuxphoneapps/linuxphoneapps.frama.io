+++
title = "Rhythmbox"
description = "Play and organize all your music"
aliases = []
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GFDL-1.3", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Rhythmbox developers",]
categories = [ "music player",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Audio", "AudioVideo", "Player",]
programming_languages = [ "C", "Vala",]
build_systems = [ "make",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/rhythmbox"
homepage = "https://gnome.pages.gitlab.gnome.org/rhythmbox/"
bugtracker = "https://gitlab.gnome.org/GNOME/rhythmbox/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/rhythmbox/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/screenshots/rhythmbox-main-window.png?inline=false",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/icons/hicolor/scalable/apps/org.gnome.Rhythmbox3.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Rhythmbox3"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Rhythmbox3"
flatpak_link = "https://flathub.org/apps/org.gnome.Rhythmbox3.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "rhythmbox",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-06"
repo_created_date = "2018-05-23"

+++

### Description

Rhythmbox is a music management application, designed to work well under the GNOME
desktop. In addition to music stored on your computer, it supports network shares,
podcasts, radio streams, portable music devices (including phones), and internet music
services such as Last.fm and Magnatune.

Rhythmbox is Free software, based on GTK+ and GStreamer, and is extensible via plugins
written in Python or C.

[Source](https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in)