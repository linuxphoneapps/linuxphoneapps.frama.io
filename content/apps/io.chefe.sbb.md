+++
title = "SBB"
description = "A simple gtk app to get timetable information for swiss public transport."
aliases = []
date = 2021-05-30
updated = 2024-10-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "chefe",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "transport.opendata.ch",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/chefe/sbb"
homepage = ""
bugtracker = "https://github.com/chefe/sbb/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/chefe/sbb"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1399016230380609539",]
screenshots_img = []
svg_icon_url = "https://github.com/chefe/sbb/raw/refs/heads/main/data/io.chefe.sbb.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.chefe.sbb"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/chefe/sbb/refs/heads/main/data/io.chefe.sbb.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2022-08-02"
repo_created_date = "2020-04-22"

+++