+++
title = "Lucy"
description = "Lucy's family has been abducted by Aliens! Help Lucy to reach the four UFOS and rescue her family."
aliases = ["games/es.seastorm.lucy/"]
date = 2019-03-17
updated = 2025-01-07

[taxonomies]
project_licenses = [ "Apache-2.0",]
metadata_licenses = []
app_author = ["pabloalba",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
status = [ "pre-release", "inactive",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "ArcadeGame"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/pabloalba/lucy"
homepage = ""
bugtracker = "https://github.com/pabloalba/lucy/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://play.google.com/store/apps/details?id=es.seastorm.lucy"
screenshots = [ "https://play.google.com/store/apps/details?id=es.seastorm.lucy",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "es.seastorm.lucy"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/es.seastorm.lucy/"
latest_repo_commit = "2019-08-21"
repo_created_date = "2016-03-03"

+++

### Description

Lucy's family has been abducted by Aliens! Help Lucy to reach the four UFOS and rescue her family.

[Source](https://play.google.com/store/apps/details?id=es.seastorm.lucy)
