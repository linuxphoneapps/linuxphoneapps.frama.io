+++
title = "Pix"
description = "Image Gallery"
aliases = [ "apps/org.kde.pix.desktop/",]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed",]
freedesktop_categories = [ "Graphics", "Qt", "Viewer",]
programming_languages = [ "QML", "Cpp", "C",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/maui/pix"
homepage = "https://apps.kde.org/pix"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=pix"
donations = "https://kde.org/community/donations"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/maui-pix/-/raw/master/org.kde.pix.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/pix/pix-2.png", "https://cdn.kde.org/screenshots/pix/pix-3.png", "https://cdn.kde.org/screenshots/pix/pix.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/maui/maui-pix/-/raw/master/src/assets/pix.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "org.kde.pix"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.pix"
flatpak_link = "https://flathub.org/apps/org.kde.pix.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "maui-pix", "pix",]
appstream_xml_url = "https://invent.kde.org/maui/maui-pix/-/raw/master/org.kde.pix.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.kde.pix/"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-04-21"

+++

### Description

Pix is an image gallery and image viewer. Pix can be used to open images with other applications like an image editor, add tags to the files, add annotations to pictures, rotate and share them.

[Source](https://invent.kde.org/maui/pix/-/raw/master/org.kde.pix.appdata.xml)