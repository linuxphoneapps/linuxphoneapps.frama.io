+++
title = "Parental Controls"
description = "Set parental controls and monitor usage by users"
aliases = []
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "The GNOME Project",]
categories = [ "parental controls",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.freedesktop.org/pwithnall/malcontent"
homepage = "https://gitlab.freedesktop.org/pwithnall/malcontent"
bugtracker = "https://gitlab.freedesktop.org/pwithnall/malcontent/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject/LocalisationGuide"
more_information = []
summary_source_url = "https://gitlab.freedesktop.org/pwithnall/malcontent/-/raw/main/malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in"
screenshots = [ "https://gitlab.freedesktop.org/pwithnall/malcontent/-/raw/HEAD/malcontent-control/malcontent-control.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.freedesktop.org/pwithnall/malcontent/-/raw/main/malcontent-control/icons/scalable/org.freedesktop.MalcontentControl.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.freedesktop.MalcontentControl"
scale_to_fit = "Malcontent-control"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "malcontent", "malcontent-ui",]
appstream_xml_url = "https://gitlab.freedesktop.org/pwithnall/malcontent/-/raw/main/malcontent-control/org.freedesktop.MalcontentControl.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-22"
repo_created_date = "2019-02-26"

+++

### Description

Manage users’ parental controls restrictions, controlling how long they
can use the computer for, what software they can install, and what
installed software they can run.

[Source](https://gitlab.freedesktop.org/pwithnall/malcontent/-/raw/main/malcontent-control/org.freedesktop.MalcontentControl.metainfo.xml.in)

### Notice

GUI looks fine, but neither Phosh nor Plasma Mobile are multi-user ready yet, which makes this technically useless for now.