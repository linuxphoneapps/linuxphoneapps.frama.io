+++
title = "OpenRA"
description = "Reimagining of early Westwood real-time strategy games"
aliases = ["games/net.openra.openra/"]
date = 2021-03-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "The OpenRA Developers",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "released",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "aur", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Game", "StrategyGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/OpenRA/OpenRA"
homepage = "https://www.openra.net"
bugtracker = "https://github.com/OpenRA/OpenRA/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/net.openra.OpenRA"
screenshots = [ "https://dl.flathub.org/media/net/openra/OpenRA/d0381785eeb6623542ce1d08b10e87f2/screenshots/image-1_orig.png", "https://dl.flathub.org/media/net/openra/OpenRA/d0381785eeb6623542ce1d08b10e87f2/screenshots/image-2_orig.png", "https://dl.flathub.org/media/net/openra/OpenRA/d0381785eeb6623542ce1d08b10e87f2/screenshots/image-3_orig.png", "https://dl.flathub.org/media/net/openra/OpenRA/d0381785eeb6623542ce1d08b10e87f2/screenshots/image-4_orig.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.openra.OpenRA"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.openra.OpenRA"
flatpak_link = "https://flathub.org/apps/net.openra.OpenRA.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "openra",]
appstream_xml_url = "https://raw.githubusercontent.com/OpenRA/OpenRA/bleed/packaging/linux/openra.metainfo.xml.in"
reported_by = "-Euso-"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/games/net.openra.openra/"
latest_repo_commit = "2025-01-11"
repo_created_date = "2010-10-04"

+++
