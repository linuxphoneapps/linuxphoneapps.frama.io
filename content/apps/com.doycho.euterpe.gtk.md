+++
title = "Euterpe"
description = "Audio player for the Euterpe media server"
aliases = []
date = 2022-03-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Doychin Atanasov",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "euterpe",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Audio", "AudioVideo", "GNOME", "GTK", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires always",]
tags = []

[extra]
repository = "https://github.com/ironsmile/euterpe-gtk"
homepage = "https://listen-to-euterpe.eu"
bugtracker = "https://github.com/ironsmile/euterpe-gtk/issues"
donations = "https://github.com/sponsors/ironsmile"
translations = ""
more_information = [ "https://listen-to-euterpe.eu/docs",]
summary_source_url = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in"
screenshots = [ "https://i.imgur.com/1dcIdP6.png", "https://i.imgur.com/3WYNXhm.png", "https://i.imgur.com/B5MqGpB.png", "https://i.imgur.com/FD6b2Qm.png", "https://i.imgur.com/OTyvR2c.png", "https://i.imgur.com/OgAEGf8.png", "https://i.imgur.com/TBEo3gM.png", "https://i.imgur.com/UaisFWY.png", "https://i.imgur.com/h3D7hgb.png", "https://i.imgur.com/uF4hfV8.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.doycho.euterpe.gtk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.doycho.euterpe.gtk"
flatpak_link = "https://flathub.org/apps/com.doycho.euterpe.gtk.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/com.doycho.euterpe.gtk.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "euterpe",]
appstream_xml_url = "https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-03-31"
repo_created_date = "2021-07-25"

+++


### Description

Mobile and desktop player for the self-hosted Euterpe streaming
server. You need an access to an Euterpe server running somewhere in
order for this program to be useful. You could try it out with the
demo Euterpe server, accessible at the project's website.

These are some of the things which this player supports:

* Extremely light resource usage and fast. Excellent for constrained
  mobile devices such as phones and laptops.
* Accessing Euterpe server with or without authentication.
* Playing albums or a single tracks.
* Searching the database for music.
* Browsing by albums and artists based on their tag metadata.
* Mobile first but convergent. It works on both mobile and desktop Linux.

[Source](https://raw.githubusercontent.com/ironsmile/euterpe-gtk/master/data/com.doycho.euterpe.gtk.appdata.xml.in)