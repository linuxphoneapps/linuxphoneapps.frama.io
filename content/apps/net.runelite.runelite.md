+++
title = "RuneLite"
description = "RuneLite OSRS Client"
aliases = ["games/net.runelite.runelite/"]
date = 2021-01-26
updated = 2025-01-13

[taxonomies]
project_licenses = [ "BSD-2-Clause",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "RuneLite",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "released",]
frameworks = []
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Game", "RolePlaying",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/runelite/runelite"
homepage = "https://runelite.net"
bugtracker = "https://github.com/runelite/runelite/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/1.png", "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/2.png", "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/3.png", "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/screenshot.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.runelite.RuneLite"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.runelite.RuneLite"
flatpak_link = "https://flathub.org/apps/net.runelite.RuneLite.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "runelite",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.metainfo.xml"
reported_by = "immychan"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2016-04-30"
feed_entry_id = "https://linuxphoneapps.org/games/net.runelite.runelite/"

+++

### Description

RuneLite is a free, open-source and super fast client for Old School RuneScape.

[Source](https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.metainfo.xml)
