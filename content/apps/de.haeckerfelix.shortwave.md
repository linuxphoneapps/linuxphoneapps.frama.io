+++
title = "Shortwave"
description = "Listen to internet radio"
aliases = []
date = 2020-08-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felix Häcker",]
categories = [ "internet radio",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "internet radio",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0",]
freedesktop_categories = [ "Audio", "AudioVideo",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/Shortwave"
homepage = "https://apps.gnome.org/Shortwave/"
bugtracker = "https://gitlab.gnome.org/World/Shortwave/issues"
donations = "https://liberapay.com/haecker-felix"
translations = "https://l10n.gnome.org/module/Shortwave/"
more_information = [ "https://blogs.gnome.org/haeckerfelix/2021/04/09/new-shortwave-release/", "https://apps.gnome.org/Shortwave/",]
summary_source_url = "https://gitlab.gnome.org/World/Shortwave"
screenshots = [ "https://gitlab.gnome.org/World/Shortwave/raw/main/data/screenshots/1.png", "https://gitlab.gnome.org/World/Shortwave/raw/main/data/screenshots/2.png", "https://gitlab.gnome.org/World/Shortwave/raw/main/data/screenshots/3.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "de.haeckerfelix.Shortwave"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.haeckerfelix.Shortwave"
flatpak_link = "https://flathub.org/apps/de.haeckerfelix.Shortwave.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "shortwave",]
appstream_xml_url = "https://gitlab.gnome.org/World/Shortwave/-/raw/main/data/de.haeckerfelix.Shortwave.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-12-15"

+++

### Description

Shortwave is an internet radio player that provides access to a station database with over 50,000 stations.

Features:

* Create your own library where you can add your favorite stations
* Easily search and discover new radio stations
* Automatic recognition of tracks, with the possibility to save them individually
* Responsive application layout, compatible for small and large screens
* Play audio on supported network devices (e.g. Google Chromecasts)
* Seamless integration into the GNOME desktop environment

[Source](https://gitlab.gnome.org/World/Shortwave/-/raw/main/data/de.haeckerfelix.Shortwave.metainfo.xml.in.in)