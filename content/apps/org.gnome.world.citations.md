+++
title = "Citations"
description = "Manage your bibliography"
aliases = []
date = 2024-05-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Maximiliano Sandoval",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "doi2bib",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Literature", "Science",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/citations"
homepage = "https://apps.gnome.org/Citations/"
bugtracker = "https://gitlab.gnome.org/World/citations/issues"
donations = ""
translations = "https://l10n.gnome.org/module/citations/"
more_information = [ "https://apps.gnome.org/Citations/",]
summary_source_url = "https://flathub.org/apps/org.gnome.World.Citations"
screenshots = [ "https://gitlab.gnome.org/World/citations/raw/master/data/screenshots/main_window.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/citations/-/raw/master/data/icons/org.gnome.World.Citations.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.World.Citations"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.Citations"
flatpak_link = "https://flathub.org/apps/org.gnome.World.Citations.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/citations/-/raw/master/build-aux/org.gnome.World.Citations.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "citations",]
appstream_xml_url = "https://gitlab.gnome.org/World/citations/-/raw/master/data/org.gnome.World.Citations.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2022-05-02"

+++

### Description

Manage your bibliographies using the BibTeX format.

[Source](https://gitlab.gnome.org/World/citations/-/raw/master/data/org.gnome.World.Citations.metainfo.xml.in.in)