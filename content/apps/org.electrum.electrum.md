+++
title = "Electrum"
description = "Bitcoin Wallet"
aliases = []
date = 2020-10-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "The Electrum developers",]
categories = [ "bitcoin wallet",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "QtWidgets", "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Finance", "Network",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/spesmilo/electrum"
homepage = "https://www.electrum.org/"
bugtracker = "https://github.com/spesmilo/electrum/issues"
donations = ""
translations = ""
more_information = [ "https://github.com/spesmilo/electrum/issues/6835",]
summary_source_url = "https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml"
screenshots = [ "https://dl.flathub.org/media/org/electrum/electrum/405ad897647fb291a7b378435982ce06/screenshots/image-1_orig.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/spesmilo/electrum/refs/heads/master/electrum/gui/icons/electrum_lightblue.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.electrum.electrum"
scale_to_fit = "electrum"
flathub = "https://flathub.org/apps/org.electrum.electrum"
flatpak_link = "https://flathub.org/apps/org.electrum.electrum.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "electrum",]
appstream_xml_url = "https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2012-08-02"

+++

### Description

Electrum is a lightweight Bitcoin wallet focused on speed, with low resource usage and simplifying Bitcoin.
Startup times are instant because it operates in conjunction with high-performance servers that handle the most complicated parts of the Bitcoin system.

[Source](https://raw.githubusercontent.com/spesmilo/electrum/master/org.electrum.electrum.metainfo.xml)

### Notice

Ok after scale-to-fit. The app has a Kivy GUI for Android, which should do better - but needs custom building. If you manage to run the Kivy GUI on mobile GNU/Linux, please report back :-)