+++
title = "File Shredder"
description = "Permanently delete your files"
aliases = []
date = 2022-09-17
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alan Beveridge",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "shred",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "FileTools", "Security", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/ADBeveridge/raider"
homepage = "https://apps.gnome.org/Raider"
bugtracker = "https://github.com/ADBeveridge/raider/issues"
donations = ""
translations = "https://github.com/ADBeveridge/raider/tree/develop/po"
more_information = [ "https://apps.gnome.org/Raider/",]
summary_source_url = "https://raw.githubusercontent.com/ADBeveridge/raider/develop/data/com.github.ADBeveridge.Raider.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot1.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/1.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/2.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/3.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/4.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/5.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/6.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "com.github.ADBeveridge.Raider"
scale_to_fit = "com.github.ADBeveridge.Raider.Help"
flathub = "https://flathub.org/apps/com.github.ADBeveridge.Raider"
flatpak_link = "https://flathub.org/apps/com.github.ADBeveridge.Raider.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "raider-file-shredder",]
appstream_xml_url = "https://raw.githubusercontent.com/ADBeveridge/raider/develop/data/com.github.ADBeveridge.Raider.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-28"
repo_created_date = "2021-10-21"

+++

### Description

File Shredder is a file deletion program designed to permanently
remove sensitive files from your computer, enhancing data privacy.

Within a certain limit, it is effective. However, modern SSDs use certain technologies
to extend its lifetime, which has the side effect of ensuring that shredding is never
perfect, and no software can fix that. But shredding significantly increases the
difficulty of data recovery since it requires specialized software and hardware.

[Source](https://raw.githubusercontent.com/ADBeveridge/raider/develop/data/com.github.ADBeveridge.Raider.metainfo.xml.in.in)