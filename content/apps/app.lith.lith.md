+++
title = "Lith"
description = "WeeChat Relay Client"
aliases = [ "apps/app.lith.lith.desktop/",]
date = 2021-04-18
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martin Bříza",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = [ "WeeChat",]
services = [ "IRC",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/LithApp/Lith"
homepage = "https://lith.app"
bugtracker = "https://github.com/LithApp/Lith/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml.in"
screenshots = [ "https://lith.app/assets/main.png",]
screenshots_img = []
non_svg_icon_url = "https://github.com/LithApp/Lith/blob/master/assets/icons/linux/hicolor/128x128/apps/app.lith.Lith.png"
all_features_touch = false
intended_for_mobile = false
app_id = "app.lith.Lith"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.lith.Lith"
flatpak_link = "https://flathub.org/apps/app.lith.Lith.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/LithApp/Lith/refs/heads/master/dist/linux/flatpak/app.Lith.Lith.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "lith",]
appstream_xml_url = "https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/app.lith.lith/"
latest_repo_commit = "2025-01-03"
repo_created_date = "2016-09-18"

+++

### Description

With Lith, you can connect to your WeeChat instance from any device.

[Source](https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml)

### Notice

WeeChat client – you will need to set up and configure WeeChat to use this.