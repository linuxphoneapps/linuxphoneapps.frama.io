+++
title = "newsFish"
description = "Nextcloud news client for Kirigami"
aliases = []
date = 2024-09-23
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "piggz",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = [ "Nextcloud News",]
packaged_in = []
freedesktop_categories = [ "Qt", "Network", "Feed",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/piggz/newsFish"
homepage = "https://github.com/piggz/newsFish/"
bugtracker = "https://github.com/piggz/newsFish/issues"
donations = "https://www.paypal.me/piggz"
translations = ""
more_information = []
summary_source_url = "https://github.com/piggz/newsFish"
screenshots = [ "https://github.com/piggz/newsFish/blob/main/screenshot1.jpg?raw=true", "https://github.com/piggz/newsFish/blob/main/screenshot2.jpg?raw=true", "https://github.com/piggz/newsFish/blob/main/screenshot3.png", "https://github.com/piggz/newsFish/blob/main/screenshot4.jpg?raw=true",]
screenshots_img = []
non_svg_icon_url = "https://raw.githubusercontent.com/piggz/newsFish/refs/heads/main/uk.co.piggz.newsfish.png"
all_features_touch = false
intended_for_mobile = false
app_id = "uk.co.piggz.newsfish"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
feed_entry_id = "https://linuxphoneapps.org/apps/uk.co.piggz.newsfish/"
latest_repo_commit = "2024-06-18"
repo_created_date = "2023-06-10"

+++

### Description

 Nextcloud news client for Kirigami

[Source](https://github.com/piggz/newsFish)

### Notice

Primarily made for Sailfish OS, ported to Ubuntu Touch, but can also run on other distributions.
