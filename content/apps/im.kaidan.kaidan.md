+++
title = "Kaidan"
description = "Modern chat app for every device"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = [ "QXmpp",]
services = [ "XMPP",]
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium",]
freedesktop_categories = [ "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/network/kaidan"
homepage = "https://kaidan.im"
bugtracker = "https://invent.kde.org/network/kaidan/-/issues"
donations = ""
translations = "https://l10n.kde.org/stats/gui/trunk-kf5/package/kaidan/"
more_information = [ "https://www.kaidan.im/2023/05/05/kaidan-0.9.0/", "https://xmpp.org/software/kaidan/",]
summary_source_url = "https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml"
screenshots = [ "https://www.kaidan.im/images/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/network/kaidan/-/raw/master/data/images/kaidan.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "im.kaidan.kaidan"
scale_to_fit = ""
flathub = "https://flathub.org/apps/im.kaidan.kaidan"
flatpak_link = "https://flathub.org/apps/im.kaidan.kaidan.flatpakref"
flatpak_recipe = "https://invent.kde.org/network/kaidan/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kaidan",]
appstream_xml_url = "https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2019-05-06"

+++

### Description

Kaidan is a user-friendly and modern chat app for every device. It uses the open communication protocol XMPP (Jabber). Unlike other chat apps, you are not dependent on one specific service provider.

Kaidan does not have all basic features yet and has still some stability issues. But we do our best to improve it!

[Source](https://invent.kde.org/network/kaidan/-/raw/master/misc/im.kaidan.kaidan.appdata.xml)