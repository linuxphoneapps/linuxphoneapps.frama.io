+++
title = "Kirigami Gallery"
description = "Shows examples of Kirigami components and allows you to play with them"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Marco Martin <mart@kde.org>",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Development",]
programming_languages = [ "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://invent.kde.org/sdk/kirigami-gallery"
homepage = "https://kde.org"
bugtracker = "https://invent.kde.org/sdk/kirigami-gallery/-/issues/"
donations = "https://www.kde.org/donate.php?app=org.kde.kirigami2.gallery"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/sdk/kirigami-gallery/-/raw/master/org.kde.kirigami2.gallery.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kirigami-gallery/kirigami-gallery.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.kde.kirigami2.gallery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "kirigami-gallery",]
appstream_xml_url = "https://invent.kde.org/sdk/kirigami-gallery/-/raw/master/org.kde.kirigami2.gallery.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-05-16"

+++

### Description

View examples of Kirigami components

[Source](https://invent.kde.org/sdk/kirigami-gallery/-/raw/master/org.kde.kirigami2.gallery.appdata.xml)