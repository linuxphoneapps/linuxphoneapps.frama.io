+++
title = "Community Remote"
description = "Roon control for family and friends"
aliases = []
date = 2024-11-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "The Appgineer",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Flutter",]
backends = [ "Rust Roon API",]
services = [ "Roon",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "AudioVideo", "Player",]
programming_languages = [ "Dart", "Rust",]
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/TheAppgineer/community_remote"
homepage = "https://theappgineer.com/community_remote/"
bugtracker = "https://github.com/TheAppgineer/community_remote/issues"
donations = "https://buymeacoffee.com/theappgineer"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.theappgineer.community_remote"
screenshots = [ "https://raw.githubusercontent.com/TheAppgineer/community_remote/0.2.0/images/linux-dark.png", "https://raw.githubusercontent.com/TheAppgineer/community_remote/0.2.0/images/linux-gridview.png", "https://raw.githubusercontent.com/TheAppgineer/community_remote/0.2.0/images/linux-light.png", "https://raw.githubusercontent.com/TheAppgineer/community_remote/0.2.0/images/linux-nowplaying.png", "https://raw.githubusercontent.com/TheAppgineer/community_remote/0.2.0/images/linux-small-nowplaying.png", "https://raw.githubusercontent.com/TheAppgineer/community_remote/0.2.0/images/linux-small.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = "https://raw.githubusercontent.com/TheAppgineer/community_remote/refs/heads/master/flatpak/icons/256x256/com.theappgineer.community_remote.png"
all_features_touch = false
intended_for_mobile = false
app_id = "com.theappgineer.community_remote"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.theappgineer.community_remote"
flatpak_link = "https://flathub.org/apps/com.theappgineer.community_remote.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/TheAppgineer/community_remote/refs/heads/master/flatpak/com.theappgineer.community_remote.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/TheAppgineer/community_remote/refs/heads/master/flatpak/com.theappgineer.community_remote.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.theappgineer.community_remote/"
latest_repo_commit = "2024-12-26"
repo_created_date = "2024-01-21"

+++

### Description

Community Remote has a focus on use-cases not covered by the official Roon Remote.
Linux support is the obvious one, others are access control, random album playback and more.

[Source](https://raw.githubusercontent.com/TheAppgineer/community_remote/refs/heads/master/flatpak/com.theappgineer.community_remote.metainfo.xml)