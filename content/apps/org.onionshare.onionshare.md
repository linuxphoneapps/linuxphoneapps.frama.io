+++
title = "OnionShare"
description = "Securely and anonymously share files, host websites, and chat with friends"
aliases = []
date = 2020-11-28
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Micah Lee",]
categories = [ "file transfer", "chat",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "QtQuick",]
backends = []
services = [ "tor",]
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "FileTransfer", "Network",]
programming_languages = [ "Python",]
build_systems = [ "custom",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/onionshare/onionshare"
homepage = "https://onionshare.org/"
bugtracker = "https://github.com/onionshare/onionshare/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/onionshare/onionshare/main/desktop/org.onionshare.OnionShare.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/onionshare/onionshare/main/docs/source/_static/screenshots/tabs.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "org.onionshare.OnionShare"
scale_to_fit = "python3"
flathub = "https://flathub.org/apps/org.onionshare.OnionShare"
flatpak_link = "https://flathub.org/apps/org.onionshare.OnionShare.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "onionshare",]
appstream_xml_url = "https://raw.githubusercontent.com/onionshare/onionshare/main/desktop/org.onionshare.OnionShare.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2014-05-20"

+++

### Description

OnionShare is an open source tool that lets you securely and anonymously share files, host websites, and chat with friends using the Tor network.

[Source](https://raw.githubusercontent.com/onionshare/onionshare/main/desktop/org.onionshare.OnionShare.appdata.xml)

### Notice

Scaling is almost fine with scale-to-fit, drag and drop is tough to unachievable.