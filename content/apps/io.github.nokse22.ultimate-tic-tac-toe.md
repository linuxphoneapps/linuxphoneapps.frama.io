+++
title = "Ultimate Tic Tac Toe"
description = "(Tic Tac Toe)²"
aliases = ["games/io.github.nokse22.ultimate-tic-tac-toe/"]
date = 2024-06-22
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nokse",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://github.com/Nokse22/ultimate-tic-tac-toe"
homepage = "https://github.com/Nokse22/ultimate-tic-tac-toe"
bugtracker = "https://github.com/Nokse22/ultimate-tic-tac-toe/issues"
donations = "https://ko-fi.com/nokse22"
translations = "https://github.com/Nokse22/ultimate-tic-tac-toe/tree/master/po"
more_information = []
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/resources/screenshot%201.png", "https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/resources/screenshot%202.png", "https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/resources/screenshot%203.png", "https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/resources/screenshot%204.png", "https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/resources/screenshot%205.png",]
screenshots_img = []
svg_icon_url = "https://github.com/Nokse22/ultimate-tic-tac-toe/blob/81b27061fe2b8c7685e270964777494fb58adb5a/data/icons/hicolor/scalable/apps/io.github.nokse22.ultimate-tic-tac-toe.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.nokse22.ultimate-tic-tac-toe"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.nokse22.ultimate-tic-tac-toe"
flatpak_link = "https://flathub.org/apps/io.github.nokse22.ultimate-tic-tac-toe.flatpakref"
flatpak_recipe = "https://github.com/Nokse22/ultimate-tic-tac-toe/blob/81b27061fe2b8c7685e270964777494fb58adb5a/io.github.nokse22.ultimate-tic-tac-toe.json"
snapcraft = "https://snapcraft.io/ultimate-tic-tac-toe"
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ultimate-tic-tac-toe",]
appstream_xml_url = "https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/io.github.nokse22.ultimate-tic-tac-toe.metainfo.xml.in"
reported_by = "Nokse"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/games/io.github.nokse22.ultimate-tic-tac-toe/"
latest_repo_commit = "2024-11-29"
repo_created_date = "2023-08-30"

+++


### Description

Play with your friends or against the algorithm. You can read the rules in the game.

[Source](https://raw.githubusercontent.com/Nokse22/ultimate-tic-tac-toe/master/data/io.github.nokse22.ultimate-tic-tac-toe.metainfo.xml.in)
