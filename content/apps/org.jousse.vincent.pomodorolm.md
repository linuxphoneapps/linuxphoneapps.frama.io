+++
title = "pomodorolm"
description = "A simple, good looking and configurable pomodoro tracker with tray icon"
aliases = []
date = 2024-10-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Vincent Jousse",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Tauri",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Elm", "Rust",]
build_systems = [ "cargo", "npm",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/vjousse/pomodorolm"
homepage = "https://github.com/vjousse/pomodorolm"
bugtracker = "https://github.com/vjousse/pomodorolm/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/vjousse/pomodorolm/refs/heads/main/flatpak/org.jousse.vincent.Pomodorolm.metainfo.xml"
screenshots = [ "https://raw.githubusercontent.com/vjousse/pomodorolm/main/screenshot.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = "https://github.com/vjousse/pomodorolm/blob/main/flatpak/org.jousse.vincent.Pomodorolm.png"
all_features_touch = false
intended_for_mobile = false
app_id = "org.jousse.vincent.Pomodorolm"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.jousse.vincent.Pomodorolm"
flatpak_link = "https://flathub.org/apps/org.jousse.vincent.Pomodorolm.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/vjousse/pomodorolm/refs/heads/main/flatpak/org.jousse.vincent.Pomodorolm.yml"
snapcraft = "https://snapcraft.io/pomodorolm"
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/vjousse/pomodorolm/refs/heads/main/snapcraft.yaml"
appimage_x86_64_url = "https://github.com/vjousse/pomodorolm/releases/"
appimage_aarch64_url = ""
repology = [ "pomodorolm",]
appstream_xml_url = "https://raw.githubusercontent.com/vjousse/pomodorolm/refs/heads/main/flatpak/org.jousse.vincent.Pomodorolm.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/org.jousse.vincent.pomodorolm/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-03-21"

+++

### Description

Pomodorolm is a simple, good looking and configurable time tracker fot the Pomodoro technique. Work by batch of 25 minutes and enhance your productivity.

Pomodorolm provides a tray icon updated with the remaining time. You can change its look and feel by using themes and you if none suits you, you can create your own theme. All sessions durations are configurable and you can choose to play sounds too (ticking sound, end of session and so on). You can even display notifications if you want to.

[Source](https://raw.githubusercontent.com/vjousse/pomodorolm/refs/heads/main/flatpak/org.jousse.vincent.Pomodorolm.metainfo.xml)