+++
title = "frogr"
description = "A flickr uploader for GNOME"
aliases = []
date = 2020-10-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Mario Sanchez Prada",]
categories = [ "photo management",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = [ "Flickr",]
packaged_in = [ "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Graphics",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/frogr"
homepage = "https://wiki.gnome.org/Apps/Frogr"
bugtracker = "https://gitlab.gnome.org/GNOME/frogr/-/issues"
donations = "https://www.gnome.org/donate"
translations = "https://l10n.gnome.org/module/frogr/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/screenshots/frogr-screenshot.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/icons/hicolor/scalable/apps/org.gnome.frogr.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.frogr"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.frogr"
flatpak_link = "https://flathub.org/apps/org.gnome.frogr.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/flatpak/org.gnome.frogr.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "frogr",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-11-30"
repo_created_date = "2018-05-22"

+++

### Description

frogr allows users to manage their accounts on the Flickr photo hosting
service with a simple and direct interface.

It supports all the basic Flickr features, including uploading
pictures, adding descriptions, setting tags and managing sets
and groups pools.

[Source](https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in)