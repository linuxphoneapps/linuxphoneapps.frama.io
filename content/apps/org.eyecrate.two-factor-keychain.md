+++
title = "Two-Factor-Keychain"
description = ""
aliases = []
date = 2020-03-04
updated = 2024-12-30

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = []
categories = [ "OTP generator",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = []
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://git.eyecreate.org/eyecreate/Two-Factor-Keychain"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://www.eyecreate.org/images/two-factor-demo-fs8.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "org.eyecrate.two-factor-keychain"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/oeg.eyecrate.two-factor-keychain/"
latest_repo_commit = "2018-11-12"
repo_created_date = "2018-09-21"

+++

### Description

Python app intended for mobile/desktop linux computers for managing OTP codes.
