+++
title = "QPrompt"
description = "Personal teleprompter software for all video creators"
aliases = [ "apps/com.cuperino.qprompt.desktop/",]
date = 2021-11-09
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-4.0",]
app_author = [ "Javier O. Cordero Pérez",]
categories = [ "teleprompter",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Cuperino/QPrompt-Teleprompter"
homepage = "https://qprompt.app"
bugtracker = "https://feedback.qprompt.app"
donations = "https://www.patreon.com/qpromptapp"
translations = "https://l10n.qprompt.app"
more_information = [ "https://sourceforge.net/projects/qprompt/",]
summary_source_url = "https://raw.githubusercontent.com/Cuperino/QPrompt/main/com.cuperino.qprompt.appdata.xml"
screenshots = [ "https://qprompt.app/assets/img/countdown.png", "https://qprompt.app/assets/img/slider-1.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Cuperino/QPrompt-Teleprompter/refs/heads/main/src/icons/qprompt.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.cuperino.qprompt"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.cuperino.qprompt"
flatpak_link = "https://flathub.org/apps/com.cuperino.qprompt.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "qprompt",]
appstream_xml_url = "https://raw.githubusercontent.com/Cuperino/QPrompt/main/com.cuperino.qprompt.appdata.xml"
reported_by = "Cuperino"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2020-10-24"

+++

### Description

Open source personal teleprompter software for all video creators. Built with ease of use, control accuracy, fast performance, and cross-platform support in mind. QPrompt's convergent user interface can run on Linux, Windows, macOS, and Android.

[Source](https://raw.githubusercontent.com/Cuperino/QPrompt/main/com.cuperino.qprompt.appdata.xml)

### Notice

It's workable, but does not fit the screen perfectly. I suppose currently preparing the text on a larger screen device or in a convergent setting is best. (Not sure if I set the correct compile flags for the mobile UI yet, though)