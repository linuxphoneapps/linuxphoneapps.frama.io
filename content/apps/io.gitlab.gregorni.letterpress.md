+++
title = "Letterpress"
description = "Create beautiful ASCII art"
aliases = []
date = 2023-09-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gregor Niehl",]
categories = [ "ASCII art generator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "jp2a",]
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Graphics", "ImageProcessing", "TextTools",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/Letterpress"
homepage = "https://apps.gnome.org/Letterpress/"
bugtracker = "https://gitlab.gnome.org/World/Letterpress/-/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/letterpress/"
more_information = [ "https://apps.gnome.org/Letterpress/",]
summary_source_url = "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/io.gitlab.gregorni.Letterpress.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/copied.png", "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/saved.png", "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/screenshots/width.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/icons/hicolor/scalable/apps/io.gitlab.gregorni.Letterpress.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.gitlab.gregorni.Letterpress"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.gregorni.Letterpress"
flatpak_link = "https://flathub.org/apps/io.gitlab.gregorni.Letterpress.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2023-09-05"

+++

### Description

Letterpress converts your images into a picture made up of ASCII characters.
You can save the output to a file, copy it, and even change its resolution!

[Source](https://gitlab.gnome.org/World/Letterpress/-/raw/main/data/io.gitlab.gregorni.Letterpress.metainfo.xml.in.in)