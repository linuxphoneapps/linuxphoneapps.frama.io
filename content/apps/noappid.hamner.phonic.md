+++
title = "Phonic"
description = "Phonic is a GTK audiobook player targeted at mobile Linux."
aliases = []
date = 2020-09-06
updated = 2024-10-28

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "hamner",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~hamner/Phonic"
latest_repo_commit = "2022-01-21"
homepage = "https://sr.ht/~hamner/Phonic"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~hamner/Phonic"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "phonic",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
repo_created_date = "2022-01-21"

+++
