+++
title = "PDF Arranger"
description = "Merge, shuffle, and crop PDFs"
aliases = []
date = 2020-10-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The PDF Arranger team",]
categories = [ "office",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Office",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/pdfarranger/pdfarranger"
homepage = "https://github.com/pdfarranger/pdfarranger"
bugtracker = "https://github.com/pdfarranger/pdfarranger/issues"
donations = ""
translations = "https://github.com/pdfarranger/pdfarranger#for-translators"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml"
screenshots = [ "https://github.com/pdfarranger/pdfarranger/raw/main/data/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/refs/heads/main/data/icons/hicolor/scalable/apps/com.github.jeromerobert.pdfarranger.svg"
all_features_touch = true
intended_for_mobile = false
app_id = "com.github.jeromerobert.pdfarranger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.jeromerobert.pdfarranger"
flatpak_link = "https://flathub.org/apps/com.github.jeromerobert.pdfarranger.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/com.github.jeromerobert.pdfarranger/refs/heads/master/com.github.jeromerobert.pdfarranger.yaml"
snapcraft = ""
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/refs/heads/main/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pdfarranger",]
appstream_xml_url = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2015-10-21"

+++

### Description

PDF Arranger is a small application, which helps the user to merge or split pdf
documents and rotate, crop and rearrange their pages using an interactive and
intuitive graphical interface.

It is a frontend for pikepdf.

[Source](https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml)