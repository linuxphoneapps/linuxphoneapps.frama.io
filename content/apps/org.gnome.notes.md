+++
title = "Notes"
description = "Notes for GNOME"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later", "LGPL-2.0-or-later", "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Pierre-Yves Luyten",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = [ "gnome-settings-daemon",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-notes"
homepage = "https://wiki.gnome.org/Apps/Notes"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-notes/-/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-notes/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/default.png", "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/edit.png", "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/list.png", "https://gitlab.gnome.org/GNOME/gnome-notes/raw/master/data/appdata/select.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/icons/hicolor/scalable/apps/org.gnome.Notes.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "org.gnome.Notes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Notes"
flatpak_link = "https://flathub.org/apps/org.gnome.Notes.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/build-aux/flatpak/org.gnome.Notes.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-notes",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-01"
repo_created_date = "2018-05-04"

+++

### Description

A quick and easy way to make freeform notes or jot down simple lists. Store as many notes as you like and share them by email.

You can store your notes locally on your computer or sync with online services like ownCloud.

[Source](https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in)

### Notice

No release in the past three years, despite some activities in the repo.