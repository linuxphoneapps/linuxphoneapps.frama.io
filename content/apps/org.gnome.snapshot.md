+++
title = "Camera"
description = "Take pictures and videos"
aliases = []
date = 2023-11-16
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libcamera", "pipewire",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "AudioVideo", "Recorder", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/snapshot"
homepage = "https://apps.gnome.org/Snapshot/"
bugtracker = "https://gitlab.gnome.org/GNOME/snapshot/issues"
donations = "https://www.gnome.org/donate"
translations = "https://l10n.gnome.org/module/snapshot/"
more_information = [ "https://teams.pages.gitlab.gnome.org/Websites/welcome.gnome.org/en/app/Snapshot/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/snapshot/-/raw/main/data/resources/org.gnome.Snapshot.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/snapshot/raw/main/data/screenshots/gallery.png", "https://gitlab.gnome.org/GNOME/snapshot/raw/main/data/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Snapshot"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Snapshot"
flatpak_link = "https://flathub.org/apps/org.gnome.Snapshot.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-snapshot",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/snapshot/-/raw/main/data/resources/org.gnome.Snapshot.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-02-04"

+++

### Description

Take pictures and videos on your computer, tablet, or phone.

[Source](https://gitlab.gnome.org/GNOME/snapshot/-/raw/main/data/resources/org.gnome.Snapshot.metainfo.xml.in.in)

### Notice

Among other devices, this Camera app works on the PINE64 PinePhone Pro.