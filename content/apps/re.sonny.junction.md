+++
title = "Junction"
description = "Application chooser"
aliases = []
date = 2023-06-25
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Sonny Piers",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Settings", "System", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://junction.sonny.re/source"
homepage = "https://junction.sonny.re"
bugtracker = "https://junction.sonny.re/feedback"
donations = "https://junction.sonny.re/donate"
translations = "https://junction.sonny.re/translate"
more_information = [ "https://apps.gnome.org/Junction/",]
summary_source_url = ""
screenshots = [ "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot-file.png", "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot-mobile.png", "https://raw.githubusercontent.com/sonnyp/Junction/main/data/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/re.sonny.junction/1.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "re.sonny.Junction"
scale_to_fit = ""
flathub = "https://flathub.org/apps/re.sonny.Junction"
flatpak_link = "https://flathub.org/apps/re.sonny.Junction.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "junction-application-browser",]
appstream_xml_url = "https://raw.githubusercontent.com/sonnyp/Junction/main/data/re.sonny.Junction.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-08-16"
repo_created_date = "2021-05-30"

+++


### Description

Junction pops up automatically when you open a file or link in another app.
Use the mouse or keyboard navigation to choose which app you want to open it with.

Features:

* Choose the app to open with
* Show the location before opening
* Edit the URL before opening
* Show a hint for insecure links
* Keyboard navigation
* Supports desktop actions on right click
* Middle Click or Ctrl + Click to open in multiple applications
* Linux Mobile support

[Source](https://raw.githubusercontent.com/sonnyp/Junction/main/data/re.sonny.Junction.metainfo.xml)