+++
title = "Liri Text"
description = "Edit text files"
aliases = []
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Liri developers",]
categories = [ "text editor",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "aur", "flathub",]
freedesktop_categories = [ "TextEditor", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lirios/text"
homepage = "https://liri.io/apps/text"
bugtracker = "https://github.com/lirios/text/issues/new"
donations = "https://liri.io/get-involved/"
translations = "https://www.transifex.com/lirios/liri-text/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/lirios/text/develop/data/io.liri.Text.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/lirios/text/develop/.project/screenshots/text1.png", "https://raw.githubusercontent.com/lirios/text/develop/.project/screenshots/text2.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/lirios/text/refs/heads/develop/data/icons/io.liri.Text.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.liri.Text"
scale_to_fit = "io.liri.Text"
flathub = "https://flathub.org/apps/io.liri.Text"
flatpak_link = "https://flathub.org/apps/io.liri.Text.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/lirios/text/refs/heads/develop/dist/flatpak/io.liri.Text.json"
snapcraft = ""
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/lirios/text/refs/heads/develop/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "liri-text",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/text/develop/data/io.liri.Text.appdata.xml"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-04-06"
repo_created_date = "2016-02-03"

+++


### Description

Liri Text is a cross-platform text editor made in accordance with Material Design,
aiming at simplicity and ease of use.

It is best suited for taking notes, writing text documents or
quick programming with its syntax highlighting feature.

[Source](https://raw.githubusercontent.com/lirios/text/develop/data/io.liri.Text.appdata.xml)