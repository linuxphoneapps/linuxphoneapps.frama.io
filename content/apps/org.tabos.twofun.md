+++
title = "TwoFun"
description = "Touch based reaction game for two players"
aliases = ["games/org.tabos.twofun/"]
date = 2019-02-01
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Game", "ActionGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/tabos/twofun/"
homepage = "https://tabos.org/projects/twofun/"
bugtracker = "https://gitlab.com/tabos/twofun/issues"
donations = "https://www.paypal.me/tabos/10"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.tabos.twofun"
screenshots = [ "https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun1.png", "https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun2.png", "https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun3.png", "https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun4.png", "https://gitlab.com/tabos/twofun/-/raw/main/data/screenshots/twofun5.png",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "org.tabos.twofun"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.twofun"
flatpak_link = "https://flathub.org/apps/org.tabos.twofun.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.com/tabos/twofun/-/raw/main/data/org.tabos.twofun.metainfo.xml.in"
reported_by = "cahfofpai"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-23"
repo_created_date = "2018-11-18"
feed_entry_id = "https://linuxphoneapps.org/games/org.tabos.twofun/"

+++


### Description

Multiplayer game collection for touch devices, like "Hit when white", "3 of a kind", "Color matching", and more

[Source](https://gitlab.com/tabos/twofun/-/raw/main/data/org.tabos.twofun.metainfo.xml.in)
