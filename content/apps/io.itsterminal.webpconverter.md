+++
title = "WebP Converter"
description = "The fastest way to convert images to WebP"
aliases = []
date = 2024-10-19
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michael Scuteri",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Graphics", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/MichaelScuteri/webp-converter-gnome"
homepage = "https://github.com/flathub/io.itsterminal.WebPConverter"
bugtracker = "https://github.com/flathub/io.itsterminal.WebPConverter/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.itsterminal.WebPConverter"
screenshots = [ "https://github.com/MichaelScuteri/webp-converter-gnome/blob/d5d2f7411967bf35e6d8aceb92931eb8cb676b81/screenshots/start-screen-light.png", "https://raw.githubusercontent.com/MichaelScuteri/webp-converter-gnome/d5d2f7411967bf35e6d8aceb92931eb8cb676b81/screenshots/main-view-dark.png", "https://raw.githubusercontent.com/MichaelScuteri/webp-converter-gnome/d5d2f7411967bf35e6d8aceb92931eb8cb676b81/screenshots/main-view-light.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/MichaelScuteri/webp-converter-gnome/refs/heads/main/data/icons/io.itsterminal.WebPConverter.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.itsterminal.WebPConverter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.itsterminal.WebPConverter"
flatpak_link = "https://flathub.org/apps/io.itsterminal.WebPConverter.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/MichaelScuteri/webp-converter-gnome/refs/heads/main/io.itsterminal.WebPConverter.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/MichaelScuteri/webp-converter-gnome/refs/heads/main/data/appstream/io.itsterminal.WebPConverter.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/io.itsterminal.webpconverter/"
latest_repo_commit = "2024-10-30"
repo_created_date = "2024-09-25"

+++


### Description

WebP Converter allows you to convert images to the WebP format. It is on average 93% faster than online alternatives, with no limit on amount of converts and ability to control compression quality.

With a compression setting of 60, you will notice minimal reduction in quality while shrinking file size to around 90% of its original size.

[Source](https://raw.githubusercontent.com/MichaelScuteri/webp-converter-gnome/refs/heads/main/data/appstream/io.itsterminal.WebPConverter.appdata.xml)