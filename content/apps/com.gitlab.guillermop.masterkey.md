+++
title = "Master Key"
description = "Manage passwords without saving them"
aliases = []
date = 2021-02-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Guillermo Peña",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Security", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/guillermop/master-key"
homepage = "https://gitlab.com/guillermop/master-key/"
bugtracker = "https://gitlab.com/guillermop/master-key/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.gitlab.guillermop.MasterKey"
screenshots = [ "https://gitlab.com/guillermop/master-key/-/raw/master/data/screenshots/screenshot1.png", "https://gitlab.com/guillermop/master-key/-/raw/master/data/screenshots/screenshot2.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "com.gitlab.guillermop.MasterKey"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.guillermop.MasterKey"
flatpak_link = "https://flathub.org/apps/com.gitlab.guillermop.MasterKey.flatpakref"
flatpak_recipe = "https://gitlab.com/guillermop/master-key/-/raw/master/com.gitlab.guillermop.MasterKey.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "master-key",]
appstream_xml_url = "https://gitlab.com/guillermop/master-key/-/raw/master/data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
latest_repo_commit = "2024-11-17"
repo_created_date = "2021-01-08"

+++


### Description

Master Key is a password manager application that generates and manages passwords without the
need to store them

Features:

* Passwords are generated using a combination of a master key, a site and a login
* Passwords are never stored
* Passwords can be recreated everywhere
* The accounts information is encrypted
* Follows the GNOME Human Interface Guidelines

[Source](https://gitlab.com/guillermop/master-key/-/raw/master/data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in)

### Notice

Ported to GTK4 with 1.2.0, used GTK3/libadwaita before.