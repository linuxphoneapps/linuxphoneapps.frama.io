+++
title = "Identity"
description = "Compare images and videos"
aliases = []
date = 2020-10-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ivan Molodetskikh",]
categories = [ "image and video comparison",]
mobile_compatibility = [ "4",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "2DGraphics", "AudioVideo", "Graphics", "Science", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/YaLTeR/identity"
homepage = "https://gitlab.gnome.org/YaLTeR/identity"
bugtracker = "https://gitlab.gnome.org/YaLTeR/identity/issues"
donations = ""
translations = "https://l10n.gnome.org/module/identity/"
more_information = [ "https://apps.gnome.org/Identity/",]
summary_source_url = "https://gitlab.gnome.org/YaLTeR/identity"
screenshots = [ "https://gitlab.gnome.org/YaLTeR/identity/uploads/0876907d00c9737d5bb5376a82255baa/row.png", "https://gitlab.gnome.org/YaLTeR/identity/uploads/2c6d515c5a952d14829be99a1bada08c/tab.png", "https://gitlab.gnome.org/YaLTeR/identity/uploads/7c5a7d2191b259f7c846226f7e074826/column.png", "https://gitlab.gnome.org/YaLTeR/identity/uploads/fc69820af95cb95231936b185c4e061f/media-properties.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.gitlab.YaLTeR.Identity"
scale_to_fit = "identity"
flathub = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.Identity"
flatpak_link = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.Identity.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "identity",]
appstream_xml_url = "https://gitlab.gnome.org/YaLTeR/identity/-/raw/master/data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-10"
repo_created_date = "2020-09-15"

+++

### Description

A program for comparing multiple versions of an image or video.

[Source](https://gitlab.gnome.org/YaLTeR/identity/-/raw/master/data/org.gnome.gitlab.YaLTeR.Identity.metainfo.xml.in.in)