+++
title = "Flowtime"
description = "Spend your time wisely"
aliases = []
date = 2021-12-24
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Diego Iván",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Clock", "GNOME", "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Diego-Ivan/Flowtime"
homepage = "https://github.com/Diego-Ivan/Flowtime"
bugtracker = "https://github.com/Diego-Ivan/Flowtime/issues"
donations = ""
translations = "https://github.com/Diego-Ivan/Flowtime/tree/main/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/01.png", "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/02.png", "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/03.png", "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/screenshots/04.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/refs/heads/main/data/icons/hicolor/scalable/apps/io.github.diegoivanme.flowtime.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.diegoivanme.flowtime"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.diegoivanme.flowtime"
flatpak_link = "https://flathub.org/apps/io.github.diegoivanme.flowtime.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/refs/heads/main/build-aux/flatpak/io.github.diegoivanme.flowtime.devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "flowtime",]
appstream_xml_url = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_repology"
latest_repo_commit = "2024-10-15"
repo_created_date = "2021-12-12"

+++

### Description

The Pomodoro technique is efficient for tasks you find boring, but having to take a break when you are 100% concentrated in something you like might be annoying. That's why the Flowtime technique exists: take appropriate breaks without losing your flow.

Instead of strict work and break times, you work as long as you wish and your break is a portion of how long you worked (by default, 20%). For example, if you work for an hour, your break is 12 minutes. This lets you concentrate on your work and take appropriate breaks when convenient without a rigid schedule.

[Source](https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in)