+++
title = "Pipeline"
description = "Follow video creators"
aliases = []
date = 2021-03-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "YouTube", "lbry", "PeerTube",]
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "AudioVideo", "Video",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = [ "recommends always",]
tags = []

[extra]
repository = "https://gitlab.com/schmiddi-on-mobile/pipeline"
homepage = "https://mobile.schmidhuberj.de/pipeline/"
bugtracker = "https://gitlab.com/schmiddi-on-mobile/pipeline/issues"
donations = "https://gitlab.com/schmiddi-on-mobile/pipeline#donate"
translations = "https://hosted.weblate.org/engage/schmiddi-on-mobile/"
more_information = [ "https://www.tubefeeder.de/wiki/different-player.html",]
summary_source_url = "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/de.schmidhuberj.tubefeeder.metainfo.xml"
screenshots = [ "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/screenshots/channel.png", "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/screenshots/video.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/1.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/2.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/3.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/4.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/5.png",]
svg_icon_url = "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/icons/de.schmidhuberj.tubefeeder.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "de.schmidhuberj.tubefeeder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.tubefeeder"
flatpak_link = "https://flathub.org/apps/de.schmidhuberj.tubefeeder.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pipeline", "pipeline-gtk",]
appstream_xml_url = "https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/de.schmidhuberj.tubefeeder.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-10"
repo_created_date = "2023-08-10"

+++

### Description

Pipeline lets you watch and download videos from YouTube and PeerTube, all without needing to navigate through
different websites. Its adaptive design allows you to enjoy content on any screen size.

Pipeline allows you to:

* Translators: Part of the description of the application in the metainfo.
* Search for you favorite channels and subscribe to them.
* Aggregate the videos of all subscriptions into a single feed.
* Filter out unwanted items from the feed, like short videos or videos from a series.
* Play those videos either in the built-in video player or with any other video player of your choice.
* Download videos for offline viewing.
* Manage videos you want to watch later.
* Import your subscriptions from NewPipe or YouTube.
* Supports YouTube, PeerTube with the possibility to extend to other platforms on request.

[Source](https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/de.schmidhuberj.tubefeeder.metainfo.xml.in)

### Notice

Make sure to read the [wiki](http://www.tubefeeder.de/wiki/). Ported to GTK4/libadwaita with release 1.6.0, renamed from Tubefeeder to Pipeline in July 2023 with v1.11.0.