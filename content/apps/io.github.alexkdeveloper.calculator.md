+++
title = "Calculator"
description = "A simple string calculator"
aliases = []
date = 2024-05-05
updated = 2024-10-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex Kryuchkov",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/alexkdeveloper/calculator"
homepage = "https://github.com/alexkdeveloper/calculator"
bugtracker = "https://github.com/alexkdeveloper/calculator/issues"
donations = ""
translations = "https://github.com/alexkdeveloper/calculator/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.alexkdeveloper.calculator"
screenshots = [ "https://dl.flathub.org/media/io/github/alexkdeveloper.calculator/9758705b1785ffe34912a817e49c630f/screenshots/image-1_orig.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/alexkdeveloper/calculator/main/data/icons/hicolor/scalable/apps/io.github.alexkdeveloper.calculator.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.alexkdeveloper.calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.alexkdeveloper.calculator"
flatpak_link = "https://flathub.org/apps/io.github.alexkdeveloper.calculator.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/alexkdeveloper/calculator/main/io.github.alexkdeveloper.calculator.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/alexkdeveloper/calculator/main/data/io.github.alexkdeveloper.calculator.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "script"
latest_repo_commit = "2024-05-07"
repo_created_date = "2023-10-16"

+++

### Description

A simple string calculator that can perform addition, subtraction, multiplication, division, exponentiation and root extraction, taking into account the order of actions.

[Source](https://raw.githubusercontent.com/alexkdeveloper/calculator/main/data/io.github.alexkdeveloper.calculator.metainfo.xml.in)