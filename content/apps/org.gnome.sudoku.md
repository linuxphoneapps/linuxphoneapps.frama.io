+++
title = "GNOME Sudoku"
description = "Test yourself in the classic puzzle"
aliases = ["games/org.gnome.sudoku/"]
date = 2023-02-07
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "game",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Game", "LogicGame",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-sudoku"
homepage = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/wikis/home"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-sudoku/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.metainfo.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/screenshot-dark.png", "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/icons/hicolor/scalable/org.gnome.Sudoku.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Sudoku"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Sudoku"
flatpak_link = "https://flathub.org/apps/org.gnome.Sudoku.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/build-aux/org.gnome.Sudoku.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-sudoku",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2018-05-22"
feed_entry_id = "https://linuxphoneapps.org/games/org.gnome.sudoku/"

+++

### Description

GNOME Sudoku is a must-install for puzzle lovers. Based on the popular
Japanese game, it comes with a simple interface that makes playing Sudoku
fun for players of all skill level.

Features:

* Automatically save and restore your session.
* Generate an infinite amount of new puzzles with qqwing.
* Create and solve custom puzzles from scratch.
* Adjust a scalable grid to your preferences.
* Use the mouse, keyboard or touch to complete the puzzles.
* Set up advanced strategies like X-Wings with earmarks.
* Take advantage of the highlighter to narrow down possibilities.
* Get help with the built-in warnings.
* Time yourself to keep track of your progression.
* Print puzzles to play Sudoku on the go.

[Source](https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.metainfo.xml.in.in)
