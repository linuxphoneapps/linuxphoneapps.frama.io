+++
title = "Delta Chat"
description = "Delta Chat email-based messenger"
aliases = []
date = 2022-04-11
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Delta Chat",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "Electron",]
backends = [ "libdeltachat",]
services = [ "deltachat",]
packaged_in = [ "arch", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "Chat", "InstantMessaging", "Network",]
programming_languages = [ "TypeScript",]
build_systems = [ "npm",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/deltachat/deltachat-desktop"
homepage = "https://delta.chat/"
bugtracker = "https://github.com/deltachat/deltachat-desktop/issues"
donations = "https://delta.chat/en/contribute#donate-money-or-devices"
translations = "https://www.transifex.com/delta-chat/public/"
more_information = [ "https://delta.chat/en/help",]
summary_source_url = "https://raw.githubusercontent.com/flathub/chat.delta.desktop/master/chat.delta.desktop.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/1.46_darkmode_default_bg.png", "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/desktop/linux/1.46_create_chatmail_profile.png", "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/desktop/linux/1.46_darkmode_beach_bg.png", "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/desktop/linux/1.46_light_mode_beach_bg.png", "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/desktop/linux/1.46_login_with_email.png", "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/desktop/linux/1.46_small_screen_mode_chat.png", "https://raw.githubusercontent.com/deltachat/interface/main/screenshots/2024/desktop/linux/1.46_small_screen_mode_chat_list.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/chat.delta.desktop/1.png", "https://img.linuxphoneapps.org/chat.delta.desktop/2.png", "https://img.linuxphoneapps.org/chat.delta.desktop/3.png", "https://img.linuxphoneapps.org/chat.delta.desktop/4.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "chat.delta.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/chat.delta.desktop"
flatpak_link = "https://flathub.org/apps/chat.delta.desktop.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "deltachat-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/chat.delta.desktop/master/chat.delta.desktop.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-08-20"

+++

### Description

Chat over email and head back to the future with us!

Delta Chat is like Telegram or Whatsapp but without the tracking
or central control. Check out our GDPR compliancy statement.

Delta Chat doesn’t have their own servers but uses the most
massive and diverse open messaging system ever: the existing
e-mail server network.

Chat with anyone if you know their e-mail address, no need for
them to install DeltaChat! All you need is a standard e-mail
account.

[Source](https://raw.githubusercontent.com/flathub/chat.delta.desktop/master/chat.delta.desktop.appdata.xml)

### Notice

If you have redraw issues, follow the [instructions in this comment](https://codeberg.org/lk108/deltatouch/issues/1#issuecomment-2449021).