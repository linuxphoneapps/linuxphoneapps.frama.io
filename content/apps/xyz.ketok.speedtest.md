+++
title = "Speedtest"
description = "Measure your internet connection speed"
aliases = []
date = 2024-02-02
updated = 2024-12-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Ketok", ]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "librespeed",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Network", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/Ketok4321/speedtest"
homepage = "https://github.com/Ketok4321/speedtest"
bugtracker = "https://github.com/Ketok4321/speedtest/issues"
donations = ""
translations = "https://github.com/Ketok4321/speedtest/tree/main/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/Ketok4321/speedtest/main/data/xyz.ketok.Speedtest.metainfo.xml.in"
screenshots = [ "https://raw.githubusercontent.com/Ketok4321/speedtest/main/screenshots/1.png", "https://raw.githubusercontent.com/Ketok4321/speedtest/main/screenshots/2.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Ketok4321/speedtest/main/data/icons/hicolor/scalable/apps/xyz.ketok.Speedtest.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "xyz.ketok.Speedtest"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.ketok.Speedtest"
flatpak_link = "https://flathub.org/apps/xyz.ketok.Speedtest.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/Ketok4321/speedtest/main/xyz.ketok.Speedtest.Devel.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Ketok4321/speedtest/main/data/xyz.ketok.Speedtest.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-11-27"
repo_created_date = "2023-07-19"

+++

### Description

Desktop client for librespeed using gtk4+libadwaita

[Source](https://raw.githubusercontent.com/Ketok4321/speedtest/main/data/xyz.ketok.Speedtest.metainfo.xml.in)

### Notice

Mobile friendly since 1.3.0.
