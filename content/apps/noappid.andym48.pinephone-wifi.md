+++
title = "Wifi Connect"
description = "Scan for Wifi  SSIDs, connect, disconnect, and restart services on your linux phone"
aliases = []
date = 2023-12-24
updated = 2024-10-12

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "AndyM48",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Tk",]
backends = [ "NetworkManager,nmcli",]
services = []
packaged_in = []
freedesktop_categories = [ "Utility",]
programming_languages = [ "Tcl",]
build_systems = [ "custom",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/AndyM48/pinephone_wifi"
homepage = ""
bugtracker = "https://gitlab.com/AndyM48/pinephone_wifi/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/AndyM48/pinephone_wifi"
screenshots = [ "https://gitlab.com/AndyM48/pinephone_wifi/-/raw/main/screenshots/list.png", "https://gitlab.com/AndyM48/pinephone_wifi/-/raw/main/screenshots/password.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = true
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "AndyM48"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-04-15"
repo_created_date = "2023-10-21"

+++

### Description

- Check that wifi is services are running

- List wifi connections available

- Show known wifi connections

- Show active wifi connections

- Restart wifi services on request
