+++
title = "Notify"
description = "Receive notifications from ntfy.sh."
aliases = []
date = 2024-11-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "ranfdev",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "nfty.sh",]
packaged_in = [ "flathub", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable",]
freedesktop_categories = [ "GNOME", "GTK", "Network", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/ranfdev/Notify"
homepage = "https://github.com/ranfdev/Notify"
bugtracker = "https://github.com/ranfdev/Notify/issues"
donations = "https://github.com/sponsors/ranfdev"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.ranfdev.Notify"
screenshots = [ "https://dl.flathub.org/media/com/ranfdev/Notify/715d482ec5e745785083560a55ad631f/screenshots/image-1_orig.png", "https://dl.flathub.org/media/com/ranfdev/Notify/715d482ec5e745785083560a55ad631f/screenshots/image-2_orig.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/ranfdev/Notify/refs/heads/main/data/icons/com.ranfdev.Notify.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "com.ranfdev.Notify"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.ranfdev.Notify"
flatpak_link = "https://flathub.org/apps/com.ranfdev.Notify.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/ranfdev/Notify/refs/heads/main/build-aux/com.ranfdev.Notify.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "notify-client",]
appstream_xml_url = "https://raw.githubusercontent.com/ranfdev/Notify/refs/heads/main/data/resources/com.ranfdev.Notify.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/com.ranfdev.notify/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2023-10-08"

+++

### Description

Versatile GTK4 client designed to seamlessly interface with the ntfy.sh notification service

[Source](https://raw.githubusercontent.com/ranfdev/Notify/refs/heads/main/data/resources/com.ranfdev.Notify.metainfo.xml.in.in)