+++
title = "Date of Catholic Easter"
description = "Calculate the date of Catholic Easter"
aliases = []
date = 2023-02-24
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alex Kryuchkov",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Education",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/alexkdeveloper/easter"
homepage = "http://github.com/alexkdeveloper/easter"
bugtracker = "http://github.com/alexkdeveloper/easter/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/alexkdeveloper/easter/main/data/com.github.alexkdeveloper.easter.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/alexkdeveloper/easter/main/data/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.alexkdeveloper.easter/1.png", "https://img.linuxphoneapps.org/com.github.alexkdeveloper.easter/2.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.alexkdeveloper.easter"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.alexkdeveloper.easter"
flatpak_link = "https://flathub.org/apps/com.github.alexkdeveloper.easter.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/alexkdeveloper/easter/main/com.github.alexkdeveloper.easter.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/alexkdeveloper/easter/main/data/com.github.alexkdeveloper.easter.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2023-09-22"
repo_created_date = "2022-04-11"

+++

### Description
Program for calculating the date of Catholic Easter

[Source](https://raw.githubusercontent.com/alexkdeveloper/easter/main/data/com.github.alexkdeveloper.easter.appdata.xml.in)