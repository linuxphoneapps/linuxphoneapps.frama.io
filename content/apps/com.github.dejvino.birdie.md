+++
title = "Birdie"
description = "Wakeup Alarm App for a Linux Phone"
aliases = []
date = 2021-04-24
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "dejvino",]
categories = [ "alarm clock",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Utility", "Clock",]
programming_languages = [ "Python", "C",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/Dejvino/birdie"
homepage = ""
bugtracker = "https://github.com/Dejvino/birdie/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Dejvino/birdie"
screenshots = [ "https://github.com/Dejvino/birdie/blob/master/screenshots/default.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/Dejvino/birdie/refs/heads/master/com.github.dejvino.birdie.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.github.dejvino.birdie"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "birdie",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2023-09-07"
repo_created_date = "2021-04-17"

+++

### Description

Alarm app designed for Linux phones.

#### Features
- system wakes up from power saving mode (suspend) to play the alarm
- single alarm schedule
- alarm is repeated for selected days of the week
- snooze button
- pleasant wake up sound (included)
- gradual volume increase of the alarm
- alarm test mode
- alarm accessible from a lockscreen (via MPRIS)
- landscape and portrait mode layouts

[Source](https://github.com/Dejvino/birdie)

### Notice
An improvement on [wake-mobile](https://linuxphoneapps.org/apps/org.gnome.gitlab.kailueke.wakemobile/).