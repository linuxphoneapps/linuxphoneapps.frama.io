+++
title = "Rocket"
description = "GTK Gemini browser"
aliases = []
date = 2021-05-16
updated = 2024-10-28

[taxonomies]
project_licenses = [ "EUPL-1.2",]
metadata_licenses = []
app_author = [ "alva",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Network",]
programming_languages = [ "Zig",]
build_systems = [ "zig",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~alva/rocket"
latest_repo_commit = "2024-02-05"
homepage = "https://sr.ht/~alva/rocket/"
bugtracker = "https://todo.sr.ht/~alva/rocket"
donations = ""
translations = ""
more_information = [ "https://todo.sr.ht/~alva/rocket?search=status%3Aopen%20label%3A%22feature%22",]
summary_source_url = "https://git.sr.ht/~alva/rocket"
screenshots = [ "https://git.sr.ht/~alva/rocket/blob/trunk/share/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~alva/rocket/blob/trunk/share/is.u8.rocket.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "is.u8.rocket"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
repo_created_date = "2020-12-03"

+++

### Description

Rocket is a Gemini browser. Somewhat usable, albeit with missing or incomplete features. [Source](https://git.sr.ht/~alva/rocket)

### Notice
Evaluated while it was still GTK3/libadwaita.
