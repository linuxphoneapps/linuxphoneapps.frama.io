+++
title = "Echo"
description = "Ping websites"
aliases = []
date = 2024-08-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lo",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/lo2dev/Echo"
homepage = "https://lo2dev.github.io/portal/echo"
bugtracker = "https://github.com/lo2dev/Echo/issues"
donations = "https://lo2dev.github.io/support"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/io.github.lo2dev.Echo"
screenshots = [ "https://raw.githubusercontent.com/lo2dev/Echo/master/data/screenshots/1.png", "https://raw.githubusercontent.com/lo2dev/Echo/master/data/screenshots/2.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/lo2dev/Echo/master/data/icons/hicolor/scalable/apps/io.github.lo2dev.Echo.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.lo2dev.Echo"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lo2dev.Echo"
flatpak_link = "https://flathub.org/apps/io.github.lo2dev.Echo.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/lo2dev/Echo/master/io.github.lo2dev.Echo.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "echo",]
appstream_xml_url = "https://raw.githubusercontent.com/lo2dev/Echo/master/data/io.github.lo2dev.Echo.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.lo2dev.echo/"
latest_repo_commit = "2024-12-19"
repo_created_date = "2024-05-12"

+++

### Description

Utility to ping websites.

Features include:

* Advanced ping parameters like ping count, timeout, etc

[Source](https://raw.githubusercontent.com/lo2dev/Echo/master/data/io.github.lo2dev.Echo.metainfo.xml.in)