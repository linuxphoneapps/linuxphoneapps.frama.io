+++
title = "Mousai"
description = "Identify songs in seconds"
aliases = []
date = 2021-03-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Dave Patrick Caberto",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "audd.io",]
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://github.com/SeaDve/Mousai"
homepage = "https://github.com/SeaDve/Mousai"
bugtracker = "https://github.com/SeaDve/Mousai/issues"
donations = "https://seadve.github.io/donate/"
translations = "https://hosted.weblate.org/projects/seadve/mousai"
more_information = [ "https://apps.gnome.org/Mousai/",]
summary_source_url = "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/io.github.seadve.Mousai.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/resources/screenshots/screenshot1.png", "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/resources/screenshots/screenshot2.png", "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/resources/screenshots/screenshot3.png", "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/resources/screenshots/screenshot4.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/SeaDve/Mousai/refs/heads/main/data/icons/io.github.seadve.Mousai.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "io.github.seadve.Mousai"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.seadve.Mousai"
flatpak_link = "https://flathub.org/apps/io.github.seadve.Mousai.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/SeaDve/Mousai/refs/heads/main/build-aux/io.github.seadve.Mousai.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mousai",]
appstream_xml_url = "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/io.github.seadve.Mousai.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-02"
repo_created_date = "2021-03-29"

+++

### Description

Discover songs you are aching to know with an easy-to-use interface.

Mousai is a simple application that can recognize songs similar to Shazam. Just click the listen button, and then wait a few seconds. It will magically return the title and artist of that song!

Note: This uses the API of audd.io, so it is necessary to log in to their site to get more trials.

Why you will love Mousai?

* 🎵 Identify songs within seconds or save for later when offline
* 🎙️ Recognize from desktop audio or your microphone
* 🎸 Build a repertoire of recognized songs
* 🎼 Quickly preview the song within the interface
* 🌐 Browse and listen the song from different providers
* 📱 Easy-to-use user interface

[Source](https://raw.githubusercontent.com/SeaDve/Mousai/main/data/io.github.seadve.Mousai.metainfo.xml.in.in)