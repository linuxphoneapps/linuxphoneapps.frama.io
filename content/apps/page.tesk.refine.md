+++
title = "Refine"
description = "Tweak various aspects of GNOME"
aliases = []
date = 2025-01-06
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Hari Rana (TheEvilSkeleton)",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = []

[extra]
repository = "https://gitlab.gnome.org/TheEvilSkeleton/Refine"
homepage = "https://tesk.page/refine"
bugtracker = "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/issues"
donations = "https://dir.floss.fund/view/funding/@tesk.page"
translations = "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/tree/main/po"
more_information = []
summary_source_url = "https://flathub.org/apps/page.tesk.Refine"
screenshots = [ "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/raw/main/data/screenshots/1.png", "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/raw/main/data/screenshots/2.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/raw/main/data/icons/hicolor/scalable/apps/page.tesk.Refine.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "page.tesk.Refine"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.tesk.Refine"
flatpak_link = "https://flathub.org/apps/page.tesk.Refine.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/raw/main/build-aux/page.tesk.Refine.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "refine",]
appstream_xml_url = "https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/raw/main/data/page.tesk.Refine.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/page.tesk.refine/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2024-12-14"

+++

### Description

Refine helps discover advanced and experimental features in GNOME.

[Source](https://gitlab.gnome.org/TheEvilSkeleton/Refine/-/raw/main/data/page.tesk.Refine.metainfo.xml.in.in)