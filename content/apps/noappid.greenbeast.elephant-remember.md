+++
title = "Elephant Remember"
description = "Reminder app built for Phosh that syncs with Gnome-Calendar in the backend but is faster and more mobile friendly"
aliases = []
date = 2021-09-18
updated = 2024-10-12

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "greenbeast",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3",]
backends = [ "icalendar",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.com/greenbeast/elephant_remember"
homepage = ""
bugtracker = "https://gitlab.com/greenbeast/elephant_remember/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/elephant_remember"
screenshots = [ "https://gitlab.com/greenbeast/elephant_remember/-/tree/master/Screenshots",]
screenshots_img = []
svg_icon_url = "https://gitlab.com/greenbeast/elephant_remember/-/raw/master/Icons/grey_elephant.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "greenbeast"
updated_by = "check_via_git_api"
latest_repo_commit = "2021-10-11"
repo_created_date = "2021-09-03"

+++

### Description

Basic reminder app built with Phosh (specifically Mobian) in mind. I got tired of Gnome Calendar being not very user friendly and pretty slow so what this
  does is take the reminder you make and appends to the calendar.ics file that your Gnome Calendar reads (found in /home/mobian/.local/share/evolution/calendar/system/calendar.ics)
  and sets a reminder for it. This then has the Evolution Alarm Notify backend that Gnome Calendar uses read this as an event and that will take care of actually notifying
  you. It also has a “Week in Review” tab that reads from your calendar file and shows you upcoming events (though sadly there are issues with recurring events that I would love some help with).

  [Source](https://gitlab.com/greenbeast/elephant_remember)

### Notice

Last commit in October 2021.