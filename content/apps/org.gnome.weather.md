+++
title = "Weather"
description = "Show weather conditions and forecast"
aliases = []
date = 2021-05-30
updated = 2025-01-13

[taxonomies]
project_licenses = [ "CC-BY-3.0", "CC-BY-SA-3.0", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "api.met.no",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Core", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-weather"
homepage = "https://wiki.gnome.org/Apps/Weather"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-weather/issues"
donations = "http://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/Weather/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-weather/-/raw/master/data/org.gnome.Weather.appdata.xml.in.in"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-weather/raw/master/misc/screenshots/city-view.png", "https://gitlab.gnome.org/GNOME/gnome-weather/raw/master/misc/screenshots/world-popover.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Weather"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Weather"
flatpak_link = "https://flathub.org/apps/org.gnome.Weather.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-weather",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-weather/-/raw/master/data/org.gnome.Weather.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2018-05-23"

+++

### Description

A small application that allows you to monitor the current weather
conditions for your city, or anywhere in the world.

It provides access to detailed forecasts, up to 7 days, with hourly
details for the current and next day, using various internet services.

It also optionally integrates with the GNOME Shell, allowing you to see
the current conditions of the most recently searched cities by just
typing the name in the Activities Overview.

[Source](https://gitlab.gnome.org/GNOME/gnome-weather/-/raw/master/data/org.gnome.Weather.appdata.xml.in.in)

### Notice

Mobile compliant since version 40. Was GTK3/libhandy before 42.