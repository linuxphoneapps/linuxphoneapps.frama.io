+++
title = "alrisha"
description = "an experimental Qt/QML destkop browser for Gemini"
aliases = []
date = 2021-05-13
updated = 2024-10-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "fabrixxm",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet", "manual status maintenance",]

[extra]
repository = "https://git.sr.ht/~fabrixxm/alrisha"
latest_repo_commit = "2020-06-14"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://git.sr.ht/~fabrixxm/alrisha/tree/master/item/alrisha-desktop/README.md",]
summary_source_url = "https://git.sr.ht/~fabrixxm/alrisha/"
screenshots = [ "https://cdn.fosstodon.org/media_attachments/files/106/228/564/835/571/888/original/6379aab1087cf83b.png",]
screenshots_img = []
svg_icon_url = "https://git.sr.ht/~fabrixxm/alrisha/blob/master/harbour-alrisha/icons/scalable/harbour-alrisha.svg"
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_git"
repo_created_date = "2020-05-03"

+++

### Notice

No icon or launcher, a bit buggy, but it works!
