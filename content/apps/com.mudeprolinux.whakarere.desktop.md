+++
title = "Whakarere"
description = "GTK4 Whatsapp Client"
aliases = [ "apps/com.mudeprolinux.whakarere/",]
date = 2024-02-09
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "WhatsApp Web",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/tobagin/whakarere"
homepage = "http://www.mudeprolinux.com/"
bugtracker = "https://github.com/tobagin/whakarere/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.mudeprolinux.whakarere"
screenshots = [ "https://github.com/tobagin/whakarere/blob/main/screenshots/screenshot1.png?raw=true", "https://github.com/tobagin/whakarere/blob/main/screenshots/screenshot2.png?raw=true",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/tobagin/whakarere/main/data/icons/hicolor/scalable/apps/com.mudeprolinux.whakarere.svg"
all_features_touch = false
intended_for_mobile = false
app_id = "com.mudeprolinux.whakarere.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.mudeprolinux.whakarere"
flatpak_link = "https://flathub.org/apps/com.mudeprolinux.whakarere.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/tobagin/whakarere/main/com.mudeprolinux.whakarere.yml"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/tobagin/whakarere/main/data/com.mudeprolinux.whakarere.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/com.mudeprolinux.whakarere/"
latest_repo_commit = "2024-02-24"
repo_created_date = "2023-10-29"

+++


### Description

No description

[Source](https://raw.githubusercontent.com/tobagin/whakarere/main/data/com.mudeprolinux.whakarere.metainfo.xml.in)

### Notice

The webview fits, WhatsApp Web is not hacked to become reponsive, but can be used.