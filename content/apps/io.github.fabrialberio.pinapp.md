+++
title = "PinApp"
description = "Create and edit app shortcuts"
aliases = []
date = 2024-11-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Fabrizio Alberio",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/fabrialberio/PinApp"
homepage = "https://github.com/fabrialberio/PinApp"
bugtracker = "https://github.com/fabrialberio/PinApp/issues"
donations = ""
translations = "https://github.com/fabrialberio/PinApp/tree/master/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/fabrialberio/PinApp/refs/heads/master/data/io.github.fabrialberio.pinapp.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/fabrialberio/PinApp/master/data/appstream/1.png", "https://raw.githubusercontent.com/fabrialberio/PinApp/master/data/appstream/2.png", "https://raw.githubusercontent.com/fabrialberio/PinApp/master/data/appstream/3.png", "https://raw.githubusercontent.com/fabrialberio/PinApp/master/data/appstream/4.png", "https://raw.githubusercontent.com/fabrialberio/PinApp/master/data/appstream/5.png",]
screenshots_img = []
svg_icon_url = "https://github.com/fabrialberio/PinApp/blob/master/data/icons/hicolor/scalable/apps/io.github.fabrialberio.pinapp.svg"
non_svg_icon_url = ""
all_features_touch = true
intended_for_mobile = false
app_id = "io.github.fabrialberio.pinapp"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.fabrialberio.pinapp"
flatpak_link = "https://flathub.org/apps/io.github.fabrialberio.pinapp.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/fabrialberio/PinApp/refs/heads/master/io.github.fabrialberio.pinapp.dev.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "pinapp",]
appstream_xml_url = "https://raw.githubusercontent.com/fabrialberio/Pins/refs/heads/master/data/io.github.fabrialberio.pinapp.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.fabrialberio.pinapp/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2022-05-21"

+++

### Description

PinApp is a simple application that lets you customize your app list.

Some of the possible uses are

* Changing an app icon that doesn't fit in with your theme
* Creating custom shortcuts to websites
* Hiding apps you don't want to see
* Editing properties in .desktop files

The app is made with Libadwaita and GTK4, with consideration for small-screen devices

[Source](https://raw.githubusercontent.com/fabrialberio/PinApp/refs/heads/master/data/io.github.fabrialberio.pinapp.appdata.xml.in)
