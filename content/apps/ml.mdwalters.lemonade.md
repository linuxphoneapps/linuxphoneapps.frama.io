+++
title = "Lemonade"
description = "Follow discussions on Lemmy"
aliases = []
date = 2023-08-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "M.D. Walters",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "lemmy",]
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://codeberg.org/theycallhermax/lemonade"
homepage = "https://github.com/mdwalters/lemonade"
bugtracker = "https://github.com/mdwalters/lemonade/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/theycallhermax/lemonade/raw/branch/main/data/ml.mdwalters.Lemonade.appdata.xml.in"
screenshots = [ "https://i.ibb.co/0jf8RR4/image.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "ml.mdwalters.Lemonade"
scale_to_fit = ""
flathub = "https://flathub.org/apps/ml.mdwalters.Lemonade"
flatpak_link = "https://flathub.org/apps/ml.mdwalters.Lemonade.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://codeberg.org/theycallhermax/lemonade/raw/branch/main/data/ml.mdwalters.Lemonade.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-03"
repo_created_date = "2023-10-14"

+++

### Description

Lemonade is a sleek and modern Lemmy client that is designed to provide a seamless browsing experience for Lemmy users. Built with the latest technology, Lemonade utilizes the power of libadwaita and GTK 4 to deliver a beautiful and intuitive user interface that is both easy to use and visually appealing.

With Lemonade, you can easily browse your favorite communities, discover new content, and engage with other users. The client is optimized for speed and performance, ensuring that you can quickly and efficiently navigate through Lemmy without any lag or delays.

[Source](https://raw.githubusercontent.com/theycallhermax/lemonade/9ecac7cec3e423462049d0e916a0b25590b1a8c1/data/ml.mdwalters.Lemonade.appdata.xml.in)

### Notice

This app was early WIP.