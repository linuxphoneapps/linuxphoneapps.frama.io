+++
title = "Amazfish"
description = "Smart watch sync app"
aliases = [ "apps/noappid.piggz.harbour-amazfish/",]
date = 2021-05-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Adam Pigg",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Sports", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/piggz/harbour-amazfish"
homepage = "https://github.com/piggz/harbour-amazfish"
bugtracker = "https://github.com/piggz/harbour-amazfish/issues"
donations = "https://paypal.me/piggz"
translations = "https://hosted.weblate.org/projects/harbour-amazfish/"
more_information = []
summary_source_url = "https://flathub.org/apps/uk.co.piggz.amazfish"
screenshots = [ "https://github.com/piggz/harbour-amazfish/raw/master/screenshots/plasma/first-page.png", "https://github.com/piggz/harbour-amazfish/raw/master/screenshots/plasma/pairing-page.png", "https://github.com/piggz/harbour-amazfish/raw/master/screenshots/plasma/settings-application.png", "https://github.com/piggz/harbour-amazfish/raw/master/screenshots/plasma/settings-profile.png", "https://github.com/piggz/harbour-amazfish/raw/master/screenshots/plasma/sports-running.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/piggz/harbour-amazfish/refs/heads/master/harbour-amazfish-ui.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "uk.co.piggz.amazfish"
scale_to_fit = ""
flathub = "https://flathub.org/apps/uk.co.piggz.amazfish"
flatpak_link = "https://flathub.org/apps/uk.co.piggz.amazfish.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/uk.co.piggz.amazfish/refs/heads/master/uk.co.piggz.amazfish.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "amazfish",]
appstream_xml_url = "https://raw.githubusercontent.com/piggz/harbour-amazfish/refs/heads/master/ui/harbour-amazfish-ui.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/noappid.piggz.harbour-amazfish/"
latest_repo_commit = "2025-01-10"
repo_created_date = "2018-04-08"

+++

### Description

Companion application for Huami Devices (such as Amazfit Bip, Cor, MiBand2/3 and GTS and GTS) and the Pinetime Infinitime.

[Source](https://raw.githubusercontent.com/piggz/harbour-amazfish/refs/heads/master/ui/harbour-amazfish-ui.appdata.xml)

### Notice

Companion app for smartwatches, including PineTime and Bangle.js. Originally for SailfishOS, has been ported to Kirigami.