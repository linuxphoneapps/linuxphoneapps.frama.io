+++
title = "Screenshot"
description = "Take screenshots"
aliases = []
date = 2024-08-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Cédric Bellegarde",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/gnumdk/screenshot"
homepage = "https://gitlab.gnome.org/gnumdk/screenshot"
bugtracker = "https://gitlab.gnome.org/gnumdk/screenshot/-/issues"
donations = "https://www.patreon.com/gnumdk"
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.adishatz.Screenshot"
screenshots = [ "https://gitlab.gnome.org/gnumdk/screenshot/-/raw/main/screenshots/screenshot.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/gnumdk/screenshot/-/raw/main/data/icons/hicolor/scalable/apps/org.adishatz.Screenshot.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.adishatz.Screenshot"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.adishatz.Screenshot"
flatpak_link = "https://flathub.org/apps/org.adishatz.Screenshot.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/gnumdk/screenshot/-/raw/main/org.adishatz.Screenshot.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/gnumdk/screenshot/-/raw/main/data/org.adishatz.Screenshot.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_appstream"
feed_entry_id = "https://linuxphoneapps.org/apps/org.adishatz.screenshot/"
latest_repo_commit = "2024-09-24"
repo_created_date = "2024-08-23"

+++


### Description

Simple application to take screenshots.

[Source](https://gitlab.gnome.org/gnumdk/screenshot/-/raw/main/data/org.adishatz.Screenshot.metainfo.xml.in)