+++
title = "Characters"
description = "Character map application"
aliases = []
date = 2021-03-31
updated = 2025-01-13

[taxonomies]
project_licenses = [ "BSD-3-Clause", "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "C", "JavaScript",]
build_systems = [ "meson",]
requires_internet = []
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-characters"
homepage = "https://apps.gnome.org/Characters/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-characters/issues"
donations = "https://www.gnome.org/donate"
translations = "https://l10n.gnome.org/module/gnome-characters"
more_information = [ "https://apps.gnome.org/Characters/",]
summary_source_url = "https://wiki.gnome.org/Apps/Characters"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-characters/raw/main/data/screenshots/screenshot1.png", "https://gitlab.gnome.org/GNOME/gnome-characters/raw/main/data/screenshots/screenshot2.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/data/icons/hicolor/scalable/apps/org.gnome.Characters.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.Characters"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Characters"
flatpak_link = "https://flathub.org/apps/org.gnome.Characters.flatpakref"
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/gnome-characters"
snap_link = ""
snap_recipe = "https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/build-aux/snap/snapcraft.yaml"
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-characters",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/data/org.gnome.Characters.appdata.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-11"
repo_created_date = "2018-01-24"

+++

### Description

Characters is a simple utility application to find and insert unusual characters. It allows you to quickly find the character you are looking for by searching for keywords.

You can also browse characters by categories, such as Punctuation, Pictures, etc.

[Source](https://gitlab.gnome.org/GNOME/gnome-characters/-/raw/main/data/org.gnome.Characters.appdata.xml.in.in)