+++
title = "Wince"
description = "Search for restaurants and businesses"
aliases = []
date = 2022-12-25
updated = 2024-10-12

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "wildeyedskies",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Yelp",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Crystal",]
build_systems = [ "make",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://gitlab.gnome.org/wildeyedskies/wince"
homepage = ""
bugtracker = "https://gitlab.gnome.org/wildeyedskies/wince/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/wildeyedskies/wince"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "space.quietfeathers.Wince"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "wildeyedskies"
updated_by = "check_via_git_api"
latest_repo_commit = "2023-03-14"
repo_created_date = "2022-12-25"

+++

### Description