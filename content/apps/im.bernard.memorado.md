+++
title = "Memorado"
description = "Memorize anything"
aliases = []
date = 2024-03-29
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Memorado Developers",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/wbernard/Memorado"
homepage = "https://bernard.im/memorado"
bugtracker = "https://github.com/wbernard/Memorado/issues/new"
donations = ""
translations = "https://github.com/wbernard/Memorado/tree/main/po"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/wbernard/Memorado/main/data/im.bernard.Memorado.metainfo.xml.in.in"
screenshots = [ "https://raw.githubusercontent.com/wbernard/Memorado/main/data/screenshots/decks.png", "https://raw.githubusercontent.com/wbernard/Memorado/main/data/screenshots/edit-deck.png", "https://raw.githubusercontent.com/wbernard/Memorado/main/data/screenshots/practice.png",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/wbernard/Memorado/main/data/icons/hicolor/scalable/apps/im.bernard.Memorado.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "im.bernard.Memorado"
scale_to_fit = ""
flathub = "https://flathub.org/apps/im.bernard.Memorado"
flatpak_link = "https://flathub.org/apps/im.bernard.Memorado.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/wbernard/Memorado/main/im.bernard.Memorado.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/wbernard/Memorado/main/data/im.bernard.Memorado.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2023-08-27"

+++

### Description

Learn using spaced repetition. Create cards with a question and answer, and practice with them.

Great for learning new languages, memorizing terms and definitions, or anything else you want to get better at.
Create or import decks, practice them, or export to share them with friends.

[Source](https://raw.githubusercontent.com/wbernard/Memorado/main/data/im.bernard.Memorado.metainfo.xml.in.in)