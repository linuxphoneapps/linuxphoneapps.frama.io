+++
title = "DewDuct"
description = "A YouTube player"
aliases = []
date = 2024-05-04
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "DaKnig",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Invidious",]
services = [ "YouTube",]
packaged_in = [ "alpine_edge",]
freedesktop_categories = [ "AudioVideo", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = [ "supports offline-only",]
tags = []

[extra]
repository = "https://github.com/DaKnig/DewDuct"
homepage = "https://github.com/DaKnig/DewDuct"
bugtracker = "https://github.com/DaKnig/DewDuct/issues"
donations = ""
translations = ""
more_information = [ "https://github.com/DaKnig/DewDuct",]
summary_source_url = "https://raw.githubusercontent.com/DaKnig/DewDuct/master/data/null.daknig.dewduct.metainfo.xml"
screenshots = [ "https://private-user-images.githubusercontent.com/37626476/325923951-a48193cf-ebe0-44ef-ae89-8163a668b595.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTQyMjA1MTYsIm5iZiI6MTcxNDIyMDIxNiwicGF0aCI6Ii8zNzYyNjQ3Ni8zMjU5MjM5NTEtYTQ4MTkzY2YtZWJlMC00NGVmLWFlODktODE2M2E2NjhiNTk1LnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA0MjclMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNDI3VDEyMTY1NlomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWIyNWE0OGFlNzA5YWQxMzJiZTM3MThkYTQ0YTZmOTk3ODljODE2NTE4OWM0MmUyNWRkMGU4YTcyYjk2Y2JiYWMmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.uAgguPmv4gMHiWwjcu7bT0EwwpD62TiPKICZU2G6nD8", "https://private-user-images.githubusercontent.com/37626476/325927805-aced4e7b-5f76-4035-bdc5-54c6754fd794.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTQyMjA1MTYsIm5iZiI6MTcxNDIyMDIxNiwicGF0aCI6Ii8zNzYyNjQ3Ni8zMjU5Mjc4MDUtYWNlZDRlN2ItNWY3Ni00MDM1LWJkYzUtNTRjNjc1NGZkNzk0LnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA0MjclMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNDI3VDEyMTY1NlomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPTYzZmQ4N2E3MDU5ZWU3YTg2NTRiZDhhNzhjMGFjZWM0Y2YwYzkyYTkzYTViMjMzMzY1YjA1MWQ2ZDZlOTU3ZWUmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.SCXVdqZ-5LxrArNeSKUTgxOE1AF9iTeQKXbjcNwQ8Ss", "https://private-user-images.githubusercontent.com/37626476/325948567-4ea8957e-99d4-4ebc-aaf6-8893784d6df8.png?jwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJnaXRodWIuY29tIiwiYXVkIjoicmF3LmdpdGh1YnVzZXJjb250ZW50LmNvbSIsImtleSI6ImtleTUiLCJleHAiOjE3MTQyMjA1MTYsIm5iZiI6MTcxNDIyMDIxNiwicGF0aCI6Ii8zNzYyNjQ3Ni8zMjU5NDg1NjctNGVhODk1N2UtOTlkNC00ZWJjLWFhZjYtODg5Mzc4NGQ2ZGY4LnBuZz9YLUFtei1BbGdvcml0aG09QVdTNC1ITUFDLVNIQTI1NiZYLUFtei1DcmVkZW50aWFsPUFLSUFWQ09EWUxTQTUzUFFLNFpBJTJGMjAyNDA0MjclMkZ1cy1lYXN0LTElMkZzMyUyRmF3czRfcmVxdWVzdCZYLUFtei1EYXRlPTIwMjQwNDI3VDEyMTY1NlomWC1BbXotRXhwaXJlcz0zMDAmWC1BbXotU2lnbmF0dXJlPWIxYjM0MmVjMDYyYWYzNTMyMjM0OTg2OGNiOGY1ODQzZjdmMmFhN2Y2Nzg2MWQ3OWIxZDlkMmIyYmFjY2EzNDEmWC1BbXotU2lnbmVkSGVhZGVycz1ob3N0JmFjdG9yX2lkPTAma2V5X2lkPTAmcmVwb19pZD0wIn0.77CYusPqt19C2RgQlx6xq3L6Eu-80YzCl-Rrcgdml0k",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "null.daknig.dewduct"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "dewduct",]
appstream_xml_url = "https://raw.githubusercontent.com/DaKnig/DewDuct/master/data/null.daknig.dewduct.metainfo.xml"
reported_by = "DaKnig"
updated_by = "check_via_github_api"
latest_repo_commit = "2024-08-01"
repo_created_date = "2023-06-22"

+++

### Description

YouTube client for Linux mobile and desktop devices. Based on GTK, using Invidious as a backend.

[Source](https://raw.githubusercontent.com/DaKnig/DewDuct/master/data/null.daknig.dewduct.metainfo.xml)