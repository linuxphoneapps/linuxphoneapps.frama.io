+++
title = "tvtoday"
description = "A application to inspect the television program of today. Made for the pinephone."
aliases = []
date = 2021-07-29
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "schmiddiii",]
categories = [ "tv guide",]
mobile_compatibility = [ "5",]
status = [ "pre-release", "inactive",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "TV Spielfilm",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/Schmiddiii/tvtoday"
homepage = ""
bugtracker = "https://github.com/Schmiddiii/tvtoday/issues/"
donations = ""
translations = ""
more_information = [ "https://github.com/Schmiddiii/tvtoday/wiki",]
summary_source_url = "https://github.com/Schmiddiii/tvtoday"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1420807553357864968",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2021-04-07"
repo_created_date = "2021-04-03"

+++

### Description

This application is still in development, but should work. This application currently only supports TV SPIELFILM, but can be extended to work on any website serving similar content. [Source](https://github.com/Schmiddiii/tvtoday)

### Notice
WIP, works, limited to German TV currently, but adding other providers of tv information is documented at https://github.com/Schmiddiii/tvtoday/wiki/Creating-Providers