+++
title = "Warp"
description = "Fast and secure file transfer"
aliases = []
date = 2022-05-21
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Fina Wilke",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Magic Wormhole",]
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_13", "debian_unstable", "devuan_unstable", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust", "Python",]
build_systems = [ "meson", "cargo",]
requires_internet = []
tags = [ "GNOME Circle",]

[extra]
repository = "https://gitlab.gnome.org/World/warp/"
homepage = "https://apps.gnome.org/Warp/"
bugtracker = "https://gitlab.gnome.org/World/warp/issues"
donations = ""
translations = "https://l10n.gnome.org/module/warp/"
more_information = [ "https://apps.gnome.org/Warp/",]
summary_source_url = "https://apps.gnome.org/app/app.drey.Warp/"
screenshots = [ "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot1-dark.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot1-light.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot2-dark.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot2-light.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot3-dark.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot3-light.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot4-dark.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot4-light.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot5-dark.png", "https://gitlab.gnome.org/World/warp/raw/main/data/screenshots/screenshot5-light.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.Warp"
scale_to_fit = "app.drey.Warp"
flathub = "https://flathub.org/apps/app.drey.Warp"
flatpak_link = "https://flathub.org/apps/app.drey.Warp.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "warp-share-files",]
appstream_xml_url = "https://gitlab.gnome.org/World/warp/-/raw/main/data/app.drey.Warp.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2022-03-19"

+++

### Description

Warp allows you to securely send files to each other via the internet or local network by
exchanging a word-based code.

The best transfer method will be determined using the “Magic Wormhole” protocol which includes
local network transfer if possible.

Features

* Send files between multiple devices
* Every file transfer is encrypted
* Directly transfer files on the local network if possible
* An internet connection is required
* QR Code support
* Compatibility with the Magic Wormhole command line client and all other compatible apps

[Source](https://gitlab.gnome.org/World/warp/-/raw/main/data/app.drey.Warp.metainfo.xml.in.in)

### Notice

Fully adaptive since 0.2.0.