+++
title = "Fielding"
description = "Test your REST APIs"
aliases = []
date = 2024-01-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Felipe Kinoshita", "KDE Community",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = [ "pre-release",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = [ "fedora_40", "fedora_41", "fedora_rawhide", "gnuguix",]
freedesktop_categories = [ "KDE", "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]
requires_internet = []
tags = [ "apps.kde.org",]

[extra]
repository = "https://invent.kde.org/utilities/fielding"
homepage = "https://apps.kde.org/fielding/"
bugtracker = "https://bugs.kde.org/enter_bug.cgi?product=fielding"
donations = ""
translations = ""
more_information = [ "https://apps.kde.org/fielding/",]
summary_source_url = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.metainfo.xml"
screenshots = [ "https://cdn.kde.org/screenshots/fielding/fielding.png",]
screenshots_img = []
svg_icon_url = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "org.kde.fielding"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "fielding",]
appstream_xml_url = "https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.metainfo.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-06"
repo_created_date = "2022-04-28"

+++

### Description

Fielding makes it easy to test your REST API, it allows you to make requests for the most used HTTP methods.

[Source](https://invent.kde.org/utilities/fielding/-/raw/master/org.kde.fielding.metainfo.xml)

### Notice

Pre-release, not packaged for any mobile distribution yet. Needs to be evaluated.