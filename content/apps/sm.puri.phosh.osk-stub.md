+++
title = "Phosh OSK"
description = "An alternative On Screen Keyboard for Phosh"
aliases = []
date = 2024-08-15
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = []
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "aur", "debian_13", "debian_unstable", "devuan_unstable", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/guidog/phosh-osk-stub"
homepage = "https://gitlab.gnome.org/guidog/phosh-osk-stub"
bugtracker = "https://gitlab.gnome.org/guidog/phosh-osk-stub"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/guidog/phosh-osk-stub/-/raw/main/data/mobi.phosh.OskStub.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org//guidog/phosh-osk-stub/-/raw/main/screenshots/pos-emoji.png?inline=false", "https://gitlab.gnome.org//guidog/phosh-osk-stub/-/raw/main/screenshots/pos-popover.png?inline=false",]
screenshots_img = []
svg_icon_url = ""
all_features_touch = true
intended_for_mobile = true
app_id = "sm.puri.phosh.osk-stub"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "phosh-osk-stub",]
appstream_xml_url = "https://gitlab.gnome.org/guidog/phosh-osk-stub/-/raw/main/data/mobi.phosh.OskStub.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/sm.puri.phosh.oskstub/"
latest_repo_commit = "2025-01-08"
repo_created_date = "2022-02-07"

+++

### Description

The on screen keyboard has the following features:

* Character popovers
* Word completion / prediction using different completion engines
* Gesture based cursor movement
* Lots of emojis 😀
* Debug mode to debug text-input/input-method issues
* More than 30 different layouts
* Configurable short cuts in terminal layout

It acts as a Wayland input-method or virtual keyboard.

This keyboard is still in development.

[Source](https://gitlab.gnome.org/guidog/phosh-osk-stub/-/raw/main/data/mobi.phosh.OskStub.metainfo.xml.in)