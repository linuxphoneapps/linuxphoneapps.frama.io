+++
title = "Key Rack"
description = "View and edit passwords"
aliases = []
date = 2024-01-14
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Felix Häcker", "Sophie Herold",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "arch", "archlinuxarm_aarch64", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.gnome.org/sophie-h/key-rack/"
homepage = "https://gitlab.gnome.org/sophie-h/key-rack"
bugtracker = "https://gitlab.gnome.org/sophie-h/key-rack/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/key-rack/-/raw/main/data/app.drey.KeyRack.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/-/project/23102/uploads/4bd105b8bb37498ac0d7d55283448528/image.png", "https://gitlab.gnome.org/-/project/23102/uploads/f37e08127a1336d74e255a7b959b0185/image.png",]
screenshots_img = []
svg_icon_url = "https://gitlab.gnome.org/World/key-rack/-/raw/main/data/app.drey.KeyRack.svg"
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.KeyRack"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.KeyRack"
flatpak_link = "https://flathub.org/apps/app.drey.KeyRack.flatpakref"
flatpak_recipe = "https://gitlab.gnome.org/World/key-rack/-/raw/main/build-aux/app.drey.KeyRack.Devel.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "key-rack",]
appstream_xml_url = "https://gitlab.gnome.org/World/key-rack/-/raw/main/data/app.drey.KeyRack.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2024-12-14"
repo_created_date = "2022-09-22"

+++

### Description

Key Rack allows you to view, create and edit secrets, such as passwords or tokens, stored by apps. It supports both Flatpak secrets and system-wide keyrings.

[Source](https://gitlab.gnome.org/World/key-rack/-/raw/main/data/app.drey.KeyRack.metainfo.xml.in)