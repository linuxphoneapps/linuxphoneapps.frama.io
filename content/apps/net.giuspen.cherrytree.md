+++
title = "CherryTree"
description = "Hierarchical Note Taking"
aliases = []
date = 2020-12-27
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Giuseppe Penone",]
categories = [ "note taking",]
mobile_compatibility = [ "3",]
status = [ "released",]
frameworks = [ "GTK",]
backends = []
services = []
packaged_in = [ "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_11", "debian_12", "debian_13", "debian_unstable", "devuan_4_0", "devuan_unstable", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "opensuse_tumbleweed", "pureos_byzantium", "pureos_landing",]
freedesktop_categories = [ "Utility",]
programming_languages = []
build_systems = []
requires_internet = []
tags = []

[extra]
repository = "https://github.com/giuspen/cherrytree"
homepage = "https://www.giuspen.net/cherrytree"
bugtracker = "https://github.com/giuspen/cherrytree/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/net.giuspen.cherrytree"
screenshots = [ "https://www.giuspen.net/images/cherrytree-main_window_code.png", "https://www.giuspen.net/images/cherrytree-main_window_text.png",]
screenshots_img = []
svg_icon_url = ""
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "net.giuspen.cherrytree"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.giuspen.cherrytree"
flatpak_link = "https://flathub.org/apps/net.giuspen.cherrytree.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "cherrytree",]
appstream_xml_url = "https://raw.githubusercontent.com/giuspen/cherrytree/refs/heads/master/data/_net.giuspen.cherrytree.metainfo.xml"
reported_by = "linmob"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/net.giuspen.cherrytree/"
latest_repo_commit = "2025-01-12"
repo_created_date = "2015-03-22"

+++

### Description

A hierarchical note taking application, featuring rich text and syntax highlighting, storing data in either a single file (xml or sqlite) or multiple files and directories.
Cherrytree is not just about having a place to write notes and to-do items and keeping them organized, it's also a place you can store links, pictures, tables, even entire documents. It can be your one program for all the miscellaneous information you have and want to keep. All those little bits of information you have scattered around your hard drive can be conveniently placed into a Cherrytree document where you can easily find it.

The Main Features:

* Rich Text supporting Images, Tables, Code Boxes, Hyperlinks and Anchors, Spell Check, Lists
* Syntax Highlighting supporting many programming languages
* Password Protection, Powerful Find and Replace utilities, TOC generator
* Print, Save as PDF, Export to HTML, Export to Plain Text
* Import from Several other Note Taking Applications

[Source](https://raw.githubusercontent.com/giuspen/cherrytree/refs/heads/master/data/_net.giuspen.cherrytree.metainfo.xml)