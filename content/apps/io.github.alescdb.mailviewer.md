+++
title = "MailViewer"
description = "EML and MSG file viewer"
aliases = []
date = 2024-11-05
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alexandre Del Bigio",]
categories = [ "email",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Email",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/alescdb/mailviewer"
homepage = "https://github.com/alescdb/mailviewer"
bugtracker = "https://github.com/alescdb/mailviewer/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://raw.githubusercontent.com/alescdb/mailviewer/refs/heads/main/data/io.github.alescdb.mailviewer.metainfo.xml.in"
screenshots = [ "https://github.com/alescdb/mailviewer/blob/b6a1c8a703416b3d88a7fcbc66af5a4a26a395fc/images/mailviewer_0.9.92_dark_1.png?raw=true", "https://github.com/alescdb/mailviewer/blob/b6a1c8a703416b3d88a7fcbc66af5a4a26a395fc/images/mailviewer_0.9.92_dark_2.png?raw=true", "https://github.com/alescdb/mailviewer/blob/b6a1c8a703416b3d88a7fcbc66af5a4a26a395fc/images/mailviewer_0.9.92_light_1.png?raw=true", "https://github.com/alescdb/mailviewer/blob/b6a1c8a703416b3d88a7fcbc66af5a4a26a395fc/images/mailviewer_0.9.92_light_2.png?raw=true",]
screenshots_img = []
svg_icon_url = "https://raw.githubusercontent.com/alescdb/mailviewer/refs/heads/main/data/icons/hicolor/scalable/apps/io.github.alescdb.mailviewer.svg"
non_svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = "io.github.alescdb.mailviewer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.alescdb.mailviewer"
flatpak_link = "https://flathub.org/apps/io.github.alescdb.mailviewer.flatpakref"
flatpak_recipe = "https://raw.githubusercontent.com/alescdb/mailviewer/refs/heads/main/io.github.alescdb.mailviewer.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/alescdb/mailviewer/refs/heads/main/data/io.github.alescdb.mailviewer.metainfo.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
feed_entry_id = "https://linuxphoneapps.org/apps/io.github.alescdb.mailviewer/"
latest_repo_commit = "2025-01-01"
repo_created_date = "2024-10-23"

+++

### Description

MailViewer is a GNOME application that allows users to read and decode .eml and .msg files
(email files) without having to install any additional software or create an account.

It provides a graphical interface for easy navigation and rendering of email content,
including attachments, HTML, and plain text.

[Source](https://raw.githubusercontent.com/alescdb/mailviewer/refs/heads/main/data/io.github.alescdb.mailviewer.metainfo.xml.in)