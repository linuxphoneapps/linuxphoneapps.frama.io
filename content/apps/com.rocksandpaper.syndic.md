+++
title = "Syndic"
description = "Feed reader for casual browsing"
aliases = []
date = 2022-09-03
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-3.0",]
app_author = [ "Connor Carney",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "Feed", "Network", "News",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]
requires_internet = []
tags = []

[extra]
repository = "https://github.com/cscarney/syndic"
homepage = "http://syndic.rocksandpaper.com/"
bugtracker = "https://github.com/cscarney/syndic/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/syndic/"
more_information = []
summary_source_url = "https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml"
screenshots = [ "https://syndic.rocksandpaper.com/doc/screenshots/syndic-mid.png", "https://syndic.rocksandpaper.com/doc/screenshots/syndic-narrow.png", "https://syndic.rocksandpaper.com/doc/screenshots/syndic-wide.png",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = "com.rocksandpaper.syndic"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.rocksandpaper.syndic"
flatpak_link = "https://flathub.org/apps/com.rocksandpaper.syndic.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-09"
repo_created_date = "2020-05-13"

+++

### Description

Syndic is a simple, touch-friendly feed reader for KDE Plasma Desktop. Subscribe to any site that provides an RSS or Atom feed. Get automatically-generated
highlights from your collection, search for articles by keyword, or page through a list of every new article.

Features include:

[Source](https://raw.githubusercontent.com/cscarney/syndic/master/com.rocksandpaper.syndic.appdata.xml)