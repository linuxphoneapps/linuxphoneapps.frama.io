+++
title = "ICE SSB"
description = "Tool to create Chromium/Chrome/Firefox/Vivaldi SSBs in Peppermint OS."
aliases = []
date = 2020-10-21
updated = 2024-10-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "peppermintos",]
categories = [ "web app laucher",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)",]

[extra]
repository = "https://github.com/peppermintos/ice"
homepage = ""
bugtracker = "https://github.com/peppermintos/ice/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/peppermintos/ice"
screenshots = []
screenshots_img = []
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = "ice"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "ice-ssb",]
appstream_xml_url = ""
reported_by = "1peter10"
updated_by = "check_via_github_api"
latest_repo_commit = "2022-02-18"
repo_created_date = "2011-03-30"

+++

### Notice
Only tested with Firefox, Android User Agent makes this a lot less useful.
Last commit 2021-04-29.