+++
title = "Half Life"
description = "Sci-fi first-person shooter developed and published by Valve."
aliases = ["games/noappid.valvesoftware.halflife/"]
date = 2021-03-14
updated = 2025-01-07

[taxonomies]
project_licenses = [ "Proprietary",]
metadata_licenses = []
app_author = ["Valve Software",]
categories = [ "game",]
mobile_compatibility = [ "1",]
status = [ "pre-release",]
frameworks = []
backends = []
services = []
packaged_in = []
freedesktop_categories = ["Game", "Shooter"]
programming_languages = []
build_systems = []
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "not packaged yet",]

[extra]
repository = "https://github.com/ValveSoftware/halflife"
homepage = "https://half-life.fandom.com/wiki/Half-Life"
bugtracker = "https://github.com/ValveSoftware/halflife/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = []
appstream_xml_url = ""
reported_by = "-Euso-"
updated_by = "1Maxnet1"
feed_entry_id = "https://linuxphoneapps.org/games/noappid.valvesoftware.halflife/"
latest_repo_commit = "2024-10-02"
repo_created_date = "2013-02-14"

+++
