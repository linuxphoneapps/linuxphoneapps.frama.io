+++
title = "mobpass"
description = "A mobile interface for gopass"
aliases = []
date = 2020-03-24
updated = 2024-10-28

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "anjan",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = [ "released", "inactive",]
frameworks = [ "QtQuick",]
backends = [ "gopass",]
services = []
packaged_in = [ "alpine_edge",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]
requires_internet = []
tags = [ "No Appstream Metadata (manual maintenance)", "manual status maintenance", "no icon",]

[extra]
repository = "https://git.sr.ht/~anjan/mobpass"
latest_repo_commit = "2020-05-05"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~anjan/mobpass"
screenshots = []
screenshots_img = []
svg_icon_url = ""
all_features_touch = false
intended_for_mobile = false
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "mobpass",]
appstream_xml_url = ""
reported_by = "anjandev"
updated_by = "check_via_git"
repo_created_date = "2020-02-21"

+++

### Notice

Currently broken in Alpine Edge due to hardcoded paths for Python 3.8 with later Python releases being current.
