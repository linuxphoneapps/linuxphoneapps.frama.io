+++
title = "Text Editor"
description = "Edit text files"
aliases = []
date = 2023-01-22
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_20", "alpine_3_21", "alpine_edge", "arch", "archlinuxarm_aarch64", "aur", "debian_12", "debian_13", "debian_unstable", "devuan_unstable", "fedora_40", "fedora_41", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_24_05", "nix_stable_24_11", "nix_unstable", "openmandriva_5_0", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "TextEditor", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = [ "requires offline-only",]
tags = [ "GNOME Core Apps",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-text-editor"
homepage = "https://apps.gnome.org/TextEditor/"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-text-editor/issues"
donations = "https://www.gnome.org/donate/"
translations = "https://l10n.gnome.org/module/gnome-text-editor/"
more_information = [ "https://apps.gnome.org/TextEditor/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/text-editor/text-editor-1.png", "https://static.gnome.org/appdata/gnome-43/text-editor/text-editor-2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.text-editor/1.png", "https://img.linuxphoneapps.org/org.gnome.text-editor/2.png", "https://img.linuxphoneapps.org/org.gnome.text-editor/3.png",]
all_features_touch = true
intended_for_mobile = true
app_id = "org.gnome.TextEditor"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.TextEditor"
flatpak_link = "https://flathub.org/apps/org.gnome.TextEditor.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "gnome-text-editor",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.metainfo.xml.in.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-13"
repo_created_date = "2020-04-09"

+++

### Description

GNOME Text Editor is a simple text editor focused on a pleasing default experience.

[Source](https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in)

### Notice

Mobile friendly since release 43.