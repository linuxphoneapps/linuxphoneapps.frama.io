+++
title = "Roger Router"
description = "Journal, Fax-Software and Call-Monitor for AVM FRITZ!Box or compatible"
aliases = []
date = 2022-04-12
updated = 2025-01-13

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "released",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "FRITZ!Box",]
packaged_in = [ "aur", "debian_11", "devuan_4_0", "flathub", "pureos_byzantium",]
freedesktop_categories = [ "Network",]
programming_languages = [ "C",]
build_systems = [ "meson",]
requires_internet = []
tags = []

[extra]
repository = "https://gitlab.com/tabos/rogerrouter"
homepage = "https://www.tabos.org"
bugtracker = "https://www.tabos.org/forum/"
donations = "https://www.tabos.org/"
translations = "https://gitlab.com/tabos/rogerrouter/-/tree/master/po"
more_information = []
summary_source_url = "https://gitlab.com/tabos/rogerrouter/-/raw/master/data/org.tabos.roger.appdata.xml.in"
screenshots = [ "https://tabos.gitlab.io/project/rogerrouter/roger-journal.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.tabos.roger/1.png", "https://img.linuxphoneapps.org/org.tabos.roger/2.png", "https://img.linuxphoneapps.org/org.tabos.roger/3.png", "https://img.linuxphoneapps.org/org.tabos.roger/4.png", "https://img.linuxphoneapps.org/org.tabos.roger/5.png", "https://img.linuxphoneapps.org/org.tabos.roger/6.png",]
all_features_touch = false
intended_for_mobile = false
app_id = "org.tabos.roger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.roger"
flatpak_link = "https://flathub.org/apps/org.tabos.roger.flatpakref"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
appimage_x86_64_url = ""
appimage_aarch64_url = ""
repology = [ "roger-router",]
appstream_xml_url = "https://gitlab.com/tabos/rogerrouter/-/raw/master/data/org.tabos.roger.appdata.xml.in"
reported_by = "1peter10"
updated_by = "check_via_git_api"
latest_repo_commit = "2025-01-12"
repo_created_date = "2018-05-27"

+++

### Description

Roger Router is a clean solution for controlling the FRITZ!Box or compatible router with Linux. It offers a rich feature list, including call monitor, journal, fax support and also has a great integration into several address books.

[Source](https://gitlab.com/tabos/rogerrouter/-/raw/master/data/org.tabos.roger.appdata.xml.in)