+++
title = "Alpine 3.17"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

Alpine 3.19 was released on 2023-12-07. It's used in [postmarketOS 23.12](../postmarketos-23-12), which may contain some backports for stable service packs and other postmarketOS specific packages.
