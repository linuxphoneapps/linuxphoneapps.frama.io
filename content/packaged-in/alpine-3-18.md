+++
title = "Alpine 3.18"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

Alpine 3.18 was released on 2023-05-09. It's used in [postmarketOS 23.06](../postmarketos-23-06), which may contain some backports for stable service packs and other postmarketOS specific packages.
