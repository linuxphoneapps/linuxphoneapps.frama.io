+++
title = "postmarketOS edge"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

postmarketOS edge is the rolling/testing branch of postmarketOS. postmarketOS itself only contains some postmarketOS/mobile specific packages (e.g., device kernels and firmware). All other packages can be found in [Alpine edge](../alpine-edge/).
