+++
title = "New Listings of Q2/2024: More than 512 Apps"
date = 2024-06-30T12:00:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]
+++

The first half of 2024 has already passed. Time flies! 
Let's have a look at what happened last quarter!

<!-- more -->


### New listings

17 apps were added in the 2nd quarter of 2024:

#### April
* [Usage](https://linuxphoneapps.org/apps/org.gnome.usage/) - A nice way to view information about use of system resources 

#### May
* [DewDuct](https://linuxphoneapps.org/apps/null.daknig.dewduct/) - A YouTube player _Thank you, DaKnig, for creating and adding the app!_
* [Elastic](https://linuxphoneapps.org/apps/app.drey.elastic/) - Design spring animations 
* [Wildcard](https://linuxphoneapps.org/apps/com.felipekinoshita.wildcard/) - Test your regular expressions 
* [Calculator](https://linuxphoneapps.org/apps/io.github.alexkdeveloper.calculator/) - A simple string calculator 
* [Impression](https://linuxphoneapps.org/apps/io.gitlab.adhami3310.impression/) - Create bootable drives 
* [Lorem](https://linuxphoneapps.org/apps/org.gnome.design.lorem/) - Generate placeholder text 
* [Fonts](https://linuxphoneapps.org/apps/org.gnome.font-viewer/) - View fonts on your system 
* [Citations](https://linuxphoneapps.org/apps/org.gnome.world.citations/) - Manage your bibliography 
* [Graphs](https://linuxphoneapps.org/apps/se.sjoerd.graphs/) - Plot and manipulate data 
* [Ptyxis](https://linuxphoneapps.org/apps/app.devsuite.ptyxis/) - Container-oriented terminal 
* [Fotema](https://linuxphoneapps.org/apps/app.fotema.fotema/) - Admire your photos 
* [Web Apps](https://linuxphoneapps.org/apps/net.codelogistics.webapps/) - Install websites as apps 

#### June
* [Papers](https://linuxphoneapps.org/apps/org.gnome.papers/) - Read documents 
* [Showtime](https://linuxphoneapps.org/apps/org.gnome.showtime/) - Watch without distraction 
* [Alpaka](https://linuxphoneapps.org/apps/org.kde.alpaka/) - Alpaka is a client for the Ollama AI service, which lets you run LLM models such as Llama 2. _Thank you, cahfofpai, for adding the app!_
* [eSIM Manager](https://linuxphoneapps.org/apps/eu.lucaweiss.lpa_gtk/) - Download and manage eSIM profiles 

#### Games 

3 games were added in the past quarter:

* [Chess Clock](https://linuxphoneapps.org/games/com.clarahobbs.chessclock/) - Time games of over-the-board chess 
* [Animatch](https://linuxphoneapps.org/games/com.holypangolin.animatch/) - A cute match-3 game 
* [Ultimate Tic Tac Toe](https://linuxphoneapps.org/games/io.github.nokse22.ultimate-tic-tac-toe/) - (Tic Tac Toe)² _Thank you, Nokse, for creating and adding the app!_


### Maintenance

Most listings were automatically updated, and many of them were additionally edited by hand and brought up to date. 
And because information regarding services or frameworks can't be updated automatically ... 

### Thanks

I would like to thank everybody who contributed in the past quarter:
- [Max](https://framagit.org/1Maxnet1), for a number of fixes,
- [cahfofpai](https://framagit.org/cahfofpai), for adding Alpaka, and a so far [unmerged MR](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/merge_requests/104),
- [nokse22](https://framagit.org/Nokse), for creating and adding Ultimate Tic Tac Toe and updating listings of other apps and games they have created!
... and everyone I may have forgotten, as this list is based entirely on repository contributions!

Thank you all so much!


## Please contribute

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io) - the repo's readme is hopefully helpful. Feel free to ask away in our [Matrix room](https://matrix.to/#/#linuxphoneapps:matrix.org)!

Fixing existing app or game listings is just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

![Footer on the bottom of the page](https://linuxphoneapps.org/img/app-list-footer.png)

So if you see that something is wrong, take the time and help fix it - by fixing it, or by reporting it to the [issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/new) or via [e-mail](https://lists.sr.ht/~linuxphoneapps/errors).

### Adding apps

If you want to add an app (if you don't know any, check [Apps to be added](https://linuxphoneapps.org/lists/apps-to-be-added/) ;-) ), 

Fortunately, we now have an easy way to do this:

 [LPA helper](https://linuxphoneapps.org/lpa_helper.html) is ugly, but it works. We hope to integrate it properly soon - if you have questions or suggestions, do not hesitate to ask/get in touch!

See also:
- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

## What's next?

I still have not managed to write the post with plans and goals for 2024, and it makes less sense to write one with every day that goes by. We do have a [public issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues), that lists things that should be changed. Progress on my side has been very slow, apologies for that! There's a glimmer of hope though, as, from September onwards, I should have a bit more time by switching to a four day work week.

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org - it can also be useful for your friends or yourself if you only use the Linux Desktop, as we constantly check for and add new, awesome adaptive Linux apps!

