+++
title = "New Listings of Q3/2024: 539 Apps and better app discovery"
date = 2024-10-03T19:50:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]
+++

It's October, three thirds of 2024 have already passed. Time flies!
Let's have a look at what happened last quarter!

<!-- more -->

### A new feature

First, let's get into a feature that was added today: _Advanced Search_

- [Advanced Search for Apps](https://linuxphoneapps.org/find/apps/)
- [Advanced Search for Games](https://linuxphoneapps.org/find/games/)

Check the announement on Mastodon:
- [LinuxPhoneApps.org: "It took literally forever, but…" - FLOSS.social](https://floss.social/@linuxphoneapps/113243585548398782). _Feedback welcome!_


It has been in the work for a long time. It was inspired by this issue:
- [allow filtering by multiple criteria (#57) · Issues · LinuxPhoneApps / linuxphoneapps.org · GitLab](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/57)

Despite taking to so long, it is not much different from [Linux Phone Apps Table - Explore data from LinuxPhoneApps.org](https://dbeley.github.io/lpa-table/) and lacks some features while having some others - but it a fully integrated part of the site and always fully up-to-date, as the every change to the contents included in the list is immediately reflected there too.

### New listings

23 apps were added in the third quarter of 2024:

#### July
* [Transito](https://linuxphoneapps.org/apps/noappid.mil.transito/) - FOSS data-provider-agnostic (GTFS) public transportation app
* [PowerPlant](https://linuxphoneapps.org/apps/org.kde.powerplant/) - A small app helping you to keep your plants alive

#### August
* [Alpaca](https://linuxphoneapps.org/apps/com.jeffser.alpaca/) - Chat with local AI models
* [Pulp](https://linuxphoneapps.org/apps/org.gnome.gitlab.cheywood.pulp/) - Skim excessive feeds
* [Archives](https://linuxphoneapps.org/apps/dev.geopjr.archives/) - Create and view web archives
* [Echo](https://linuxphoneapps.org/apps/io.github.lo2dev.echo/) - Ping websites
* [Fidei](https://linuxphoneapps.org/apps/arpa.sp1rit.fidei/) - Bible reader
* [EV](https://linuxphoneapps.org/apps/mobi.phosh.ev/) - Electric Vehicle information
* [Phosh OSK](https://linuxphoneapps.org/apps/sm.puri.phosh.oskstub/) - An experimental On Screen Keyboard for Phosh
* [Picker](https://linuxphoneapps.org/apps/io.github.mezoahmedii.picker/) - Randomly pick somthing to do
* [Unfettered Keyboard](https://linuxphoneapps.org/apps/noappid.flamingradian.unfettered-keyboard/) - The Unfettered Keyboard is an on-screen keyboard for Linux mobile.
* [Keychain](https://linuxphoneapps.org/apps/org.kde.keychain/) - Manage your passwords
* [Concessio](https://linuxphoneapps.org/apps/io.github.ronniedroid.concessio/) - Understand file permissions
* [Screenshot](https://linuxphoneapps.org/apps/org.adishatz.screenshot/) - Take screenshots

#### September
* [Goains](https://linuxphoneapps.org/apps/noappid.eeelco.goains/) - Workout tracking app. _Thank you, Eeelco, for creating and adding the app!_
* [Binary](https://linuxphoneapps.org/apps/io.github.fizzyizzy05.binary/) - Convert numbers between bases
* [Arca](https://linuxphoneapps.org/apps/org.kde.arca.desktop/) - Archive manager
* [Shutter](https://linuxphoneapps.org/apps/noappid.piggz.harbour-shutter/) - Shutter is a camera application which exposes all available camera parameters to the user.
* [newsFish](https://linuxphoneapps.org/apps/uk.co.piggz.newsfish/) - Nextcloud news client for Kirigami
* [Plattenalbum](https://linuxphoneapps.org/apps/de.wagnermartin.plattenalbum/) - Connect to your music
* [GhostCloud](https://linuxphoneapps.org/apps/me.fredl.ghostcloud/) - A modern cross-platform client for Nextcloud, ownCloud & WebDav.
* [Mirror Hall](https://linuxphoneapps.org/apps/eu.nokun.mirrorhall/) - Use Linux devices as virtual displays in a peer-to-peer fashion
* [Bitritter](https://linuxphoneapps.org/apps/noappid.chfkch.bitritter/) - Simple GTK app (Relm4 framework) to interact with Bitwarden/Vaultwarden vaults


### Games

Three games were added in the past quarter:

* [boxclip](https://linuxphoneapps.org/games/noappid.glitchapp.boxclip-mobile/) - "Boxclip", a 2D platformer engine, with an emphasis on interactive map editing. _Thank you, glichapp, for adding creating the game!_
* [Skladnik](https://linuxphoneapps.org/games/org.kde.skladnik/) - Sokoban Game. _Thank you, ltworf, for contributing to the game and adding it!_
* [explosive-c4](https://linuxphoneapps.org/games/noappid.ltworf.explosive-c4/) - Connect4 clone. _Thank you, ltworf, for creating and adding the game!_


### Maintenance

Most listings were automatically updated, and many of them were additionally edited by hand and brought up to date.
And because information regarding services or frameworks can't be updated automatically, help with these is especially welcome!

### Thanks

I would like to thank everybody who contributed in the past quarter:
- [Adam](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/commit/feec928ef7573af76b658b82aa7e8cf73b2200d3)
- [Guido](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/84)
... and everyone I may have forgotten, as this list is based entirely on repository contributions and new issues!

Thank you all so much!


## Please contribute

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io) - the repo's readme is hopefully helpful. Feel free to ask away in our [Matrix room](https://matrix.to/#/#linuxphoneapps:matrix.org)!

Fixing existing app or game listings is just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

![Footer on the bottom of the page](https://linuxphoneapps.org/img/app-list-footer.png)

So if you see that something is wrong, take the time and help fix it - by fixing it, or by reporting it to the [issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/new) or via [e-mail](https://lists.sr.ht/~linuxphoneapps/errors).

### Adding apps

If you want to add an app (if you don't know any, check [Apps to be added](https://linuxphoneapps.org/lists/apps-to-be-added/) ;-) ),

Fortunately, we now have an easy way to do this:

 [LPA helper](https://linuxphoneapps.org/lpa_helper.html) is ugly, but it works. We hope to integrate it properly soon - if you have questions or suggestions, do not hesitate to ask/get in touch!

See also:
- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

## What's next?

We do have a [public issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues), that lists things that should be changed. Lately the focus has been on adding more things that should be done - to then work on them.

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org - it can also be useful for your friends or yourself if you only use the Linux Desktop, as we constantly check for and add new, awesome adaptive Linux apps!

