+++
title = "New Listings of Q4/2024: Adding 32 Apps, Three Games and other improvements."
date = 2025-01-20
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]
+++

We're way into January 2025, so let's have a look at we accomplished in the last quarter of 2024, plus a glimpse at things to look forward to in 2025!

<!-- more -->

### New app listings

We've added 32 apps, pushing the total count to 571.[^1]

#### October
* [Netsleuth](https://linuxphoneapps.org/apps/io.github.vmkspv.netsleuth/) - Calculate IP subnets
* [< polycule >](https://linuxphoneapps.org/apps/business.braid.polycule/) - A geeky and efficient [matrix] client for power users
* [Rosary](https://linuxphoneapps.org/apps/io.github.roseblume.rosary/) - Study Christianity
* [Calculator](https://linuxphoneapps.org/apps/dev.edfloreshz.calculator/) - Beyond the equation
* [Uno Calculator](https://linuxphoneapps.org/apps/uno.platform.uno-calculator/) - Linux port of the Microsoft Windows calculator app
* [splitcat](https://linuxphoneapps.org/apps/lol.janjic.splitcat/) - Splitcat application packaged as a Flatpak.
* [Lock](https://linuxphoneapps.org/apps/com.konstantintutsch.lock/) - Process data with GnuPG
* [Diurnals](https://linuxphoneapps.org/apps/io.github.sss_says_snek.diurnals/) - Get daily Todoist notifications
* [WebP Converter](https://linuxphoneapps.org/apps/io.itsterminal.webpconverter/) - The fastest way to convert images to WebP
* [pomodorolm](https://linuxphoneapps.org/apps/org.jousse.vincent.pomodorolm/) - A simple, good looking and configurable pomodoro tracker with tray icon
* [Meshtastic Client](https://linuxphoneapps.org/apps/org.kop316.meshtastic/) - Graphically control meshtastic devices
* [Caffeine](https://linuxphoneapps.org/apps/com.konstantintutsch.caffeine/) - Calculate your coffee
* [Turn On](https://linuxphoneapps.org/apps/de.swsnr.turnon/) - Turn on devices in your network
* [Cursed Delta](https://linuxphoneapps.org/apps/noappid.adbenitez.deltachat-cursed/) - A lightweight Delta Chat client for the command line . _Thank you, magdesign, for adding the app!_
* [Fruit Credits](https://linuxphoneapps.org/apps/com.dz4k.fruitcredits/) - Keep plaintext accounts
* [Add Water](https://linuxphoneapps.org/apps/dev.qwery.addwater/) - Keep Firefox in fashion
* [NymphCast Player](https://linuxphoneapps.org/apps/noappid.mayaposch.nymphcast/) - NymphCast Player provides NymphCast client functionality format in a graphical (Qt-based) format . _Thank you, krzno, for notifying us about this app!_

#### November
* [Morphosis](https://linuxphoneapps.org/apps/garden.jamie.morphosis/) - Convert your documents
* [Mobile Config for Firefox](https://linuxphoneapps.org/apps/org.postmarketos.mobile_config_firefox/) - Mobile and privacy friendly configuration for Firefox (distro-independent)
* [Community Remote](https://linuxphoneapps.org/apps/com.theappgineer.community_remote/) - Roon control for family and friends
* [MailViewer](https://linuxphoneapps.org/apps/io.github.alescdb.mailviewer/) - EML and MSG file viewer
* [Keypunch](https://linuxphoneapps.org/apps/dev.bragefuglseth.keypunch/) - Practice your typing skills
* [Calligraphy](https://linuxphoneapps.org/apps/dev.geopjr.calligraphy/) - Turn text into ASCII banners
* [Nimlem](https://linuxphoneapps.org/apps/org.codeberg.boorb.nimlem/) - An linux mobile first GTK4 client for Lemmy
* [Notify](https://linuxphoneapps.org/apps/com.ranfdev.notify/) - Receive notifications from ntfy.sh
* [Play Timer](https://linuxphoneapps.org/apps/io.github.efogdev.mpris-timer/) - Native-feeling timers
* [PinApp](https://linuxphoneapps.org/apps/io.github.fabrialberio.pinapp/) - Create and edit app shortcuts
* [Counters](https://linuxphoneapps.org/apps/io.gitlab.guillermop.counters/) - Keep track of anything
* [Poliedros](https://linuxphoneapps.org/apps/io.github.kriptolix.poliedros/) - Poliedros is a multi-type dice roller.

#### December
* [Kumo](https://linuxphoneapps.org/apps/noappid.catacombing.kumo/) - A Wayland Mobile Browser
* [Geoclue Stumbler](https://linuxphoneapps.org/apps/org.kop316.stumbler/) - Submit Cell Tower/WiFi AP data to geoclue
* [Supersonik](https://linuxphoneapps.org/apps/uk.co.piggz.supersonik/) - Subsonic music client

### Games

Three games were added in the past quarter:

* [Chess engine](https://linuxphoneapps.org/games/noappid.glitchapp.chess-engine/) - A fully compliant, playable chess engine written in Lua and rendered with the Love2D game framework forked from https://github.com/Azzla/chess-engine. . _Thank you, glitchapp, for creating and adding the app!_
* [Clockwind](https://linuxphoneapps.org/games/noappid.glitchapp.clockwind/) - a platform game with time travel mechanichs forked from https://alesan99.itch.io/clockwind . _Thank you, glitchapp, for creating and adding the app!_
* [Flood It](https://linuxphoneapps.org/games/io.github.tfuxu.floodit/) - Flood the board

### Maintenance

Listings have also been updated, most only automagically, but many also by hand. We've also eliminated some technical debt by [removing the csv-list of archived apps](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/86), which were re-added to the site as listings where it made sense.

This past quarter also again saw some work with upstream (e.g., [1](https://github.com/workbenchdev/Biblioteca/pull/122), [2](https://gitlab.com/kop316/gtk-meshtastic-client/-/merge_requests/4), [3](https://github.com/largestgithubuseronearth/addwater/issues/18), to share a few examples), and towards packaging in Alpine and Flathub.

### Thanks

I would like to thank everybody who contributed in the past quarter:
- [Max](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/merge_requests?scope=all&state=merged&author_username=1Maxnet1) - and for [taking over the maintainership](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/70#note_2175535) of the games list/the games listed!
Thanks again for your great work!


## What's to come in 2025?

We do have a [public issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues), that lists things that we aim to improve.

Recent happenings like the fact that [Flathub now has a collection of Mobile Friendly Apps](https://flathub.org/apps/collection/mobile/1) and that [developer tools for GNOME now make testing for mobile easier](https://blogs.gnome.org/alicem/2024/12/19/mobile-testing-in-libadwaita/), make our outlook on 2025 quite positive. We also have hopes that - like GitHub or not - [Linux arm64 hosted runners now available for free in public repositories (Public Preview)](https://github.blog/changelog/2025-01-16-linux-arm64-hosted-runners-now-available-for-free-in-public-repositories-public-preview/) is going to help with having more Linux-y, but also cross platform apps (e.g., Flutter apps) available without tedious self-compiling.

All these changes should make the ecosystem better, and we hope to also improve LinuxPhoneApps.org, as time allows.

Also, LinuxPhoneApps.org will be present at FOSDEM (in some way). Peter won't do [a talk](https://linuxphoneapps.org/blog/a-talk-at-fosdem-2024/) this year, but (poorly designed) stickers have been ordered and will be available at the [Linux on Mobile stand in building K](https://fosdem.org/2025/stands/). If you want to meet to talk things through, please get in touch!

For those interested in Mobile Linux, [linmob.net is going to provide a collection of things to watch out for](https://linmob.net/fosdem2025/), like it did [last year](https://linmob.net/fosdem2024/).

## Contributions welcome

All relevant parts of LinuxPhoneApps live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io) - the repo's README is hopefully helpful. Feel free to ask away in our [Matrix room](https://matrix.to/#/#linuxphoneapps:matrix.org) if you have questions!

Fixing existing app or game listings is just one click away (provided you have a framagit.org account) - every listing has an edit link at the bottom of the page.

So if you see that something is wrong, please take the time and help fix it - by fixing it, or by reporting it to the [issue tracker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/new) or via [e-mail](https://lists.sr.ht/~linuxphoneapps/errors).

### Adding apps

If you want to help by adding an app:

[LPA helper](https://linuxphoneapps.org/lpa_helper.html) is ugly, but it works. If you have questions or suggestions, do not hesitate to ask/get in touch!

See also:
- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

## Feedback, thoughts?

Thank you for reading this post! If you enjoyed reading it, please share it and spread the word LinuxPhoneApps.org - it can also be useful for your friends or yourself if you only use the Linux Desktop, as we constantly check for and add new, awesome adaptive Linux apps that also work great on your laptop or desktop!


[^1]: The current count is different, as we've re-added some list from old archive.csv, added two more apps this January and had to mark [one more app](https://linuxphoneapps.org/apps/com.jvieira.tpt.metronome/) as [gone](https://linuxphoneapps.org/status/gone/), removing it from the app count.
