+++
title = "New apps of LINMOBapps, Q1/2022 🎉"
date = 2022-04-07T23:00:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]

[extra]
lead = "Let's have a quick and dirty blog post listing all the additions to the app list in the first quarter of 2022!"
images = []
+++


### January

* [Osmin](https://linuxphoneapps.org/apps/noappid.janbar.osmin/), a GPS Navigator On-Road/Off-Road for Android, Sailfish and Linux phones. _A great QtQuick maps app. It’s available in Manjaro's repos, so if you are using Manjaro or Arch,[^1] do give it a try. Thanks Karry for contributing this app!_
* [Store Cards](https://linuxphoneapps.org/apps/noappid.fdservices.storecards/),a Tk based utility to keep all your store cards on your linux phone.
* [Strike](https://linuxphoneapps.org/apps/org.kde.strike/) is Maui's simple minimal IDE for the Linux phones. Code, build, and run from the phone!

### February

No apps were added in February.🧐

### March

* [GadgetController](https://linuxphoneapps.org/apps/de.beaerlin.gadgetcontroller/), a pythonic GTK/libhandy utility to use your PinePhone as an USB gadget.
* [Huely](https://linuxphoneapps.org/apps/com.github.benpocalypse.huely/), an app to control WiFi lights compatible with the Android Magic Home app using this simple app. _Sadly it seems to be a few pixels to wide, so it's not a **5**._
* [Déjà Dup Backups](https://linuxphoneapps.org/apps/org.gnome.dejadup/), a duplicity based simple backup tool that uses GTK4/libadwaita for a convergent UI,
* [Warpinator](https://linuxphoneapps.org/apps/org.x.warpinator/), an utility allowing you to easily connect multiple computers on a local area network and share files quickly and securely, developed for Linux Mint,
* [cpupower-gui](https://linuxphoneapps.org/apps/org.rnd2.cpupower_gui.desktop/), a graphical program that is used to change the scaling frequency limits of your CPU,
* [DieBahn](https://linuxphoneapps.org/apps/de.schmidhuberj.diebahn/), a GTK4 frontend for the travel information of the german railway,
* [Reactions](https://linuxphoneapps.org/apps/dev.atoft.reactions/), written in Zig, GTK4 and libadwaita, it uses the GIPHY search API,
* [Passes](https://linuxphoneapps.org/apps/me.sanchezrodriguez.passes/), a simple GTK4/libadwaita tool that allows you to import and take your digital passes (.pkpass files) with you,
* [Polari](https://linuxphoneapps.org/apps/org.gnome.polari/), a simple IRC Client that is designed to integrate seamlessly with GNOME, working on mobile since its GTK4/libadwaita overhaul for release 42, _There's a bug with quick nickname editing, not sure whether Polari or Squeekboard are at fault here, - but it generally works, which is awesome, as [mobile friendly IRC clients|(https://linuxphoneapps.org/categories/irc-client/) are rare!_
* [Powersupply](https://linuxphoneapps.org/apps/noappid.martijnbraam.powersupply/), a PinePhone power status tool that should have been added forever ago,
* [Euterpe GTK](https://linuxphoneapps.org/apps/com.doycho.euterpe.gtk/), a convergent desktop and mobile client for Euterpe, a self-hostable music streaming server, realized with GTK3/libhandy,
* [Mepo](https://linuxphoneapps.org/apps/noappid.mil.mepo/) a fast, simple, and hackable OSM map viewer for Linux, designed with the Pinephone & mobile linux in mind; realized with Zig and SDL, _Thanks milesalan for creating and adding this amazingly performant little maps app!_
* [Random](https://linuxphoneapps.org/apps/page.codeberg.foreverxml.random/), a GTK4/libadwaita utility that make randomization easy so you don't need to flip a physical coin,
* [Furtherance](https://github.com/lakoliu/Furtherance), a time tracking app written in Rust with GTK 4/libadwaita, _Unfortunately ciirca 40 pixels too wide, sadly crashes while adding tasks on aarch64._
* [WhatsWeb](https://linuxphoneapps.org/apps/noappid.alefnode.mobile-whatsweb/), an Electron frontend for WhatsApp Web for Mobile Linux Devices, that makes web.whatsapp.com responsive,
* [Friendiqa](https://linuxphoneapps.org/apps/de.manic.friendiqa/), a Qt/QML App for Friendica, a federated, decentralized social network,
* [Swatch](https://linuxphoneapps.org/apps/org.gabmus.swatch/), a color palette manager utilizing GTK4 and libadwaita, and finally
* [Karlender](https://linuxphoneapps.org/apps/codes.loers.karlender/), a GTK4/libadwaita, mobile-friendly calendar application written in Rust supporting CalDAV. _Karlender is really nice, as it supports a day view - something GNOME Calendar (be it in its GTK4/libadwaita non-mobile friendly [upstream](https://gitlab.gnome.org/GNOME/gnome-calendar) or its [adaptive port by Purism](https://source.puri.sm/Librem5/debs/gnome-calendar)) just does not offer._

Thanks to everyone involved in developing and submitting these apps!👏

### Other noteworthy changes

The re-evaluation of archived apps in the process of launching this site was really worth it: Turns out that the only Discord app we ever listed, [gtkcord3](https://linuxphoneapps.org/apps/com.github.diamondburned.gtkcord3/), once abandoned, was revived by its developer and is definitely better than no option for all you Discord users!

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org. While we're still in an early state, the page should only become more useful from here on.

If you want to contribute an/your app to the list, please check the [FAQ](@/docs/help/faq.md#join-the-effort)!

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org)!


[^1]: The [PKGBUILD](https://gitlab.manjaro.org/manjaro-arm/packages/community/osmin/-/raw/master/PKGBUILD) should also work for other Arch-based distributions
