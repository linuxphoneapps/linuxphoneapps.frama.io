+++
title = "Making status information maintainable"
date = 2024-11-23T16:30:00+01:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]

+++

Having status information always felt important for me when I came up with LinuxPhoneApps.org.
When choosing between multiple apps, it can help to convey how mature an app is. Therefore, we had several values to communicate an apps status:

- pre-release for apps that have no tags or releases,
- early for new apps in an early project state,
- maturing,
- mature,
- inactive,
- unmaintained,
- archived.

These have been replaced by a smaller set of status:

- pre-release,
- released,
- archived and, additionally, if a project has seen zero commits in two years,
- inactive.

These can be determined via the various APIs of codehosting platforms or via cloning the repos themselves and checking for git tags which is a clear advantage.

The change reduces subjectiveness and ambiguity. What does early mean? And when is a project really mature? One can see this as a function of time, but scope has to be considered, too: For example, a fully-fledged word processor takes way, way longer to mature than a simple Pomodoro timer.

To not remove information, we've added dates: The first commit and the latest commit. Despite having a [checker](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/checkers/check_via_repo_api.py?ref_type=heads) that now programmatically gets all this information for all apps hosted on GitHub and gitlab-, gitea- and forgejo-based code forges (and a [slower solution](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/checkers/check_via_git.py?ref_type=heads#L318) for all other code hosted in git repositories), it still has to be run manually. One painpoint are API keys - we list too many apps to not run into rate-limiting. Thanks, scrapers!

So while we've made progress, there's more to figure out.
If you want to help with figuring this out so that we can run our checker, say, weekly, please get in touch [via email](mailto:linuxphoneapps@linmob.net) or on [Matrix](https://matrix.to/#/#linuxphoneapps:matrix.org).

