+++
title = "New Apps of LinuxPhoneApps.org, Q2/2023: 15 New Apps, Packaging Info and Tooling Improvements"
date = 2023-08-02T20:00:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]

+++

It's already August, so let's have a late look at the changes that happened in the second quarter of 2023!
<!-- more -->
### New apps

The following fifteen apps where added in the second quarter of 2023:

#### April 

- [Karoto Shopping List](https://linuxphoneapps.org/apps/page.codeberg.drrac27.karoto/), an advanced shopping list app.
- [Arianna](https://linuxphoneapps.org/apps/org.kde.arianna/), an e-book reader app for Plasma Desktop and mobile.
- [Loupe](https://linuxphoneapps.org/apps/org.gnome.loupe/), a simple GTK4/libadwaita image viewer.
- [Converter](https://linuxphoneapps.org/apps/io.gitlab.adhami3310.converter/), a tool to convert and manipulate images.
- [Telegraph](https://linuxphoneapps.org/apps/io.github.fkinoshita.telegraph/), an utility to write and decode morse code.
- [List](https://linuxphoneapps.org/apps/io.github.mrvladus.list/), a simple To Do application.
- [hamlocator](https://linuxphoneapps.org/apps/eu.fl9.hamlocator/), "display a maidenhead locator".
- [Collision](https://linuxphoneapps.org/apps/dev.geopjr.collision/), "check hashes for your files".

#### May

- [Bavarder](https://linuxphoneapps.org/apps/io.github.bavarder.bavarder/), an app to chit chat with GPT.

#### June

- [Junction](https://linuxphoneapps.org/apps/re.sonny.junction/) lets you choose the application to open files and links.
- [Imaginer](https://linuxphoneapps.org/apps/page.codeberg.imaginer.imaginer/), a tool to generate pictures with AI.
- [Speech Note](https://linuxphoneapps.org/apps/net.mkiol.speechnote/): Note taking and reading with Speech to Text and Text to Speech.
- [Footage](https://linuxphoneapps.org/apps/io.gitlab.adhami3310.footage/): Trim, flip, rotate and crop individual clips.
- [Teleprompter](https://linuxphoneapps.org/apps/io.github.nokse22.teleprompter/), ... well, a teleprompter! 
- [Reminders](https://linuxphoneapps.org/apps/io.github.dgsasha.remembrance/), an app to set reminders and manage tasks.

In total, LinuxPhoneApps.org now lists 430 apps, yay!

### Please contribute

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io).

As a consequence, fixing existing app or game listings is now just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

Adding new apps (or games) has hopefully not become too much more difficult - you now have to create a new markdown file to add an app. Lately, I've come up [with a tool to come up with a bare draft](https://framagit.org/1peter10/lpa-new-app-helper) of a new listing, which can then be augmented by running the existing [checkers](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/tree/main/checkers). 

We have initial documentation describing the new contribution flow:

- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

Documentation and tooling should improve over time, contributions to both are duely appreciated.

### A useful new feature

[As already announced](@/blog/better-packaging-information.md), we've added a way to make it easier to find out whether an app is packaged for your distribution (or available on Snapcraft or Flathub).

On section overview pages ([apps](https://linuxphoneapps.org/apps/), [games](https://linuxphoneapps.org/games/)) and all taxonomy term pages (all pages linked on pages like [services](https://linuxphoneapps.org/services/), [categories](https://linuxphoneapps.org/categories/), you can now easily grasp if/how you can install an app. The underlying change also makes the [Packaged in taxonomy](https://linuxphoneapps.org/packaged-in/) more useful. The data is provided by [repology.org](https://repology.org/), and will be updated about monthly - the last update was done on July 15th, 2023. For more current data, we still embed the current svg from Repology on the individual app listings.

### Still to be considered and fixed 

_The first part of this section was already part of the previous progress report:_

Talking about ratings: The way apps are rated a thing we still are thinking about. They are a hard topic, not just because changes to the rating system would then have to be applied to more than 400 apps. Among the considerations for the future are new separate sections (apps and games are sections in [Zola](https://getzola.org) terminology) for Tablet apps (how to rate those with form factors ranging from 7" to 13"?) or TUI apps (think [Sxmo](https://sxmo.org)).

If you are willing to help with the concept, please describe it in an [issue](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) - be prepared to contribute to the MR that adds it, though - also we should start out with at least 5 listings to make the section worthwhile.


For the remainder of Q3/2023, adding apps, work on UI and documentation will be the key focus - if time allows. Expect a report on what has been achieved early next quarter!

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org - it can also be useful for your friends that only use the Linux Desktop!

If you want to contribute an/your app or work on the website itself, please check the [FAQ](@/docs/help/faq.md#join-the-effort)![^1] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org) or subscribe/post to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss)!


[^1]: If you are not aware of any new apps, check out apps listed in [apps-to-be-added](https://linuxphoneapps.org/lists/apps-to-be-added/), test the app and add it!
