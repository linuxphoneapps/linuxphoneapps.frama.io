+++
title = "New apps of LinuxPhoneApps.org, Q1/2023: Easier Editing Edition"
date = 2023-04-16T10:00:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]

+++

It's mid April, and here, are the news and changes of LinuxPhoneApps.org in early 2023, starting with the added apps.
<!-- more -->
### New apps

The following nine apps where added in the first quarter of 2023:

#### January

- [Feeel](https://linuxphoneapps.org/apps/com.enjoyingfoss.feeel), a cross-platform Flutter home workout app that respects your privacy.
- [Lemmur](https://linuxphoneapps.org/apps/com.krawieck.lemmur/), a mobile client for Lemmy - a federated reddit alternative. 
- [GNOME Text Editor](https://linuxphoneapps.org/apps/org.gnome.texteditor/), a simple text editor focused on a pleasing default experience.

#### February

- [Capsule](https://linuxphoneapps.org/apps/nl.v0yd.capsule/), a medication tracker for GNOME.
- [Monophony](https://linuxphoneapps.org/apps/com.gitlab.zehkira.monophony/), a GTK4/libadwaita app that facilitates streaming music from YouTube
- [Date of Catholic Easter](https://linuxphoneapps.org/apps/com.github.alexkdeveloper.easter/), a program for calculating the date of Catholic Easter
- [Punchclock](https://linuxphoneapps.org/apps/codes.loers.punchclock/), track time for your tasks.


#### March

- [Carburetor](https://linuxphoneapps.org/apps/org.tractor.carburetor/), a GTK and Libawaita app for connecting to Tor network. _Thanks to danialbehzadi for creating and adding this app!_ 
- [Powertrack](https://linuxphoneapps.org/apps/de.somefoo.powertrack/), a tool to track battery statistics. _Thanks to somefoo for creating the app and [telling us about it](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss/%3CCAMBPg2d%3D7oke53D1Vzxr%3DPR+akwrV5kpuJTuyakS3sX-Gdw8zQ%40mail.gmail.com%3E)._

### Simplifying the way this site is generated

Since April 14th, 2023, the way this site is generated and thus also the ways to contribute to it have been simplified. All relevant parts now live in [one repository](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io).

As a consequence, fixing existing app or game listings is now just one click away (provided you have a framagit account) - every listing has an edit link at the bottom of the page.

Adding new apps (or games) has hopefully not become too much more difficult - you now have to create a new markdown file to add an app. 

We have initial documentation describing the new contribution flow:

- [Submit apps via GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/)
- [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/)

You can also send your prepared listing via email to the [devel-list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel).

Documentation and tooling should improve over time, contributions to both are duely appreciated.

### Tiny new features

Aside from this big change, the way to list flatpaks has been improved. Whether it's Flathub, another repo or download option, or just a .yml file to use with flatpak-builder, we have a custom TOML frontmatter value for each that then gets [displayed accordingly](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/templates/apps/page.html#L168-182). This was also [implemented](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/templates/apps/page.html#L183-200) for snaps in a similar fashion.

New values that are supposed to help understanding the limitations of an app better have also been introduced.

* [`intended_for_mobile`](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/templates/apps/page.html#L122-124) is supposed to signal whether an app is intended to work with small touchscreens, going by its description and appstream metadata.
* [`all-features-touch`](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/blob/main/templates/apps/page.html#L114-121) could be called "the [Seahorse](https://linuxphoneapps.org/apps/org.gnome.seahorse.application/) feature": With some apps, functionality will remain behind a right click and not accessible when using just a touch screen.

### Future

Talking about ratings: Ratings are a thing we still are thinking about. They are a hard topic, not just because changes to the rating system would then have to be applied to more than 400 apps. Among the considerations for the future are new separate sections (apps and games are sections in [Zola](https://getzola.org) terminology) for Tablet apps (how to rate those with form factors ranging from 7" to 13"?) or TUI apps (think [Sxmo](https://sxmo.org)).

If you are willing to work on the concept, please describe it in an [issue](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) - be prepared to contribute to the MR that adds it, though - also we should start out with at least 5 listings to make the section worthwhile.

For the remainder of Q2/2023 maintenance is a focus, alongside with work on usability/UI and documentation. Expect a report on what has been achieved early next quarter!

### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org!

If you want to contribute an/your app or work on the website itself, please check the [FAQ](@/docs/help/faq.md#join-the-effort)![^1] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org) or subscribe/post to our [discuss mailing list](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss)!


[^1]: If you are not aware of any new apps, check out apps listed in [apps-to-be-added](https://linuxphoneapps.org/lists/apps-to-be-added/), test the app and add it!
