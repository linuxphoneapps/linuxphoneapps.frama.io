+++
title = "Merging sections: Games now part of apps."
date = 2025-02-10T11:30:00+01:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["1peter10"]

+++

Every reasonable site (or app store) that list apps includes games and does not seperate them out like LinuxPhoneApps.org used to do.

LinuxPhoneApps.org only did that due to its history. During the LINMOBapps days, Peter seperated games out into a seperate games.csv list, to reduce the workload of maintenance and when adding addtional information.
This seperation was kept in place when launching LinuxPhoneApps.org, as over time, in part due to maintainership, that data had diverged in structure.

Thanks to [Max](https://framagit.org/1Maxnet1), who recently took over maintenance of games listed, we could now merge lists, making the site and tooling around it quite a bit simpler.

While we tried our best to do this properly and not break incoming links, please get in touch [via email](mailto:linuxphoneapps@linmob.net) or on [Matrix](https://matrix.to/#/#linuxphoneapps:matrix.org) if you think that mishaps occured - or better yet, open [an issue](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) or fix it via Merge Request!

