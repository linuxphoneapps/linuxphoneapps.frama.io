$(document).ready(function() {
    document.title = 'LinuxPhoneApps.org Games Table - Advanced Search';
    // Create a div for SearchBuilder if it doesn't exist
    if ($('#searchBuilder').length === 0) {
        $('<div id="searchBuilder"></div>').insertBefore('#insights');
    }
    // DataTable initialisation
    var table = $('#insights').DataTable({
        "dom": `
            <"datatable-wrapper"
                <"datatable-top"Q>
                <"datatable-middle"rt>
                <"datatable-bottom"
                    <"datatable-info-length"
                        <"datatable-bottom-left"l>
                        <"datatable-bottom-right"i>
                    >
                    <"datatable-pagination"p>
                    <"datatable-buttons"B>
                >
            >
        `,
        "buttons": [
            'copy',
            'csv',
            'colvis'
        ],
        "paging": true,
        "autoWidth": true,
        "lengthMenu": [[10, 25, 50, 100, 1000, -1], [10, 25, 50, 100, 1000, "All"]],
        "pageLength": 50,
        "scrollToTop": true,
        "order": [[3, 'desc']],
        "columnDefs": [
            { targets: [7], render: DataTable.render.datetime('X', 'YYYY-MM-DD', 'en') },
            { width: "30%", targets: [0, 1] },
            { width: "10%", targets: [2, 3, 4] },
        ],
        "responsive": true,
        "searchBuilder": {
            "container": '#searchBuilder'
        },
        "language": {
            "lengthMenu": "Show _MENU_ entries",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "searchBuilder": {
                "title": "Advanced Search",
                "button": {
                    "0": "Advanced Search",
                    "_": "Advanced Search (%d)"
                }
            }
        },
        "initComplete": function(settings, json) {
            // Create a new search input
            var searchInput = $('<input type="search" class="datatable-search" placeholder="Search table...">');
            // Add the search input to #navbar-search
            $('#navbar-search').empty().append(searchInput);
            // Bind the search functionality
            searchInput.on('keyup', function() {
                table.search(this.value).draw();
            });
            console.log('Search box added to navbar');
        }
    });
    // Optional: Add some styling to the SearchBuilder container
    $('#searchBuilder').css('margin-bottom', '20px');
    console.log('DataTable initialized with custom layout and SearchBuilder language');
});
