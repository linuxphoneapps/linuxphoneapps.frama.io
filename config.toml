# The URL the site will be built for
base_url = "https://linuxphoneapps.org"
title = "LinuxPhoneApps.org"
description = "An App Directory for PinePhone, Librem 5 and other mainline Linux phones."

# The default language; used in feeds and search index
# Note: the search index doesn't support Chinese/Japanese/Korean Languages
default_language = "en"

theme = "lpadoks"
# Whether to automatically compile all Sass files in the sass directory
compile_sass = true
# Whether to generate a feed file for the site
generate_feeds = true

# When set to "true", the generated HTML files are minified.
minify_html = false
slugify.paths = "safe"
# The taxonomies to be rendered for the site and their configuration.
taxonomies = [
  {name = "services", feed = true},
  {name = "categories", feed = true},
  {name = "frameworks", feed = true},
  {name = "mobile_compatibility"},
  {name = "project_licenses"},
  {name = "metadata_licenses"},
  {name = "backends", feed = true},
  {name = "freedesktop_categories", feed = true},
  {name = "build_systems"},
  {name = "programming_languages"},
  {name = "status"},
  {name = "authors"},
  {name = "app_author", feed = true},
  {name = "packaged_in",feed = true},
  {name = "requires_internet"},
  {name = "tags",feed = true},
]
# Whether to build a search index to be used later on by a JavaScript library
build_search_index = true

[search]
# Whether to include the title of the page/section in the index
include_title = true
# Whether to include the description of the page/section in the index
include_description = true
# Whether to include the rendered content of the page/section in the index
include_content = true

[markdown]
# Whether to do syntax highlighting
# Theme can be customised by setting the `highlight_theme` variable to a theme supported by Zola
highlight_code = true

[extra]
# Put all your custom variables here
pwa = true

author = "LinuxPhoneApps Contributors"
github = "https://framagit.org/linuxphoneapps"
twitter = "https://twitter.com/linuxphoneapps"
email = "mail@linuxphoneapps.org"

# If running on netlify.app site, set to true
# is_netlify = true

# Set HTML file language
language_code = "en-US"

# Set theme-color meta tag for Chrome browser
theme_color = "#fff"

# More about site's title
title_separator = "|"  # set as |, -, _, etc
title_addition = "Apps for Linux Phone OSes that do not have a centralized app store"


# Set date format in blog publish metadata
timeformat = "%B %e, %Y" # e.g. June 14, 2021
timezone = "Europe/Copenhagen"

# Edit page on reposity or not
edit_page = true
edit_app = false
docs_repo = "https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io"
repo_branch = "main"

## Math settings
# options: true, false. Enable math support globally,
# default: false. You can always enable math on a per page.
math = false
library = "katex"  # options: "katex", "mathjax". default is "katex".

## Open Graph + Twitter Cards
[extra.open]
enable = true
# this image will be used as fallback if a page has no image of its own
image = "lpa.png"
twitter_site = "linuxphoneapps"
twitter_creator = "linuxphoneapps"
og_locale = "en_US"

## JSON-LD
[extra.schema]
type = "Organization"
logo = "logo-lpa.png"
twitter = "https://twitter.com/linuxphoneapps"
linked_in = ""
github = "https://framagit.org/linuxphoneapps"
section = "apps" # see config.extra.main~url
## Sitelinks Search Box
site_links_search_box = true


# Menu items
[[extra.menu.main]]
name = "Apps"
section = "apps"
url = "/apps/"
weight = 20

[[extra.menu.main]]
name = "Games"
section = "lists"
url = "/freedesktop-categories/game/"
weight = 20

[[extra.menu.main]]
name = "Docs "
section = "docs"
url = "/docs/"
weight = 20

[[extra.menu.main]]
name = "Blog "
section = "blog"
url = "/blog/"
weight = 20

[[extra.menu.social]]
name = "@LinuxPhoneApps"
pre = '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M23 3a10.9 10.9 0 0 1-3.14 1.53 4.48 4.48 0 0 0-7.86 3v1A10.66 10.66 0 0 1 3 4s-4 9 5 13a11.64 11.64 0 0 1-7 2c9 5 20 0 20-11.5a4.5 4.5 0 0 0-.08-.83A7.72 7.72 0 0 0 23 3z"></path></svg>'
url = "https://twitter.com/linuxphoneapps"
weight = 10


# Footer contents
[extra.footer]
note = 'The content of LinuxPhoneApps.org is, just like its origins, the <a href="https://framagit.org/linmobapps/linmobapps.frama.io">LINMOBapps</a> and <a href="https://mglapps.frama.io">MGLApps</a> app lists, licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International</a>.<br><br>The LinuxPhoneApps.org contributors do not take any warranty for the correctness of any information on this page. If you want to be sure, please visit the mentioned sites and check the information.'

info = 'Powered by <a href="https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io">framagit</a>, <a href="https://www.getzola.org/">Zola</a>, <a href="https://github.com/aaranxu/adidoks">AdiDoks</a>, <a href="https://github.com/feimosi/baguetteBox.js">baguetteBox</a>, <a href="https://datatables.net/">DataTables</a> and <a href="https://github.com/oxalorg/sakura/">Sakura CSS</a>.'

[[extra.footer.nav]]
name = "Privacy"
url = "/privacy-policy/"
weight = 10

[[extra.footer.nav]]
name = "Code of Conduct"
url = "/docs/contributing/code-of-conduct/"
weight = 20

[[extra.footer.nav]]
name = "FAQ"
url = "/docs/help/faq/"
weight = 25

[[extra.footer.nav]]
name = "Apps ATOM feed (includes games)"
url = "/apps/atom.xml"
weight = 30

[[extra.footer.nav]]
name = "Blog ATOM feed"
url = "/blog/atom.xml"
weight = 40

[[extra.footer.nav]]
name = "Everything ATOM feed"
url = "/atom.xml"
weight = 45

