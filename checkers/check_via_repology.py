#!/usr/bin/env python3

import datetime
import pathlib
import sys
import traceback
import time
from collections import deque
from typing import Optional, Deque

import frontmatter
import httpx
from httpx import HTTPError

import utils

# List of valid distributions
valid_distributions = [
    "alpine_3_20",
    "alpine_3_21",
    "alpine_edge",
    "archlinuxarm_aarch64",
    "archlinuxarm_armv7h",
    "arch",
    "aur",
    "debian_11",
    "debian_12",
    "debian_13",
    "debian_unstable",
    "debian_experimental",
    "devuan_4_0",
    "devuan_unstable",
    "fedora_40",
    "fedora_41",
    "fedora_42",
    "fedora_rawhide",
    "gentoo",
    "gnuguix",
    "nix_stable_24_05",
    "nix_stable_24_11",
    "nix_unstable",
    "manjaro_stable",
    "manjaro_unstable",
    "opensuse_tumbleweed",
    "openmandriva_5_0",
    "postmarketos_master",
    "pureos_byzantium",
    "pureos_landing",
]


class RateLimiter:
    def __init__(self, requests_per_second: float):
        """Initialize rate limiter with requests per second limit"""
        self.min_interval = 1.0 / requests_per_second
        self.last_request_time = 0.0

    def wait(self):
        """Wait if necessary to comply with rate limit"""
        now = time.time()
        time_since_last = now - self.last_request_time

        if time_since_last < self.min_interval:
            wait_time = self.min_interval - time_since_last
            time.sleep(wait_time)

        self.last_request_time = time.time()


def load_repology(client: httpx.Client, package_name: str, rate_limiter: RateLimiter) -> Optional[list]:
    if not package_name:
        return None

    url = f"https://repology.org/api/v1/project/{package_name}"
    max_retries = 3
    initial_retry_delay = 2  # seconds

    for attempt in range(max_retries):
        try:
            # Wait for rate limit
            rate_limiter.wait()

            # Make request
            response = client.get(url)
            response.raise_for_status()
            return response.json()

        except httpx.HTTPStatusError as e:
            if e.response.status_code == 429:  # Too Many Requests
                # For rate limit errors, wait longer
                retry_delay = initial_retry_delay * (attempt + 1) * 2
                print(f"Rate limited, waiting {retry_delay} seconds...", file=sys.stderr)
                time.sleep(retry_delay)
            elif e.response.status_code >= 500:
                # For server errors, retry with backoff
                retry_delay = initial_retry_delay * (2**attempt)
                print(f"Server error {e.response.status_code}, attempt {attempt + 1}/{max_retries}, " f"waiting {retry_delay} seconds...", file=sys.stderr)
                time.sleep(retry_delay)
            else:
                # Client errors (4xx) are not retried
                print(f"HTTP Error {e.response.status_code} loading {url}: {e}", file=sys.stderr)
                return None
        except httpx.RequestError as e:
            # Network-related errors
            retry_delay = initial_retry_delay * (2**attempt)
            print(f"Request Error loading {url} (attempt {attempt + 1}/{max_retries}): {e}", file=sys.stderr)
            if attempt < max_retries - 1:
                time.sleep(retry_delay)
        except Exception as e:
            print(f"Unexpected error loading {url}: {e}", file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            return None

    print(f"Failed to load {url} after {max_retries} attempts", file=sys.stderr)
    return None


def get_repology_packaged_in(client, item, rate_limiter):
    repology_package_names = utils.get_recursive(item, "extra.repology")
    if not repology_package_names:
        return False

    if not repology_package_names:
        print("Repology name not specified, not querying repology", file=sys.stderr)
        return None

    packaged_in = set()
    api_failures = False
    repology_success = False

    for package_name in repology_package_names:
        repology_packages = load_repology(client, package_name, rate_limiter)
        if not repology_packages:
            print(f"No data returned for package {package_name} from Repology API", file=sys.stderr)
            api_failures = True
            continue

        repology_success = True
        for package in repology_packages:
            if (repo := package.get("repo")) in valid_distributions:
                packaged_in.add(repo)

    # Add non-Repology package sources if they exist
    if utils.get_recursive(item, "extra.flathub"):
        packaged_in.add("flathub")
    if utils.get_recursive(item, "extra.snapcraft"):
        packaged_in.add("snapcraft")

    # If any API calls failed and we either got no results or only flathub/snapcraft,
    # return False to preserve existing data
    if api_failures and not repology_success:
        return False

    return list(packaged_in)


def check(client, item, rate_limiter, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")

    properties = [
        {"apps_key": "taxonomies.packaged_in", "handler": get_repology_packaged_in},
    ]
    found = False
    for property in properties:
        try:
            found_entry = utils.sanitize(property["handler"](client, item, rate_limiter))

            # Skip updates if the handler returned False (indicating API failure)
            if found_entry is False:
                print(f"{item_name}: Skipping update due to API failure", file=sys.stderr)
                continue

            # Skip if no change is needed
            if not found_entry or found_entry == utils.get_recursive(item, property["apps_key"]):
                continue

            message = f'{item_name}: {property["apps_key"]} '
            if not utils.get_recursive(item, property["apps_key"]):
                message += "new: "
            else:
                message += f'outdated "{utils.get_recursive(item, property["apps_key"])}" -> '
            message += f'"{found_entry}"'
            print(message, file=sys.stderr)

            found = True
            if update:
                utils.set_recursive(item, property["apps_key"], found_entry)
                utils.set_recursive(item, "updated", datetime.date.today())
                utils.set_recursive(item, "extra.updated_by", "check_via_repology")

        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_key"]}:', file=sys.stderr)
            traceback.print_exception(e, file=sys.stderr)
            continue

    return found


def check_file(client: httpx.Client, filename: pathlib.Path, rate_limiter: RateLimiter, update: bool = False):
    with open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.load(f)
    found = check(client, doc.metadata, rate_limiter, update)

    if found and update:
        print(f"Writing changes to {filename}")
        with open(filename, mode="w", encoding="utf-8") as f:
            f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


def run(path: pathlib.Path, update: bool = False):
    # Increase timeout to 60 seconds
    timeout = httpx.Timeout(60.0, connect=30.0)
    headers = {
        "User-Agent": "LinuxPhoneApps-Repology-Checker/1.0 (https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues)",
    }

    # Initialize rate limiter (1 request per second)
    rate_limiter = RateLimiter(requests_per_second=1.0)

    with httpx.Client(timeout=timeout, headers=headers) as client:
        found = False
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            if check_file(client, filename, rate_limiter, update):
                found = True
        return found


def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    found = run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    main()
