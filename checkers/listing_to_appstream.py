#!/usr/bin/env python3

import asyncio
import pathlib
import sys
import argparse
import os
from typing import Dict, Any

import aiofiles
import frontmatter
import xml.etree.ElementTree as ET
from xml.dom import minidom

import utils  # Assuming utils.py is in the same directory


async def load_md_file(filename: pathlib.Path) -> Dict[str, Any]:
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    page_content = utils.parse_markdown(doc.content)
    doc.metadata |= {
        "content": page_content["description"] or "",
        "content_source": page_content["description_source"] or "",
    }
    return doc.metadata


def create_appstream_xml(metadata: Dict[str, Any], app_id: str = None) -> ET.Element:
    root = ET.Element("component", type="desktop-application")

    # Add basic information
    ET.SubElement(root, "id").text = app_id or utils.get_recursive(metadata, "extra.app_id", "")
    ET.SubElement(root, "name").text = metadata.get("title", "")
    ET.SubElement(root, "summary").text = metadata.get("description", "").strip(".")

    # Add description
    description = ET.SubElement(root, "description")
    ET.SubElement(description, "p").text = metadata.get("content", "")

    # Add metadata license and project license
    if "No Appstream Metadata (manual maintenance)" in utils.get_recursive(metadata, "taxonomies.tags", []):
        ET.SubElement(root, "metadata_license").text = "CC0-1.0"
    else:
        ET.SubElement(root, "metadata_license").text = ", ".join(utils.get_recursive(metadata, "taxonomies.metadata_licenses", []))
    ET.SubElement(root, "project_license").text = ", ".join(utils.get_recursive(metadata, "taxonomies.project_licenses", []))

    # Add URLs
    url_types = {
        "homepage": "extra.homepage",
        "bugtracker": "extra.bugtracker",
        "donation": "extra.donations",
        "translate": "extra.translations",
        "vcs-browser": "extra.repository",
    }
    for url_type, metadata_key in url_types.items():
        url = utils.get_recursive(metadata, metadata_key, "")
        if url:
            ET.SubElement(root, "url", type=url_type).text = url

    # Add launchable
    launchable = ET.SubElement(root, "launchable", {"type": "desktop-id"})
    launchable.text = app_id + ".desktop"

    # Add categories
    categories = ET.SubElement(root, "categories")
    for category in utils.get_recursive(metadata, "taxonomies.freedesktop_categories", []):
        ET.SubElement(categories, "category").text = category

    # Add screenshots
    extracted_screenshots = utils.get_recursive(metadata, "extra.screenshots", [])
    if len(extracted_screenshots) > 0:
        screenshots = ET.SubElement(root, "screenshots")
        for screenshot_url in extracted_screenshots:
            if screenshot_url == extracted_screenshots[0]:
                screenshot = ET.SubElement(screenshots, "screenshot", {"type": "default"})
            else:
                screenshot = ET.SubElement(screenshots, "screenshot")
            ET.SubElement(screenshot, "image").text = screenshot_url

    # Add developer information
    developer = ET.SubElement(root, "developer")
    ET.SubElement(developer, "name").text = ", ".join(utils.get_recursive(metadata, "taxonomies.app_author", []))

    # Add mobile compatibility information
    mobile_compatibility = utils.get_recursive(metadata, "taxonomies.mobile_compatibility", [])
    if "5" in mobile_compatibility:
        recommends = ET.SubElement(root, "recommends")
        ET.SubElement(recommends, "display_length", compare="ge").text = "360"

        supports = ET.SubElement(root, "supports")
        for control in ["pointing", "keyboard", "touch"]:
            ET.SubElement(supports, "control").text = control

    return root


def prettify_xml(elem: ET.Element) -> str:
    rough_string = ET.tostring(elem, "utf-8")
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")


async def generate_appstream_xml(input_file: pathlib.Path, output_file: pathlib.Path, app_id: str = None):
    metadata = await load_md_file(input_file)
    root = create_appstream_xml(metadata, app_id)
    xml_content = prettify_xml(root)

    async with aiofiles.open(output_file, mode="w", encoding="utf-8") as f:
        await f.write(xml_content)


# You can modify this variable to set a default output folder
DEFAULT_OUTPUT_FOLDER = pathlib.Path(os.getcwd())


async def main():
    parser = argparse.ArgumentParser(description="Generate AppStream XML metadata from .md files")
    parser.add_argument("input_file", type=pathlib.Path, help="Input .md file")
    parser.add_argument("-o", "--output", type=pathlib.Path, help="Output XML file (default: based on app_id in current directory)")
    parser.add_argument("--appid", help="Specify app_id if not present in the .md file")
    parser.add_argument("--output-folder", type=pathlib.Path, help=f"Specify output folder (default: {DEFAULT_OUTPUT_FOLDER})")
    args = parser.parse_args()

    metadata = await load_md_file(args.input_file)
    app_id = args.appid or utils.get_recursive(metadata, "extra.app_id")

    if not app_id:
        print("Error: No app_id found in the .md file and none provided via --appid", file=sys.stderr)
        sys.exit(1)

    output_folder = args.output_folder or DEFAULT_OUTPUT_FOLDER

    if not args.output:
        output_file = output_folder / f"{app_id}.metainfo.xml"
    else:
        output_file = args.output

    # Ensure the output folder exists
    output_file.parent.mkdir(parents=True, exist_ok=True)

    await generate_appstream_xml(args.input_file, output_file, app_id)
    print(f"AppStream XML metadata generated: {output_file}")


if __name__ == "__main__":
    asyncio.run(main())
