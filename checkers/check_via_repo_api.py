#!/usr/bin/env python3

import datetime
import pathlib
import sys
import traceback
from urllib.parse import urlparse
import argparse

import frontmatter
import httpx
import gitlab
from gitlab import exceptions as gitlab_exceptions

import utils

# Dictionary to store API keys for different Git instances
API_KEYS = {
    # GitHub instance
    "github.com": {"type": "github", "token": None},
    # GitLab instances
    "gitlab.com": {"type": "gitlab", "method": "httpx", "token": None},
    "gitlab.gnome.org": {"type": "gitlab", "method": "httpx", "token": None},
    "invent.kde.org": {"type": "gitlab", "method": "httpx", "token": None},
    "framagit.org": {"type": "gitlab", "method": "httpx", "token": None},
    "git.jami.net": {"type": "gitlab", "method": "httpx", "token": None},
    "git.eyecreate.org": {"type": "gitlab", "method": "httpx", "token": None},
    "dev.gajim.org": {"type": "gitlab", "method": "httpx", "token": None},
    "gitlab.postmarketos.org": {"type": "gitlab", "method": "httpx", "token": None},
    "code.briarproject.org": {"type": "gitlab", "method": "httpx", "token": None},
    "source.puri.sm": {"type": "gitlab", "method": "httpx", "token": None},
    "gitlab.freedesktop.org": {"type": "gitlab", "method": "httpx", "token": None},
    "gitlab.shinice.net": {"type": "gitlab", "method": "httpx", "token": None},
    # Forgejo/Gitea instances
    "git.itmodulo.eu": {"type": "forgejo", "token": None},
    "codeberg.org": {"type": "forgejo", "token": None},
    "git.friendi.ca": {"type": "forgejo", "token": None},
    "code.smolnet.org": {"type": "forgejo", "token": None},
}


def is_gitlab_url(url):
    parsed = urlparse(url)
    return parsed.netloc in API_KEYS and API_KEYS[parsed.netloc]["type"] == "gitlab"


def is_github_url(url):
    parsed = urlparse(url)
    return parsed.netloc == "github.com"


def is_forgejo_url(url):
    parsed = urlparse(url)
    return parsed.netloc in API_KEYS and API_KEYS[parsed.netloc]["type"] == "forgejo"


def clean_repo_url_for_api(url):
    if not url:
        return url

    cleaned_url = url.rstrip("/")
    if cleaned_url.endswith(".git"):
        cleaned_url = cleaned_url[:-4]

    if "/-/tree/" in cleaned_url:
        cleaned_url = cleaned_url.split("/-/tree/")[0]

    if "/tree/" in cleaned_url:
        cleaned_url = cleaned_url.split("/tree/")[0]

    return cleaned_url


def get_gitlab_repo_info(client, repo_url):
    repo_url_api = clean_repo_url_for_api(repo_url)
    parsed_url = urlparse(repo_url_api)
    instance = parsed_url.netloc
    path_parts = parsed_url.path.strip("/").split("/")

    if len(path_parts) < 2:
        print(f"Invalid GitLab URL: {repo_url}")
        return None

    owner = path_parts[0]
    repo = "/".join(path_parts[1:])

    instance_info = API_KEYS[instance]
    api_key = instance_info["token"]

    if instance_info["method"] == "gitlab_lib":
        return get_gitlab_repo_info_with_lib(instance, owner, repo, api_key, repo_url)
    else:
        return get_gitlab_repo_info_with_httpx(instance, owner, repo, api_key, repo_url)


def get_gitlab_repo_info_with_lib(instance, owner, repo, api_key, repo_url):
    try:
        gl = gitlab.Gitlab(f"https://{instance}", private_token=api_key, timeout=30)

        try:
            gl.auth()
        except gitlab_exceptions.GitlabAuthenticationError:
            print(f"Warning: Authentication failed for {instance}. Proceeding with unauthenticated access.")

        project = gl.projects.get(f"{owner}/{repo}")

        is_archived = project.archived
        last_activity = project.last_activity_at
        created_at = project.created_at

        if last_activity:
            last_activity = datetime.datetime.strptime(last_activity, "%Y-%m-%dT%H:%M:%S.%fZ")
            is_inactive = (datetime.datetime.now() - last_activity) > datetime.timedelta(days=730)  # 2 years
        else:
            is_inactive = None

        if created_at:
            created_at = datetime.datetime.strptime(created_at, "%Y-%m-%dT%H:%M:%S.%fZ").strftime("%Y-%m-%d")

        has_releases_or_tags = False
        if not is_archived:
            try:
                # Use get_all=True to fetch all releases
                releases = project.releases.list(get_all=True)
                has_releases_or_tags = len(releases) > 0
            except gitlab_exceptions.GitlabListError:
                print(f"Warning: Unable to fetch releases for {repo_url}")

            if not has_releases_or_tags:
                try:
                    # Use get_all=True to fetch all tags
                    tags = project.tags.list(get_all=True)
                    has_releases_or_tags = len(tags) > 0
                except gitlab_exceptions.GitlabListError:
                    print(f"Warning: Unable to fetch tags for {repo_url}")

        return {
            "is_archived": is_archived,
            "has_releases_or_tags": has_releases_or_tags,
            "is_inactive": is_inactive,
            "last_activity": last_activity.strftime("%Y-%m-%d") if last_activity else None,
            "created_at": created_at,
            "url": repo_url,
        }

    except gitlab_exceptions.GitlabHttpError as e:
        print(f"GitLab API Error for {repo_url}: {str(e)}")
    except Exception as e:
        print(f"Error in get_gitlab_repo_info for {repo_url}: {str(e)}")
        print(traceback.format_exc())

    return None


def get_gitlab_repo_info_with_httpx(instance, owner, repo, api_key, repo_url):
    headers = {"PRIVATE-TOKEN": api_key} if api_key else {}
    base_url = f"https://{instance}/api/v4"

    project_path = f"{owner}/{repo}".replace("/", "%2F")
    project_url = f"{base_url}/projects/{project_path}"

    try:
        with httpx.Client(timeout=30.0, follow_redirects=True) as client:
            response = client.get(project_url, headers=headers)
            response.raise_for_status()
            project_data = response.json()
            is_archived = project_data.get("archived", None)
            last_activity = project_data.get("last_activity_at")
            created_at = project_data.get("created_at")
            if last_activity:
                last_activity = datetime.datetime.fromisoformat(last_activity.replace("Z", "+00:00"))
                is_inactive = (datetime.datetime.now(datetime.timezone.utc) - last_activity) > datetime.timedelta(days=730)  # 2 years
            else:
                is_inactive = None
            if created_at:
                created_at = datetime.datetime.fromisoformat(created_at.replace("Z", "+00:00")).strftime("%Y-%m-%d")
            has_releases_or_tags = False
            if is_archived is not None and not is_archived:
                releases_url = f"{project_url}/releases"
                releases_response = client.get(releases_url, headers=headers)
                if releases_response.status_code == 200:
                    has_releases_or_tags = len(releases_response.json()) > 0
                if not has_releases_or_tags:
                    tags_url = f"{project_url}/repository/tags"
                    tags_response = client.get(tags_url, headers=headers)
                    if tags_response.status_code == 200:
                        has_releases_or_tags = len(tags_response.json()) > 0
            return {
                "is_archived": is_archived,
                "has_releases_or_tags": has_releases_or_tags,
                "is_inactive": is_inactive,
                "last_activity": last_activity.strftime("%Y-%m-%d") if last_activity else None,
                "created_at": created_at,
                "url": repo_url,
            }
    except httpx.HTTPStatusError as e:
        print(f"HTTP error occurred while fetching repository info for {repo_url}: {e}")
    except httpx.RequestError as e:
        print(f"An error occurred while requesting {repo_url}: {e}")
    except json.JSONDecodeError:
        print(f"Error decoding JSON response from {repo_url}")
    except Exception as e:
        print(f"An unexpected error occurred while processing {repo_url}: {e}")
        print(traceback.format_exc())
    return None


def get_github_repo_info(client, repo_url):
    repo_url_api = clean_repo_url_for_api(repo_url)
    parts = repo_url_api.split("/")
    instance = "github.com"
    owner = parts[-2]
    repo = parts[-1]
    api_url = f"https://api.github.com/repos/{owner}/{repo}"
    try:
        headers = {}
        if instance in API_KEYS and API_KEYS[instance]["token"]:
            headers["Authorization"] = f"token {API_KEYS[instance]['token']}"
        response = client.get(api_url, headers=headers)
        if response.status_code != httpx.codes.OK:
            print(f"Error loading {api_url}", file=sys.stderr)
            return None
        repo_data = response.json()
        # Check for releases
        releases_response = client.get(f"{api_url}/releases", headers=headers)
        has_releases = len(releases_response.json()) > 0 if releases_response.status_code == httpx.codes.OK else False
        # Check for tags if no releases
        if not has_releases:
            tags_response = client.get(f"{api_url}/tags", headers=headers)
            has_tags = len(tags_response.json()) > 0 if tags_response.status_code == httpx.codes.OK else False
        else:
            has_tags = False
        last_push = datetime.datetime.strptime(repo_data.get("pushed_at"), "%Y-%m-%dT%H:%M:%SZ")
        is_inactive = (datetime.datetime.now() - last_push) > datetime.timedelta(days=730)  # 2 years
        return {
            "is_archived": repo_data.get("archived", False),
            "has_releases_or_tags": has_releases or has_tags,
            "is_inactive": is_inactive,
            "last_activity": last_push.strftime("%Y-%m-%d"),
            "created_at": datetime.datetime.strptime(repo_data.get("created_at"), "%Y-%m-%dT%H:%M:%SZ").strftime("%Y-%m-%d"),
            "url": repo_url,
        }
    except Exception as e:
        print(f"Error loading {api_url}:", file=sys.stderr)
        traceback.print_exception(type(e), e, e.__traceback__, file=sys.stderr)
        return None


def get_forgejo_repo_info(client, repo_url):
    repo_url_api = clean_repo_url_for_api(repo_url)
    parsed_url = urlparse(repo_url_api)
    instance = parsed_url.netloc
    path_parts = parsed_url.path.strip("/").split("/")

    if len(path_parts) < 2:
        print(f"Invalid Gitea/Forgejo URL: {repo_url}")
        return None

    owner = path_parts[0]
    repo = "/".join(path_parts[1:])

    api_url = f"{parsed_url.scheme}://{instance}/api/v1/repos/{owner}/{repo}"

    try:
        headers = {}
        if API_KEYS[instance]:
            headers["Authorization"] = f"token {API_KEYS[instance]}"

        response = client.get(api_url, headers=headers)
        if response.status_code != httpx.codes.OK:
            print(f"Error fetching repository info for {repo_url}: {response.status_code}", file=sys.stderr)
            return None

        repo_data = response.json()
        is_archived = repo_data.get("archived", False)
        last_push = datetime.datetime.fromisoformat(repo_data.get("updated_at").replace("Z", "+00:00"))
        created_at = datetime.datetime.fromisoformat(repo_data.get("created_at").replace("Z", "+00:00"))
        is_inactive = (datetime.datetime.now(datetime.timezone.utc) - last_push) > datetime.timedelta(days=730)  # 2 years

        releases_url = f"{api_url}/releases"
        releases_response = client.get(releases_url, headers=headers)
        has_releases = len(releases_response.json()) > 0 if releases_response.status_code == httpx.codes.OK else False

        if not has_releases:
            tags_url = f"{api_url}/tags"
            tags_response = client.get(tags_url, headers=headers)
            has_tags = len(tags_response.json()) > 0 if tags_response.status_code == httpx.codes.OK else False
        else:
            has_tags = False

        return {
            "is_archived": is_archived,
            "has_releases_or_tags": has_releases or has_tags,
            "is_inactive": is_inactive,
            "last_activity": last_push.strftime("%Y-%m-%d"),
            "created_at": created_at.strftime("%Y-%m-%d"),
            "url": repo_url,
        }
    except Exception as e:
        print(f"Error fetching repository info for {repo_url}:", file=sys.stderr)
        traceback.print_exception(e, file=sys.stderr)
        return None


def get_repo_status(client, item):
    repo_url = utils.get_recursive(item, "extra.repository")
    if not repo_url:
        return None

    if is_gitlab_url(repo_url):
        repo_info = get_gitlab_repo_info(client, repo_url)
    elif is_github_url(repo_url):
        repo_info = get_github_repo_info(client, repo_url)
    elif is_forgejo_url(repo_url):
        repo_info = get_forgejo_repo_info(client, repo_url)
    else:
        print(f"Unsupported Git host for URL: {repo_url}")
        return None

    if not repo_info:
        return None

    status = []
    if repo_info["is_archived"]:
        status.append("archived")
    elif repo_info["is_archived"] is None:
        status = None
    elif not repo_info["has_releases_or_tags"]:
        status.append("pre-release")
    else:
        status.append("released")

    if status is not None and repo_info["is_inactive"]:
        status.append("inactive")

    return {"taxonomies.status": status, "extra.repository": repo_info["url"], "extra.latest_repo_commit": repo_info["last_activity"], "extra.repo_created_date": repo_info["created_at"]}


def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")

    try:
        repo_status = utils.sanitize(get_repo_status(client, item))
    except Exception as e:
        print(f"{item_name}: Error handling repository status:", file=sys.stderr)
        traceback.print_exception(e, file=sys.stderr)
        return False

    if not repo_status:
        return False

    found = False
    for key, value in repo_status.items():
        current_value = utils.get_recursive(item, key)
        if value is None:
            continue
        if value != current_value:
            message = f"{item_name}: {key} "
            if not current_value:
                message += "new: "
            else:
                message += f'outdated "{current_value}" -> '
            message += f'"{value}"'
            print(message, file=sys.stderr)

            found = True
            if update:
                utils.set_recursive(item, key, value)

    if update and found:
        utils.set_recursive(item, "updated", datetime.date.today())
        utils.set_recursive(item, "extra.updated_by", "check_via_git_api")

    return found


def check_file(client: httpx.Client, filename: pathlib.Path, update: bool = False):
    with open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.load(f)
    found = check(client, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        with open(filename, mode="w", encoding="utf-8") as f:
            f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


def run(path: pathlib.Path, update: bool = False):
    with httpx.Client(timeout=30.0) as client:
        found = False
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            if check_file(client, filename, update):
                found = True
        return found


def main():
    parser = argparse.ArgumentParser(description="Check or fix Git repository status in markdown files.")
    parser.add_argument("mode", choices=["check", "fix"], help="Mode of operation: 'check' to only check status, 'fix' to update status if needed")
    parser.add_argument("path", help="Path to the folder or file containing markdown files")
    parser.add_argument("--github_token", help="GitHub Personal Access Token for authenticated requests")

    # Add arguments for GitLab and Forgejo tokens
    for instance, info in API_KEYS.items():
        parser.add_argument(f"--{instance.replace('.', '_')}_token", help=f"{'GitLab' if info['type'] == 'gitlab' else 'Forgejo/Gitea'} Personal Access Token for {instance}")

    args = parser.parse_args()

    update = args.mode == "fix"
    apps_path = pathlib.Path(args.path)

    # Set API keys from command line arguments
    if args.github_token:
        API_KEYS["github.com"] = {"type": "github", "token": args.github_token}

    for instance in API_KEYS:
        arg_name = f"{instance.replace('.', '_')}_token"
        if hasattr(args, arg_name) and getattr(args, arg_name):
            API_KEYS[instance]["token"] = getattr(args, arg_name)

    found = run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    main()
