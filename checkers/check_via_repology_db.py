#!/usr/bin/env python3

import datetime
import pathlib
import sys
import traceback
import urllib.request
import os
import tempfile
from typing import Dict, Set, List
import frontmatter
import utils
import zstandard
import re

# Keep the same list of valid distributions
valid_distributions = [
    "alpine_3_19",
    "alpine_3_20",
    "alpine_edge",
    "archlinuxarm_aarch64",
    "archlinuxarm_armv7h",
    "arch",
    "aur",
    "debian_11",
    "debian_12",
    "debian_13",
    "debian_unstable",
    "debian_experimental",
    "devuan_4_0",
    "devuan_unstable",
    "fedora_39",
    "fedora_40",
    "fedora_41",
    "fedora_rawhide",
    "gentoo",
    "gnuguix",
    "nix_stable_23_11",
    "nix_stable_24_05",
    "nix_unstable",
    "manjaro_stable",
    "manjaro_unstable",
    "opensuse_tumbleweed",
    "openmandriva_5_0",
    "postmarketos_master",
    "pureos_byzantium",
    "pureos_landing",
]


class RepologyDB:
    def __init__(self):
        self.dump_url = "https://dumps.repology.org/repology-database-dump-latest.sql.zst"
        self.cache_dir = pathlib.Path(tempfile.gettempdir()) / "repology-cache"
        self.cache_dir.mkdir(exist_ok=True)
        self.package_data: Dict[str, Set[str]] = {}

    def _get_cached_dump_path(self) -> pathlib.Path:
        return self.cache_dir / f"repology-dump-{datetime.date.today()}.sql.zst"

    def _download_if_needed(self) -> pathlib.Path:
        dump_path = self._get_cached_dump_path()
        if not dump_path.exists():
            print("Downloading fresh Repology database dump...", file=sys.stderr)
            try:
                # Clean up old dumps
                for old_dump in self.cache_dir.glob("repology-dump-*.sql.zst"):
                    old_dump.unlink()

                # Download new dump with progress indicator
                print(f"Downloading from {self.dump_url}...", file=sys.stderr)

                def report_progress(block_num, block_size, total_size):
                    downloaded = block_num * block_size
                    if total_size > 0:
                        percent = min(100, downloaded * 100 / total_size)
                        sys.stderr.write(f"\rDownload progress: {percent:.1f}%")
                    else:
                        sys.stderr.write(f"\rDownloaded: {downloaded / 1024 / 1024:.1f} MB")
                    sys.stderr.flush()

                urllib.request.urlretrieve(self.dump_url, dump_path, report_progress)
                print("\nDownload complete.", file=sys.stderr)

            except Exception as e:
                print(f"\nError downloading database dump: {e}", file=sys.stderr)
                if dump_path.exists():
                    dump_path.unlink()
                raise

        return dump_path

    def _process_dump(self, dump_path: pathlib.Path) -> None:
        print("Processing database dump (this may take a few minutes)...", file=sys.stderr)

        # Regex to match INSERT statements for packages
        insert_pattern = re.compile(r"INSERT INTO public\.packages \(.*?\) VALUES \((.*?)\);")
        # Regex to extract project name and repo from values
        value_pattern = re.compile(r"'([^']*)'")  # Simple pattern to match quoted strings

        try:
            with open(dump_path, "rb") as compressed:
                dctx = zstandard.ZstdDecompressor()
                with dctx.stream_reader(compressed) as reader:
                    buffer = ""
                    while True:
                        chunk = reader.read(8192)
                        if not chunk:
                            break

                        buffer += chunk.decode("utf-8", errors="ignore")

                        # Process complete lines
                        lines = buffer.split("\n")
                        buffer = lines[-1]  # Keep the last incomplete line

                        for line in lines[:-1]:
                            if "INSERT INTO public.packages" in line:
                                matches = insert_pattern.finditer(line)
                                for match in matches:
                                    values = value_pattern.findall(match.group(1))
                                    if len(values) >= 2:  # Ensure we have enough values
                                        project_name = values[0]  # Assuming project name is first
                                        repo = values[1]  # Assuming repo is second

                                        if repo in valid_distributions:
                                            if project_name not in self.package_data:
                                                self.package_data[project_name] = set()
                                            self.package_data[project_name].add(repo)

        except Exception as e:
            print(f"Error processing dump file: {e}", file=sys.stderr)
            raise

        print(f"Found {len(self.package_data)} unique projects", file=sys.stderr)

    def load_data(self) -> None:
        """Load package data from the dump"""
        dump_path = self._download_if_needed()
        if not dump_path.exists() or dump_path.stat().st_size == 0:
            raise RuntimeError("Failed to obtain valid database dump")

        self._process_dump(dump_path)

    def get_packaged_in(self, package_names: List[str]) -> Set[str]:
        """Get the list of distributions a package is available in"""
        result = set()
        for name in package_names:
            if name in self.package_data:
                result.update(self.package_data[name])
        return result


def check_file(db: RepologyDB, filename: pathlib.Path, update: bool = False) -> bool:
    with open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.load(f)

    item = doc.metadata
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")

    # Get existing packaged_in value
    existing_packaged_in = set(utils.get_recursive(item, "taxonomies.packaged_in", []))

    # Get package names from metadata
    repology_package_names = utils.get_recursive(item, "extra.repology", [])
    if not repology_package_names:
        return False

    # Query database for package information
    new_packaged_in = db.get_packaged_in(repology_package_names)

    # Add Flathub and Snapcraft if present
    if utils.get_recursive(item, "extra.flathub"):
        new_packaged_in.add("flathub")
    if utils.get_recursive(item, "extra.snapcraft"):
        new_packaged_in.add("snapcraft")

    # Convert to sorted list
    new_packaged_in = sorted(new_packaged_in)

    # Check if there are changes
    if new_packaged_in == list(existing_packaged_in):
        return False

    # Report changes
    message = f"{item_name}: taxonomies.packaged_in "
    if not existing_packaged_in:
        message += "new: "
    else:
        message += f'outdated "{list(existing_packaged_in)}" -> '
    message += f'"{new_packaged_in}"'
    print(message, file=sys.stderr)

    if update:
        utils.set_recursive(item, "taxonomies.packaged_in", new_packaged_in)
        utils.set_recursive(item, "updated", datetime.date.today())
        utils.set_recursive(item, "extra.updated_by", "check_via_repology_db")

        print(f"Writing changes to {filename}")
        with open(filename, mode="w", encoding="utf-8") as f:
            f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return True


def run(path: pathlib.Path, update: bool = False) -> bool:
    db = RepologyDB()
    try:
        db.load_data()

        found = False
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            try:
                if check_file(db, filename, update):
                    found = True
            except Exception as e:
                print(f"Error processing {filename}:", file=sys.stderr)
                traceback.print_exception(e, file=sys.stderr)

        return found

    except Exception as e:
        print("Error loading Repology database:", file=sys.stderr)
        traceback.print_exception(e, file=sys.stderr)
        return False


def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    found = run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    main()
