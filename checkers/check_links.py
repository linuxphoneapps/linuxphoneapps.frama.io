#!/usr/bin/env python3
import asyncio
import datetime
import io
import pathlib
import sys
import urllib.parse
from itertools import chain

import aiofiles
import frontmatter
import httpx
from PIL import Image

import utils


def is_link(url):
    return url.strip().startswith("http")


async def check_link_reachable(client, url):
    try:
        response = await client.head(url, follow_redirects=False)
        code = response.status_code
        reason = response.reason_phrase.capitalize()
        if code == 200:
            return None
        elif code == 301 or code == 308:
            location = this_location = urllib.parse.urljoin(url, response.headers["Location"])
            message = f"Permanent redirect to {location}"
            # recursive resolution
            if (result := await check_link_reachable(client, location)) is not None:  # reachable
                if result[0] is not None:  # redirect url
                    location, message = result
                    message += f" via {this_location}"
            return (location, message)
        elif code == 302 or code == 307:
            location = urllib.parse.urljoin(url, response.headers["Location"])
            return (None, f"Temporary redirect ({code}) to {location}")
        return (None, f"{reason} ({code})")
    except Exception as e:
        return (None, f"Error occurred checking {url}: {e}")


async def check_link_https(client, url):
    if url.startswith("https"):
        return None

    suggestion = url.replace("http", "https")
    if await check_link_reachable(client, suggestion) is not None:  # not reachable
        suggestion = None

    return (suggestion, "Not HTTPS!")


async def check_field(client, field):
    if not isinstance(field, str):
        return field
    if not is_link(field.strip()):
        return field

    checkers = [check_link_reachable, check_link_https]
    for checker in checkers:
        result = await checker(client, field)
        if not result:  # reachable
            continue
        found_url, result_message = result
        print(f"{field}: {result_message}", file=sys.stderr)
        if found_url:
            field = found_url
    return field


async def check_screenshot(client, image_url):
    try:
        async with client.stream("GET", image_url) as response:
            content_type = response.headers["content-type"]
            if not content_type.startswith("image/"):
                return f'{image_url}: Invalid image type "{content_type}"'

            image = Image.open(io.BytesIO(await response.aread()))
            width, height = image.size
            if width > height:
                return f"{image_url}: Landscape image with dimensions {width}x{height} detected but expecting mobile images to be portrait ones"
        return None
    except Exception as e:
        return f"{image_url}: Error occurred: {e}"


# check links for error
async def check(client, item, update=False, item_root=None):
    if item_root is None:
        item_root = item

    found = False
    for key in item:
        if isinstance(item[key], dict):
            found |= await check(client, item[key], update, item_root)
            continue

        if isinstance(item[key], list):
            field = [await check_field(client, entry) for entry in item[key]]
        else:
            field = await check_field(client, item[key])
        if field != item[key]:
            found = True
            if update:
                item[key] = field
                utils.set_recursive(item_root, "updated", datetime.date.today())
                utils.set_recursive(item_root, "extra.updated_by", "script")

    screenshots = chain(filter(None, utils.get_recursive(item, "extra.screenshots", [])), filter(None, utils.get_recursive(item, "extra.screenshots_img", [])))
    for screenshot in screenshots:
        screenshot_error = await check_screenshot(client, screenshot)
        if screenshot_error is not None:
            print(screenshot_error, file=sys.stderr)

    return found


async def check_file(client: httpx.AsyncClient, filename: pathlib.Path, update: bool = False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found = await check(client, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(path: pathlib.Path, update: bool = False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    found = await run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
