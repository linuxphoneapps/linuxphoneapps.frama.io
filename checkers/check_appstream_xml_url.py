#!/usr/bin/env python3
import asyncio
import sys
import httpx
import pathlib
import re


async def check_link_reachable(client, url):
    try:
        response = await client.head(url, follow_redirects=False)
        code = response.status_code
        if code == 200:
            return None  # Successful, no need to report
        elif code in (301, 302, 307, 308):
            location = response.headers.get("Location", "")
            return f"Redirect ({code}) to {location}"
        return f"{response.reason_phrase.capitalize()} ({code})"
    except Exception as e:
        return f"Error occurred: {e}"


async def find_and_check_url(client, file_path):
    try:
        with open(file_path, "r") as file:
            content = file.read()
            match = re.search(r'appstream_xml_url\s*=\s*"(https?://[^"]+)"', content)
            if match:
                url = match.group(1)
                result = await check_link_reachable(client, url)
                if result:  # Only return if there's an issue
                    return f"File: {file_path}\nURL: {url}\nIssue: {result}\n"
    except Exception as e:
        return f"Error processing {file_path}: {e}\n"
    return None  # No issues or no URL found


async def main():
    if len(sys.argv) < 2:
        print(f"Syntax: {sys.argv[0]} PATH")
        sys.exit(1)

    path = pathlib.Path(sys.argv[1])

    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        if path.is_dir():
            for file_path in path.glob("**/*.md"):
                tasks.append(asyncio.ensure_future(find_and_check_url(client, file_path)))
        else:
            tasks.append(asyncio.ensure_future(find_and_check_url(client, path)))

        results = await asyncio.gather(*tasks)
        issues_found = False
        for result in results:
            if result:
                print(result)
                issues_found = True

        if not issues_found:
            print("No issues found.")
        else:
            sys.exit(1)  # Exit with non-zero status if issues were found


if __name__ == "__main__":
    asyncio.run(main())
