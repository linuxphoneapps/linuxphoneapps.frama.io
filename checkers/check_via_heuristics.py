#!/usr/bin/env python3

import asyncio
import datetime
import pathlib
import re
import sys

import aiofiles
import frontmatter
import httpx

import utils


def get_app_id(item):
    if flathub := utils.get_recursive(item, "extra.flathub", ""):
        if m := re.fullmatch(r"https://flathub.org/apps/(?P<app_id>.+)", flathub, re.IGNORECASE):
            return m.group("app_id")
    if appstream := utils.get_recursive(item, "extra.appstream_xml_url", ""):
        if m := re.fullmatch(r"https?://.*?/(?P<app_id>[a-z._]+)\.(?:metainfo|appdata)\.xml(?:\.in)*", appstream, re.IGNORECASE):
            return m.group("app_id")

    return None


def get_app_author(item):
    repo_url = utils.get_recursive(item, "extra.repository", "")
    if repo_url.startswith("https://code.qt.io/"):
        return "Qt Company"
    elif repo_url.startswith("https://invent.kde.org/plasma-mobile/"):
        return "Plasma Mobile Developers"
    elif repo_url.startswith("https://invent.kde.org/"):
        return "KDE Community"
    elif repo_url.startswith("https://gitlab.com/postmarketOS/"):
        return "postmarketOS Developers"
    elif repo_url.startswith("https://gitlab.gnome.org/"):
        return "GNOME Developers"
    else:
        return repo_url.split("/")[-2].lower().replace("~", "").replace("_", "-")


def is_github_or_gitea(s):
    github_urls = [
        "https://code.smolnet.org/",
        "https://codeberg.org/",
        "https://github.com/",
    ]
    return any(s.startswith(url) for url in github_urls)


def is_gitlab(s):
    gitlab_urls = [
        "https://dev.gajim.org/",
        "https://framagit.org/",
        "https://git.eyecreate.org/",
        "https://gitlab.com/",
        "https://gitlab.freedesktop.org/",
        "https://gitlab.gnome.org/",
        "https://gitlab.shinice.net/",
        "https://invent.kde.org/",
        "https://salsa.debian.org/",
        "https://source.puri.sm/",
        "https://gitlab.postmarketos.org/",
    ]
    return any(s.startswith(url) for url in gitlab_urls)


def is_sourcehut(s):
    return re.match(r"^https?://(?:[^.]+\.)?sr.ht/", s) is not None


def is_flathub(s):
    return s.startswith("https://flathub.org")


def get_bugtracker(item):
    repo_url = utils.get_recursive(item, "extra.repository", "")
    if repo_url[-1] == "/":
        repo_url = repo_url[:-1]

    if is_github_or_gitea(repo_url):
        return repo_url + "/issues/"
    elif is_gitlab(repo_url):
        return repo_url + "/-/issues/"
    elif is_sourcehut(repo_url):
        return re.sub(r"^https?://(?:[^.]+\.)?sr\.ht/(.*)$", r"https://todo.sr.ht/\g<1>", repo_url).lower()
    raise Exception(f"Could not determine bugtracker based on repository {repo_url}")


def get_flathub(item):
    flatpak = utils.get_recursive(item, "extra.flatpak_link", "")
    return flatpak.replace(".flatpakref", "") if is_flathub(flatpak) else ""


async def check(client, item, update=False):
    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")
    properties = [
        {"apps_key": "extra.app_id", "handler": get_app_id},
        {"apps_key": "taxonomies.app_author", "handler": get_app_author},
        {"apps_key": "extra.bugtracker", "handler": get_bugtracker},
        {"apps_key": "extra.flathub", "handler": get_flathub},
    ]
    found = False
    for property in properties:
        if utils.get_recursive(item, property["apps_key"]):
            continue  # ignore non-empty fields

        try:
            found_entry = utils.sanitize(property["handler"](item))
        except Exception as e:
            print(f'{item_name}: Error handling {property["apps_key"]}: {e}', file=sys.stderr)
            # traceback.print_exception(e, file=sys.stderr)
            continue

        if not found_entry:
            continue  # ignore empty result

        found = True
        print(f'{item_name}: {property["apps_key"]}: {found_entry}', file=sys.stderr)
        if update:
            utils.set_recursive(item, property["apps_key"], found_entry)
            utils.set_recursive(item, "updated", datetime.date.today())
            utils.set_recursive(item, "extra.updated_by", "script")

    return found


async def check_file(client: httpx.AsyncClient, filename: pathlib.Path, update: bool = False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found = await check(client, doc.metadata, update)

    if found and update:
        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(path: pathlib.Path, update: bool = False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH")
        sys.exit(1)
    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    found = await run(apps_path, update)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
