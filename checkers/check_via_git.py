#!/usr/bin/env python3

import asyncio
import pathlib
import re
import sys
import tempfile
from datetime import datetime, timedelta

import aiofiles
import appstream_python
import frontmatter
import git
import httpx

import utils


def is_sourcehut(url):
    return re.match(r"^https?://(?:[^.]+\.)?sr.ht/", url) is not None


def is_qt(url):
    return re.match(r"^https?://code.qt.io/", url) is not None


def is_sigxcpu(url):
    return re.match(r"^https?://git.sigxcpu.org/", url) is not None


def get_repository_url(repo):
    if is_sourcehut(repo):
        # Preserve case sensitivity for sourcehut
        return re.sub(r"^https?://(?:[^.]+\.)?sr\.ht/(.*)$", r"https://git.sr.ht/\g<1>", repo), None

    if is_qt(repo):
        return re.sub(r"^https?://code\.qt\.io/(?:cgit/)?(.*\.git).*$", r"https://code.qt.io/\g<1>", repo), None

    if is_sigxcpu(repo):
        return re.sub(r"^https?://git\.sigxcpu\.org/(?:cgit/)?([^.]*(?:\.git)?).*$", r"https://git.sigxcpu.org/cgit/\1", repo), None

    branch = None
    if (m := re.search(r"(?:/-)?/tree/(?P<branch>.*)$", repo)) is not None:  # Github, Gitlab
        repo = repo[: m.start()]
        branch = m.groupdict().get("branch")

    if repo[-1] == "/":
        repo = repo[:-1]

    return re.match(r"^(?:[^.]*(?:\.(?!git))?)*", repo)[0] + ".git", branch


def get_repo_creation_date(repo):
    try:
        # Get all commits and find the earliest one
        commits = list(repo.iter_commits())
        earliest_commit = commits[-1]  # Last commit in the list is the earliest
        return earliest_commit.committed_datetime.strftime("%Y-%m-%d")
    except Exception as e:
        print(f"Error getting repository creation date: {e}", file=sys.stderr)
        return None


def get_latest_tag(repo):
    try:
        tags = sorted(repo.tags, key=lambda t: t.commit.committed_datetime)
        if tags:
            latest = tags[-1]
            return {"name": str(latest), "date": latest.commit.committed_datetime.strftime("%Y-%m-%d"), "commit": latest.commit.hexsha}
    except Exception as e:
        print(f"Error getting latest tag: {e}", file=sys.stderr)
    return None


def should_be_marked_inactive(latest_date_str):
    try:
        latest_date = datetime.strptime(latest_date_str, "%Y-%m-%d")
        two_years_ago = datetime.now() - timedelta(days=730)  # 2 years * 365 days
        return latest_date < two_years_ago
    except Exception:
        return False


def update_status(item, has_tags, latest_date):
    current_status = utils.get_recursive(item, "taxonomies.status")
    if not current_status:
        current_status = []
    elif isinstance(current_status, str):
        current_status = [current_status]

    new_status = current_status.copy()
    made_changes = False

    if "taxonomies" not in item:
        item["taxonomies"] = {}

    if "archived" not in new_status:
        # Handle pre-release/released status
        is_released = "released" in new_status
        is_pre_release = "pre-release" in new_status

        if is_pre_release and has_tags:
            new_status.remove("pre-release")
            new_status.append("released")
            made_changes = True
        elif not (is_released or is_pre_release) and not has_tags:
            new_status.append("pre-release")
            made_changes = True

    if latest_date:
        is_inactive = should_be_marked_inactive(latest_date)
        currently_inactive = "inactive" in new_status

        if is_inactive and not currently_inactive:
            new_status.append("inactive")
            made_changes = True
        elif not is_inactive and currently_inactive:
            new_status.remove("inactive")
            made_changes = True

    if any(status in new_status for status in ["gone", "archived"]):
        return False

    if made_changes:
        new_status = utils.sanitize(new_status)  # Sort and remove duplicates
        utils.set_recursive(item, "taxonomies.status", new_status)

    return made_changes


def construct_raw_url(repo_url, branch, file_path):
    if is_sourcehut(repo_url):
        # Convert git.sr.ht to sr.ht for raw files
        return re.sub(r"^https?://git\.sr\.ht/([^/]+/[^/]+)/?.*$", rf"https://git.sr.ht/\1/blob/master/{file_path}", repo_url)
    elif is_qt(repo_url):
        return f"https://code.qt.io/cgit/{file_path}"
    elif is_sigxcpu(repo_url):
        return f"https://git.sigxcpu.org/cgit/{file_path}/plain/{file_path}"
    else:
        # Assume GitHub/GitLab style
        base_url = repo_url.replace(".git", "").replace("git@github.com:", "https://github.com/")
        return f"{base_url}/raw/{branch or 'master'}/{file_path}"


async def find_appstream_xml(repo_url, branch=None, repo_path="repo"):
    try:
        kwargs = {}
        if branch is not None:
            kwargs["branch"] = branch
        # Remove depth limit to get all commits for creation date
        repo = git.Repo.clone_from(repo_url, repo_path, **kwargs)

        def pred(x, y):
            if x.type != "blob":
                return False

            name = pathlib.Path(x.path).name
            if "metainfo.xml" not in name and "appdata.xml" not in name:
                return False

            return True

        files = [x.path for x in repo.head.commit.tree.traverse(pred)]
        creation_date = get_repo_creation_date(repo)
        return files, creation_date
    except Exception as e:
        print(f"Error checking git repository {repo_url} for AppStream file: {e}", file=sys.stderr)
        return None, None


async def load_appstream(file):
    if not file:
        return None
    app = appstream_python.AppstreamComponent()
    try:
        async with aiofiles.open(file, mode="rb") as f:
            app.load_bytes(await f.read())
    except Exception as e:
        print(f"Error loading {file}: {e}", file=sys.stderr)
        return None
    return app


async def check(client, item, update=False, status_check_only=False):
    made_changes = False

    # Check if the app is marked as "gone"
    if utils.get_recursive(item, "taxonomies.status") == "gone":
        print(f"Skipping check for gone app: {utils.get_recursive(item, 'extra.app_id') or utils.get_recursive(item, 'title', '')}", file=sys.stderr)
        return False, made_changes

    # If status_check_only is True, check if the item has "manual status maintenance" tag
    if status_check_only:
        tags = utils.get_recursive(item, "taxonomies.tags", [])
        if "manual status maintenance" not in tags:
            print(f"Skipping check for {utils.get_recursive(item, 'extra.app_id') or utils.get_recursive(item, 'title', '')}: no manual status maintenance tag", file=sys.stderr)
            return False, made_changes

    item_name = utils.get_recursive(item, "extra.app_id") or utils.get_recursive(item, "title", "")
    has_appstream = bool(utils.get_recursive(item, "extra.appstream_xml_url", "").strip())

    repo, branch = get_repository_url(utils.get_recursive(item, "extra.repository", ""))
    if not repo:
        print(f"No repository specified for {item_name}", file=sys.stderr)
        return False, made_changes

    if not has_appstream:
        print(f"No AppStream url found for {item_name}, checking {repo}, {branch=}", file=sys.stderr)
    else:
        print(f"Checking dates for {item_name} at {repo}, {branch=}", file=sys.stderr)

    async with aiofiles.tempfile.TemporaryDirectory() as tmpdir:
        repo_path = pathlib.Path(tmpdir) / "repo"
        try:
            kwargs = {}
            if branch is not None:
                kwargs["branch"] = branch
            # Clone repository
            repo = git.Repo.clone_from(repo, repo_path, **kwargs)

            # Get dates
            commits = list(repo.iter_commits())
            creation_date = commits[-1].committed_datetime.strftime("%Y-%m-%d")  # Earliest commit
            latest_date = commits[0].committed_datetime.strftime("%Y-%m-%d")  # Latest commit

            # Check tags
            tag_info = get_latest_tag(repo)
            has_tags = bool(tag_info)

            # Update status based on tags and latest commit date
            if update and latest_date:
                status_changes = update_status(item, has_tags, latest_date)
                made_changes = made_changes or status_changes

            # Only process AppStream files if we don't already have a URL and not in status_check_only mode
            appstream_xml_files = []
            if not has_appstream and not status_check_only:

                def pred(x, y):
                    if x.type != "blob":
                        return False
                    name = pathlib.Path(x.path).name
                    return "metainfo.xml" in name or "appdata.xml" in name

                appstream_xml_files = [x.path for x in repo.head.commit.tree.traverse(pred)]

        except Exception as e:
            print(f"Error checking git repository {repo}: {e}", file=sys.stderr)
            return False, made_changes

        if creation_date and update and not status_check_only and not utils.get_recursive(item, "extra.repo_created_date"):
            utils.set_recursive(item, "extra.repo_created_date", creation_date)
            print(f"Added repository creation date for {item_name}: {creation_date}")
            made_changes = True

        if latest_date and update:
            current_latest = utils.get_recursive(item, "extra.latest_repo_commit")
            if current_latest != latest_date:
                utils.set_recursive(item, "extra.latest_repo_commit", latest_date)
                print(f"Updated latest commit date for {item_name}: {latest_date}")
                made_changes = True

        found_appstream = False
        if not has_appstream and not status_check_only:
            valid_files = []
            for file_path in appstream_xml_files:
                if await load_appstream(repo_path / file_path):
                    valid_files.append(file_path)

            if valid_files:
                print(f"Found AppStream metainfo files for {item_name} with {repo} at paths {', '.join(valid_files)}")
                if update:
                    raw_url = construct_raw_url(repo, branch, valid_files[0])
                    utils.set_recursive(item, "extra.appstream_xml_url", raw_url)
                    print(f"Added AppStream XML URL for {item_name}: {raw_url}")
                    made_changes = True
                found_appstream = True

    return found_appstream, made_changes


async def check_file(client: httpx.AsyncClient, filename: pathlib.Path, update: bool = False, status_check_only: bool = False):
    async with aiofiles.open(filename, mode="r", encoding="utf-8") as f:
        doc = frontmatter.loads(await f.read())

    found, made_changes = await check(client, doc.metadata, update, status_check_only)

    if found or made_changes:
        utils.set_recursive(doc.metadata, "updated", datetime.now().strftime("%Y-%m-%d"))
        utils.set_recursive(doc.metadata, "extra.updated_by", "check_via_git")

        print(f"Writing changes to {filename}")
        async with aiofiles.open(filename, mode="w", encoding="utf-8") as f:
            await f.write(frontmatter.dumps(doc, handler=frontmatter.default_handlers.TOMLHandler()))

    return found


async def run(path: pathlib.Path, update: bool = False, status_check_only: bool = False):
    async with httpx.AsyncClient(timeout=30.0) as client:
        tasks = []
        files = path.glob("**/*.md") if path.is_dir() else [path]
        for filename in files:
            if filename.name == "_index.md":
                continue
            tasks.append(asyncio.ensure_future(check_file(client, filename, update, status_check_only)))
        found = any(await asyncio.gather(*tasks))
        return found


async def main():
    if len(sys.argv) < 3:
        print(f"Syntax: {sys.argv[0]} check|fix PATH [--no-api-status-check-only]")
        sys.exit(1)

    update = sys.argv[1] == "fix"
    apps_path = pathlib.Path(sys.argv[2])
    status_check_only = "--no-api-status-check-only" in sys.argv

    found = await run(apps_path, update, status_check_only)
    if found and not update:
        print(f'Errors found! Run "{sys.argv[0]} fix {apps_path}" to apply suggested changes.', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    asyncio.run(main())
