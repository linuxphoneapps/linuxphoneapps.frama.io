# linuxphoneapps.org

## What is this?

LinuxPhoneApps.org is an app directory for Linux Phones like the Librem 5, PinePhone (Pro) or any of the beautiful devices running [postmarketOS](https://wiki.postmarketos.org/wiki/Devices), [Mobian](https://wiki.debian.org/Mobian/Devices), [Kupfer](https://kupfer.gitlab.io/devices/index.html), [Droidian](https://devices.droidian.org/) or [similar](https://linmob.net/resources/#software) that run distributions that lack a proper app store showing only mobile-friendly apps.


It's build upon [Zola](https://getzola.org) with a heavily modified [adidoks](https://www.getzola.org/themes/adidoks/) theme replacement. It was conceived as a replacement for [LINMOBapps.frama.io](https://linmobapps.frama.io), with the goals of
* performing well on slower phones (LINMOBapps did not) ☑️,
* allows for easier contributions for developers and users ☑️,
* provides more information and requires less maintenance ☑️.

It's up and running at [linuxphoneapps.org](https://linuxphoneapps.org)!

Please join this project and contribute!

## FAQ

### How to get this running locally to hack on it?

* [Install the latest release of Zola](https://www.getzola.org/documentation/getting-started/installation/) ([Linux Distro packaging status](https://repology.org/project/zola/versions))
* Clone this repo `git clone https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io.git` (or fork it first and ssh clone your fork)
* `cd linuxphoneapps.frama.io`
* After that, running `zola serve` should get the site up and running locally, so that you can see what your changes work!

To get the pythonic dependencies for the checkers  (in checkers) use:

```sh
  pip install -r checkers/requirements.txt
```


### How does this roughly work?

* `content` contains sub-folders for the content, e.g. apps, docs or further info on taxonomies - e.g., to add a new app listing, you need to put the file in `content/apps/`.
* `themes` contains the lpadoks theme and with it the templates that render that content. For building templates, check out the documentation for [Zola](https://www.getzola.org/documentation/getting-started/overview/) and [Tera](https://tera.netlify.app/docs/), Zola's templating language.
* `config.toml` defines a bunch of base variables for the site,
- `static` contains static files, e.g., favicons, the bad css that makes lpadoks look "okay-ish", and lpa_helper.html,
* `templates` is not a working directory anymore, edit them in `themes/lpadoks/templates/` directly.

### How to add apps or games?

See [Contributing](https://linuxphoneapps.org/docs/contributing/), mainly [Submit Apps on GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/) and [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/). [lpa_helper.html](https://linuxphoneapps.org/lpa_helper.html) is likely the easiest way to construct a correctly formatted new app listing.

App listings (including Games) live in `content/apps/`.

### What do all these frontmatter values mean?

If confused while editing, consult [Listings explained](https://linuxphoneapps.org/docs/contributing/listings-explained/). If it does not help you find an answer to your question, [open an issue](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/new) or ask in the [Matrix chat](https://matrix.to/#/#linuxphoneapps:matrix.org).


### What can I help with?


See the open [issues](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues).

### Remaining roadmap (see previous revisions of this document for more)

To be re-done.

See the open [issues](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues) for now.


Aside from usability and design improvements to the site, the following would be great:

*  Web app for Linux Phones (preferable with a UX as [Oliver Smith describes in "Linux Mobile vs. The Social Dilemma" (minute ~ 18:00)](https://fosdem.org/2022/schedule/event/mobile_social_dilemma/))
